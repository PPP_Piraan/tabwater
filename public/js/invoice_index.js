var a = 0;
$(document).ready(function () {
    $('.example').DataTable({
        initComplete: function () {
            this.api().columns([ 2]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    });

    $('.example tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        $(this).toggleClass('bg-d1f2eb')
    });

    $('#button').click(function () {
        if (a == 0) {
           // table.destroy()
            $('.example tbody tr').addClass('selected');
            $('.example tbody tr').addClass('bg-d1f2eb')
            a = 1
        } else {
         //   table = $('.example').DataTable();
            $('.example tbody tr').removeClass('selected');
            $('.example tbody tr').removeClass('bg-d1f2eb')
            a = 0
        }
    });
});

$('#print_invoice').click(function () {
    idArr = [];
    $('.example tr.data').each(function () {
        idArr.push($(this).data('id'));
    })
    console.log(idArr)
    //ทำการp ส่ง idArr ไปปริ้น 
    window.location.href= "/invoice/print_multi_invoice/" + JSON.stringify(idArr)
    // $.get('/invoice/print_multi_invoice/' + JSON.stringify(idArr)).done(function (res) {
    //     console.log(res)
    // })
})