<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NumberSequence extends Model
{
    protected $table = 'sequence_number';
    protected $fillable =[
       'id', 'user_sq_number', 'user_meter_sq_number'
    ];
}
