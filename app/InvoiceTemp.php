<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTemp extends Model
{
    protected $table = 'invoice_temp';
}
