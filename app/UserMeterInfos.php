<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeterInfos extends Model
{
    protected $table = 'user_meter_infos';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    // public function user_profile()
    // {
    //     return $this->belongsTo('App\UserProfile', 'user_id_fk', 'user_id');
    // }
    public function user_profile()
    {
        return $this->belongsTo('App\UserProfile', 'user_id', 'user_id');
    }

    public function tabwatermeter()
    {
        return $this->belongsTo('App\TabwaterMeter', 'metertype');
    }

    public function zone()
    {
        return $this->belongsTo('App\Zone', 'undertake_zone_id', 'id');
    }

    public function subzone()
    {
        return $this->belongsTo('App\Subzone', 'undertake_subzone_id', 'id');
    }

    // public function invoice()
    // {
    //     return $this->hasMany('App\Invoice', 'meter_id_fk', 'meternumber');
    // }
    public function invoice()
    {
        return $this->hasMany('App\Invoice', 'user_id', 'user_id');
    }

    public function invoice_history()
    {
        return $this->hasMany('App\Models\InvoiceHistory', 'user_id', 'user_id');
    }

    public function invoice_last_inctive_inv_period()
    {
        return $this->hasMany('App\Invoice', 'meter_id_fk', 'meternumber');
    }
    public function invoice_by_user_id()
    {
        return $this->hasMany('App\Invoice', 'meter_id_fk', 'meternumber');
    }
    public function invoice_owe_total()
    {
        return $this->hasMany('App\Invoice', 'meter_id_fk', 'meternumber');
    }

    public function cutmeterHistory()
    {
        return $this->hasMany('App\CutmeterHistory', 'user_id_fk', 'user_id');
    }
    public function meter_type()
    {
        return $this->belongsTo(TabwaterMeter::class, 'metertype', 'id');
    }
}
