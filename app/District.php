<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'district';

    public function tambon(){
        return $this->hasMany('App\Tambon','district_code');
    }

    public function province(){
        
        return $this->belongsTo('App\Province','province_code','province_code');
    }
}
