<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabwaterUserCategory extends Model
{
    protected $table = 'tabwater_use_category';
}
