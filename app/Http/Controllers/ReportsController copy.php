<?php

namespace App\Http\Controllers;

use App\BudgetYear;
use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoicePeriodController as apiInvoicePeriodCtrl;
use App\Http\Controllers\Api\ReportsController as apiReportCtrl;
use App\Invoice;
use App\InvoicePeriod;
use App\Subzone;
use App\UserMeterInfos;
use App\UserProfile;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{

    public function index()
    {
        $inv_periods = InvoicePeriod::where('status', 'active')->orderBy('id', 'desc')->get();
        $zones = Zone::all();
        $invs = [];
        return view('reports.index', compact('inv_periods', 'zones', 'invs'));
    }

    public function users()
    {
        $zones = Zone::all();

        return view('reports.users', compact('zones'));
    }

    public function usedtabwater_infos()
    {
        return view('reports.usedtabwater_infos');
    }

    public function owe(REQUEST $request)
    {
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

        $zones = Zone::all();
        $start = $request->get('invperiodstart');
        $end = $request->get('invperiodend');
        return view('reports.owe', compact('zones', 'start', 'end', 'invoice_period'));
    }

    private function microtime_float()
    {
        list($usec, $sec) = \explode(" ", \microtime());
        return ((float) $usec + (float) $sec);
    }
    public function payment(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $fnCtrl = new FunctionsController();
        $apiReportCtrl = new apiReportCtrl();
        if (collect($request)->isEmpty()) {
            $a = [
                'inv_period_id' => 'all',
                'zone_id' => 'all',
                'subzone_id' => 'all',
                'fromdate' => date('Y-m-d'),
                'todate' => date('Y-m-d'),
                'cashier_id' => 'all',
            ];
            $request->merge($a);
            $fromdateTh = $fnCtrl->engDateToThaiDateFormat(date('Y-m-d'));
            $todateTh = $fnCtrl->engDateToThaiDateFormat(date('Y-m-d'));
        } else {
            //เปลี่ยนวันไทย ไปเป็นวันอังกฤษ
            $fromdate = $fnCtrl->thaiDateToEngDateFormat($request->get('fromdate'));
            $todate = $fnCtrl->thaiDateToEngDateFormat($request->get('todate'));
            $request->merge([
                'fromdate' => $fromdate,
                'todate' => $todate,
            ]);
            $fromdateTh = $request->get('fromdate');
            $todateTh = $request->get('todate');
        }

        //หาผลรวมของผู้จ่ายแล้วแยกตามรอบบิล
        // $paidSummaryByInvPeroid = collect(json_decode($apiReportCtrl->payment_summary()->content(), true))->values();
        //หาผู้จ่ายแล้ว แบบ subzone และ รอบบิล
        $paidInfosArr = $this->testspeed($request);
          $paidInfos = collect($paidInfosArr)->groupBy('user_id')->values()->toArray();

        // $paidInfos = collect($paidInfosArr)->groupBy('meter_id')->values();

        // $currentInvPeriodName = InvoicePeriod::where('id', $currentInvPeriod->id)->get(['inv_period_name'])[0];
        // $currentInvPeriodNameThaiExplode = \explode('-', $currentInvPeriodName->inv_period_name);
        // $currentInvPeriodNameThai        = $fnCtrl->shortThaiMonth($currentInvPeriodNameThaiExplode[0])." 25".$currentInvPeriodNameThaiExplode[1];
        //หาเจ้าหน้าที่การเงินและผู้ดูแลระบบ
        if (collect($paidInfos)->isNotEmpty()) {
            $cashier_name = $request->get('cashier_id') == 'all' ? 'ทั้งหมด' : $paidInfos[0][0]['accounting']['cashier']['name'];
        } else {
            $cashier_name = '';
        }

        $subzone_id = $request->get('subzone_id');
        $zone_id = $request->get('zone_id');
        $zones = Zone::all();
        $subzones = $zone_id != 'all' && $subzone_id != 'all' ? Subzone::all() : 'all';
        $inv_periods = InvoicePeriod::orderBy('id', 'desc')->get(['id', 'inv_period_name']);
        $receiptions = DB::table('users')
            ->join('user_profile', 'user_profile.user_id', '=', 'users.id')
            ->whereIn('users.user_cat_id', [1,2])
            ->where('users.status', '=', 'active')
            ->select('user_profile.name', 'user_profile.user_id')

            ->get();
        return view('reports.payment', compact('zones', 'subzones', 'paidInfos',
            // 'inv_periods', 'currentInvPeriodNameThai',
            // 'currentInvPeriod', 'currentInvPeriodName',
            'subzone_id', 'zone_id',
            // 'paidSummaryByInvPeroid',
            'receiptions',
            'cashier_name',
            'fromdateTh', 'todateTh'
        ));
    }

    private function testspeed(Request $request)
    {
        $fnCtrl = new FunctionsController();
        //หาจาก  usermeterinfos[undertake_zone_id  undertake_subzone_id] -> invoice table
        $zone_id = $request->get('zone_id');
        $subzone_id = $request->get('subzone_id');
        $inv_period_id = $request->get('inv_period');
        $fromdate = $request->get('fromdate');
        $todate = $request->get('todate');
        $cashier_id = $request->get('cashier_id');

        $paids =  Invoice::with(
            [
                'user_profile' => function($query){
                    return $query->select('user_id', 'name', 'address');
               },
               'usermeterinfos'=> function($query){
                    return $query->select('user_id', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id');
               },
               'usermeterinfos.zone' => function($query){
                    return $query->select('id', 'zone_name');
                },
                'usermeterinfos.subzone'=> function($query){
                    return $query->select('id', 'subzone_name');
                },
                'invoice_period' => function($query){
                    return $query->select('id', 'inv_period_name');
                },
                'accounting'=> function($query){
                    return $query->select('id', 'cashier', 'total', 'updated_at');
                },
                'accounting.cashier'=> function($query){
                    return $query->select('user_id', 'name', 'updated_at');
                }
                
            ]
        )
        ->select(
            'user_id', 'currentmeter', 'lastmeter', 'recorder_id', 'updated_at',
            DB::raw('(currentmeter - lastmeter) as water_used'), 'status as iv_status',
            DB::raw('(currentmeter - lastmeter)*8 as mustpaid'), 'receipt_id', 'inv_period_id'

        );
        
        if($inv_period_id != 'all'){
            $paids = $paids->where('inv_period_id', '=', $inv_period_id);
        }
       

        $paids = $paids->where('receipt_id', '<>', 0)
            ->where('status', '=', 'paid')
            // ->where(DB::raw("date(updated_at)"), '>=', $fromdate)
            // ->where(DB::raw("date(updated_at)"), '<=', $todate)
             ->where('updated_at', '>=', $fromdate.' 00:00:00')
             ->where('updated_at', '<=', $todate.' 23:59:59')

            ->get();

        if ($cashier_id != 'all') {
            //filter เอาผู้รับเงินที่ต้องการ
            return collect($paids)->filter(function($v) use ($cashier_id){
                return $v['accounting']['cashier'] == $cashier_id;
            });
        }
      
  
        return collect($paids)->filter(function($v) use($fromdate, $todate){
            return $v->accounting->updated_at >= $fromdate." 00:00:00" && $v->accounting->updated_at <= $todate." 23:59:59" ;
        });
    }

    public function search(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $zone_id = $request->get('zone_id');
        $query = Invoice::where('created_at', '>=', '2021-03-23 00:00:00')
            ->where('created_at', '<=', '2021-03-27 23:59:00')
            ->with(['usermeterinfos' => function ($query) use ($zone_id) {
                return $zone_id != 'all' ? $query->where('undertake_subzone_id', $zone_id) : true;
            }, 'user_profile']);
        if ($request->get('invoicetype') != 'all') {
            $query = $query->where('status', $request->get('invoicetype'));
        }
        $invs = $query->get();

        $invsTemp = $invs->filter(function ($val) {
            return collect($val->usermeterinfos)->isNotEmpty();
        });
        $invs = collect($invsTemp)->groupBy('user_id');
        $inv_periods = InvoicePeriod::orderBy('id', 'desc')->get();
        $zones = Zone::all();
        return view('reports.index', compact('inv_periods', 'zones', 'invs'));

    }

    public function daily_receipt(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');

        $apiReportCtrl = new apiReportCtrl();
        if ($request->get('date') == "") {
            $d = date('d') + 1;
            $m = date('m');
            $yTh = date('Y') + 543;

        } else {
            $date = \explode("/", $request->get('date'));
            $d = $date[0];
            $m = $date[1];
            $yTh = $date[2];
        }
        $date = $d . "/" . $m . "/" . $yTh;
        $receipts = json_decode($apiReportCtrl->daily_receipt($d, $m, $yTh)->content(), true);
        return view('reports.daily_receipt', compact('receipts', 'date'));
    }

    public function water_used(Request $request)
    {
        // ค่าตั้งต้น
        $popularity = \Lava::DataTable();
        $zone_and_subzone_selected_text = '';
        $budgetyears = BudgetYear::where('status', '<>', 'deleted')->get(['id', 'budgetyear']);
        $zones = Zone::where('deleted', 0)->get(['id', 'zone_name']);
        $zone_id = $request->get('zone_id');

        // เริ่มต้นหาปีงบประมาณปัจจุบัน และ รอบบิล
        if (collect($request)->isEmpty()) {
            $selected_budgetYear = BudgetYear::where('status', 'active')
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' )->where('deleted', '=', 0);
                }])
                ->get(['id'])
                ->first();
            $a = [
                'zone_id' => 'all',
                'subzone_id' => 'all',
            ];
            $request->merge($a);
        } else {
            $selected_budgetYear = BudgetYear::where('id', $request->get('budgetyear_id'))
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' );
                }])
                ->get(['id'])->first();

        }


        //หารอบบิลของปีงบประมาณที่ถูกเลือก
        // $invP_of_selected_budgetyear = InvoicePeriod::where('budgetyear_id', $selected_budgetYear->id)
        //     ->where('deleted', '<>', 1)
        //     ->get(['id']);
         $invPeriod_selected_buggetYear_array = collect($selected_budgetYear->invoicePeriod)->pluck('id');
         
        $startTime = microtime(true);
        //1. find all users with invoice infos

    //     $new = DB::table('invoice as iv')
    //     ->whereIn('iv.inv_period_id',[15,16])//$invPeriod_selected_buggetYear_array
    //    ->join('user_meter_infos as umf', 'umf.user_id', '=', 'iv.user_id')
    //    ->select(
    //        'iv.inv_period_id', 'iv.user_id', Db::raw('iv.currentmeter-iv.lastmeter as diff'),
    //        'umf.undertake_zone_id'
           
    //     )
    //     ->get();
    //     $inv_period_filter = collect($new)->filter(function($v){
    //         return $v->diff >= 0;
    //     });
    //    $inv_period_grouped = collect($inv_period_filter)->groupBy('inv_period_id');
    //     $arr = collect([]);
    //     foreach($inv_period_grouped as $inv_period){
    //         $zone_grouped =  collect($inv_period)->groupBy('undertake_zone_id');

    //         foreach($zone_grouped as $key => $zone){
    //             $arr->push([
    //                 'inv_period' => $inv_period[0]->inv_period_id,
    //                 'zone_name' => $key,
    //                 'members' => collect($zone)->count(),
    //                 'water_used' => collect($zone)->sum('diff'),
    //                 'mustpaid' => collect($zone)->sum('diff') *8,
    //                 'reserve_paid' => collect($zone)->filter(function($v){
    //                                     return $v->diff == 0;
    //                                 })->count() * 10
    //             ]);
    //         }
           
    //     }
    // return    $arr;
    //      $new = UserMeterInfos::with([
    //         'invoice' => function($query) use ($invPeriod_selected_buggetYear_array){
    //             return $query->select('user_id','inv_period_id',
    //             // 'currentmeter', 'lastmeter',
    //             DB::raw('currentmeter - lastmeter as diff'))
    //             ->whereIn('inv_period_id', $invPeriod_selected_buggetYear_array);
    //         },

    //     ])
    //     ->whereIn('undertake_zone_id', [1,2])   
    //     ->where('deleted', 0)
    //     ->orderBy('user_id', 'asc')
    //      ->get(['user_id', 'undertake_zone_id']);
        
    //     //2. group by invoice_period_id
    //   return  $new_grouped = collect($new)->groupBy('invoice_period_id');
    //     //3. group by undertake_zone_id

    //     //4. 
    //     $user_meterInfos_invoice_is_not_empty =  collect($new)->filter(function($v){
    //          return collect($v->invoice)->isNotEmpty();
    //      });
       
    //     $new_zone_id_grouped = collect($user_meterInfos_invoice_is_not_empty)->groupBy('undertake_zone_id');
    //     foreach($new_zone_id_grouped as $key => $zone_id_infos){
    //         return collect($zone_id_infos)->pluck('invoice')->flatten()->groupBy('inv_period_id');
    //         return collect($zone_id_infos->invoice)->pluck('diff');
    //     }
        $waterUsedSql = DB::table('user_meter_infos as umf')
            ->join('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
            ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
            ->join('subzone as sz', 'sz.id', '=', 'umf.undertake_subzone_id')
            ->join('zone as z', 'z.id', '=', 'umf.undertake_zone_id')
            // ->where('umf.status', '<>', 'inactive')
            ->whereIn('iv.inv_period_id', $invPeriod_selected_buggetYear_array)
            ->select(
                DB::raw('(iv.currentmeter - iv.lastmeter) as waterUsed'),
                'iv.inv_period_id',
                'umf.undertake_subzone_id',
                'z.id as zone_id'
            );

        if ($request->get('zone_id') != 'all') {
            $zone = Zone::where('id', $request->get('zone_id'))->get('zone_name');
            $zone_and_subzone_selected_text .= ' ' . $zone[0]->zone_name;
            if ($request->get('subzone_id') != 'all') {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_subzone_id', '=', $request->get('zone_id'));
                $subzone = subZone::where('id', $request->get('subzone_id'))->get('subzone_name');
                $zone_and_subzone_selected_text .= ' เส้นทางจัดเก็บ ' . $subzone[0]->subzone_name;
            } else {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_zone_id', '=', $request->get('zone_id'));
            }
        }
        $waterUsed2 = $waterUsedSql
            ->orderBy('zone_id', 'asc')
            ->orderBy('umf.undertake_subzone_id', 'asc')
            ->get();

            $waterUsed = collect($waterUsed2)->filter(function($v){
                // dd($v->waterUsed);
                return $v->waterUsed >= 0;
            });
    //    return     "Time:  " . number_format((microtime(true) - $startTime), 4) . " Seconds\n";

        //ถ้า $waterUsed เป็น [] ให้ return ไปหน้า view
        if (collect($waterUsed)->count() == 0) {
            return view('reports.water_used', [
                'datas' => [],
                'budgetyears' => $budgetyears,
                'selected_budgetYear' => $selected_budgetYear,
                'zones' => $zones,
            ]);
        }

        $waterUsedGroupedBySubzone = collect($waterUsed)->groupBy('undertake_subzone_id')->values();
        $datas = collect([]);
        $popularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('จำนวนน้ำที่ใช้ (หน่วย)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');
        foreach ($waterUsedGroupedBySubzone as $key => $subzone) {
            //ทำการหาผลรวมการใช้น้ำของแต่ละรอบบิลของแต่ละ subzone
            //แล้วทำการเก็บไว้ใน array datas
            $invP_in_subzone_info_arr = collect([]);
            foreach ($invPeriod_selected_buggetYear_array as $invP_id) {
                //วนลูป รอบบิลของปีงบประมาณที่เลือก เพื่อทำการ filter หา ผลรวม waterUsed
                $filtered = collect($subzone)->filter(function ($item) use ($invP_id) {
                    return $item->inv_period_id == $invP_id;
                })->sum('waterUsed');
                $find_invP_name = InvoicePeriod::where('id', $invP_id)->get(['inv_period_name']);
                $invP_in_subzone_info_arr->push([
                    'inv_period_id' => $invP_id,
                    'inv_period_name' => $find_invP_name[0]->inv_period_name,
                    'water_used_sum' => $filtered,
                ]);

                if ($request->get('subzone_id') != 'all') {
                    $popularity->addRow([$find_invP_name[0]->inv_period_name, $filtered]);
                }
            }
            $find_subzone_name = Subzone::where('id', $subzone[0]->undertake_subzone_id)->get('subzone_name');
            $datas->push([
                'subzone_id' => $subzone[0]->undertake_subzone_id,
                'members' => DB::table('user_meter_infos')->where('undertake_subzone_id', $subzone[0]->undertake_subzone_id)->count(),
                'zone_id' => $subzone[0]->zone_id,
                'subzone_name' => $find_subzone_name[0]->subzone_name,
                'values' => $invP_in_subzone_info_arr,
                'total' => collect($invP_in_subzone_info_arr)->sum('water_used_sum'),
            ]);

            if ($request->get('subzone_id') == 'all') {
                $_water_used_sum = collect($invP_in_subzone_info_arr)->sum('water_used_sum');
                $popularity->addRow([$find_subzone_name[0]->subzone_name, $_water_used_sum, $_water_used_sum, 'fill-opacity: 0.6']);
            }

        }

        $Stocks = \Lava::ColumnChart('Stocks', $popularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],
            'height' => 400,
            // 'width'  => 110
        ]);

        return view('reports.water_used', compact('Stocks', 'datas', 'zones', 'budgetyears', 'zone_id', 'zone_and_subzone_selected_text', 'selected_budgetYear',));
    }

    public function water_used_backup(Request $request)
    {
        // ค่าตั้งต้น
        $popularity = \Lava::DataTable();
        $zone_and_subzone_selected_text = '';
        $budgetyears = BudgetYear::where('status', '<>', 'deleted')->get(['id', 'budgetyear']);
        $zones = Zone::where('deleted', 0)->get(['id', 'zone_name']);
        $zone_id = $request->get('zone_id');

        // เริ่มต้นหาปีงบประมาณปัจจุบัน และ รอบบิล
        if (collect($request)->isEmpty()) {
            $selected_budgetYear = BudgetYear::where('status', 'active')
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' )->where('deleted', '=', 0);
                }])
                ->get(['id'])
                ->first();
            $a = [
                'zone_id' => 'all',
                'subzone_id' => 'all',
            ];
            $request->merge($a);
        } else {
            $selected_budgetYear = BudgetYear::where('id', $request->get('budgetyear_id'))
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' );
                }])
                ->get(['id'])->first();
        }
        //หารอบบิลของปีงบประมาณที่ถูกเลือก
        // $invP_of_selected_budgetyear = InvoicePeriod::where('budgetyear_id', $selected_budgetYear->id)
        //     ->where('deleted', '<>', 1)
        //     ->get(['id']);
         $invPeriod_selected_buggetYear_array = collect($selected_budgetYear->invoicePeriod)->pluck('id');
        $startTime = microtime(true);

        $waterUsedSql = DB::table('user_meter_infos as umf')
            ->join('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
            ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
            ->join('subzone as sz', 'sz.id', '=', 'umf.undertake_subzone_id')
            ->join('zone as z', 'z.id', '=', 'umf.undertake_zone_id')
            ->where('umf.status', '<>', 'inactive')
            ->whereIn('iv.inv_period_id', $invPeriod_selected_buggetYear_array)
            ->select(
                DB::raw('(iv.currentmeter - iv.lastmeter) as waterUsed'),
                'iv.inv_period_id',
                'umf.undertake_subzone_id',
                'z.id as zone_id'
            );

        if ($request->get('zone_id') != 'all') {
            $zone = Zone::where('id', $request->get('zone_id'))->get('zone_name');
            $zone_and_subzone_selected_text .= ' ' . $zone[0]->zone_name;
            if ($request->get('subzone_id') != 'all') {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_subzone_id', '=', $request->get('zone_id'));
                $subzone = subZone::where('id', $request->get('subzone_id'))->get('subzone_name');
                $zone_and_subzone_selected_text .= ' เส้นทางจัดเก็บ ' . $subzone[0]->subzone_name;
            } else {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_zone_id', '=', $request->get('zone_id'));
            }
        }
        $waterUsed = $waterUsedSql
            ->orderBy('zone_id', 'asc')
            ->orderBy('umf.undertake_subzone_id', 'asc')
            ->get();
            "Time:  " . number_format((microtime(true) - $startTime), 4) . " Seconds\n";

        //ถ้า $waterUsed เป็น [] ให้ return ไปหน้า view
        if (collect($waterUsed)->count() == 0) {
            return view('reports.water_used', [
                'datas' => [],
                'budgetyears' => $budgetyears,
                'selected_budgetYear' => $selected_budgetYear,
                'zones' => $zones,
            ]);
        }

        $waterUsedGroupedBySubzone = collect($waterUsed)->groupBy('undertake_subzone_id')->values();
        $datas = collect([]);
        $popularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('จำนวนน้ำที่ใช้ (หน่วย)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');
        foreach ($waterUsedGroupedBySubzone as $key => $subzone) {
            //ทำการหาผลรวมการใช้น้ำของแต่ละรอบบิลของแต่ละ subzone
            //แล้วทำการเก็บไว้ใน array datas
            $invP_in_subzone_info_arr = collect([]);
            foreach ($invPeriod_selected_buggetYear_array as $invP_id) {
                //วนลูป รอบบิลของปีงบประมาณที่เลือก เพื่อทำการ filter หา ผลรวม waterUsed
                $filtered = collect($subzone)->filter(function ($item) use ($invP_id) {
                    return $item->inv_period_id == $invP_id;
                })->sum('waterUsed');
                $find_invP_name = InvoicePeriod::where('id', $invP_id)->get(['inv_period_name']);
                $invP_in_subzone_info_arr->push([
                    'inv_period_id' => $invP_id,
                    'inv_period_name' => $find_invP_name[0]->inv_period_name,
                    'water_used_sum' => $filtered,
                ]);

                if ($request->get('subzone_id') != 'all') {
                    $popularity->addRow([$find_invP_name[0]->inv_period_name, $filtered]);
                }
            }
            $find_subzone_name = Subzone::where('id', $subzone[0]->undertake_subzone_id)->get('subzone_name');
            $datas->push([
                'subzone_id' => $subzone[0]->undertake_subzone_id,
                'zone_id' => $subzone[0]->zone_id,
                'subzone_name' => $find_subzone_name[0]->subzone_name,
                'values' => $invP_in_subzone_info_arr,
                'total' => collect($invP_in_subzone_info_arr)->sum('water_used_sum'),
            ]);

            if ($request->get('subzone_id') == 'all') {
                $_water_used_sum = collect($invP_in_subzone_info_arr)->sum('water_used_sum');
                $popularity->addRow([$find_subzone_name[0]->subzone_name, $_water_used_sum, $_water_used_sum, 'fill-opacity: 0.6']);
            }

        }

        $Stocks = \Lava::ColumnChart('Stocks', $popularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],
            'height' => 400,
            // 'width'  => 110
        ]);

        return view('reports.water_used', compact('Stocks', 'datas', 'zones', 'budgetyears', 'zone_id', 'zone_and_subzone_selected_text', 'selected_budgetYear'));
    }

    public function meter_record_history(REQUEST $request)
    {
        $budgetyear = 'now';
        $zone_id = 'all';
        if (collect($request)->isNotEmpty()) {
            $budgetyear = $request->get('budgetyear');
            $zone_id = $request->get('zone_id');
        }
        date_default_timezone_set('Asia/Bangkok');
        $zones = Zone::all();
        $budgetyears = BudgetYear::all();
        $current_budget_year = collect($budgetyears)->filter(function ($v) {
            return $v->status == 'active';
        })->values()->first();

        if ($budgetyear == 'now') {
            $budgetyear = $current_budget_year->id;
        }
        // $inv_periods = InvoicePeriod::where('status', '<>', 'deleted')->orderBy('id', 'asc')->get();
        $apiReportCtrl = new apiReportCtrl();
        $infos = json_decode($apiReportCtrl->meter_record_history($budgetyear, $zone_id)->content(), true);

        $apiInvoicePeriodCtrl = new apiInvoicePeriodCtrl();
        $inv_period_lists = json_decode($apiInvoicePeriodCtrl->inv_period_lists($budgetyear)->content(), true);
        return view('reports.meter_record_history', compact('zones', 'infos', 'inv_period_lists', 'budgetyears', 'current_budget_year'));
    }

    public function summary(Request $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $apiReportCtrl = new apiReportCtrl();
        $apiFuncCtrl = new FunctionsController();
        $apiInvoicePeriodCtrl = new apiInvoicePeriodCtrl();

        //ถ้า request  = [] ให้หาปีงบประมาณปัจจุบัน
        if (collect($request)->isEmpty()) {
            $selectedBudgetYear = BudgetYear::where('status', 'active')->get();
        } else {
            $selectedBudgetYear = BudgetYear::where('id', $request->get('budgetyear'))->get();
        }

        $invPeriodSelectedBuggetYear = InvoicePeriod::where('budgetyear_id', $selectedBudgetYear[0]->id)
            ->where('deleted', 0)
            ->get('id');
        $invPeriodSelectedBuggetYearArray = collect($invPeriodSelectedBuggetYear)->pluck('id')->toArray();

        //  $sum_water_used = DB::table('invoice as iv')
        //  ->join('user_meter_infos as umf', 'umf.user_id', '=', 'iv.user_id')
        // ->select(
        //     // 'iv.id', 
        //     'iv.user_id', 'iv.inv_period_id', 'iv.status', 
        //     'iv.deleted', 'iv.receipt_id',
        //     'iv.currentmeter', 'iv.lastmeter',
        //     Db::Raw('iv.currentmeter - iv.lastmeter as diff'),
        //     'umf.undertake_subzone_id'
        // )
        // ->whereIn('iv.inv_period_id', [21])
        // ->orderBy('iv.status', 'desc')
        // ->get();

        // return collect($sum_water_used)->sum('diff')*8;
        // return collect($sum_water_used)->sortBy('status');
        
    //     $sum_water_used_filter = collect($sum_water_used)->filter(function($v){
    //         return $v->diff >= 0;
    //     });

    //    return $grouped_by_subzone = collect($sum_water_used_filter)->groupBy('undertake_subzone_id')->sort();
        
    //     $water_used_paid_status = collect($sum_water_used_filter)->filter(function($v){
    //         return $v->status == 'paid';
    //     });

    //     $water_used_owe_status = collect($sum_water_used_filter)->filter(function($v){
    //         return $v->status == 'owe' || $v->status == 'invoice';
    //     });

    //     return [
    //         'members' => collect($sum_water_used_filter)->groupBy('user_id')->count(),
    //         'water_used' => collect($sum_water_used_filter)->sum('diff'),
    //         'net' => collect($sum_water_used_filter)->sum('diff') * 8,
    //         'เก็บได้จริง' => collect($water_used_paid_status)->sum('diff') * 8,
    //         'ค้างชำระ' => collect($water_used_owe_status)->sum('diff') * 8,
    //     ];

         $datasSql = DB::table('invoice as iv')
         ->join('user_meter_infos as umf', 'umf.user_id', '=', 'iv.user_id')
        ->select(
            'iv.id', 'iv.user_id', 'iv.inv_period_id', 'iv.status', 'iv.deleted', 'iv.receipt_id',
            'iv.currentmeter', 'iv.lastmeter',
            Db::Raw('iv.currentmeter - iv.lastmeter as diff'),
            'umf.undertake_subzone_id', 'umf.undertake_zone_id'
        )
        ->whereIn('iv.inv_period_id', [15,16,17,18,19,20,21,22])//
        ->orderBy('iv.status', 'desc')
        ->get();

        $data_filtered =  collect($datasSql)->filter(function($v){
            return ($v->status != 'deleted' || $v->deleted == 0) || $v->receipt_id > 0;
        })->values()->sortBy('undertake_subzone_id');

        $paid_total = collect($data_filtered)->filter(function($v){
            return $v->diff >= 0;
        })->sum('diff');

        $reserve_total = collect($data_filtered)->filter(function($v){
            return $v->diff == 0 && $v->status != 'init';
        })->count();
        $inv_period_id_groupeds = collect($data_filtered)->groupBy('inv_period_id');
        $res_array = collect([]);
        
        $totals = collect([]);
        foreach($inv_period_id_groupeds as $key => $inv_periods){
            $inv_periods_filter = collect($inv_periods)->filter(function($v){
                return collect($v->undertake_subzone_id)->isNotEmpty();
            });
            $water_user_total = collect($inv_periods_filter)->sum('diff');
            $reserve_total_x_10 = collect($inv_periods_filter)->filter(function($v){
                return $v->diff == 0;
            })->count()*10;
            $totals->push([
                'ivp' => $key,
                'members_total' =>  collect($inv_periods_filter)->count(),
                'water_used_total' => $water_user_total,
                'water_used_total_x_8' => $water_user_total*8,
                'reserve_total_x_10' => $reserve_total_x_10,
                'total'=> ($water_user_total*8) +$reserve_total_x_10
            ]);
            

            $subzone_groupeds = collect($inv_periods_filter)->groupBy('undertake_subzone_id');
            foreach($subzone_groupeds as  $subzone){
                $subzone_water_used_eq_0 = collect($subzone)->filter(function($v){
                    return $v->diff == 0;
                });
                $subzone_water_used_morethan_0 = collect($subzone)->filter(function($v){
                    return $v->diff > 0;
                });
                $subzone_paid = collect($subzone_water_used_morethan_0)->filter(function($v){
                    return $v->status == 'paid';
                });
    
                $subzone_owe = collect($subzone_water_used_morethan_0)->filter(function($v){
                    return $v->status == 'owe' ||  $v->status == 'invoice';
                });
                $subzone_reserve_paid = collect($subzone_water_used_eq_0)->filter(function($v){
                    return $v->status == 'paid';
                });
    
                $subzone_reserve_owe = collect($subzone_water_used_eq_0)->filter(function($v){
                    return $v->status == 'owe' ||  $v->status == 'invoice';
                });
    
                $subzone_name = Subzone::where('id', $subzone[0]->undertake_subzone_id)->get(['subzone_name']);
                $inv_period_name = InvoicePeriod::where('id', $subzone[0]->inv_period_id)->get('inv_period_name');
                $invperoid_explode =  explode('-', $inv_period_name[0]->inv_period_name);
                $res_array->push([
                    'invoice_period' => $invperoid_explode[1].'-'.$invperoid_explode[0],
                    'zone_id' => $subzone[0]->undertake_zone_id,
                    'subzone_id' => $subzone[0]->undertake_subzone_id,
                    'subzone_name' => $subzone_name[0]->subzone_name,
                    'members' => collect($subzone)->groupBy('user_id')->count(),
                    'water_used' => collect($subzone_water_used_morethan_0)->sum('diff'),
                    'water_used_amount' => collect($subzone_water_used_morethan_0)->sum('diff') *8 ,
                    'water_used_paid' => collect($subzone_paid)->sum('diff') *8,
                    'water_used_owe' => collect($subzone_owe)->sum('diff') *8,
                    'reserve_amount' => collect($subzone_water_used_eq_0)->count() *10,
                    'reserve_paid' => collect($subzone_reserve_paid)->count() *10,
                    'reserve_owe' => collect($subzone_reserve_owe)->count() *10,
                ]);
            }
            
        }
        
        $res_group =  collect($res_array)->sortBy("invoice_period")->groupBy('subzone_name');
        // );//->groupBy('subzone_id')->values();
        $res = collect($res_group)->sort();
        $res_totals = collect($totals)->sortBy('ivp')->values();
        // return $res_array;
        // $datasSql = DB::table('invoice as iv')
        //     ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
        //     ->select(
        //         'iv.updated_at', 'iv.inv_period_id',
        //         'iv.status as iv_status',
        //         DB::raw('(iv.currentmeter - iv.lastmeter)*8 as mustpaid'),
        //         // 'ivp.inv_period_name',
        //     )
        //     ->where('iv.status', '<>', 'deleted')
        //     ->whereIn('iv.inv_period_id', $invPeriodSelectedBuggetYearArray);

        // $invPeriodDatas = $datasSql
        //     ->orderBy('iv.inv_period_id', 'asc')
        //     ->get();

        // $filterByMonth = collect([]);
        // $groupedByMonth = collect([]);
        // $groupByinvoicePeriod = collect([]);
        // //หาผลรวม โดย  group รอบบิล
        // $invPeriodgroupeds = collect($invPeriodDatas)->groupBy('inv_period_id');
        // foreach ($invPeriodgroupeds as $invPeriodgrouped) {
        //     //ทำการ group โดยแบ่งตามสถานะ
        //     $groupedOweAndInvoice = collect($invPeriodgrouped)->filter(function ($item) {
        //         return $item->iv_status == 'owe' || $item->iv_status == 'invoice';
        //     });
        //     $groupedPaid = collect($invPeriodgrouped)->filter(function ($item) {
        //         return $item->iv_status == 'paid';
        //     });
        //     //หาว่า oweและ invoice เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
        //     $sumWaterUsedOweAndInvoice = collect($groupedOweAndInvoice)->sum('mustpaid');
        //     $reserveMeterOweAndInvoiceCount = collect($groupedOweAndInvoice)->pipe(function ($groupedOweAndInvoice) {
        //         return collect($groupedOweAndInvoice)->sum(function ($item) {
        //             return $item->mustpaid == 0;
        //         }) * 10;
        //     });
        //     //หาว่า paid เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
        //     $sumWaterUsedPaid = collect($groupedPaid)->sum('mustpaid');
        //     $reserveMeterPaidCount = collect($groupedPaid)->pipe(function ($groupedPaid) {
        //         return collect($groupedPaid)->sum(function ($item) {
        //             return $item->mustpaid == 0;
        //         }) * 10;
        //     });

        //     // หาข้อมูลรอบบิล
        //     $invPeriod = InvoicePeriod::where('id', $invPeriodgrouped[0]->inv_period_id)->get(['inv_period_name']);
        //     $monthArr = explode("-", $invPeriod[0]->inv_period_name);
        //     $monthTh = $apiFuncCtrl->fullThaiMonth($monthArr[0]) . " 25" . $monthArr[1];

        //     $groupByinvoicePeriod->push([
        //         'inv_period_name' => $invPeriod[0]->inv_period_name,
        //         'monthName' => $monthTh,
        //         'oweAndInvoice' => [
        //             'total' => $sumWaterUsedOweAndInvoice + $reserveMeterOweAndInvoiceCount,
        //             'water_used_sum' => $sumWaterUsedOweAndInvoice,
        //             'reservemeter_sum' => $reserveMeterOweAndInvoiceCount,
        //         ],
        //         'paid' => [
        //             'total' => $sumWaterUsedPaid + $reserveMeterPaidCount,
        //             'water_used_sum' => $sumWaterUsedPaid,
        //             'reservemeter_sum' => $reserveMeterPaidCount,
        //         ],
        //     ]);

        // }
        // //หาผลรวม โดย group เดือน
        // for ($i = 2; $i <= 12; $i++) {
        //     $a = $i < 10 ? "0" . $i : $i;
        //     //filter แยกเป้นเดือน
        //     $filtered = collect($invPeriodDatas)->filter(function ($item, $key) use ($a) {
        //         $res = strpos($item->updated_at, '-' . $a . '-');
        //         return $res == 4;
        //     });

        //     $paidTotal = 0;
        //     $paid_reserve = 0;
        //     $oweTotal = 0;
        //     $owe_reserve = 0;
        //     $invoiceTotal = 0;
        //     $invoice_reserve = 0;
        //     $paid_paid = 0;
        //     //filter แยกเป็นสถานะ inv owe paid ของแต่ละเดือน
        //     if (collect($filtered)->isNotEmpty()) {
        //         $groupedByStatus = collect($filtered)->flatten()->groupBy('iv_status');

        //         if (isset($groupedByStatus['paid'])) {
        //             $paid_reserve = collect($groupedByStatus['paid'])->filter(function ($item) {
        //                 return $item->mustpaid == 0;
        //             })->count();
        //             $paidTotal = collect($groupedByStatus['paid'])->sum('mustpaid') + $paid_reserve * 10;
        //         }
        //         if (isset($groupedByStatus['owe'])) {
        //             $owe_reserve = collect($groupedByStatus['owe'])->filter(function ($item) {
        //                 return $item->mustpaid == 0;
        //             })->count();
        //             $oweTotal = collect($groupedByStatus['owe'])->sum('mustpaid') + $owe_reserve * 10;

        //         }
        //         if (isset($groupedByStatus['invoice'])) {
        //             $invoice_reserve = collect($groupedByStatus['invoice'])->filter(function ($item) {
        //                 return $item->mustpaid == 0;
        //             })->count();
        //             $invoiceTotal = collect($groupedByStatus['invoice'])->sum('mustpaid') + $invoice_reserve * 10;

        //         }

        //         //หาเดืิิอน
        //         $findMonth = collect($groupedByStatus)->first()->values();
        //         $mothAndTimeExp = explode(" ", $findMonth[0]->updated_at);
        //         $year = explode("-", $mothAndTimeExp[0])[0] + 543;
        //         $month = $apiFuncCtrl->fullThaiMonth($a) . " " . $year;
        //         $_oweTotal = $oweTotal > 0 ? $oweTotal - ($owe_reserve * 10) : 0;
        //         $_invoiceTotal = $invoiceTotal > 0 ? $invoiceTotal - ($invoice_reserve * 10) : 0;

        //         $groupedByMonth->push([
        //             'month' => $month,
        //             'data' => [
        //                 'paid' => [
        //                     'paidTotal' => $paidTotal,
        //                     'paid_reserve' => $paid_reserve * 10 + $invoice_reserve * 10,
        //                     'used_water_paid' => $paidTotal > 0 ? $paidTotal - ($paid_reserve * 10) : 0,
        //                 ],
        //                 'owe' => [
        //                     'oweTotal' => $oweTotal + $invoiceTotal,
        //                     'owe_reserve' => $owe_reserve * 10,
        //                     'used_water_paid' => $_oweTotal + $_invoiceTotal,
        //                 ],
        //             ],
        //         ]);
        //     } // if collect isNotEmpty
        //     // return($groupedByMonth);

        // } //fore

        $budgetyears = BudgetYear::all();

        return view('reports.summary', compact('res', 'res_totals', 'selectedBudgetYear',  'budgetyears'));
        // return view('reports.summary', compact('res','selectedBudgetYear', 'groupedByMonth', 'groupByinvoicePeriod', 'budgetyears'));

    }

    public function summary2(Request $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $apiFuncCtrl = new FunctionsController();
        //ถ้า request  = [] ให้หาปีงบประมาณปัจจุบัน
        if (collect($request)->isEmpty()) {
            $selectedBudgetYear = BudgetYear::where('status', 'active')->get();
        } else {
            $selectedBudgetYear = BudgetYear::where('id', $request->get('budgetyear'))->get();
        }
        //หา รอบบิลสุดท้ายของปีงบประมาณก่อนถ้ามี เพราะต้องเอาค่ามิเตอร์มาลบกับ ค่ามิเตอร์เดือนแรกของ รอบบิลปัจจุบัน
        $last_prev_inv_period_of_prev_budget_year = BudgetYear::where('status', 'inactive')
            ->with(['invoicePeriod' => function ($query) {
                $query->select('id', 'inv_period_name', 'budgetyear_id')
                    ->where('deleted', 0)
                    ->orderBy('id', 'desc')->limit(1);
            }])
            ->get(['id'])->last();

        $invPeriodSelectedBuggetYear = InvoicePeriod::where('budgetyear_id', $selectedBudgetYear[0]->id)
            ->where('deleted', 0)
            ->get('id');
        $invPeriodSelectedBuggetYearArray = collect($invPeriodSelectedBuggetYear)->pluck('id')->toArray();
        // return gettype($invPeriodSelectedBuggetYearArray);

        if (collect($last_prev_inv_period_of_prev_budget_year)->isNotEmpty()) {
            //ถ้ามีรอบบิลสุดท้ายของปีงบประมาณก่อน ให้ทำการ add เข้าไปในตำแหน่งที่ 0 ของ $invPeriodSelectedBuggetYear
            array_unshift($invPeriodSelectedBuggetYearArray, $last_prev_inv_period_of_prev_budget_year['invoicePeriod'][0]->id);
        }

        $a = Invoice::where('inv_period_id', 13)->orWhere('inv_period_id', 15)
            ->get(['user_id', 'currentmeter', 'inv_period_id'])
            ->groupBy(['user_id', 'inv_period_id']);
        $aa = collect($a)->values();
        // return collect($aa)->reduce(function ($carry, $v) {
        //     $i = 0;
        //     if (!isset($v['15'])) {
        //         dd($v);
        //     }
        //     if ($v['13'][0]->currentmeter == $v['15'][0]->currentmeter) {
        //         $i = 1;
        //     }
        //     return $carry + $i;
        // });
        $datasSql = DB::table('invoice as iv')
            ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
            ->select(
                'iv.updated_at', 'iv.inv_period_id',
                'iv.status as iv_status',
                DB::raw('(iv.currentmeter - iv.lastmeter)*8 as mustpaid'),
                // 'ivp.inv_period_name',
            )
            ->where('iv.status', '<>', 'deleted')
            ->whereIn('iv.inv_period_id', $invPeriodSelectedBuggetYearArray);

        $invPeriodDatas = $datasSql
            ->orderBy('iv.inv_period_id', 'asc')
            ->get();

        $filterByMonth = collect([]);
        $groupedByMonth = collect([]);
        $groupByinvoicePeriod = collect([]);
        //หาผลรวม โดย  group รอบบิล
        $invPeriodgroupeds = collect($invPeriodDatas)->groupBy('inv_period_id');
        foreach ($invPeriodgroupeds as $invPeriodgrouped) {
            //ทำการ group โดยแบ่งตามสถานะ
            $groupedOweAndInvoice = collect($invPeriodgrouped)->filter(function ($item) {
                return $item->iv_status == 'owe' || $item->iv_status == 'invoice';
            });
            $groupedPaid = collect($invPeriodgrouped)->filter(function ($item) {
                return $item->iv_status == 'paid';
            });
            //หาว่า oweและ invoice เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
            $sumWaterUsedOweAndInvoice = collect($groupedOweAndInvoice)->sum('mustpaid');
            $reserveMeterOweAndInvoiceCount = collect($groupedOweAndInvoice)->pipe(function ($groupedOweAndInvoice) {
                return collect($groupedOweAndInvoice)->sum(function ($item) {
                    return $item->mustpaid == 0;
                }) * 10;
            });
            //หาว่า paid เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
            $sumWaterUsedPaid = collect($groupedPaid)->sum('mustpaid');
            $reserveMeterPaidCount = collect($groupedPaid)->pipe(function ($groupedPaid) {
                return collect($groupedPaid)->sum(function ($item) {
                    return $item->mustpaid == 0;
                }) * 10;
            });

            // หาข้อมูลรอบบิล
            $invPeriod = InvoicePeriod::where('id', $invPeriodgrouped[0]->inv_period_id)->get(['inv_period_name']);
            $monthArr = explode("-", $invPeriod[0]->inv_period_name);
            $monthTh = $apiFuncCtrl->fullThaiMonth($monthArr[0]) . " 25" . $monthArr[1];

            $groupByinvoicePeriod->push([
                'inv_period_name' => $invPeriod[0]->inv_period_name,
                'monthName' => $monthTh,
                'oweAndInvoice' => [
                    'total' => $sumWaterUsedOweAndInvoice + $reserveMeterOweAndInvoiceCount,
                    'water_used_sum' => $sumWaterUsedOweAndInvoice,
                    'reservemeter_sum' => $reserveMeterOweAndInvoiceCount,
                ],
                'paid' => [
                    'total' => $sumWaterUsedPaid + $reserveMeterPaidCount,
                    'water_used_sum' => $sumWaterUsedPaid,
                    'reservemeter_sum' => $reserveMeterPaidCount,
                ],
            ]);

        }
        //หาผลรวม โดย group เดือน
        for ($i = 2; $i <= 12; $i++) {
            $a = $i < 10 ? "0" . $i : $i;
            //filter แยกเป้นเดือน
            $filtered = collect($invPeriodDatas)->filter(function ($item, $key) use ($a) {
                $res = strpos($item->updated_at, '-' . $a . '-');
                return $res == 4;
            });

            $paidTotal = 0;
            $paid_reserve = 0;
            $oweTotal = 0;
            $owe_reserve = 0;
            $invoiceTotal = 0;
            $invoice_reserve = 0;
            $paid_paid = 0;
            //filter แยกเป็นสถานะ inv owe paid ของแต่ละเดือน
            if (collect($filtered)->isNotEmpty()) {
                $groupedByStatus = collect($filtered)->flatten()->groupBy('iv_status');

                if (isset($groupedByStatus['paid'])) {
                    $paid_reserve = collect($groupedByStatus['paid'])->filter(function ($item) {
                        return $item->mustpaid == 0;
                    })->count();
                    $paidTotal = collect($groupedByStatus['paid'])->sum('mustpaid') + $paid_reserve * 10;
                }
                if (isset($groupedByStatus['owe'])) {
                    $owe_reserve = collect($groupedByStatus['owe'])->filter(function ($item) {
                        return $item->mustpaid == 0;
                    })->count();
                    $oweTotal = collect($groupedByStatus['owe'])->sum('mustpaid') + $owe_reserve * 10;

                }
                if (isset($groupedByStatus['invoice'])) {
                    $invoice_reserve = collect($groupedByStatus['invoice'])->filter(function ($item) {
                        return $item->mustpaid == 0;
                    })->count();
                    $invoiceTotal = collect($groupedByStatus['invoice'])->sum('mustpaid') + $invoice_reserve * 10;

                }

                //หาเดืิิอน
                $findMonth = collect($groupedByStatus)->first()->values();
                $mothAndTimeExp = explode(" ", $findMonth[0]->updated_at);
                $year = explode("-", $mothAndTimeExp[0])[0] + 543;
                $month = $apiFuncCtrl->fullThaiMonth($a) . " " . $year;
                $_oweTotal = $oweTotal > 0 ? $oweTotal - ($owe_reserve * 10) : 0;
                $_invoiceTotal = $invoiceTotal > 0 ? $invoiceTotal - ($invoice_reserve * 10) : 0;

                $groupedByMonth->push([
                    'month' => $month,
                    'data' => [
                        'paid' => [
                            'paidTotal' => $paidTotal,
                            'paid_reserve' => $paid_reserve * 10 + $invoice_reserve * 10,
                            'used_water_paid' => $paidTotal > 0 ? $paidTotal - ($paid_reserve * 10) : 0,
                        ],
                        'owe' => [
                            'oweTotal' => $oweTotal + $invoiceTotal,
                            'owe_reserve' => $owe_reserve * 10,
                            'used_water_paid' => $_oweTotal + $_invoiceTotal,
                        ],
                    ],
                ]);
            } // if collect isNotEmpty
            // return($groupedByMonth);

        } //fore

        $budgetyears = BudgetYear::all();

        return view('reports.summary', compact('selectedBudgetYear', 'groupedByMonth', 'groupByinvoicePeriod', 'budgetyears'));

    }

    public function cutmeter()
    {
        $cutmeters = UserMeterInfos::where('status', 'cutmeter')
            ->with('user_profile', 'zone', 'subzone')
            ->get();
        return view('reports.cutmeter', compact('cutmeters'));
    }

}
