<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\UploadBankSlip;

class UploadBankSlipController extends Controller
{
    public function index()
    {

        $bankSlips = UploadBankSlip::
            with([
            'user_profile' => function ($query) {
                $query->select('user_id', 'name', 'address', 'zone_id', 'subzone_id');
            },
            'user_profile.zone' => function ($query) {
                $query->select('id', 'zone_name');
            },
            'user_profile.subzone' => function ($query) {
                $query->select('id', 'subzone_name');
            },
            'user_profile.userMeterInfos' => function ($query) {
                $query->select('user_id', 'meternumber', 'undertake_subzone_id');
            },
            'user_profile.userMeterInfos' => function ($query) {
                $query->select('user_id', 'meternumber', 'undertake_subzone_id');
            },
            'user_profile.userMeterInfos.subzone' => function ($query) {
                $query->select('id', 'subzone_name as subzone_record_route');
            },
        ])
            ->where('deleted', 0)
            ->where('status', 1)
            ->get(['id', 'user_id', 'image', 'invoice_id_list', 'mustpaid', 'status']);
        return view('upload_bank_slip.index', compact('bankSlips'));
    }

    function print($id) {
        $bankSlips = UploadBankSlip::
            with([
            'user_profile' => function ($query) {
                $query->select('user_id', 'name', 'address', 'zone_id', 'subzone_id');
            },
            'user_profile.zone' => function ($query) {
                $query->select('id', 'zone_name');
            },
            'user_profile.subzone' => function ($query) {
                $query->select('id', 'subzone_name');
            },
            'user_profile.userMeterInfos' => function ($query) {
                $query->select('user_id', 'meternumber', 'undertake_subzone_id');
            },
            'user_profile.userMeterInfos' => function ($query) {
                $query->select('user_id', 'meternumber', 'undertake_subzone_id');
            },
            'user_profile.userMeterInfos.subzone' => function ($query) {
                $query->select('id', 'subzone_name as subzone_record_route');
            },
        ])
            ->where('deleted', 0)
            ->where('id', $id)
            ->get(['id', 'user_id', 'image', 'invoice_id_list', 'mustpaid', 'status']);
        $invoice_id_lists = \json_decode($bankSlips[0]->invoice_id_list, true);
        $invoiceArray = collect([]);
        foreach ($invoice_id_lists as $invoice) {
            $inv = Invoice::
                with([
                'invoice_period' => function ($query) {
                    return $query->select('id', 'inv_period_name');
                },
            ])
                ->where('id', $invoice['iv_id'])->get([
                'inv_period_id', 'user_id', 'lastmeter', 'currentmeter', 'status',
            ]);
            $invoiceArray->push($inv);
        }
        return view('upload_bank_slip.print', compact('bankSlips', 'invoiceArray'));
    }

}