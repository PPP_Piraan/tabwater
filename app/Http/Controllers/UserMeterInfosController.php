<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserMeterInfos;
use App\Subzone;

class UserMeterInfosController extends Controller
{
    public function find_subzone($zone_id, $subzone_id = 0){
        $resVal =0;
        if($zone_id > 0){
            $subzones = Subzone::where('zone_id', $zone_id)->get('id');
            if(collect($subzones)->isNotEmpty()){
                foreach($subzones as $subzone){
                    $undertake_subzone_id = UserMeterInfos::where('undertake_subzone_id', $subzone->id)
                                            ->get()->first();

                    if(collect($undertake_subzone_id)->isNotEmpty()){
                        $resVal = 1;
                        break;
                    }

                }
            }
        }else{
            $undertake_subzone_id = UserMeterInfos::where('undertake_subzone_id', $subzone_id)->get()->first();
            if(collect($undertake_subzone_id)->isNotEmpty()){
                $resVal = 1;
            }
        }

        return $resVal;
    }


    public function manageOweCount()
    {
        $aa = UserMeterInfos::
            with([
            'invoice' => function ($query) {
                return $query->select('inv_period_id', 'status','meter_id_fk')
                    ->whereIn('status', ['owe', 'invoice']);
            },
        ])->where('status', 'active')->get(['user_id_fk', 'owe_count', 'meternumber', 'status']);

        $arr = collect([]);
        $i = 1;
        foreach ($aa as $a) {
            //นับ invoice status เท่ากับ owe หรือ invioce แล้วทำการ update
            //owe_count ให้  user_meter_infos ใหม่
            $oweInvCount = collect($a->invoice)->count(); // + 1; // บวก row  status == invoice

            $update = UserMeterInfos::where('meternumber', $a->meternumber)->update([
                'owe_count' => $oweInvCount,
                'status' => 'active',
                'discounttype' => $oweInvCount >= 3 ? $oweInvCount : 0,
            ]);


        };
        // return $arr;
    }

}
