<?php

namespace App\Http\Controllers;

use App\Settings;
use App\Staff;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\NumberSequence;
use App\UserMeterInfos;

class SettingsController extends Controller
{
    public function index()
    {
        $organization_sql = Settings::where('name', 'organization')->get(['values'])->first();
        if (collect($organization_sql)->count() == 0) {
            return view('settings.index');
        }
        $organizations = \json_decode($organization_sql['values'], true);
        $logo = Settings::where('name', 'logo')->get(['values'])->first();
        $meternumber_code = Settings::where('name', 'meternumber_code')->get(['values'])->first();
        $payment_expired_date = Settings::where('name', 'payment_expired_date')->get(['values'])->first();
        $owe_count = Settings::where('name', 'owe_count')->get(['values'])->first();
        $vat = Settings::where('name', 'vat')->get(['values'])->first();
        $signs_sql = Settings::where('name', 'sign')->get(['values']);
        $signs = collect([]);
        foreach ($signs_sql as $sign) {
            $signs->push(json_decode($sign['values']));
        }
        // return $signs;
        return view('settings.index', \compact('logo', 'signs', 'vat', 'organizations',
            'meternumber_code', 'payment_expired_date', 'owe_count'));
    }

    public function budgetyear()
    {
        return view("settings.budgetyear");
    }

    public function getTambonInfos()
    {
        $settings = Settings::where('name', 'organization')->get();
        return json_decode($settings[0]['values'], true);
    }

    public function updatebudgetyear(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $setting = Settings::first();
        $newData = [['id' => 1, 'startyear' => $request['start'],
            'endyear' => $request['end'], 'create_date' => date('Y-m-d H:i:s'),
            'recorder' => 1]];
        if (is_null($setting)) {
            $newSetting = new Settings;
            $newSetting->budgetyear = \json_encode($newData);
            $newSetting->save();
        } else {
            $lastSetting = json_decode($setting->budgetyear, true);
            $appenddata = ['id' => 1, 'startyear' => $request['start'],
                'endyear' => $request['end'], 'create_date' => date('Y-m-d H:i:s'),
                'recorder' => 1];
            array_push($lastSetting, $appenddata);
            $setting->budgetyear = \json_encode($lastSetting);
            // $setting->save();
        }

        return \response()->json($newData);
    }

    public function create_and_update(REQUEST $request)
    {
        $this->validate($request, [
            // // 'filenames' => 'required',
            // 'filenames.*' => 'mimes:png,jpg,jpeg|max:1014',
            // 'logo.*' => 'mimes:png,jpg,jpeg|max:1014',
            // 'logo' => 'required',
            // "organization_name" => 'required',
            // "department_name" => 'required',
            // "organize_address" => 'required',
            // "organize_zone" => 'required',
            // "organize_road" => 'required',
            // "organize_tambon" => 'required',
            // "organize_district" => 'required',
            // "organize_province" => 'required',
            // "organize_zipcode" => 'required',
            // "organize_phone" => 'required',
            // "organize_email" => 'required',
            // "meternumber_code" => 'required',
            // "owe_count" => 'required',
            // "payment_expired_date" => 'required',

        ]);

        DB::table('settings')->truncate();
        Settings::create([
            'name' => 'organization',
            'values' => \json_encode([
                "organization_name" => $request->get('organization_name'),
                "organization_short_name" => $request->get('organization_short_name'),
                "organize_address" => $request->get('organize_address'),
                "organize_zone" => $request->get('organize_zone'),
                "organize_road" => $request->get('organize_road'),
                "organize_tambon" => $request->get('organize_tambon'),
                "organize_district" => $request->get('organize_district'),
                "organize_province" => $request->get('organize_province'),
                "organize_zipcode" => $request->get('organize_zipcode'),
                "organize_phone" => $request->get('organize_phone'),
                "department_name" => $request->get('department_name'),
                "department_short_name" => $request->get('department_short_name'),
                "department_phone" => $request->get('department_phone'),
                "organize_email" => $request->get('organize_email'),
            ]),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Settings::create([
            'name' => 'meternumber_code',
            'values' => $request->get('meternumber_code'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);
        Settings::create([
            'name' => 'owe_count',
            'values' => $request->get('owe_count'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);
        Settings::create([
            'name' => 'payment_expired_date',
            'values' => $request->get('payment_expired_date'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);
        Settings::create([
            'name' => 'vat',
            'values' => $request->get('vat'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);

        $array = collect([]);
        if (collect($request->get('img_name'))->count() > 0) {
            foreach ($request->get('img_name') as $key => $item) {
                if ($item['change_image'] == 0 ) {
                    // ไม่มีการเปลี่ยนรูป
                    $image_name = $item;
                } else {
                    //มีการเปลี่ยนรูป
                    if ($request->file('filenames')[$key]) {
                        // upload รูปใหม่
                        $image_name = 'sign_' . rand() . '.' . $request->file('filenames')[$key]->extension();
                        $request->file('filenames')[$key]->move(public_path() . '/sign/', $image_name);
                        //ลบรูปเก่า
                        unlink(public_path() . '/sign/'. $item['old_name']);
                    }
                }
                $array->push([
                    'name' => $request->get('sign')[$key]['name'],
                    'position' => $request->get('sign')[$key]['position'],
                    'image' => $image_name,
                ]);
            }
            Settings::create([
                'name' => 'sign',
                'values' => json_encode($array),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        }
        if ($request->hasFile('logo')) {
            return $request;
            //เพิ่มรูปใหม่
            $image_name = time() . '.' . $request->file('logo')->extension();

            Settings::create([
                'name' => 'logo',
                'values' => $image_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $request->file('logo')->move(public_path() . '/logo/', $image_name);

        }
        return redirect('settings');

    }

    public function edit()
    {
        $organization_sql = Settings::where('name', 'organization')->get(['values'])->first();
        $organizations = \json_decode($organization_sql['values'], true);
        // return ;
        $logo = Settings::where('name', 'logo')->get(['values'])->first();
        $logo_values = collect($logo)->count() == 0 ? 0 : \json_decode($logo->values, true);
        $meternumber_code = Settings::where('name', 'meternumber_code')->get(['values'])->first();
        $payment_expired_date = Settings::where('name', 'payment_expired_date')->get(['values'])->first();
        $owe_count = Settings::where('name', 'owe_count')->get(['values'])->first();
        $signs_sql = Settings::where('name', 'sign')->get(['values']);
        $signs = collect([]);
        foreach ($signs_sql as $sign) {
            $signs->push(json_decode($sign['values']));
        }
        // return $signs;
        return view('settings.edit', \compact('logo_values', 'signs', 'organizations',
            'meternumber_code', 'payment_expired_date', 'owe_count'));
    }

    public function import_excel($info_type=""){
        if($info_type == 'staff'){
            $staffs = Staff::whereIn('status', ['active', 'inactive'])->get();
            return view('settings.import_excel', compact('staffs'));
        }else if($info_type == 'members'){
            $members = UserProfile::where('status', 1)->get();
            return view('settings.import_excel', compact('members'));
        }
        return view('settings.import_excel');
    }

    public function upload_excel(REQUEST $request){
            //Allowed mime types
        $excelMimes = array('text/xls', 'text/xlsx', 'application/excel', 'application/vnd.msexcel', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        // Validate whether selected file is a Excel file
        if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $excelMimes)){

            // If the file is uploaded
            if(is_uploaded_file($_FILES['file']['tmp_name'])){
                $reader = new Xlsx();
                $spreadsheet = $reader->load($_FILES['file']['tmp_name']);
                $worksheet = $spreadsheet->getActiveSheet();
                $worksheet_arr = $worksheet->toArray();

                // Remove header row
                unset($worksheet_arr[0]);
                unset($worksheet_arr[1]);
                $worksheet_filter = collect($worksheet_arr)->filter(function($v){
                    return collect($v[1])->isNotEmpty();
                });

                $sequence_number = NumberSequence::first();
                if($request->get("info_type") == "staff"){
                    foreach($worksheet_filter as $row){
                        //ให้ทำการ Insert data
                        $initStaffs = new Staff();
                        $initStaffs->username = $row[0];
                        $initStaffs->password = Hash::make($row[1]);
                        $initStaffs->name = $row[2];
                        $initStaffs->phone = $row[3];
                        $initStaffs->status = $row[4];
                        $initStaffs->user_cate_id = $row[5];
                        $initStaffs->created_at = date("Y-m-d H:i:s");
                        $initStaffs->updated_at = date("Y-m-d H:i:s");
                        $initStaffs->save();
                    }
                }else{
                    $user_id    = $sequence_number->user_sq_number;
                    $meter_id   = $sequence_number->user_meter_sq_number;
                    $user_data  = [];
                    $userProfileData = [];

                    foreach($worksheet_filter as $row){
                        //ให้ทำการ Insert data
                        $user_data[] =[
                            'user_id'           => $user_id,
                            'username'          => 'user' . $user_id,
                            'password'          => Hash::make($row[2]),
                            'role_id'           => 3,
                            'email'             => $row[6],
                            'email_verified_at' => date('Y-m-d H:i:s'),
                            'remember_token'    => Hash::make('user' . $row[1]."-".date('Y-m-d H:i:s')),
                            'status'            => 'active',
                            'created_at'        => date('Y-m-d H:i:s'),
                            'updated_at'        => date('Y-m-d H:i:s')
                        ];
                        $userProfileData[] = [
                            'user_id'           => $user_id,
                            'prefix'            => $row[3],
                            'name'              => $row[4]." ".$row[5],
                            'id_card'           => $row[6],
                            'line_id'           => $row[7],
                            'phone'             => $row[9],
                            'gender'            => $row[10],
                            'address'           => $row[11],
                            'zone_id'           => $row[12],
                            'subzone_id'        => $row[13],
                            'tambon_code'       => $row[14],
                            'district_code'     => $row[15],
                            'province_code'     => $row[16],
                            'status'            => 'active',
                            'comment'           => '',
                            'created_at'        => date('Y-m-d H:i:s'),
                            'updated_at'        => date('Y-m-d H:i:s'),
                        ];

                        $meternumberCode = Settings::where('id', 2)->first();
                        $usermeterInfoData[] = [
                            'meternumber'           => $meter_id,
                            'user_id_fk'            => $user_id,
                            'metertype'             => $row[21],
                            'undertake_zone_id'     => $row[22],
                            'undertake_subzone_id'  => $row[23],
                            'acceptace_date'        => date('Y-m-d'),
                            'payment_id'            => 0,
                            'discounttype'          => 0,
                            'recorder_id'           => Auth::id(),
                            'status'                => 'active',
                            'owe_count'             => 0,
                            'comment'               => '',
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        ];
                        $user_id++;
                        $meter_id++;
                    }
                    if(User::insert($user_data)){
                        if(UserProfile::insert($userProfileData)){
                            UserMeterInfos::insert($usermeterInfoData) ? '' : 'ไม่สามารถบันทึกข้อมูล UserMeterInfos สำเร็จ';
                        }else{
                            return 'ไม่สามารถบันทึกข้อมูล UserProfile สำเร็จ';
                        }
                    }else{
                        return 'ไม่สามารถบันทึกข้อมูล Users สำเร็จ';
                    }

                    NumberSequence::find(1)->update([
                        'user_sq_number'        => $user_id,
                        'user_meter_sq_number'  => $meter_id,
                    ]);

                }
            }
        }

        return redirect('settings/import_excel/'.$request->get("info_type"));
    }

    private function meternumber($id){
        $meternumber = $id;
        if($id>0 && $id < 10){
            $meternumber = "0000".$id;
        }else if($id>10 && $id < 100){
            $meternumber = "000".$id;
        }else if($id>100 && $id < 1000){
            $meternumber = "00".$id;
        }else if($id>1000 && $id < 10000){
            $meternumber = "0".$id;
        }
        return $meternumber;
    }

}
