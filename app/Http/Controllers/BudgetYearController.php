<?php

namespace App\Http\Controllers;

use App\BudgetYear;
use App\Http\Controllers\Api\FunctionsController;
use App\InvoicePeriod;
use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\UsermeterInfosController as userMeterInfosClass;
use App\Models\InvoiceHistory;
class BudgetYearController extends Controller
{
    public function index()
    {
        $funcCtrl = new FunctionsController();

        $budgetyears = BudgetYear::orderBy('budgetyear', 'desc')->get();
        foreach ($budgetyears as $budgetyear) {
            $budgetyear->startdate = $funcCtrl->engDateToThaiDateFormat($budgetyear->startdate);
            $budgetyear->enddate = $funcCtrl->engDateToThaiDateFormat($budgetyear->enddate);
        }

        return view('budgetyear.index', \compact('budgetyears'));
    }

    public function create()
    {
        // return $this->storeInvoicePrvBudgetyear('2567');
        return view('budgetyear.create');
    }

    private function storeInvoicePrvBudgetyear($budgetyear){

        $byear = 2;
        $last_inv_period = InvoicePeriod::where('budgetyear_id', $byear)->whereIn('status', ['active', 'inactive'])->get(['id']);
        $last_inv_pr = collect([]);
        foreach($last_inv_period as $last){
            $last_inv_pr->add($last->id);
        }

        $invs = collect(Invoice::whereIn('inv_period_id', $last_inv_pr )
                            ->where('status', 'paid')
                            ->get()
                        )->groupBy('user_id')->values();

        foreach($invs as $inv){
            $json =  collect(collect($inv)->values())->toJson();
            $invHis = new InvoiceHistory();
            $invHis->user_id = $inv[0]->user_id;
            $invHis->budgetyear_id = $byear;
            $invHis->invoice_json_val = $json;
            $invHis->save();
            foreach($inv as $val){
                Invoice::where('id', $val->id)->delete();
            }

        }//foreeach

    }

    public function store(Request $request)
    {

        $request->validate([
            'budgetyear' => 'integer:required',
        ]);
        date_default_timezone_set('Asia/Bangkok');
        //`check มี budgetyear status active ไหม
        $currentBudgetYear = BudgetYear::where('status', 'active')->first();
        if(collect($currentBudgetYear)->isNotEmpty()){
            $activeInvPeroidOfCurrentBudgetYear = InvoicePeriod::where('budgetyear_id', $currentBudgetYear->id)
                ->where('status','active')
                ->get(['id', 'budgetyear_id']);

            Invoice::where('inv_period_id', $activeInvPeroidOfCurrentBudgetYear[0]->id)
                        ->where('status', 'init')
                        ->update([
                            'deleted' => 1,
                            'status'  => 'no_record',
                            'comment' => 'ไม่มีการบันทึกเลขมิเตอร์'
                        ]);
            Invoice::where('inv_period_id', $activeInvPeroidOfCurrentBudgetYear[0]->id)
                        ->where('status', 'invoice')
                        ->update([
                            'status' => 'owe',
                        ]);


            // update status == invactive  activeInvoice period of currentBudgetYear
            InvoicePeriod::where('budgetyear_id', $currentBudgetYear->id)
                ->where('status','active')->update([
                    'status' => 'inactive',
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

            //update status == inactive currentBudgetyear
            BudgetYear::where('status', 'active')->update([
                'status' => 'inactive',
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        $funcCtrl = new FunctionsController();
        //สร้างปีงบประมาณใหม่
        $budgetyear = new BudgetYear();
        $budgetyear->budgetyear = $request->get('budgetyear');
        $budgetyear->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('start'));
        $budgetyear->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('end'));
        $budgetyear->status = $request->get('status');
        $budgetyear->created_at = date('Y-m-d H:i:s');
        $budgetyear->updated_at = date('Y-m-d H:i:s');
        $budgetyear->save();

        $userMeterInfos = new userMeterInfosClass();
        $userMeterInfos->manageOweCount();
        return redirect('/budgetyear');
    }

    public function edit($id)
    {
        $budgetyear = BudgetYear::find($id);
        $funcCtrl = new FunctionsController();

        $budgetyear->startdate = $funcCtrl->engDateToThaiDateFormat($budgetyear->startdate);
        $budgetyear->enddate = $funcCtrl->engDateToThaiDateFormat($budgetyear->enddate);

        return view('budgetyear.edit', compact('budgetyear'));
    }

    public function delete($id)
    {
        $budgetyear = BudgetYear::find($id);

        $budgetyear->delete();

        return view('budgetyear.edit', compact('budgetyear'));
    }

    public function update(Request $request, $id)
    {
        $funcCtrl = new FunctionsController();

        $budgetyear = BudgetYear::find($id);
        $budgetyear->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('start'));
        $budgetyear->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('end'));
        $budgetyear->save();

        return redirect('/budgetyear');
    }
}
