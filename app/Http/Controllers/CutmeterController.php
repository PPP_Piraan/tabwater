<?php

namespace App\Http\Controllers;

use App\CutmeterHistory;
use App\Http\Controllers\Api\CutmeterController as ApiCutmeterCtrl;
use App\Http\Controllers\Api\UsersController as ApiUsersCtrl;
use App\InvoicePeriod;
use App\Invoice;
use App\Subzone;
use App\User;
use App\UserMeterInfos;
use App\UserProfile;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\facades\DB;

class CutmeterController extends Controller
{
    public function index($subzone_id = "")
    {
        $zones = Zone::all();
        $zone_id_selected = $subzone_id == "" ? 'all' : $subzone_id;
        $subzone_id_selected = 'all';
        $UserOweCountOver1Times = UserMeterInfos::where('status', '<>' ,'deleted')
                            ->where('deleted', 0)
                            ->where('owe_count', '>=',2)
                            ->with([
                                'user_profile'=>function($query){
                                    return $query->select('name', 'user_id', 'address');
                                },
                                'zone' =>function($query){
                                    return $query->select('id', 'zone_name');
                                },
                                'cutmeterHistory' =>function($query){
                                    return $query->select('user_id', 'inv_period_id', 'status','process_history');
                                }
                            ])
                            ->get(['user_id', 'undertake_zone_id', 'owe_count']);





        return view('cutmeter.index', compact('UserOweCountOver1Times', 'zone_id_selected', 'subzone_id_selected'));
    }

    public function create($user_id)
    {
        $tambon_infos_db = DB::table('settings')
            ->where('name', 'organization')
            ->select('values')
            ->get();
        $tambon_infos = collect(json_decode($tambon_infos_db[0]->values, true))->toArray();
        $user = UserMeterInfos::where('user_id', $user_id)
            ->where('deleted', 0)
            ->with([
                'user_profile' => function ($query) {
                    $query->select('name', 'address', 'phone', 'user_id', 'zone_id');
                },
                'zone:zone_name,id',
                'subzone:subzone_name,id',
            ])
            ->get(['user_id', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id']);

        $cutmeter_status = [['id' => '1', 'value' => 'ล็อคมิเตอร์']];

        $tabwatermans = User::where('user_cat_id', 4)
            ->with(['user_profile:name,user_id'])
            ->where('status', 'active')
            ->get(['id']);

        return view('cutmeter.create', compact('user', 'cutmeter_status', 'tabwatermans', 'tambon_infos', ));
    }

    public function store(REQUEST $request){
        // return $request;
        $process = 'cutmeter'; //เคส $request->get('status') == 1
        if($request->get('status') == 2){
            $process = 'install meter';
        }
        $twman_info = ['cutmeter_status' => $process,
                        'operate_date' => $request->get('operate_date'),
                        'operate_time' => $request->get('operate_time'),
                        'twman' =>$request->get('tabwaterman')
                    ];
        $inv_period_cutmeter = InvoicePeriod::where('status', 'active')->get('id');
        $create_cutmeter = new CutmeterHistory;
        $create_cutmeter->user_id = $request->get('user_id');
        $create_cutmeter->inv_period_id = $inv_period_cutmeter[0]->id;
        $create_cutmeter->process_history = \json_encode($twman_info);
        $create_cutmeter->status = $request->get('status');
        $create_cutmeter->comment = $request->get('comment');
        $create_cutmeter->created_at = date('Y-m-d H:i:s');
        $create_cutmeter->updated_at = date('Y-m-d H:i:s');
        // $create_cutmeter->save();
        // $this->printDisambledOrCompleteForHead($request->get('user_id'), $inv_period_cutmeter[0]->id);
        return $this->printDisambledOrCompleteForHead(1313, 39);
        // return redirect('cutmeter/index');
    }

    function print(REQUEST $request) {
        date_default_timezone_set('Asia/Bangkok');
        $from_view = $request->get('from_view');
        $funcCtrl = new FunctionsController();
        $oweArray = [];
        $apiInvoiceCtrl = new ApiInvoiceCtrl();
        foreach ($request->get('user_id') as $key => $on) {
            if ($on == 'on') {
                //หาการใช้น้ำ 5 เดือนล่าสุด
                $last5InvoiceByInvoicePeriod = Invoice::where('user_id', $key)
                    ->whereIn('status', ['owe', 'invoice'])
                    ->with('invoice_period', 'user_profile', 'user_profile.zone',
                        'user_profile.userMeterInfos'
                    )
                    ->orderBy('inv_period_id', 'desc')
                    ->get();
                if (collect($last5InvoiceByInvoicePeriod)->count() > 0) {
                    // หาผลรวมการให้น้ำของ status ที่เป็น owe และ invoice ปัจจุบัน
                    $sumUsedWaterByOweAndInvoiceStatus = collect($last5InvoiceByInvoicePeriod)->sum(function ($inv) use ($funcCtrl) {
                        $inv['invoice_period']['startdate'] = $funcCtrl->engDateToThaiDateFormat($inv['invoice_period']['startdate']);
                        $inv['invoice_period']['enddate'] = $funcCtrl->engDateToThaiDateFormat($inv['invoice_period']['enddate']);

                        if ($inv['status'] == 'invoice' || $inv['status'] == 'owe') {
                            $a = $inv['currentmeter'] - $inv['lastmeter'];
                            return $a;
                        }
                    });
                    $inv['water_used'] = $sumUsedWaterByOweAndInvoiceStatus;
                    $inv['paid'] = $sumUsedWaterByOweAndInvoiceStatus * 8;

                    array_push($oweArray, [
                        'res' => collect($last5InvoiceByInvoicePeriod)->reverse()->flatten(),
                    ]);
                }
            }
        }

        return view('owepaper.print', compact('oweArray', 'from_view'));

    }

    public function edit($user_id)
    {
        $tambon_infos_db = DB::table('settings')
            ->where('name', 'organization')
            ->select('values')
            ->get();
        $tambon_infos = collect(json_decode($tambon_infos_db[0]->values, true))->toArray();
        $user = UserMeterInfos::where('user_id', $user_id)
            ->where('deleted', 0)
            ->with([
                'user_profile' => function ($query) {
                    $query->select('name', 'address', 'phone', 'user_id', 'zone_id');
                },
                'zone:zone_name,id',
                'subzone:subzone_name,id',
            ])
            ->get(['user_id', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id']);

        $cutmeter_user_current_state = CutmeterHistory::where('user_id', $user_id)->where('pending', 1)->get(['status', 'twman_id']);
        $cutmeter_user_status = collect($cutmeter_user_current_state)->count() == 0 ? '' : $cutmeter_user_current_state[0]->status;
        $twman_appoint_json = collect($cutmeter_user_current_state)->count() == 0 ? 0 : \json_decode($cutmeter_user_current_state[0]->twman_id);
        $twman_appoint = $twman_appoint_json == 0 ? [(object) ['user_id' => ''], (object) ['user_id' => ''], (object) ['user_id' => '']] : $twman_appoint_json;

        $cutmeter_status = [['id' => '1', 'value' => 'ล็อคมิเตอร์'],
            ['id' => '3', 'value' => 'ติดตั้งมิเตอร์สำเร็จ'],
            ['id' => '4', 'value' => 'ยกเลิก']];

        $tabwatermans = User::where('user_cat_id', 4)
            ->with(['user_profile:name,user_id'])
            ->where('status', 'active')
            ->get(['id']);

        //ถ้า status == disambled สเตป ต่อไป จะ เปลี่ยน status เป็น  complete
        //ให้ check owe_count ใน user_data_infos table ว่า <2 ไหม
        //ถ้าไม่ให้ $show_submit_btn = 'hidden' และ $owe_count_text = 'ยังมีไม่การชำระเงินค่าน้ำประปาที่ค้าง'
        $show_submit_btn = '';
        $owe_count_text = '';

        if ($cutmeter_user_status == '1') {
            $check_owe_count = UserMeterInfos::where('user_id', $user_id)
                ->where('status', 'active')
                ->where('deleted', 0)
                ->where('cutmeter', )
                ->get(['owe_count']);
            if ($check_owe_count[0]->owe_count >= 3) {
                $show_submit_btn = 'hidden';
                $owe_count_text = 'ยังมีไม่การชำระเงินค่าน้ำประปาที่ค้าง';
            }
        }

        return view('cutmeter.edit', compact('user', 'twman_appoint', 'cutmeter_status', 'cutmeter_user_status', 'tabwatermans', 'tambon_infos', 'cutmeter_user_status', 'show_submit_btn', 'owe_count_text'));
    }

    public function update(REQUEST $request, $user_id)
    {
        // return $request;
        //ถ้าเป็นการยกหม้อครั้งแรกให้  create

            // $twman = ['cutmeter_status' => , 'twman' =>$request->get('tabwaterman')];
            $create_cutmeter = new CutmeterHistory;
            $create_cutmeter->cutmeter_id = $cutmeter_id;
            $create_cutmeter->user_id = $user_id;
            $create_cutmeter->operate_date = $request->get('operate_date');
            $create_cutmeter->operate_time = $request->get('operate_time');
            $create_cutmeter->twman_id = \json_encode();
            $create_cutmeter->status = $request->get('status');
            $create_cutmeter->comment = $request->get('comment');
            $create_cutmeter->pending = $request->get('status') == 'complete' ? 2 : $pending;
            $create_cutmeter->created_at = date('Y-m-d H:i:s');
            $create_cutmeter->updated_at = date('Y-m-d H:i:s');
            $create_cutmeter->save();



        return $this->printDisambledOrCompleteForHead($user_id, $request->get('status'));

        // return redirect('cutmeter/index');

    }


    public function update2(REQUEST $request, $user_id)
    {
        return $request;
        //ถ้าเป็นการยกหม้อครั้งแรกให้  create
        $cutmeter_id = 1;
        if ($request->get('status') == 'disambled') {
            $invoice_period = InvoicePeriod::where('status', 'active')->get(['id'])->first();
            $cutmeter_id = $invoice_period->id;
            //update  user_meter_infos
            //ใหั cutmeter == 1
            $discounttype = UserMeterInfos::where('user_id', $user_id)->where('deleted', 0)
                ->where('status', 'active')->get(['owe_count']);
            $update_usermeter_infos = UserMeterInfos::where('user_id', $user_id)->where('deleted', 0)
                ->where('status', 'active')->update([
                'cutmeter' => 1, // lockมิเตอร์
            ]);
        } else {
            //หา row ของ user_id ที่  pending == 1
            $active_pending_sql = CutmeterHistory::where('user_id', $user_id)->where('pending', 1);
            $active_pending = $active_pending_sql->get();
            $cutmeter_id = $active_pending[0]->cutmeter_id;
            //update pending == 0
            $active_pending_sql->update([
                'pending' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        }
        if ($request->get('status') == 'cancel') {
            //ถ้าทำการยกเลิก
            //ให้ทำการ update pending == 2 และ deleted = 1 ของ user ที่มี cutmeter_id ปัจจุบัน
            $update_when_renew_use_meter = CutmeterHistory::where('cutmeter_id', $cutmeter_id)
                ->where('user_id', $user_id)->update([
                'pending' => 2,
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $update_usermeter_infos = UserMeterInfos::where('user_id', $user_id)->where('deleted', 0)
                ->where('status', 'active')->update([
                'cutmeter' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            // $twman = ['cutmeter_status' => , 'twman' =>$request->get('tabwaterman')];
            $create_cutmeter = new CutmeterHistory;
            $create_cutmeter->cutmeter_id = $cutmeter_id;
            $create_cutmeter->user_id = $user_id;
            $create_cutmeter->operate_date = $request->get('operate_date');
            $create_cutmeter->operate_time = $request->get('operate_time');
            $create_cutmeter->twman_id = \json_encode();
            $create_cutmeter->status = $request->get('status');
            $create_cutmeter->comment = $request->get('comment');
            $create_cutmeter->pending = $request->get('status') == 'complete' ? 2 : $pending;
            $create_cutmeter->created_at = date('Y-m-d H:i:s');
            $create_cutmeter->updated_at = date('Y-m-d H:i:s');
            $create_cutmeter->save();

            if ($request->get('status') == 'complete') {
                //update  user_meter_infos
                //ใหั status == active
                $usermeter_infos = UserMeterInfos::where('user_id', $user_id)->where('deleted', 0)
                    ->where('status', 'active')->get(['owe_count']);
                $update_usermeter_infos = UserMeterInfos::where('user_id', $user_id)->where('deleted', 0)
                    ->where('status', 'active')->update([
                    'comment' => '',
                    'cutmeter' => $usermeter_infos[0]->owe_count >= 3 ? 1 : 0,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            }
        }
        return $this->printDisambledOrCompleteForHead($user_id, $request->get('status'));

        // return redirect('cutmeter/index');

    }

    public function reset_cutmeter_status($user_id){
        $usermeter_infos_query = UserMeterInfos::where('user_id', $user_id);
        $subzone = $usermeter_infos_query->get('undertake_subzone_id')->first();
        $usermeter_infos_query->update([
            'cutmeter' => 0
        ]);
        return redirect('cutmeter/index/'.$subzone->undertake_subzone_id);
    }

    private function printDisambledOrCompleteForHead($user_id, $inv_period_cutmeter)
    {
        $apiCutmeterCtrl = new ApiCutmeterCtrl;
        $apiUsersCtrl = new ApiUsersCtrl;
        $user = \json_decode($apiUsersCtrl->user($user_id)->content(), true);
        $headTwman = UserProfile::where('user_id', 2915)->get();
        $recorder = UserProfile::where('user_id', 905)->get();//วรรณวิภา  ไชยะนนท์

        $invoiceOweAndIvoiceStatus = collect($user[0]['invoice'])->filter(function ($v) {
            return $v['status'] == 'owe' || $v['status'] == 'invoice';
        });
        $status = 1;
        $cutmeteriInfos = $apiCutmeterCtrl->get_process_history($user_id , $inv_period_cutmeter);
        return view('cutmeter.print', compact('cutmeteriInfos', 'invoiceOweAndIvoiceStatus', 'user', 'status', 'headTwman', 'recorder'));
    }

    public static function cutmeterUserCount(){
        // $count = CutmeterHistory::whereIn('status', [1,2])->count();
        $count = UserMeterInfos::where('status', '<>' ,'deleted')
        ->where('owe_count', '>=',2)->count();
        return $count;
    }
}
