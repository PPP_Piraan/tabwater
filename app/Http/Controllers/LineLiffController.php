<?php

namespace App\Http\Controllers;

use App\UploadBankSlip;
use Illuminate\Http\Request;

class LineLiffController extends Controller
{
    public function index()
    {
        return view('line_liff.index');

    }

    public function history()
    {
        return view('line_liff.history');

    }

    public function upload_slip()
    {
        return view('line_liff.upload_slip');
    }

    public function save(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time() . '.' . $request->image->extension();

        $request->image->move(public_path('slip_images'), $imageName);

        /* Store $imageName name in DATABASE from HERE */
        $bankSlip = new UploadBankSlip();
        $bankSlip->user_id = $request->get('user_id');
        $bankSlip->line_id = 'kkk'; //$request->get('line_id');
        $bankSlip->invoice_id_list = json_encode($request->get('payments'));
        $bankSlip->mustpaid = $request->get('mustpaid');
        $bankSlip->image = $imageName;
        $bankSlip->status = 1; //upload แล้ว รอตรวจสอบ
        $bankSlip->recorder_id = 2933; //Auth::id();
        $bankSlip->created_at = date('Y-m-d H:i:s');
        $bankSlip->updated_at = date('Y-m-d H:i:s');
        $bankSlip->save();

        return back()
            ->with('success', 'You have successfully upload image.')
            ->with('image', $imageName);

    }
}