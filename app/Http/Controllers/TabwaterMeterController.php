<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TabwaterMeter; 

class TabwaterMeterController extends Controller
{
    public function index(){
        $tabwatermeters = TabwaterMeter::all();
        return view('settings.tabwatermeter.index', compact('tabwatermeters'));
    }

    public function create(){
        return view('settings.tabwatermeter.create');
    }

    public function store(REQUEST $request){
        $tabwaterMeter = new TabwaterMeter();
        $tabwaterMeter->typemetername = $request->get('typemetername');
        $tabwaterMeter->price_per_unit = $request->get('price_per_unit');
        $tabwaterMeter->metersize = $request->get('metersize'); 
        $tabwaterMeter->save();
        return redirect('tabwatermeter');
    }


    public function edit($id){
        $tabwatermeter = TabwaterMeter::find($id);
        return view('settings.tabwatermeter.edit', compact('tabwatermeter'));
    }

    public function update(Request $request ,$id){
        $tabwaterMeter = TabwaterMeter::find($id);
        $tabwaterMeter->typemetername = $request->get('typemetername');
        $tabwaterMeter->price_per_unit = $request->get('price_per_unit');
        $tabwaterMeter->metersize = $request->get('metersize'); 
        $tabwaterMeter->update();
        return redirect('tabwatermeter');
    }

    public function delete($id){
        $tabwaterMeter = TabwaterMeter::find($id);
        $tabwaterMeter->delete();
        return redirect('tabwatermeter');
    }

    public function infos($id){
        return TabwaterMeter::find($id);
    }
    
}
