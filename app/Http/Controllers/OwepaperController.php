<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoiceController as ApiInvoiceCtrl;
use App\Http\Controllers\Api\OwepaperController as ApiOwepaperController;
use App\Invoice;
use App\InvoicePeriod;
use App\Zone;
use Illuminate\Http\Request;

class OwepaperController extends Controller
{
    public function index(REQUEST $request)
    {

        $zones = Zone::all();
        $apiOwepaper = new ApiOwepaperController;
        $oweInvCountGroupByUserId = $apiOwepaper->oweAndInvoiceCount();

        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();
        $zone_id_selected = 'all';
        $subzone_id_selected = 'all';
        return view('owepaper.index', compact('zones', 'zone_id_selected', 'subzone_id_selected',
                                                'oweInvCountGroupByUserId',
                                            'invoice_period'));
    }

    function print(REQUEST $request) {
        date_default_timezone_set('Asia/Bangkok');
        $from_view = $request->get('from_view');
        $funcCtrl = new FunctionsController();
        $oweArray = [];
        $apiInvoiceCtrl = new ApiInvoiceCtrl();
        foreach ($request->get('user_id') as $key => $on) {
            if ($on == 'on') {
                //หาการใช้น้ำ 5 เดือนล่าสุด
                $last5InvoiceByInvoicePeriod = Invoice::where('user_id', $key)
                    ->whereIn('status', ['owe', 'invoice'])
                    ->with('invoice_period', 'user_profile', 'user_profile.zone',
                        'user_profile.userMeterInfos'
                    )
                    ->orderBy('inv_period_id', 'desc')
                    ->get();
                if (collect($last5InvoiceByInvoicePeriod)->count() > 0) {
                    // หาผลรวมการให้น้ำของ status ที่เป็น owe และ invoice ปัจจุบัน
                    $sumUsedWaterByOweAndInvoiceStatus = collect($last5InvoiceByInvoicePeriod)->sum(function ($inv) use ($funcCtrl) {
                        $inv['invoice_period']['startdate'] = $funcCtrl->engDateToThaiDateFormat($inv['invoice_period']['startdate']);
                        $inv['invoice_period']['enddate'] = $funcCtrl->engDateToThaiDateFormat($inv['invoice_period']['enddate']);

                        if ($inv['status'] == 'invoice' || $inv['status'] == 'owe') {
                            $a = $inv['currentmeter'] - $inv['lastmeter'];
                            return $a;
                        }
                    });
                    $inv['water_used'] = $sumUsedWaterByOweAndInvoiceStatus;
                    $inv['paid'] = $sumUsedWaterByOweAndInvoiceStatus * 8;

                    array_push($oweArray, [
                        'res' => collect($last5InvoiceByInvoicePeriod)->reverse()->flatten(),
                    ]);
                }
            }
        }

        return view('owepaper.print', compact('oweArray', 'from_view'));

    }
}
