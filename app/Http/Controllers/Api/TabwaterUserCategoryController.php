<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TabwaterUserCategory;

class TabwaterUserCategoryController extends Controller
{
    public function index(){
        $tw_usr_cate = TabwaterUserCategory::all();
        return \response()->json($tw_usr_cate);
    }

    public function store(Request $request){
        $tw_usr_cate = new TabwaterUserCategory();
        $tw_usr_cate->tab_usr_cate_name = $request->get('tabwatermetertype');
        $tw_usr_cate->price_per_unit = $request->get('price_per_unit');
        $tw_usr_cate->unit_num = $request->get('unit_num');
        $tw_usr_cate->unit_name = $request->get('unit_name');

        $tw_usr_cate->save();
        return \response()->json("ทำการบันทึกข้อมูลเรียบร้อย");
    }

    public function edit($id){
        $tw_usr_cate = TabwaterUserCategory::find($id);
        return \response()->json($tw_usr_cate);
    }

    public function update(Request $request, $id){
        $tw_usr_cate = TabwaterUserCategory::find($id);
        $tw_usr_cate->tab_usr_cate_name = $request->get('tw_usr_cate_name');
        $tw_usr_cate->price_per_unit = $request->get('price_per_unit');
        $tw_usr_cate->unit_num = $request->get('unit_num');
        $tw_usr_cate->unit_name = $request->get('unit_name');
        $tw_usr_cate->update();
        return \response()->json("ทำการอัพเดทข้อมูลเรียบร้อย");
    }

    public function delete($id){
        $tw_usr_cate = TabwaterUserCategory::find($id);
        $tw_usr_cate->delete();
        return \response()->json("ทำการลบข้อมูลเรียบร้อย");

    }
}
