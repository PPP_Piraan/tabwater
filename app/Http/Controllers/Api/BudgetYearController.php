<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BudgetYear;

class BudgetYearController extends Controller
{
    public function store(Request $request){
        date_default_timezone_set('Asia/Bangkok');
        $budgetyear = new BudgetYear();
        $budgetyear->budgetyear = $request->get('budgetyear');
        $budgetyear->startdate = $request->get('fromdate');
        $budgetyear->enddate = $request->get('enddate');
        $budgetyear->status = $request->get('status');
        $budgetyear->created_at = date('Y-m-d H:i:s');
        $budgetyear->updated_at = date('Y-m-d H:i:s');
        $budgetyear->save();
        return response()->json(['res'=> 'ok', 'status'=>200]);
    }

    public function edit(Request $request){
        $budgetyear = BudgetYear::find($request);
        return response()->json($budgetyear);
    }

    public function update(Request $request){
        date_default_timezone_set('Asia/Bangkok');
        $budgetyear = BudgetYear::where('id', $request->get('id'))->first();
        $budgetyear->budgetyear = $request->get('budgetyear');
        $budgetyear->startdate = $request->get('fromdate');
        $budgetyear->enddate = $request->get('enddate');
        $budgetyear->status = $request->get('status');
        $budgetyear->updated_at = date('Y-m-d H:i:s');
        $budgetyear->update();
        return response()->json(['res'=> 'ok', 'status'=>200]);
    }

    public function find_active_status(){
        $activeBudgetYearCount = BudgetYear::where('status', 'active')->count();
        return response()->json(['active_budgetyear'=> $activeBudgetYearCount, 'status'=>200]);
    }

}
