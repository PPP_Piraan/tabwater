<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Usercategory;
class UserCategoryController extends Controller
{
    public function index(){
        $usercategories = Usercategory::all();
        return response()->json($usercategories);
    }
}
