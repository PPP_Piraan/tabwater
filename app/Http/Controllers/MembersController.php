<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Zone;
use App\Usercategory;
use App\TabWaterUser;
use App\UserProfile;
use Illuminate\Support\Facades\Hash;

class MembersController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('members.index', compact('users'));
    }

    public function create()
    {
        $user = new User;
        $usercategory = Usercategory::all();
        $zones =  Zone::all();
        return view('members.create', compact('user', 'usercategory', 'zones'));
    }

    public function store(REQUEST $request){
        date_default_timezone_set('Asia/Bangkok');
        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = Hash::make($request->password);
        $user->user_cat_id = $request->usercategory;
        $user->remember_token = '1111';
        $created_at = date('Y-m-d');
        $updated_at = date('Y-m-d');
        $user->save();
      // dd($request);
        $userProfile = new UserProfile;
        $userProfile->user_id = $user->id;
        $userProfile->name = $request->name;
        $userProfile->lastname = $request->lastname;
        $userProfile->id_card = $request->idcard;
        $userProfile->phone = $request->phone;
        $userProfile->gender = $request->gender;
        $userProfile->address = $request->address;
        $userProfile->zone = $request->zone;
        $userProfile->tabon_id = '351201';
        $userProfile->district_id = '35120';
        $userProfile->province_id = '35';
        $userProfile->created_at = date('Y-m-d');
        $userProfile->updated_at = date('Y-m-d');
        $userProfile->save();
        return redirect('members/create')->with(['message'=> 'ทำการบันทึกแล้ว']);
    }
}
