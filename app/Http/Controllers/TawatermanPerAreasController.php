<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TabwaterManPerArea;

class TawatermanPerAreasController extends Controller
{
    public function index(){
        $tabwaterman_per_areas = TabwaterManPerArea::all();
        $a = json_decode($tabwaterman_per_areas[0]->areas);
        $infos  =[];
        foreach($tabwaterman_per_areas as $tbs){
            $areas = json_decode($tbs->areas);
            array_push($infos, ['twm_id'=> $tbs->tabwaterman_id, 'zones' => $areas]);
        }
        return view('tabwaterman_per_areas.index', compact('infos'));
    }

    public function create(){
        $tabwaterman_per_areas = new TabwaterManPerArea;
        
        return view('tabwaterman_per_areas.create', compact('tabwaterman_per_areas'));
    }

    public function store(REQUEST $request){
        date_default_timezone_set('Asia/Bangkok');

        $storeTabwaterman_per_areas = new TabwaterManPerArea;
        $storeTabwaterman_per_areas->zone_name= $request->get('zone_name');
        $storeTabwaterman_per_areas->location = $request->get('location');
        $storeTabwaterman_per_areas->created_at = date('Y-m-d H:i:s');
        $storeTabwaterman_per_areas->updated_at = date('Y-m-d H:i:s');
        $storeTabwaterman_per_areas->save();
        
        return redirect('tabwaterman_per_areas')->with(['massage' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    public function update($id){
        $tabwaterman_per_areas = TabWaterManPerArea::where("tabwaterman_id", $id)->first();
        // dd($tabwaterman_per_areas);
        return view('tabwaterman_per_areas.update', compact('tabwaterman_per_areas'));
    }

    public function edit(REQUEST $request , $id){
        date_default_timezone_set('Asia/Bangkok');

        $tabwaterman_per_areas = TabWaterManPerArea::find($id);
        $tabwaterman_per_areas->zone_name = $request->get('zone_name');
        $tabwaterman_per_areas->location = $request->get('location');
        $tabwaterman_per_areas->updated_at = date('Y-m-d H:i:s');
        $tabwaterman_per_areas->save();
        return redirect('tabwaterman_per_areas')->with(['massage' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    public function delete($id){
        $tabwaterman_per_areas = TabWaterManPerArea::find($id);
        $tabwaterman_per_areas->delete();
        return redirect('tabwaterman_per_areas')->with(['massage' => 'ทำการลบข้อมูลเรียบร้อยแล้ว']);

    }
}
