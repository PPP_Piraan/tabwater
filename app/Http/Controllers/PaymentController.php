<?php

namespace App\Http\Controllers;

use App\Accounting;
use App\CutmeterHistory;
use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoiceController as ApiInvoiceController;
use App\Http\Controllers\Api\OwepaperController;
use App\Http\Controllers\Api\PaymentController as ApiPaymentController;
use App\Invoice;
use App\InvoicePeriod;
use App\Settings;
use App\UploadBankSlip;
use App\UserMeterInfos;
use App\UserProfile;
use App\Zone;
use App\Subzone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function autocompleteSearch(Request $request)
    {
        $currentInvoicePeriod = InvoicePeriod::where('status', 'active')->get();
        $query =  $request->get('query');
        $filterResult  = DB::table('user_meter_infos as umf')
        ->join('zone as z', 'umf.undertake_zone_id', '=', 'z.id')
        ->join('user_profile as uf', 'uf.user_id', '=', 'umf.user_id')
        ->join('invoice as inv', 'inv.user_id', '=', 'umf.user_id')
        ->select(DB::RAW('CONCAT(uf.address," ", z.zone_name, " - ",uf.name," - ",umf.meternumber) as aa'))
        ->where('inv.deleted', '=', 0)
        // ->where('inv_period_id', $currentInvoicePeriod[0]->id)
        // ->groupBy('umf.meternumber')
        ->get();
          $userArray = collect($filterResult)->filter(function($v) use ($query){
              return str_contains($v->aa, $query);
          })
          ->pluck('aa');
          return response()->json($userArray);
    }
    public function index(REQUEST $request)
    {
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

        $invoices_sql= Invoice::where('inv_period_id', $invoice_period->id)
        ->where('status', 'invoice')
        ->with(['usermeterinfos'=> function ($query) {
            $query->select('user_id_fk','undertake_subzone_id', 'undertake_zone_id', 'metertype','meternumber', 'owe_count');
        },'usermeterinfos.meter_type'=> function ($query){
            $query->select('id', 'price_per_unit');
        }
        ])->get();
        $invoices = collect($invoices_sql)->sortBy('usermeterinfos.meter_id_fk');

        $settings = Settings::where('name', 'meternumber_code')->get('values');
        $meternumber_code = collect($settings)->isEmpty() ? "" : $settings[0]->values;
        foreach($invoices as $inv){
            $inv->meternumberStr = $meternumber_code."".FunctionsController::createInvoiceNumberString($inv->meter_id_fk);
        }
        $subzones = collect(Subzone::all())->sortBy('zone_id');
        $page = 'index';

        return view('payment.index', compact('invoices', 'subzones','page'));
    }
    public function index_search_by_suzone(Request $request){
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();
        $subzones = Subzone::all();
        $subzone_search_lists = $request->get('subzone_id_lists');
        if(collect($request->get('subzone_id_lists'))->isEmpty()){
            $subzone_search_lists = collect($subzones)->pluck('id')->toArray();
        }
        $invoices_sql= Invoice::where('inv_period_id', $invoice_period->id)
        ->where('status', 'invoice')
        ->with(['usermeterinfos'=> function ($query) use ($subzone_search_lists) {
            $query->select('id', 'undertake_subzone_id','undertake_zone_id', 'user_id', 'metertype','meternumber', 'owe_count')
            ->whereIn('undertake_subzone_id', $subzone_search_lists);
        },'usermeterinfos.meter_type'=> function ($query){
            $query->select('id', 'price_per_unit');
        }
        ])->get();
        $invoices = collect($invoices_sql)->filter(function ($invoice) {
            return collect($invoice->usermeterinfos)->count() > 0;
        })->sortBy('usermeterinfos.user_id');
        $page = 'index_search_by_suzone';

        return view('payment.index', compact('invoices', 'subzones', 'subzone_search_lists', 'page'));
    }
    // public function index(REQUEST $request)
    // {
    //     //หาจำนวน status  เท่ากับ  [invoice, owe] และ reciept_id = 0
    //     //ในตาราง invoice
    //     $apiOwepaper = new OwepaperController();
    //     $oweInvCountGroupByUserId = $apiOwepaper->oweAndInvoiceCount();
    //     $oweAndInvoiceCountDivideBySubzone = $apiOwepaper->testOweAndInvoioceDivideBySubzone($request);
    //     if ($request->session()->has('undertake_zone_id') && $request->session()->has('undertake_subzone_id')) {
    //         $zone_id_selected = $request->session()->get('undertake_zone_id');
    //         $subzone_id_selected = $request->session()->get('undertake_subzone_id');
    //     } else {
    //         $zone_id_selected = 'all';
    //         $subzone_id_selected = 'all';
    //     }

    //     $zones = Zone::all();
    //     $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

    //     return view('payment.index', compact('zones',
    //                                             'zone_id_selected',
    //                                             'subzone_id_selected',
    //                                             'invoice_period',
    //                                         'oweInvCountGroupByUserId',
    //                                         'oweAndInvoiceCountDivideBySubzone'
    //                                     ));
    // }

    public function create(Request $request)
    {
        $paymentsArr = [];
        $total = 0;
        foreach ($request->get('data') as $inv) {
            $v = Invoice::where('id', $inv['inv_id'])
                ->with('usermeterinfos', 'usermeterinfos.user_profile', 'invoice_period')
                ->get();
            array_push($paymentsArr, $v);

            $total += ($v[0]->currentmeter - $v[0]->lastmeter) * 8;
        }
        $payments = collect($paymentsArr)->flatten();
        return view('payment.invoice_sum', compact('payments', 'total'));
    }

    public function store(Request $request)
    {
        $request->session()->put('undertake_zone_id', $request->get('undertake_zone_id_selected'));
        $request->session()->put('undertake_subzone_id', $request->get('undertake_subzone_id_selected'));

        date_default_timezone_set('Asia/Bangkok');
        $payments = collect($request->get('payments'))->filter(function ($v) {
            return isset($v['on']);
        });
        // return $request;
        $receipt = new Accounting();
        $receipt->acc_sum_paid      = $request->get('paidsum');
        $receipt->acc_sum_paid      = $request->get('vat7');
        $receipt->acc_sum_net_paid  = $request->get('mustpaid');
        $receipt->cashier           = Auth::id();
        $receipt->created_at        = date('Y-m-d H:i:s');
        $receipt->updated_at        = date('Y-m-d H:i:s');
        $receipt->save();

        //ทำการ update invoice and user_meter_infos table โดยการลบ owe_count
        //ของ user ถ้า มากกว่า 0 ออกไป count($payments)
        $this->_store_update_userMeterInfos($request->get('user_id'), $payments, $receipt->id);
        //กรณีมีการตัดมิเตอร์
        //ทำการสร้าง และ update  cutmeterHistory table ให้ status == 1
        // return $this->_store_cutmeter($request->get('user_id'));

        //ถ้ามีการจ่ายผ่าน การโอนเงินเข้าบัญชีแล้วได้ส่ง slip มาให้ check
        if (collect($request->get('paid_with_bank_slip'))->isNotEmpty()) {
            $this->_store_update_upload_bank_slip($request->get('user_id'));
        }
        return redirect('payment/receipt_print/' . $receipt->id);
    }

    private function _store_update_userMeterInfos($user_id, $payments, $receiptId)
    {
        $userMeterInfosSql  = UserMeterInfos::where('user_id_fk', $user_id);
        $oweCount           = $userMeterInfosSql->get('owe_count');
        foreach ($payments as $key => $payment) {
            //ทำการupdate invoice ให้ status เท่ากับ paid
            $update = Invoice::where('id', $payment['iv_id'])->update([
                'receipt_id'    => $receiptId,
                'status'        => 'paid',
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);

        }
        $diff = $oweCount[0]->owe_count - collect($payments)->count();
        $userMeterInfosSql->update([
            'owe_count'     => $diff < 1 ? 0 : $diff,
            'updated_at'    => date('Y-m-d H:i:s'),
            'status'        => 'active',
            'comment'       => $diff > 3 ? 'ค้างชำระเกิน 3 เดือน' : '',
        ]);
    }

    private function _store_cutmeter($user_id)
    {
        return$cutmeter_history_sql = CutmeterHistory::where('user_id', $user_id)
            ->whereIn('status', [1,2])->get();

        //ถ้าไม่มีการตัดมิเตอร์
        if (collect($cutmeter_history)->isNotEmpty()) {

        }
    }

    private function _store_update_upload_bank_slip($user_id)
    {
        $bankSlip = UploadBankSlip::where('user_id', $user_id)
            ->where('status', 1)->update([
            'status' => 2,
            'recorder_id' => Auth::id(),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    public function receipt_print(REQUEST $request, $reciept_id)
    {
        $invoicesPaidForPrint = Invoice::where('receipt_id', $reciept_id)
            ->with('usermeterinfos:user_id_fk,meternumber')
            ->get(['id','inv_period_id', 'meter_id_fk', 'lastmeter', 'currentmeter', 'status', 'receipt_id', 'recorder_id', 'created_at']);

        $newId = FunctionsController::createInvoiceNumberString($reciept_id);
        $type = 'paid_receipt';

        return view('payment.receipt_print', compact('invoicesPaidForPrint', 'newId', 'type'));
    }



    public function storeBackup(Request $request)
    {
        return $request;
        $paymentsFilter = collect($request->get('payments'))->filter(function ($v, $key) {
            return isset($v['on']);
        });
        date_default_timezone_set('Asia/Bangkok');

        $receipt = new Accounting();
        $receipt->total = $request->get('mustpaid');
        $receipt->cashier = Auth::id();
        $receipt->created_at = date('Y-m-d H:i:s');
        $receipt->updated_at = date('Y-m-d H:i:s');
        $receipt->save();
        //ทำการupdate invoice
        $apiInvCtrl = new ApiInvoiceController();
        foreach ($paymentsFilter as $key => $val) {
            $update = Invoice::where('id', $val['iv_id'])->update([
                'receipt_id' => $receipt->id,
                'status' => 'paid',
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $apiPaymentCtrl = new ApiPaymentController();
        $invoicesPaidForPrint = $apiPaymentCtrl->history($receipt->id, 'receipt');
        $newId = FunctionsController::createInvoiceNumberString($receipt->id);
        $type = 'paid_receipt';
        $cashier = DB::table('user_profile')
            ->where('user_id', '=', Auth::id())
            ->select('name')
            ->get();
        $cashiername = $cashier[0]->name;
        return view('payment.receipt_print', compact('invoicesPaidForPrint', 'newId', 'type', 'cashiername'));
    }

    public function print_payment_history($receipt_id)
    {
        $invoicesPaidForPrint = Invoice::where('receipt_id', $receipt_id)
            ->with([
                'user_profile' => function ($query) {
                    return $query->select('user_id', 'name', 'address', 'zone_id', 'subzone_id', 'tambon_code', 'district_code', 'province_code');
                },
                'user_profile.zone' => function ($query) {
                    return $query->select('id', 'zone_name as user_zone_name');
                },
                'user_profile.subzone' => function ($query) {
                    return $query->select('id', 'subzone_name as user_subzone_name');
                },
                'user_profile.tambon' => function ($query) {
                    return $query->select('tambon_code', 'tambon_name', 'zipcode');
                },
                'user_profile.district' => function ($query) {
                    return $query->select('district_code', 'district_name');
                },
                'user_profile.province' => function ($query) {
                    return $query->select('province_code', 'province_name');
                },
                'invoice_period' => function ($query) {
                    return $query->select('id', 'inv_period_name');
                },
                'usermeterinfos' => function ($query) {
                    return $query->select('user_id', 'meternumber', 'undertake_subzone_id');
                },
                'usermeterinfos.subzone' => function ($query) {
                    return $query->select('id', 'subzone_name as record_route');
                },
                'usermeterinfos.invoice' => function ($query) {
                    return $query->select('user_id', 'status', 'lastmeter', 'currentmeter')->where('status', 'owe');
                },
                'accounting' => function ($query) {
                    return $query->select('id', 'total', 'cashier', 'updated_at');
                },
                'accounting.user_profile' => function ($query) {
                    return $query->select('user_id', 'name as cashier_name');
                },
            ])
            ->get(['inv_period_id', 'user_id', 'lastmeter', 'currentmeter', 'status', 'receipt_id', 'recorder_id', 'created_at']);

        $newId = FunctionsController::createInvoiceNumberString($receipt_id);
        $type = 'paid_receipt';
        $cashier = DB::table('user_profile')
            ->where('user_id', '=', Auth::id())
            ->select('name')
            ->get();
        $cashiername = $cashier[0]->name;
        return view('payment.receipt_print', compact('invoicesPaidForPrint', 'newId', 'type', 'cashiername'));
    }

    public function search()
    {
        $apiOwepaper = new OwepaperController();
        $zones = Zone::all();
        $oweInvCountGroupByUserId = $apiOwepaper->oweAndInvoiceCount();

        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

        return view('payment.search', compact('zones', 'invoice_period', 'oweInvCountGroupByUserId'));
    }

    public function remove($receiptId)
    {
        date_default_timezone_set('Asia/Bangkok');

        //เปลี่ยนสถานะของ invoice table
        // receipt_id เป็น 0,
        //ถ้า inv_period_id เท่ากับ invoice period table ที่ status  เท่ากับ active (ปัจจุบัน) ให้
        // - invoice.status เท่ากับ invoice นอกเหนือจากนั้นให้ status เป็น owe
        $currentInvoicePeriod = InvoicePeriod::where('status', 'active')->get('id')->first();
        $invoicesTemp = Invoice::where('receipt_id', $receiptId);
        $invoices = $invoicesTemp->get(['inv_period_id', 'id']);
        foreach ($invoices as $invoice) {
            $status = 'invoice';

            if ($invoice->inv_period_id != $currentInvoicePeriod->id) {
                $status = 'owe';
            }
            Invoice::where('id', $invoice->id)->update([
                'receipt_id' => 0,
                'status' => $status,
                'recorder_id' => Auth::id(),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        //เปลี่ยนสถานะของ accouting table เป็น  0
        Accounting::where('id', $receiptId)->update([
            'status' => 0,
            'cashier' => Auth::id(),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);

        return \redirect('payment/search');
    }

    public function paymenthistory($inv_period = '', $subzone_id = '')
    {
        $sql = DB::table('user_meter_infos as umf')
            ->join('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
            ->join('user_profile as upf', 'upf.user_id', '=', 'umf.user_id')
            ->join('zone as z', 'z.id', '=', 'umf.undertake_zone_id')
            ->join('subzone as sz', 'sz.id', '=', 'umf.undertake_subzone_id')
            ->where('iv.inv_period_id', '=', $inv_period)
            ->where('iv.status', '=', 'paid')
            ->where('umf.undertake_subzone_id', '=', $subzone_id)
            ->where('umf.status', '<>', 'permanent deleted');

        $paid = $sql->get([
            'umf.user_id', 'umf.meternumber', 'umf.undertake_subzone_id', 'umf.undertake_zone_id',
            'upf.name', 'upf.address', 'iv.lastmeter', 'iv.currentmeter', 'iv.printed_time', 'iv.id',
            'upf.zone_id as user_zone_id', 'iv.comment',
            DB::raw('iv.currentmeter - iv.lastmeter as meter_net'),
            DB::raw('(iv.currentmeter - iv.lastmeter)*8 as total'),
        ]);
        $zoneInfoSql = $sql->get([
            'z.zone_name as undertake_zone', 'z.id as undertake_zone_id',
            'sz.subzone_name as undertake_subzone', 'sz.id as undertake_subzone_id',
        ]);
        $zoneInfo = collect($zoneInfoSql)->take(1);
        foreach ($paid as $iv) {
            $funcCtrl = new FunctionsController();
            $iv->user_id_string = $funcCtrl->createInvoiceNumberString($iv->user_id);
        }

        $presentInvoicePeriod = InvoicePeriod::where('id', $inv_period)->get('inv_period_name')[0];

        $memberHasInvoice = collect($paid)->sortBy('user_id');

        return view('payment.paymenthistory', compact('presentInvoicePeriod', 'zoneInfo', 'memberHasInvoice'));
    }

    public function receipted_list($user_id)
    {
        $apiPaymentCtrl = new ApiPaymentController;
        $receipted_list = $apiPaymentCtrl->history($user_id, 'receipt_history');
        return view('payment.receipted_list', compact('receipted_list'));
    }

    public function test()
    {
        return $this->testDuplicateInvoice();
        $var = [
            ['id' => 242990, 'inv_period_id' => 19, 'user_id' => 939, 'lastmeter' => 2401, 'currentmeter' => 2407, 'diff' => 6],
            ['id' => 243073, 'inv_period_id' => 19, 'user_id' => 909, 'lastmeter' => 383, 'currentmeter' => 386, 'diff' => 3],
            ['id' => 243077, 'inv_period_id' => 19, 'user_id' => 1087, 'lastmeter' => 44, 'currentmeter' => 44, 'diff' => 0],
            ['id' => 243106, 'inv_period_id' => 19, 'user_id' => 993, 'lastmeter' => 3450, 'currentmeter' => 3461, 'diff' => 11],
            ['id' => 243133, 'inv_period_id' => 19, 'user_id' => 997, 'lastmeter' => 939, 'currentmeter' => 943, 'diff' => 4],
            ['id' => 243153, 'inv_period_id' => 19, 'user_id' => 1014, 'lastmeter' => 767, 'currentmeter' => 769, 'diff' => 2],
            ['id' => 243154, 'inv_period_id' => 19, 'user_id' => 1048, 'lastmeter' => 1646, 'currentmeter' => 1659, 'diff' => 13],
            ['id' => 243159, 'inv_period_id' => 19, 'user_id' => 1054, 'lastmeter' => 1758, 'currentmeter' => 1758, 'diff' => 0],
            ['id' => 243169, 'inv_period_id' => 19, 'user_id' => 908, 'lastmeter' => 1379, 'currentmeter' => 1394, 'diff' => 15],
            ['id' => 243174, 'inv_period_id' => 19, 'user_id' => 1088, 'lastmeter' => 983, 'currentmeter' => 996, 'diff' => 13],
            ['id' => 243180, 'inv_period_id' => 19, 'user_id' => 1046, 'lastmeter' => 1300, 'currentmeter' => 1305, 'diff' => 5],
            ['id' => 243197, 'inv_period_id' => 19, 'user_id' => 1005, 'lastmeter' => 754, 'currentmeter' => 765, 'diff' => 11],
            ['id' => 243200, 'inv_period_id' => 19, 'user_id' => 947, 'lastmeter' => 505, 'currentmeter' => 508, 'diff' => 3],
            ['id' => 243207, 'inv_period_id' => 19, 'user_id' => 1078, 'lastmeter' => 622, 'currentmeter' => 635, 'diff' => 13],
            ['id' => 243279, 'inv_period_id' => 19, 'user_id' => 919, 'lastmeter' => 2104, 'currentmeter' => 2123, 'diff' => 19],
            ['id' => 243493, 'inv_period_id' => 19, 'user_id' => 1091, 'lastmeter' => 461, 'currentmeter' => 461, 'diff' => 0],
            ['id' => 243520, 'inv_period_id' => 19, 'user_id' => 920, 'lastmeter' => 70, 'currentmeter' => 100, 'diff' => 30],
            ['id' => 243643, 'inv_period_id' => 19, 'user_id' => 1047, 'lastmeter' => 260, 'currentmeter' => 264, 'diff' => 4],
            ['id' => 245557, 'inv_period_id' => 20, 'user_id' => 2927, 'lastmeter' => 274, 'currentmeter' => 317, 'diff' => 43],
            ['id' => 245560, 'inv_period_id' => 20, 'user_id' => 1013, 'lastmeter' => 89, 'currentmeter' => 91, 'diff' => 2],
            ['id' => 245561, 'inv_period_id' => 20, 'user_id' => 1012, 'lastmeter' => 9, 'currentmeter' => 9, 'diff' => 0],
            ['id' => 245565, 'inv_period_id' => 20, 'user_id' => 1035, 'lastmeter' => 81, 'currentmeter' => 84, 'diff' => 3],
            ['id' => 245566, 'inv_period_id' => 20, 'user_id' => 1104, 'lastmeter' => 1584, 'currentmeter' => 1591, 'diff' => 7],
            ['id' => 245569, 'inv_period_id' => 20, 'user_id' => 1089, 'lastmeter' => 524, 'currentmeter' => 527, 'diff' => 3],
            ['id' => 245570, 'inv_period_id' => 20, 'user_id' => 971, 'lastmeter' => 287, 'currentmeter' => 292, 'diff' => 5],
            ['id' => 245576, 'inv_period_id' => 20, 'user_id' => 1011, 'lastmeter' => 487, 'currentmeter' => 488, 'diff' => 1],
            ['id' => 245581, 'inv_period_id' => 20, 'user_id' => 939, 'lastmeter' => 2407, 'currentmeter' => 2414, 'diff' => 7],
            ['id' => 245582, 'inv_period_id' => 20, 'user_id' => 982, 'lastmeter' => 841, 'currentmeter' => 845, 'diff' => 4],
            ['id' => 245583, 'inv_period_id' => 20, 'user_id' => 1100, 'lastmeter' => 239, 'currentmeter' => 256, 'diff' => 17],
            ['id' => 245584, 'inv_period_id' => 20, 'user_id' => 922, 'lastmeter' => 4939, 'currentmeter' => 4939, 'diff' => 0],
            ['id' => 245587, 'inv_period_id' => 20, 'user_id' => 1043, 'lastmeter' => 878, 'currentmeter' => 894, 'diff' => 16],
            ['id' => 245595, 'inv_period_id' => 20, 'user_id' => 3024, 'lastmeter' => 34, 'currentmeter' => 47, 'diff' => 13],
            ['id' => 245599, 'inv_period_id' => 20, 'user_id' => 2929, 'lastmeter' => 1, 'currentmeter' => 1, 'diff' => 0],
            ['id' => 245600, 'inv_period_id' => 20, 'user_id' => 928, 'lastmeter' => 629, 'currentmeter' => 633, 'diff' => 4],
            ['id' => 245601, 'inv_period_id' => 20, 'user_id' => 927, 'lastmeter' => 663, 'currentmeter' => 663, 'diff' => 0],
            ['id' => 245602, 'inv_period_id' => 20, 'user_id' => 1087, 'lastmeter' => 44, 'currentmeter' => 44, 'diff' => 0],
            ['id' => 245603, 'inv_period_id' => 20, 'user_id' => 920, 'lastmeter' => 100, 'currentmeter' => 103, 'diff' => 3],
            ['id' => 245605, 'inv_period_id' => 20, 'user_id' => 900, 'lastmeter' => 84, 'currentmeter' => 86, 'diff' => 2],
            ['id' => 245609, 'inv_period_id' => 20, 'user_id' => 2436, 'lastmeter' => 1023, 'currentmeter' => 1027, 'diff' => 4],
            ['id' => 245758, 'inv_period_id' => 20, 'user_id' => 994, 'lastmeter' => 2467, 'currentmeter' => 2494, 'diff' => 27],
            ['id' => 245766, 'inv_period_id' => 20, 'user_id' => 955, 'lastmeter' => 1778, 'currentmeter' => 1785, 'diff' => 7],
            ['id' => 245772, 'inv_period_id' => 20, 'user_id' => 909, 'lastmeter' => 386, 'currentmeter' => 387, 'diff' => 1],
            ['id' => 245777, 'inv_period_id' => 20, 'user_id' => 946, 'lastmeter' => 912, 'currentmeter' => 936, 'diff' => 24],
            ['id' => 245779, 'inv_period_id' => 20, 'user_id' => 3005, 'lastmeter' => 224, 'currentmeter' => 253, 'diff' => 29],
            ['id' => 245782, 'inv_period_id' => 20, 'user_id' => 1073, 'lastmeter' => 757, 'currentmeter' => 776, 'diff' => 19],
            ['id' => 245786, 'inv_period_id' => 20, 'user_id' => 1053, 'lastmeter' => 787, 'currentmeter' => 791, 'diff' => 4],
            ['id' => 245788, 'inv_period_id' => 20, 'user_id' => 921, 'lastmeter' => 108, 'currentmeter' => 108, 'diff' => 0],
            ['id' => 245790, 'inv_period_id' => 20, 'user_id' => 1112, 'lastmeter' => 78, 'currentmeter' => 79, 'diff' => 1],
            ['id' => 245792, 'inv_period_id' => 20, 'user_id' => 993, 'lastmeter' => 3461, 'currentmeter' => 3473, 'diff' => 12],
            ['id' => 245798, 'inv_period_id' => 20, 'user_id' => 938, 'lastmeter' => 1603, 'currentmeter' => 1619, 'diff' => 16],
            ['id' => 245800, 'inv_period_id' => 20, 'user_id' => 1006, 'lastmeter' => 1316, 'currentmeter' => 1324, 'diff' => 8],
            ['id' => 245802, 'inv_period_id' => 20, 'user_id' => 1080, 'lastmeter' => 529, 'currentmeter' => 532, 'diff' => 3],
            ['id' => 245804, 'inv_period_id' => 20, 'user_id' => 999, 'lastmeter' => 1680, 'currentmeter' => 1697, 'diff' => 17],
            ['id' => 245822, 'inv_period_id' => 20, 'user_id' => 1075, 'lastmeter' => 1422, 'currentmeter' => 1430, 'diff' => 8],
            ['id' => 245829, 'inv_period_id' => 20, 'user_id' => 1016, 'lastmeter' => 99, 'currentmeter' => 105, 'diff' => 6],
            ['id' => 245832, 'inv_period_id' => 20, 'user_id' => 997, 'lastmeter' => 943, 'currentmeter' => 947, 'diff' => 4],
            ['id' => 245838, 'inv_period_id' => 20, 'user_id' => 970, 'lastmeter' => 428, 'currentmeter' => 429, 'diff' => 1],
            ['id' => 245841, 'inv_period_id' => 20, 'user_id' => 918, 'lastmeter' => 107, 'currentmeter' => 109, 'diff' => 2],
            ['id' => 245846, 'inv_period_id' => 20, 'user_id' => 1025, 'lastmeter' => 205, 'currentmeter' => 218, 'diff' => 13],
            ['id' => 245862, 'inv_period_id' => 20, 'user_id' => 1048, 'lastmeter' => 1659, 'currentmeter' => 1673, 'diff' => 14],
            ['id' => 245871, 'inv_period_id' => 20, 'user_id' => 1054, 'lastmeter' => 1758, 'currentmeter' => 1758, 'diff' => 0],
            ['id' => 245873, 'inv_period_id' => 20, 'user_id' => 917, 'lastmeter' => 2125, 'currentmeter' => 2134, 'diff' => 9],
            ['id' => 245876, 'inv_period_id' => 20, 'user_id' => 1014, 'lastmeter' => 769, 'currentmeter' => 771, 'diff' => 2],
            ['id' => 245894, 'inv_period_id' => 20, 'user_id' => 899, 'lastmeter' => 58, 'currentmeter' => 63, 'diff' => 5],
            ['id' => 245911, 'inv_period_id' => 20, 'user_id' => 908, 'lastmeter' => 1394, 'currentmeter' => 1411, 'diff' => 17],
            ['id' => 245917, 'inv_period_id' => 20, 'user_id' => 1088, 'lastmeter' => 996, 'currentmeter' => 1008, 'diff' => 12],
            ['id' => 245923, 'inv_period_id' => 20, 'user_id' => 940, 'lastmeter' => 1036, 'currentmeter' => 1036, 'diff' => 0],
            ['id' => 245926, 'inv_period_id' => 20, 'user_id' => 1049, 'lastmeter' => 1879, 'currentmeter' => 1890, 'diff' => 11],
            ['id' => 245931, 'inv_period_id' => 20, 'user_id' => 1050, 'lastmeter' => 445, 'currentmeter' => 445, 'diff' => 0],
            ['id' => 245950, 'inv_period_id' => 20, 'user_id' => 930, 'lastmeter' => 1141, 'currentmeter' => 1146, 'diff' => 5],
            ['id' => 245955, 'inv_period_id' => 20, 'user_id' => 948, 'lastmeter' => 744, 'currentmeter' => 749, 'diff' => 5],
            ['id' => 245959, 'inv_period_id' => 20, 'user_id' => 924, 'lastmeter' => 752, 'currentmeter' => 757, 'diff' => 5],
            ['id' => 245964, 'inv_period_id' => 20, 'user_id' => 983, 'lastmeter' => 507, 'currentmeter' => 509, 'diff' => 2],
            ['id' => 245972, 'inv_period_id' => 20, 'user_id' => 896, 'lastmeter' => 1451, 'currentmeter' => 1458, 'diff' => 7],
            ['id' => 245977, 'inv_period_id' => 20, 'user_id' => 898, 'lastmeter' => 511, 'currentmeter' => 513, 'diff' => 2],
            ['id' => 245981, 'inv_period_id' => 20, 'user_id' => 897, 'lastmeter' => 701, 'currentmeter' => 703, 'diff' => 2],
            ['id' => 245993, 'inv_period_id' => 20, 'user_id' => 913, 'lastmeter' => 588, 'currentmeter' => 599, 'diff' => 11],
            ['id' => 246005, 'inv_period_id' => 20, 'user_id' => 1094, 'lastmeter' => 869, 'currentmeter' => 869, 'diff' => 0],
            ['id' => 246008, 'inv_period_id' => 20, 'user_id' => 1023, 'lastmeter' => 1367, 'currentmeter' => 1371, 'diff' => 4],
            ['id' => 246013, 'inv_period_id' => 20, 'user_id' => 1046, 'lastmeter' => 1305, 'currentmeter' => 1312, 'diff' => 7],
            ['id' => 246019, 'inv_period_id' => 20, 'user_id' => 1005, 'lastmeter' => 765, 'currentmeter' => 776, 'diff' => 11],
            ['id' => 246022, 'inv_period_id' => 20, 'user_id' => 1058, 'lastmeter' => 1956, 'currentmeter' => 1962, 'diff' => 6],
            ['id' => 246027, 'inv_period_id' => 20, 'user_id' => 942, 'lastmeter' => 2492, 'currentmeter' => 2503, 'diff' => 11],
            ['id' => 246031, 'inv_period_id' => 20, 'user_id' => 947, 'lastmeter' => 508, 'currentmeter' => 511, 'diff' => 3],
            ['id' => 246045, 'inv_period_id' => 20, 'user_id' => 1051, 'lastmeter' => 700, 'currentmeter' => 726, 'diff' => 26],
            ['id' => 246054, 'inv_period_id' => 20, 'user_id' => 1097, 'lastmeter' => 683, 'currentmeter' => 685, 'diff' => 2],
            ['id' => 246056, 'inv_period_id' => 20, 'user_id' => 991, 'lastmeter' => 532, 'currentmeter' => 540, 'diff' => 8],
            ['id' => 246059, 'inv_period_id' => 20, 'user_id' => 1078, 'lastmeter' => 635, 'currentmeter' => 648, 'diff' => 13],
            ['id' => 246061, 'inv_period_id' => 20, 'user_id' => 951, 'lastmeter' => 3557, 'currentmeter' => 3567, 'diff' => 10],
            ['id' => 246062, 'inv_period_id' => 20, 'user_id' => 1070, 'lastmeter' => 1003, 'currentmeter' => 1006, 'diff' => 3],
            ['id' => 246064, 'inv_period_id' => 20, 'user_id' => 1069, 'lastmeter' => 2389, 'currentmeter' => 2404, 'diff' => 15],
            ['id' => 246067, 'inv_period_id' => 20, 'user_id' => 978, 'lastmeter' => 476, 'currentmeter' => 477, 'diff' => 1],
            ['id' => 246073, 'inv_period_id' => 20, 'user_id' => 961, 'lastmeter' => 1934, 'currentmeter' => 1938, 'diff' => 4],
            ['id' => 246075, 'inv_period_id' => 20, 'user_id' => 962, 'lastmeter' => 243, 'currentmeter' => 252, 'diff' => 9],
            ['id' => 246094, 'inv_period_id' => 20, 'user_id' => 979, 'lastmeter' => 277, 'currentmeter' => 277, 'diff' => 0],
            ['id' => 246097, 'inv_period_id' => 20, 'user_id' => 1028, 'lastmeter' => 1789, 'currentmeter' => 1807, 'diff' => 18],
            ['id' => 246103, 'inv_period_id' => 20, 'user_id' => 989, 'lastmeter' => 98, 'currentmeter' => 98, 'diff' => 0],
            ['id' => 246105, 'inv_period_id' => 20, 'user_id' => 988, 'lastmeter' => 937, 'currentmeter' => 943, 'diff' => 6],
            ['id' => 246109, 'inv_period_id' => 20, 'user_id' => 1034, 'lastmeter' => 2506, 'currentmeter' => 2529, 'diff' => 23],
            ['id' => 246111, 'inv_period_id' => 20, 'user_id' => 956, 'lastmeter' => 1951, 'currentmeter' => 1957, 'diff' => 6],
            ['id' => 246115, 'inv_period_id' => 20, 'user_id' => 986, 'lastmeter' => 2381, 'currentmeter' => 2397, 'diff' => 16],
            ['id' => 246118, 'inv_period_id' => 20, 'user_id' => 901, 'lastmeter' => 83, 'currentmeter' => 84, 'diff' => 1],
            ['id' => 246280, 'inv_period_id' => 20, 'user_id' => 1091, 'lastmeter' => 461, 'currentmeter' => 462, 'diff' => 1],
            ['id' => 246288, 'inv_period_id' => 20, 'user_id' => 1045, 'lastmeter' => 1152, 'currentmeter' => 1160, 'diff' => 8],
            ['id' => 246298, 'inv_period_id' => 20, 'user_id' => 1033, 'lastmeter' => 2285, 'currentmeter' => 2309, 'diff' => 24],
            ['id' => 246303, 'inv_period_id' => 20, 'user_id' => 936, 'lastmeter' => 2175, 'currentmeter' => 2205, 'diff' => 30],
            ['id' => 246304, 'inv_period_id' => 20, 'user_id' => 998, 'lastmeter' => 1260, 'currentmeter' => 1274, 'diff' => 14],
            ['id' => 246309, 'inv_period_id' => 20, 'user_id' => 985, 'lastmeter' => 1893, 'currentmeter' => 1908, 'diff' => 15],
            ['id' => 246312, 'inv_period_id' => 20, 'user_id' => 943, 'lastmeter' => 1471, 'currentmeter' => 1475, 'diff' => 4],
            ['id' => 246315, 'inv_period_id' => 20, 'user_id' => 964, 'lastmeter' => 235, 'currentmeter' => 235, 'diff' => 0],
            ['id' => 246321, 'inv_period_id' => 20, 'user_id' => 944, 'lastmeter' => 354, 'currentmeter' => 357, 'diff' => 3],
            ['id' => 246326, 'inv_period_id' => 20, 'user_id' => 923, 'lastmeter' => 792, 'currentmeter' => 802, 'diff' => 10],
            ['id' => 246328, 'inv_period_id' => 20, 'user_id' => 919, 'lastmeter' => 2123, 'currentmeter' => 2142, 'diff' => 19],
            ['id' => 246332, 'inv_period_id' => 20, 'user_id' => 1071, 'lastmeter' => 3122, 'currentmeter' => 3148, 'diff' => 26],
            ['id' => 246333, 'inv_period_id' => 20, 'user_id' => 949, 'lastmeter' => 1153, 'currentmeter' => 1153, 'diff' => 0],
            ['id' => 246336, 'inv_period_id' => 20, 'user_id' => 1037, 'lastmeter' => 424, 'currentmeter' => 451, 'diff' => 27],
            ['id' => 246339, 'inv_period_id' => 20, 'user_id' => 926, 'lastmeter' => 728, 'currentmeter' => 742, 'diff' => 14],
            ['id' => 246340, 'inv_period_id' => 20, 'user_id' => 1001, 'lastmeter' => 1316, 'currentmeter' => 1325, 'diff' => 9],
            ['id' => 246342, 'inv_period_id' => 20, 'user_id' => 1020, 'lastmeter' => 1511, 'currentmeter' => 1518, 'diff' => 7],
            ['id' => 246345, 'inv_period_id' => 20, 'user_id' => 1002, 'lastmeter' => 155, 'currentmeter' => 155, 'diff' => 0],
            ['id' => 246348, 'inv_period_id' => 20, 'user_id' => 992, 'lastmeter' => 577, 'currentmeter' => 582, 'diff' => 5],
            ['id' => 246350, 'inv_period_id' => 20, 'user_id' => 1021, 'lastmeter' => 85, 'currentmeter' => 88, 'diff' => 3],
            ['id' => 246352, 'inv_period_id' => 20, 'user_id' => 958, 'lastmeter' => 89, 'currentmeter' => 89, 'diff' => 0],
            ['id' => 246552, 'inv_period_id' => 20, 'user_id' => 957, 'lastmeter' => 1289, 'currentmeter' => 1303, 'diff' => 14],
            ['id' => 246565, 'inv_period_id' => 20, 'user_id' => 933, 'lastmeter' => 1642, 'currentmeter' => 1657, 'diff' => 15],
            ['id' => 246567, 'inv_period_id' => 20, 'user_id' => 934, 'lastmeter' => 33, 'currentmeter' => 33, 'diff' => 0],
            ['id' => 246573, 'inv_period_id' => 20, 'user_id' => 1004, 'lastmeter' => 2404, 'currentmeter' => 2419, 'diff' => 15],
            ['id' => 246580, 'inv_period_id' => 20, 'user_id' => 1027, 'lastmeter' => 1625, 'currentmeter' => 1625, 'diff' => 0],
            ['id' => 246584, 'inv_period_id' => 20, 'user_id' => 915, 'lastmeter' => 731, 'currentmeter' => 739, 'diff' => 8],
            ['id' => 246585, 'inv_period_id' => 20, 'user_id' => 3082, 'lastmeter' => 1, 'currentmeter' => 3, 'diff' => 2],
            ['id' => 246593, 'inv_period_id' => 20, 'user_id' => 892, 'lastmeter' => 2883, 'currentmeter' => 2896, 'diff' => 13],
            ['id' => 246604, 'inv_period_id' => 20, 'user_id' => 987, 'lastmeter' => 872, 'currentmeter' => 877, 'diff' => 5],
            ['id' => 246609, 'inv_period_id' => 20, 'user_id' => 893, 'lastmeter' => 142, 'currentmeter' => 147, 'diff' => 5],
            ['id' => 246616, 'inv_period_id' => 20, 'user_id' => 976, 'lastmeter' => 1352, 'currentmeter' => 1357, 'diff' => 5],
            ['id' => 246620, 'inv_period_id' => 20, 'user_id' => 990, 'lastmeter' => 587, 'currentmeter' => 596, 'diff' => 9],
            ['id' => 246628, 'inv_period_id' => 20, 'user_id' => 931, 'lastmeter' => 439, 'currentmeter' => 443, 'diff' => 4],
            ['id' => 246644, 'inv_period_id' => 20, 'user_id' => 1038, 'lastmeter' => 1854, 'currentmeter' => 1858, 'diff' => 4],
            ['id' => 246653, 'inv_period_id' => 20, 'user_id' => 2920, 'lastmeter' => 118, 'currentmeter' => 118, 'diff' => 0],
            ['id' => 246667, 'inv_period_id' => 20, 'user_id' => 1018, 'lastmeter' => 3544, 'currentmeter' => 3572, 'diff' => 28],
            ['id' => 246691, 'inv_period_id' => 20, 'user_id' => 980, 'lastmeter' => 1439, 'currentmeter' => 1445, 'diff' => 6],
            ['id' => 246721, 'inv_period_id' => 20, 'user_id' => 1055, 'lastmeter' => 1455, 'currentmeter' => 1466, 'diff' => 11],
            ['id' => 246733, 'inv_period_id' => 20, 'user_id' => 1047, 'lastmeter' => 264, 'currentmeter' => 267, 'diff' => 3],
            ['id' => 246743, 'inv_period_id' => 20, 'user_id' => 2970, 'lastmeter' => 16, 'currentmeter' => 19, 'diff' => 3],
            ['id' => 246746, 'inv_period_id' => 20, 'user_id' => 1007, 'lastmeter' => 133, 'currentmeter' => 138, 'diff' => 5],
            ['id' => 246827, 'inv_period_id' => 20, 'user_id' => 1144, 'lastmeter' => 2439, 'currentmeter' => 2445, 'diff' => 6],
            ['id' => 246834, 'inv_period_id' => 20, 'user_id' => 1868, 'lastmeter' => 181, 'currentmeter' => 181, 'diff' => 0],
        ];
        $varGroup = collect($var)->groupBy('user_id')->values();
        foreach ($varGroup as $vars) {
            // return $vars;
            $mustpaid = 0;
            foreach ($vars as $v) {
                $d = $v['diff'] == 0 ? 10 : $v['diff'] * 8;
                $mustpaid += $d;
            }
            $receipt = new Accounting();
            $receipt->total = $mustpaid;
            $receipt->net = $mustpaid;
            $receipt->cashier = 2999;
            $receipt->created_at = date('Y-m-d H:i:s');
            $receipt->updated_at = date('Y-m-d H:i:s');
            $receipt->save();
            foreach ($vars as $v) {
                $update = Invoice::where('id', $v['id'])->update([
                    'receipt_id' => $receipt->id,
                    'status' => 'paid',
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
        }
        echo 'ok';
    }

    public function testDuplicateInvoice()
    {
        $apiPaymentCtrl = new ApiPaymentController();

        $invoicesPaidForPrint = $apiPaymentCtrl->history(23707, 'receipt');
        $newId = 999;
        $type = 'paid_receipt';
        $cashier = DB::table('user_profile')
            ->where('user_id', '=', Auth::id())
            ->select('name')
            ->get();
        $cashiername = $cashier[0]->name;

        return view('payment.receipt_print', compact('invoicesPaidForPrint', 'newId', 'type', 'cashiername'));

    }

}
