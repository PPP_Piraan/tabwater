<?php

namespace App\Http\Controllers;

use App\BudgetYear;
use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoicePeriodController as apiInvoicePeriodCtrl;
use App\Http\Controllers\Api\ReportsController as apiReportCtrl;
use App\Http\Controllers\Api\OwepaperController ;
use App\Invoice;
use App\InvoicePeriod;
use App\Subzone;
use App\UserMeterInfos;
use App\UserProfile;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{

    public function index()
    {
        $inv_periods = InvoicePeriod::where('status', 'active')->orderBy('id', 'desc')->get();
        $zones = Zone::all();
        $invs = [];
        return view('reports.index', compact('inv_periods', 'zones', 'invs'));
    }

    public function users()
    {
        $zones = Zone::all();

        return view('reports.users', compact('zones'));
    }

    public function usedtabwater_infos()
    {
        return view('reports.usedtabwater_infos');
    }

    public function owe_old(REQUEST $request)
    {
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

        $zones = Zone::all();
        $start = $request->get('invperiodstart');
        $end = $request->get('invperiodend');
        return view('reports.owe', compact('zones', 'start', 'end', 'invoice_period'));
    }

    public function owe(REQUEST $request)
    {
        if(collect($request)->isEmpty()){
            $request->merge([
                'zone_id' => 'all',
                'subzone_id' => "all",
                'inv_peroid' => 'all',
                'invperiodstart' => 'all',
                'invperiodend' => 'all'
            ]);
        }
        $apiOwepapertCtrl = new OwepaperController();
        $owe_res = collect(json_decode($apiOwepapertCtrl->owe($request)->content(), true))->values();
        $sum_reserve_paid = collect($owe_res[0])->sum('sum_reserve_paid');
        $sum_paid = collect($owe_res[0])->sum('sum_paid');
        $total = $sum_reserve_paid + $sum_paid;
        $fnCtrl = new FunctionsController();
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();
        $owed_inv_periods =  collect([]);
        foreach($owe_res[1] as $owed_inv_period){
            $inv_period = InvoicePeriod::where('id', $owed_inv_period)->get('inv_period_name');
            $owed_inv_periods->push([
                'inv_period_id' => $owed_inv_period,
                'inv_period_name' => $inv_period[0]->inv_period_name
            ]);
        }
        $zones = Zone::all();
        $search_vals = [
            'zone_id' => $request->get('zone_id'),
            'subzone_id' => $request->get('subzone_id'),
            'inv_peroid' => $request->get('inv_peroid')
        ];
        return view('reports.owe', compact('owe_res', 'search_vals', 'zones', 'sum_reserve_paid', 'sum_paid', 'total', 'invoice_period', 'owed_inv_periods'));
    }


    private function microtime_float()
    {
        list($usec, $sec) = \explode(" ", \microtime());
        return ((float) $usec + (float) $sec);
    }
    public function payment(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $fnCtrl = new FunctionsController();
        $apiReportCtrl = new apiReportCtrl();
        $budgetyear = $request->get('budgetyear_id');

        $budgetyear_selected = '';
        if(collect($budgetyear)->isEmpty()){
            $budgetyear_selected = BudgetYear::where('status', 'active')
                ->with([
                    'invoicePeriod' => function($query){
                        return $query->select( 'id', 'budgetyear_id', 'inv_period_name')
                                    ->where('deleted', '0');
                    }
                ])->get(['id', 'budgetyear']);
        }else{
            $budgetyear_selected = BudgetYear::where('id', $budgetyear)
            ->with([
                'invoicePeriod' => function($query){
                    return $query->select( 'id', 'budgetyear_id', 'inv_period_name')
                                ->where('deleted', '0');
                }
            ])->get(['id', 'budgetyear']);
        }

        if (collect($request)->isEmpty()) {
            $a = [
                'inv_period_id' => 'all',
                'zone_id' => 'all',
                'subzone_id' => 'all',
                'fromdate' => date('Y-m-d'),
                'todate' => date('Y-m-d'),
                'cashier_id' => 'all',
                // 'budgetyear_id' => $budgetyear
            ];
            $request->merge($a);
            $fromdateTh = $fnCtrl->engDateToThaiDateFormat(date('Y-m-d'));
            $todateTh =  $fnCtrl->engDateToThaiDateFormat(date('Y-m-d'));
        } else {
            //เปลี่ยนวันไทย ไปเป็นวันอังกฤษ
            $fromdate = $fnCtrl->thaiDateToEngDateFormat($request->get('fromdate'));
            $todate = $fnCtrl->thaiDateToEngDateFormat($request->get('todate'));
            $request->merge([
                'fromdate' => $fromdate,
                'todate' => $todate,
            ]);
            //เอาวันที่ไทยไปแสดงหน้า view
            $fromdateTh = $request->get('fromdate');
            $todateTh = $request->get('todate');
        }

        //หาผู้จ่ายแล้ว แบบ subzone และ รอบบิล
        $paidInfosArr = $this->payment_find_paymented($request);
        $paidInfos = collect($paidInfosArr)->groupBy('user_id')->values()->toArray();

        //หาเจ้าหน้าที่การเงินและผู้ดูแลระบบ
        if (collect($paidInfos)->isNotEmpty()) {
            $cashier_name = $request->get('cashier_id') == 'all' ? 'ทั้งหมด' : $paidInfos[0][0]['accounting']['cashier_info']['name'];
        } else {
            $cashier_name = '';
        }

        $subzone_id = $request->get('subzone_id');
        $zone_id = $request->get('zone_id');
        $zones = Zone::all();
        $subzones = $zone_id != 'all' && $subzone_id != 'all' ? Subzone::all() : 'all';
        $inv_periods = InvoicePeriod::orderBy('id', 'desc')->get(['id', 'inv_period_name']);
        $receiptions = DB::table('users')
            ->join('user_profile', 'user_profile.user_id', '=', 'users.id')
            ->whereIn('users.user_cat_id', [1,2])
            ->where('users.status', '=', 'active')
            ->select('user_profile.name', 'user_profile.user_id')

            ->get();
        $budgetyear_list = BudgetYear::get(['id', 'budgetyear']);
        return view('reports.payment', compact('zones', 'subzones', 'paidInfos',
            'budgetyear_list',
            'subzone_id', 'zone_id',
            'budgetyear_selected',
            'receiptions',
            'cashier_name',
            'fromdateTh', 'todateTh',
        ));
    }

    public function ledger(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $fnCtrl = new FunctionsController();
        $apiReportCtrl = new apiReportCtrl();
        $budgetyear = $request->get('budgetyear_id');

        $budgetyear_selected = '';
        if(collect($budgetyear)->isEmpty()){
            $budgetyear_selected = BudgetYear::where('status', 'active')
                ->with([
                    'invoicePeriod' => function($query){
                        return $query->select( 'id', 'budgetyear_id', 'inv_period_name')
                                    ->where('deleted', '0');
                    }
                ])->get(['id', 'budgetyear']);
        }else{
            $budgetyear_selected = BudgetYear::where('id', $budgetyear)
            ->with([
                'invoicePeriod' => function($query){
                    return $query->select( 'id', 'budgetyear_id', 'inv_period_name')
                                ->where('deleted', '0');
                }
            ])->get(['id', 'budgetyear']);
        }

        if (collect($request)->isEmpty()) {
             $inv_period = InvoicePeriod::where('status', 'active')->get('id');
            $a = [
                'inv_period_id' => $inv_period[0]->id,
                'status' => 'all',
                // 'subzone_id' => 'all',
                // 'fromdate' => date('Y-m-d'),
                // 'todate' => date('Y-m-d'),
                // 'cashier_id' => 'all',
                // 'budgetyear_id' => $budgetyear
            ];
            $request->merge($a);
            // $fromdateTh = $fnCtrl->engDateToThaiDateFormat(date('Y-m-d'));
            // $todateTh =  $fnCtrl->engDateToThaiDateFormat(date('Y-m-d'));
        } else {
            //เปลี่ยนวันไทย ไปเป็นวันอังกฤษ
            // $fromdate = $fnCtrl->thaiDateToEngDateFormat($request->get('fromdate'));
            // $todate = $fnCtrl->thaiDateToEngDateFormat($request->get('todate'));
            // $request->merge([
            //     'fromdate' => $fromdate,
            //     'todate' => $todate,
            // ]);
            //เอาวันที่ไทยไปแสดงหน้า view
            // $fromdateTh = $request->get('fromdate');
            // $todateTh = $request->get('todate');
        }

        $inv_periods = InvoicePeriod::orderBy('id', 'desc')->get(['id', 'inv_period_name']);

         $ledgers = $this->_ledger($request);


         $current_inv_period = InvoicePeriod::where('id', $request->get('inv_period_id'))->get(['id','inv_period_name']);


        $budgetyear_list = BudgetYear::get(['id', 'budgetyear']);
        return view('reports.ledger', compact(
            // 'zones', 'subzones',
            'budgetyear_list',
            // 'subzone_id', 'zone_id',
            'budgetyear_selected',
            // 'receiptions',
            // 'fromdateTh', 'todateTh',
            'ledgers',
            'current_inv_period'
        ));
    }


    private function _ledger(Request $request)
    {
        $fnCtrl = new FunctionsController();
        //หาจาก  usermeterinfos[undertake_zone_id  undertake_subzone_id] -> invoice table
        $inv_period_id = $request->get('inv_period_id');
        $status  = $request->get('status');

        $budgetyear = $request->get('budgetyear');
        $users = UserMeterInfos::with([
            'invoice_owe_total' => function($query) use($inv_period_id){
                return $query->where('status', 'owe')
                               // ->where('inv_period_id', '<', $inv_period_id)
                                ->select('user_id',db::raw('(currentmeter - lastmeter) as prev_owe_amount' ))
                                ->get();
            },
            'invoice' => function($query) use($inv_period_id, $status){
                if($status != 'all'){
                    return $query->where('inv_period_id', $inv_period_id)
                    ->where('deleted', 0)
                    ->where('status', $status)
                    ->select('id','user_id','status', 'receipt_id','currentmeter', 'lastmeter', db::raw('(currentmeter - lastmeter) as diff') );
                }
                return $query->where('inv_period_id', $inv_period_id)
                            ->where('deleted', 0)
                            ->select('id','user_id','status', 'receipt_id','currentmeter', 'lastmeter', db::raw('(currentmeter - lastmeter) as diff') );
            },
        ])->where('deleted', 0)
        ->limit(500)
        // ->where('user_id', 11)
        ->get(['id','user_id']);

        if($status == "all"){
            return $users;
        }
        return collect($users)->filter(function($v) use ($status){

            if(collect($v->invoice)->isNotEmpty()){
              return  $v->invoice[0]->status == $status;
            }

         });


    }

    private function payment_find_paymented(Request $request)
    {
        $fnCtrl = new FunctionsController();
        //หาจาก  usermeterinfos[undertake_zone_id  undertake_subzone_id] -> invoice table
        $inv_period_id = $request->get('inv_period_id');
        $current_inv_period = InvoicePeriod::where('status', 'active')->get('id');
        $zone_id = $request->get('zone_id');
        $subzone_id = $request->get('subzone_id');

        $fromdate = $request->get('fromdate');
        $todate =  $request->get('todate');
        $cashier_id = $request->get('cashier_id');
        $budgetyear = $request->get('budgetyear');
        $paids =Invoice::with(
            [
                'user_profile' => function($query){
                    return $query->select('user_id', 'name', 'address');
               },
               'usermeterinfos'=> function($query){
                    return $query->select('user_id', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id', 'deleted', 'status');
               },
               'usermeterinfos.zone' => function($query){
                    return $query->select('id', 'zone_name');
                },
                'usermeterinfos.subzone'=> function($query){
                    return $query->select('id', 'subzone_name');
                },
                'invoice_period' => function($query){
                    return $query->select('id', 'inv_period_name', 'budgetyear_id');
                },

                'accounting'=> function($query){
                    return $query->select('id', 'cashier', 'total', 'updated_at');
                },
                'accounting.cashier_info'=> function($query) use ($cashier_id){
                    if($cashier_id != 'all'){
                        return $query->select('user_id', 'name', 'updated_at')
                        ->where('user_id', '=', $cashier_id);
                    }else{
                        return $query->select('user_id', 'name', 'updated_at');
                    }
                }

            ]
            );

        if($request->get('inv_period_id') != 'all'){
            $paids = $paids->where('inv_period_id', $request->get('inv_period_id'));
        }

        $paids = $paids->where('status', '=', 'paid')
             ->where('updated_at', 'Like', $fromdate.'%')//
             ->where('receipt_id', '<>', 0)
            ->get([
               'id', 'user_id', 'currentmeter', 'lastmeter', 'recorder_id', 'updated_at',
            DB::raw('(currentmeter - lastmeter) as water_used'), 'status',
            DB::raw('(currentmeter - lastmeter)*8 as mustpaid'), 'receipt_id', 'inv_period_id'
            ]);

            $paids_filter_active_users = collect($paids)->filter(function($v){
                return $v->usermeterinfos->status != 'permanent deleted';
                // return $v->water_used == 0;
            });
            foreach($paids_filter_active_users as $a){
                // dd($a->accounting['updated_at']);
                Invoice::where('id', $a->id)->update([
                    'updated_at' => $a->accounting['updated_at']
                ]);

            }
            if ($cashier_id != 'all') {
                //filter เอาผู้รับเงินที่ต้องการ
                return collect($paids_filter_active_users)->filter(function($v) use ($cashier_id){
                    if(collect($v->accounting['cashier_info'])->isNotEmpty()){
                        // dd($v->accounting);
                        return $v->accounting['cashier'] == $cashier_id;
                    }
                });
            }


            return collect($paids_filter_active_users)->filter(function($v) use($fromdate, $todate){
                if(collect($v->accounting)->isNotEmpty()){
                    return  $v;//$v->accounting->updated_at >= $fromdate." 00:00:00" && $v->accounting->updated_at <= $todate." 23:59:59" ;
                }
            });
    }

    public function search(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $zone_id = $request->get('zone_id');
        $query = Invoice::where('created_at', '>=', '2021-03-23 00:00:00')
            ->where('created_at', '<=', '2021-03-27 23:59:00')
            ->with(['usermeterinfos' => function ($query) use ($zone_id) {
                return $zone_id != 'all' ? $query->where('undertake_subzone_id', $zone_id) : true;
            }, 'user_profile']);
        if ($request->get('invoicetype') != 'all') {
            $query = $query->where('status', $request->get('invoicetype'));
        }
        $invs = $query->get();

        $invsTemp = $invs->filter(function ($val) {
            return collect($val->usermeterinfos)->isNotEmpty();
        });
        $invs = collect($invsTemp)->groupBy('user_id');
        $inv_periods = InvoicePeriod::orderBy('id', 'desc')->get();
        $zones = Zone::all();
        return view('reports.index', compact('inv_periods', 'zones', 'invs'));

    }

    public function daily_receipt(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');

        $apiReportCtrl = new apiReportCtrl();
        if ($request->get('date') == "") {
            $d = date('d') + 1;
            $m = date('m');
            $yTh = date('Y') + 543;

        } else {
            $date = \explode("/", $request->get('date'));
            $d = $date[0];
            $m = $date[1];
            $yTh = $date[2];
        }
        $date = $d . "/" . $m . "/" . $yTh;
        $receipts = json_decode($apiReportCtrl->daily_receipt($d, $m, $yTh)->content(), true);
        return view('reports.daily_receipt', compact('receipts', 'date'));
    }

    public function water_used(Request $request)
    {
        // ค่าตั้งต้น
        $popularity = \Lava::DataTable();
        $zone_and_subzone_selected_text = '';
        $budgetyears = BudgetYear::where('status', '<>', 'deleted')->get(['id', 'budgetyear']);
        $zones = Zone::where('deleted', 0)->get(['id', 'zone_name']);
        $zone_id = $request->get('zone_id');

        // เริ่มต้นหาปีงบประมาณปัจจุบัน และ รอบบิล
        if (collect($request)->isEmpty()) {
            $selected_budgetYear = BudgetYear::where('status', 'active')
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' )->where('deleted', '=', 0);
                }])
                ->get(['id'])
                ->first();
            $a = [
                'zone_id' => 'all',
                'subzone_id' => 'all',
            ];
            $request->merge($a);
        } else {
            $selected_budgetYear = BudgetYear::where('id', $request->get('budgetyear_id'))
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' );
                }])
                ->get(['id'])->first();

        }


        //หารอบบิลของปีงบประมาณที่ถูกเลือก
        // $invP_of_selected_budgetyear = InvoicePeriod::where('budgetyear_id', $selected_budgetYear->id)
        //     ->where('deleted', '<>', 1)
        //     ->get(['id']);
         $invPeriod_selected_buggetYear_array = collect($selected_budgetYear->invoicePeriod)->pluck('id');

        $startTime = microtime(true);
        //1. find all users with invoice infos


        $waterUsedSql = DB::table('user_meter_infos as umf')
            ->join('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
            ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
            ->join('subzone as sz', 'sz.id', '=', 'umf.undertake_subzone_id')
            ->join('zone as z', 'z.id', '=', 'umf.undertake_zone_id')
            // ->where('umf.status', '<>', 'inactive')
            ->whereIn('iv.inv_period_id', $invPeriod_selected_buggetYear_array)
            ->select(
                DB::raw('(iv.currentmeter - iv.lastmeter) as waterUsed'),
                'iv.inv_period_id',
                'umf.undertake_subzone_id',
                'z.id as zone_id'
            );

        if ($request->get('zone_id') != 'all') {
            $zone = Zone::where('id', $request->get('zone_id'))->get('zone_name');
            $zone_and_subzone_selected_text .= ' ' . $zone[0]->zone_name;
            if ($request->get('subzone_id') != 'all') {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_subzone_id', '=', $request->get('zone_id'));
                $subzone = subZone::where('id', $request->get('subzone_id'))->get('subzone_name');
                $zone_and_subzone_selected_text .= ' เส้นทางจัดเก็บ ' . $subzone[0]->subzone_name;
            } else {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_zone_id', '=', $request->get('zone_id'));
            }
        }
        $waterUsed2 = $waterUsedSql
            ->orderBy('zone_id', 'asc')
            ->orderBy('umf.undertake_subzone_id', 'asc')
            ->get();

            $waterUsed = collect($waterUsed2)->filter(function($v){
                // dd($v->waterUsed);
                return $v->waterUsed >= 0;
            });
    //    return     "Time:  " . number_format((microtime(true) - $startTime), 4) . " Seconds\n";

        //ถ้า $waterUsed เป็น [] ให้ return ไปหน้า view
        if (collect($waterUsed)->count() == 0) {
            return view('reports.water_used', [
                'datas' => [],
                'budgetyears' => $budgetyears,
                'selected_budgetYear' => $selected_budgetYear,
                'zones' => $zones,
            ]);
        }

        $waterUsedGroupedBySubzone = collect($waterUsed)->groupBy('undertake_subzone_id')->values();
        $datas = collect([]);
        $popularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('จำนวนน้ำที่ใช้ (หน่วย)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');
        foreach ($waterUsedGroupedBySubzone as $key => $subzone) {
            //ทำการหาผลรวมการใช้น้ำของแต่ละรอบบิลของแต่ละ subzone
            //แล้วทำการเก็บไว้ใน array datas
            $invP_in_subzone_info_arr = collect([]);
            foreach ($invPeriod_selected_buggetYear_array as $invP_id) {
                //วนลูป รอบบิลของปีงบประมาณที่เลือก เพื่อทำการ filter หา ผลรวม waterUsed
                $filtered = collect($subzone)->filter(function ($item) use ($invP_id) {
                    return $item->inv_period_id == $invP_id;
                })->sum('waterUsed');
                $find_invP_name = InvoicePeriod::where('id', $invP_id)->get(['inv_period_name']);
                $invP_in_subzone_info_arr->push([
                    'inv_period_id' => $invP_id,
                    'inv_period_name' => $find_invP_name[0]->inv_period_name,
                    'water_used_sum' => $filtered,
                ]);

                if ($request->get('subzone_id') != 'all') {
                    $popularity->addRow([$find_invP_name[0]->inv_period_name, $filtered]);
                }
            }
            $find_subzone_name = Subzone::where('id', $subzone[0]->undertake_subzone_id)->get('subzone_name');
            $datas->push([
                'subzone_id' => $subzone[0]->undertake_subzone_id,
                'members' => DB::table('user_meter_infos')->where('undertake_subzone_id', $subzone[0]->undertake_subzone_id)->count(),
                'zone_id' => $subzone[0]->zone_id,
                'subzone_name' => $find_subzone_name[0]->subzone_name,
                'values' => $invP_in_subzone_info_arr,
                'total' => collect($invP_in_subzone_info_arr)->sum('water_used_sum'),
            ]);

            if ($request->get('subzone_id') == 'all') {
                $_water_used_sum = collect($invP_in_subzone_info_arr)->sum('water_used_sum');
                $popularity->addRow([$find_subzone_name[0]->subzone_name, $_water_used_sum, $_water_used_sum, 'fill-opacity: 0.6']);
            }

        }

        $Stocks = \Lava::ColumnChart('Stocks', $popularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],
            'height' => 400,
            // 'width'  => 110
        ]);

        return view('reports.water_used', compact('Stocks', 'datas', 'zones', 'budgetyears', 'zone_id', 'zone_and_subzone_selected_text', 'selected_budgetYear',));
    }

    public function water_used_backup(Request $request)
    {
        // ค่าตั้งต้น
        $popularity = \Lava::DataTable();
        $zone_and_subzone_selected_text = '';
        $budgetyears = BudgetYear::where('status', '<>', 'deleted')->get(['id', 'budgetyear']);
        $zones = Zone::where('deleted', 0)->get(['id', 'zone_name']);
        $zone_id = $request->get('zone_id');

        // เริ่มต้นหาปีงบประมาณปัจจุบัน และ รอบบิล
        if (collect($request)->isEmpty()) {
            $selected_budgetYear = BudgetYear::where('status', 'active')
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' )->where('deleted', '=', 0);
                }])
                ->get(['id'])
                ->first();
            $a = [
                'zone_id' => 'all',
                'subzone_id' => 'all',
            ];
            $request->merge($a);
        } else {
            $selected_budgetYear = BudgetYear::where('id', $request->get('budgetyear_id'))
                ->with(['invoicePeriod'=>function($query){
                    $query->select('budgetyear_id', 'id' );
                }])
                ->get(['id'])->first();
        }
        //หารอบบิลของปีงบประมาณที่ถูกเลือก
        // $invP_of_selected_budgetyear = InvoicePeriod::where('budgetyear_id', $selected_budgetYear->id)
        //     ->where('deleted', '<>', 1)
        //     ->get(['id']);
         $invPeriod_selected_buggetYear_array = collect($selected_budgetYear->invoicePeriod)->pluck('id');
        $startTime = microtime(true);

        $waterUsedSql = DB::table('user_meter_infos as umf')
            ->join('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
            ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
            ->join('subzone as sz', 'sz.id', '=', 'umf.undertake_subzone_id')
            ->join('zone as z', 'z.id', '=', 'umf.undertake_zone_id')
            ->where('umf.status', '<>', 'inactive')
            ->whereIn('iv.inv_period_id', $invPeriod_selected_buggetYear_array)
            ->select(
                DB::raw('(iv.currentmeter - iv.lastmeter) as waterUsed'),
                'iv.inv_period_id',
                'umf.undertake_subzone_id',
                'z.id as zone_id'
            );

        if ($request->get('zone_id') != 'all') {
            $zone = Zone::where('id', $request->get('zone_id'))->get('zone_name');
            $zone_and_subzone_selected_text .= ' ' . $zone[0]->zone_name;
            if ($request->get('subzone_id') != 'all') {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_subzone_id', '=', $request->get('zone_id'));
                $subzone = subZone::where('id', $request->get('subzone_id'))->get('subzone_name');
                $zone_and_subzone_selected_text .= ' เส้นทางจัดเก็บ ' . $subzone[0]->subzone_name;
            } else {
                $waterUsedSql = $waterUsedSql->where('umf.undertake_zone_id', '=', $request->get('zone_id'));
            }
        }
        $waterUsed = $waterUsedSql
            ->orderBy('zone_id', 'asc')
            ->orderBy('umf.undertake_subzone_id', 'asc')
            ->get();
            "Time:  " . number_format((microtime(true) - $startTime), 4) . " Seconds\n";

        //ถ้า $waterUsed เป็น [] ให้ return ไปหน้า view
        if (collect($waterUsed)->count() == 0) {
            return view('reports.water_used', [
                'datas' => [],
                'budgetyears' => $budgetyears,
                'selected_budgetYear' => $selected_budgetYear,
                'zones' => $zones,
            ]);
        }

        $waterUsedGroupedBySubzone = collect($waterUsed)->groupBy('undertake_subzone_id')->values();
        $datas = collect([]);
        $popularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('จำนวนน้ำที่ใช้ (หน่วย)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');
        foreach ($waterUsedGroupedBySubzone as $key => $subzone) {
            //ทำการหาผลรวมการใช้น้ำของแต่ละรอบบิลของแต่ละ subzone
            //แล้วทำการเก็บไว้ใน array datas
            $invP_in_subzone_info_arr = collect([]);
            foreach ($invPeriod_selected_buggetYear_array as $invP_id) {
                //วนลูป รอบบิลของปีงบประมาณที่เลือก เพื่อทำการ filter หา ผลรวม waterUsed
                $filtered = collect($subzone)->filter(function ($item) use ($invP_id) {
                    return $item->inv_period_id == $invP_id;
                })->sum('waterUsed');
                $find_invP_name = InvoicePeriod::where('id', $invP_id)->get(['inv_period_name']);
                $invP_in_subzone_info_arr->push([
                    'inv_period_id' => $invP_id,
                    'inv_period_name' => $find_invP_name[0]->inv_period_name,
                    'water_used_sum' => $filtered,
                ]);

                if ($request->get('subzone_id') != 'all') {
                    $popularity->addRow([$find_invP_name[0]->inv_period_name, $filtered]);
                }
            }
            $find_subzone_name = Subzone::where('id', $subzone[0]->undertake_subzone_id)->get('subzone_name');
            $datas->push([
                'subzone_id' => $subzone[0]->undertake_subzone_id,
                'zone_id' => $subzone[0]->zone_id,
                'subzone_name' => $find_subzone_name[0]->subzone_name,
                'values' => $invP_in_subzone_info_arr,
                'total' => collect($invP_in_subzone_info_arr)->sum('water_used_sum'),
            ]);

            if ($request->get('subzone_id') == 'all') {
                $_water_used_sum = collect($invP_in_subzone_info_arr)->sum('water_used_sum');
                $popularity->addRow([$find_subzone_name[0]->subzone_name, $_water_used_sum, $_water_used_sum, 'fill-opacity: 0.6']);
            }

        }

        $Stocks = \Lava::ColumnChart('Stocks', $popularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],
            'height' => 400,
            // 'width'  => 110
        ]);

        return view('reports.water_used', compact('Stocks', 'datas', 'zones', 'budgetyears', 'zone_id', 'zone_and_subzone_selected_text', 'selected_budgetYear'));
    }

    public function meter_record_history(REQUEST $request)
    {

        $budgetyear = 'now';
        $zone_id = 'all';
        if (collect($request)->isNotEmpty()) {
            $budgetyear = $request->get('budgetyear');
            $zone_id = $request->get('zone_id');
        }
        date_default_timezone_set('Asia/Bangkok');
        $zones = Zone::all();
        $budgetyears = BudgetYear::all();
        $current_budget_year = collect($budgetyears)->filter(function ($v) {
            return $v->status == 'active';
        })->values()->first();

        if ($budgetyear == 'now') {
            $budgetyear = $current_budget_year->id;
        }

        //หารอบบิลที่เปิดใช้งานของ ปีงบประมาณปัจจุบัน
        $active_inv_periods = InvoicePeriod::where('deleted', '<>', $budgetyear)
            ->where('budgetyear_id', $budgetyear)
            ->orderBy('id', 'asc')->get('id');

        $inv_periods_list_array = collect($active_inv_periods)->pluck('id');
        $inv_periodsCount = collect($active_inv_periods)->count();

        //หาข้อมูลผู้ใช้น้ำที่ status = ['active', 'cutmeter', 'deleted', 'changemeter']
        //พร้อมข้อมูล invoice, user_profile,invoice_period, subzone, zone ของแต่ละ user
        $membersEloquentSql = UserMeterInfos::
            with([
            'invoice' => function ($query) use ($inv_periods_list_array) {
                $query->select('inv_period_id', 'currentmeter', 'lastmeter', 'user_id', 'status', 'created_at','comment',
                    DB::Raw('currentmeter - lastmeter as water_used'))
                    ->whereIn('inv_period_id',  $inv_periods_list_array);
            },
            'user_profile' => function ($query) {
                $query->select('name', 'zone_id', 'address', 'user_id');
            },
            'invoice.invoice_period' => function ($query) {
                $query->select('inv_period_name', 'id');
            },
            'subzone' => function ($query) {
                $query->select('subzone_name', 'id');
            },
            'zone' => function ($query) {
                $query->select('zone_name', 'id');
            },
        ])
        ->whereIn('status', ['active', 'cutmeter', 'deleted', 'changemeter'])
        ->where('deleted', 0);
        //ถ้ามีการเลือก zone
        if ($zone_id != 'all') {
            $membersEloquentSql = $membersEloquentSql->where('undertake_zone_id', $zone_id);
        }

        //เลือก แcolumn จาก user_meter_infos table
         $infos = $membersEloquentSql
            // ->limit(50)
            ->get([
                'meternumber', 'undertake_zone_id', 'undertake_subzone_id', 'user_id', 'owe_count',
            ]);
        $apiInvoicePeriodCtrl = new apiInvoicePeriodCtrl();
         $inv_period_lists = json_decode($apiInvoicePeriodCtrl->inv_period_lists($budgetyear)->content(), true);
        return view('reports.meter_record_history', compact('zones', 'infos', 'inv_period_lists', 'budgetyears', 'budgetyear','zone_id'));
    }

    public function summary(Request $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $apiReportCtrl = new apiReportCtrl();
        $apiFuncCtrl = new FunctionsController();
        $apiInvoicePeriodCtrl = new apiInvoicePeriodCtrl();

        //ถ้า request  = [] ให้หาปีงบประมาณปัจจุบัน
        if (collect($request)->isEmpty()) {
            $selectedBudgetYear = BudgetYear::where('status', 'active')->get();
        } else {
            $selectedBudgetYear = BudgetYear::where('id', $request->get('budgetyear'))->get();
        }

        $invPeriodSelectedBuggetYear = InvoicePeriod::where('budgetyear_id', $selectedBudgetYear[0]->id)
            ->where('deleted', 0)
            ->get('id');
        $invPeriodSelectedBuggetYearArray = collect($invPeriodSelectedBuggetYear)->pluck('id')->toArray();


         $datasSql = DB::table('invoice as iv')
         ->join('user_meter_infos as umf', 'umf.user_id', '=', 'iv.user_id')
        ->select(
            'iv.user_id', 'iv.inv_period_id', 'iv.status', 'iv.deleted', 'iv.receipt_id',
            Db::Raw('iv.currentmeter - iv.lastmeter as diff'),
            'umf.undertake_subzone_id', 'umf.undertake_zone_id'
        )
        ->whereIn('iv.inv_period_id', $invPeriodSelectedBuggetYearArray)
        ->orderBy('iv.status', 'desc')
        ->get();

        $data_filtered =  collect($datasSql)->filter(function($v){
            return ($v->status != 'deleted' || $v->deleted == 0) || $v->receipt_id > 0;
        })->values();

        $data_filtered_undertake_subzone = collect($data_filtered)->filter(function($v){
            return  collect($v->undertake_subzone_id)->isNotEmpty();
        })->sortBy('undertake_subzone_id');
        // $paid_total = collect($data_filtered)->filter(function($v){
        //     return $v->diff >= 0;
        // })->sum('diff');

        // $reserve_total = collect($data_filtered)->filter(function($v){
        //     return $v->diff == 0 && $v->status != 'init';
        // })->count();

        $inv_period_id_groupeds = collect($data_filtered_undertake_subzone)->groupBy('inv_period_id');

        $inv_period_id_groupeds_sorted = collect($inv_period_id_groupeds)->sort();
        $res_array = collect([]);

        $totals = collect([]);
        foreach($inv_period_id_groupeds_sorted as $key => $inv_periods){
            $inv_periods_filter = collect($inv_periods)->filter(function($v){
                return collect($v->undertake_subzone_id)->isNotEmpty();
            });
            $water_user_total = collect($inv_periods_filter)->sum('diff');
            $reserve_total_x_10 = collect($inv_periods_filter)->filter(function($v){
                return $v->diff == 0;
            })->count()*10;



            $subzone_groupeds = collect($inv_periods_filter)->groupBy('undertake_subzone_id');
            $members_total = 0;
            $water_used_total=0;
            $reserve_paid_total = 0;
            foreach($subzone_groupeds as  $subzone){

                $subzone_water_used_eq_0 = collect($subzone)->filter(function($v){
                    return $v->diff == 0;
                });
                $subzone_water_used_morethan_0 = collect($subzone)->filter(function($v){
                    return $v->diff > 0;
                });
                $subzone_paid = collect($subzone_water_used_morethan_0)->filter(function($v){
                    return $v->status == 'paid';
                });

                $subzone_owe = collect($subzone_water_used_morethan_0)->filter(function($v){
                    return $v->status == 'owe' ||  $v->status == 'invoice';
                });
                $subzone_reserve_paid = collect($subzone_water_used_eq_0)->filter(function($v){
                    return $v->status == 'paid';
                });

                $subzone_reserve_owe = collect($subzone_water_used_eq_0)->filter(function($v){
                    return $v->status == 'owe' ||  $v->status == 'invoice';
                });

                $subzone_name = Subzone::where('id', $subzone[0]->undertake_subzone_id)->get(['subzone_name']);
                $inv_period_name = InvoicePeriod::where('id', $subzone[0]->inv_period_id)->get('inv_period_name');
                $invperoid_explode =  explode('-', $inv_period_name[0]->inv_period_name);
                $members =  collect($subzone)->groupBy('user_id')->count();

                $water_used = collect($subzone_water_used_morethan_0)->sum('diff');
                $reserve_paid = collect($subzone_reserve_paid)->count() *10;
                $res_array->push([
                    'invoice_period' => $invperoid_explode[1].'-'.$invperoid_explode[0],
                    'zone_id' => $subzone[0]->undertake_zone_id,
                    'subzone_id' => $subzone[0]->undertake_subzone_id,
                    'subzone_name' => $subzone_name[0]->subzone_name,
                    'members' => $members,
                    'water_used' => $water_used,
                    'water_used_amount' => collect($subzone_water_used_morethan_0)->sum('diff') *8 ,
                    'water_used_paid' => $water_used*8,//collect($subzone_paid)->sum('diff') *8,
                    'water_used_owe' => collect($subzone_owe)->sum('diff') *8,
                    'reserve_amount' => collect($subzone_water_used_eq_0)->count() *10,
                    'reserve_paid' => $reserve_paid,
                    'reserve_owe' => collect($subzone_reserve_owe)->count() *10,
                ]);
                $members_total += $members;
                $water_used_total +=$water_used;
                $reserve_paid_total += $reserve_paid;
            }
            $totals->push([
                'ivp' => $key,
                'members_total' =>  $members_total,
                'water_used_total' => $water_used_total,
                'water_used_total_x_8' => $water_used_total*8,
                'reserve_paid_total' => $reserve_paid_total,
                'total'=> ($water_used_total*8) +$reserve_paid_total
            ]);

        }

        $res_group =  collect($res_array)->sortBy("invoice_period")->groupBy('subzone_name');
        // );//->groupBy('subzone_id')->values();
         $res = collect($res_group)->sort();
        // return $totals;
        $res_totals = collect($totals)->sortBy('ivp')->values();


        $budgetyears = BudgetYear::all();

        return view('reports.summary', compact('res', 'res_totals', 'selectedBudgetYear',  'budgetyears'));
        // return view('reports.summary', compact('res','selectedBudgetYear', 'groupedByMonth', 'groupByinvoicePeriod', 'budgetyears'));

    }

    public function summary2(Request $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $apiFuncCtrl = new FunctionsController();
        //ถ้า request  = [] ให้หาปีงบประมาณปัจจุบัน
        if (collect($request)->isEmpty()) {
            $selectedBudgetYear = BudgetYear::where('status', 'active')->get();
        } else {
            $selectedBudgetYear = BudgetYear::where('id', $request->get('budgetyear'))->get();
        }
        //หา รอบบิลสุดท้ายของปีงบประมาณก่อนถ้ามี เพราะต้องเอาค่ามิเตอร์มาลบกับ ค่ามิเตอร์เดือนแรกของ รอบบิลปัจจุบัน
        $last_prev_inv_period_of_prev_budget_year = BudgetYear::where('status', 'inactive')
            ->with(['invoicePeriod' => function ($query) {
                $query->select('id', 'inv_period_name', 'budgetyear_id')
                    ->where('deleted', 0)
                    ->orderBy('id', 'desc')->limit(1);
            }])
            ->get(['id'])->last();

        $invPeriodSelectedBuggetYear = InvoicePeriod::where('budgetyear_id', $selectedBudgetYear[0]->id)
            ->where('deleted', 0)
            ->get('id');
        $invPeriodSelectedBuggetYearArray = collect($invPeriodSelectedBuggetYear)->pluck('id')->toArray();
        // return gettype($invPeriodSelectedBuggetYearArray);

        if (collect($last_prev_inv_period_of_prev_budget_year)->isNotEmpty()) {
            //ถ้ามีรอบบิลสุดท้ายของปีงบประมาณก่อน ให้ทำการ add เข้าไปในตำแหน่งที่ 0 ของ $invPeriodSelectedBuggetYear
            array_unshift($invPeriodSelectedBuggetYearArray, $last_prev_inv_period_of_prev_budget_year['invoicePeriod'][0]->id);
        }

        $a = Invoice::where('inv_period_id', 13)->orWhere('inv_period_id', 15)
            ->get(['user_id', 'currentmeter', 'inv_period_id'])
            ->groupBy(['user_id', 'inv_period_id']);
        $aa = collect($a)->values();
        // return collect($aa)->reduce(function ($carry, $v) {
        //     $i = 0;
        //     if (!isset($v['15'])) {
        //         dd($v);
        //     }
        //     if ($v['13'][0]->currentmeter == $v['15'][0]->currentmeter) {
        //         $i = 1;
        //     }
        //     return $carry + $i;
        // });
        $datasSql = DB::table('invoice as iv')
            ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
            ->select(
                'iv.updated_at', 'iv.inv_period_id',
                'iv.status as iv_status',
                DB::raw('(iv.currentmeter - iv.lastmeter)*8 as mustpaid'),
                // 'ivp.inv_period_name',
            )
            ->where('iv.status', '<>', 'deleted')
            ->whereIn('iv.inv_period_id', $invPeriodSelectedBuggetYearArray);

        $invPeriodDatas = $datasSql
            ->orderBy('iv.inv_period_id', 'asc')
            ->get();

        $filterByMonth = collect([]);
        $groupedByMonth = collect([]);
        $groupByinvoicePeriod = collect([]);
        //หาผลรวม โดย  group รอบบิล
        $invPeriodgroupeds = collect($invPeriodDatas)->groupBy('inv_period_id');
        foreach ($invPeriodgroupeds as $invPeriodgrouped) {
            //ทำการ group โดยแบ่งตามสถานะ
            $groupedOweAndInvoice = collect($invPeriodgrouped)->filter(function ($item) {
                return $item->iv_status == 'owe' || $item->iv_status == 'invoice';
            });
            $groupedPaid = collect($invPeriodgrouped)->filter(function ($item) {
                return $item->iv_status == 'paid';
            });
            //หาว่า oweและ invoice เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
            $sumWaterUsedOweAndInvoice = collect($groupedOweAndInvoice)->sum('mustpaid');
            $reserveMeterOweAndInvoiceCount = collect($groupedOweAndInvoice)->pipe(function ($groupedOweAndInvoice) {
                return collect($groupedOweAndInvoice)->sum(function ($item) {
                    return $item->mustpaid == 0;
                }) * 10;
            });
            //หาว่า paid เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
            $sumWaterUsedPaid = collect($groupedPaid)->sum('mustpaid');
            $reserveMeterPaidCount = collect($groupedPaid)->pipe(function ($groupedPaid) {
                return collect($groupedPaid)->sum(function ($item) {
                    return $item->mustpaid == 0;
                }) * 10;
            });

            // หาข้อมูลรอบบิล
            $invPeriod = InvoicePeriod::where('id', $invPeriodgrouped[0]->inv_period_id)->get(['inv_period_name']);
            $monthArr = explode("-", $invPeriod[0]->inv_period_name);
            $monthTh = $apiFuncCtrl->fullThaiMonth($monthArr[0]) . " 25" . $monthArr[1];

            $groupByinvoicePeriod->push([
                'inv_period_name' => $invPeriod[0]->inv_period_name,
                'monthName' => $monthTh,
                'oweAndInvoice' => [
                    'total' => $sumWaterUsedOweAndInvoice + $reserveMeterOweAndInvoiceCount,
                    'water_used_sum' => $sumWaterUsedOweAndInvoice,
                    'reservemeter_sum' => $reserveMeterOweAndInvoiceCount,
                ],
                'paid' => [
                    'total' => $sumWaterUsedPaid + $reserveMeterPaidCount,
                    'water_used_sum' => $sumWaterUsedPaid,
                    'reservemeter_sum' => $reserveMeterPaidCount,
                ],
            ]);

        }
        //หาผลรวม โดย group เดือน
        for ($i = 2; $i <= 12; $i++) {
            $a = $i < 10 ? "0" . $i : $i;
            //filter แยกเป้นเดือน
            $filtered = collect($invPeriodDatas)->filter(function ($item, $key) use ($a) {
                $res = strpos($item->updated_at, '-' . $a . '-');
                return $res == 4;
            });

            $paidTotal = 0;
            $paid_reserve = 0;
            $oweTotal = 0;
            $owe_reserve = 0;
            $invoiceTotal = 0;
            $invoice_reserve = 0;
            $paid_paid = 0;
            //filter แยกเป็นสถานะ inv owe paid ของแต่ละเดือน
            if (collect($filtered)->isNotEmpty()) {
                $groupedByStatus = collect($filtered)->flatten()->groupBy('iv_status');

                if (isset($groupedByStatus['paid'])) {
                    $paid_reserve = collect($groupedByStatus['paid'])->filter(function ($item) {
                        return $item->mustpaid == 0;
                    })->count();
                    $paidTotal = collect($groupedByStatus['paid'])->sum('mustpaid') + $paid_reserve * 10;
                }
                if (isset($groupedByStatus['owe'])) {
                    $owe_reserve = collect($groupedByStatus['owe'])->filter(function ($item) {
                        return $item->mustpaid == 0;
                    })->count();
                    $oweTotal = collect($groupedByStatus['owe'])->sum('mustpaid') + $owe_reserve * 10;

                }
                if (isset($groupedByStatus['invoice'])) {
                    $invoice_reserve = collect($groupedByStatus['invoice'])->filter(function ($item) {
                        return $item->mustpaid == 0;
                    })->count();
                    $invoiceTotal = collect($groupedByStatus['invoice'])->sum('mustpaid') + $invoice_reserve * 10;

                }

                //หาเดืิิอน
                $findMonth = collect($groupedByStatus)->first()->values();
                $mothAndTimeExp = explode(" ", $findMonth[0]->updated_at);
                $year = explode("-", $mothAndTimeExp[0])[0] + 543;
                $month = $apiFuncCtrl->fullThaiMonth($a) . " " . $year;
                $_oweTotal = $oweTotal > 0 ? $oweTotal - ($owe_reserve * 10) : 0;
                $_invoiceTotal = $invoiceTotal > 0 ? $invoiceTotal - ($invoice_reserve * 10) : 0;

                $groupedByMonth->push([
                    'month' => $month,
                    'data' => [
                        'paid' => [
                            'paidTotal' => $paidTotal,
                            'paid_reserve' => $paid_reserve * 10 + $invoice_reserve * 10,
                            'used_water_paid' => $paidTotal > 0 ? $paidTotal - ($paid_reserve * 10) : 0,
                        ],
                        'owe' => [
                            'oweTotal' => $oweTotal + $invoiceTotal,
                            'owe_reserve' => $owe_reserve * 10,
                            'used_water_paid' => $_oweTotal + $_invoiceTotal,
                        ],
                    ],
                ]);
            } // if collect isNotEmpty
            // return($groupedByMonth);

        } //fore

        $budgetyears = BudgetYear::all();

        return view('reports.summary', compact('selectedBudgetYear', 'groupedByMonth', 'groupByinvoicePeriod', 'budgetyears'));

    }

    public function cutmeter()
    {
        $cutmeters = UserMeterInfos::where('status', 'cutmeter')
            ->with('user_profile', 'zone', 'subzone')
            ->get();
        return view('reports.cutmeter', compact('cutmeters'));
    }

}
//         $dates = [
//             '63-10' => [
//                 '2021-10-01','2021-10-02','2021-10-03','2021-10-04','2021-10-05',
//                 '2021-10-06','2021-10-07','2021-10-08','2021-10-09','2021-10-10',
//                 '2021-10-11','2021-10-12','2021-10-13','2021-10-14','2021-10-15',
//                 '2021-10-16','2021-10-17','2021-10-18','2021-10-19','2021-10-20',
//                 '2021-10-21','2021-10-22','2021-10-23','2021-10-24','2021-10-25',
//                 '2021-10-26','2021-10-27','2021-10-28','2021-10-29','2021-10-30','2021-10-31'
//             ],
//             '63-11' => [
//                 '2021-11-01','2021-11-02','2021-11-03','2021-11-04','2021-11-05',
//                 '2021-11-06','2021-11-07','2021-11-08','2021-11-09','2021-11-10',
//                 '2021-11-11','2021-11-12','2021-11-13','2021-11-14','2021-11-15',
//                 '2021-11-16','2021-11-17','2021-11-18','2021-11-19','2021-11-20',
//                 '2021-11-21','2021-11-22','2021-11-23','2021-11-24','2021-11-25',
//                 '2021-11-26','2021-11-27','2021-11-28','2021-11-29','2021-11-30',
//             ],
//             '63-12' => [
//                 '2021-12-01','2021-12-02','2021-12-03','2021-12-04','2021-12-05',
//                 '2021-12-06','2021-12-07','2021-12-08','2021-12-09','2021-12-10',
//                 '2021-12-11','2021-12-12','2021-12-13','2021-12-14','2021-12-15',
//                 '2021-12-16','2021-12-17','2021-12-18','2021-12-19','2021-12-20',
//                 '2021-12-21','2021-12-22','2021-12-23','2021-12-24','2021-12-25',
//                 '2021-12-26','2021-12-27','2021-12-28','2021-12-29','2021-12-30','2021-12-31'
//             ],

//             '64-1' => [
//                 '2021-01-01','2021-01-02','2021-01-03','2021-01-04','2021-01-05',
//                 '2021-01-06','2021-01-07','2021-01-08','2021-01-09','2021-01-10',
//                 '2021-01-11','2021-01-12','2021-01-13','2021-01-14','2021-01-15',
//                 '2021-01-16','2021-01-17','2021-01-18','2021-01-19','2021-01-20',
//                 '2021-01-21','2021-01-22','2021-01-23','2021-01-24','2021-01-25',
//                 '2021-01-26','2021-01-27','2021-01-28','2021-01-29','2021-01-30','2021-01-31'
//             ],


//             '64-2' => [
//                 '2021-02-01','2021-02-02','2021-02-03','2021-02-04','2021-02-05',
//                 '2021-02-06','2021-02-07','2021-02-08','2021-02-09','2021-02-10',
//                 '2021-02-11','2021-02-12','2021-02-13','2021-02-14','2021-02-15',
//                 '2021-02-16','2021-02-17','2021-02-18','2021-02-19','2021-02-20',
//                 '2021-02-21','2021-02-22','2021-02-23','2021-02-24','2021-02-25',
//                 '2021-02-26','2021-02-27','2021-02-28',
//             ],

//             '64-3' => [
//                 '2021-03-01','2021-03-02','2021-03-03','2021-03-04','2021-03-05',
//                 '2021-03-06','2021-03-07','2021-03-08','2021-03-09','2021-03-10',
//                 '2021-03-11','2021-03-12','2021-03-13','2021-03-14','2021-03-15',
//                 '2021-03-16','2021-03-17','2021-03-18','2021-03-19','2021-03-20',
//                 '2021-03-21','2021-03-22','2021-03-23','2021-03-24','2021-03-25',
//                 '2021-03-26','2021-03-27','2021-03-28','2021-03-29','2021-03-30','2021-03-31'
//             ],

//             '64-4' => [
//                 '2021-04-01','2021-04-02','2021-04-03','2021-04-04','2021-04-05',
//                 '2021-04-06','2021-04-07','2021-04-08','2021-04-09','2021-04-10',
//                 '2021-04-11','2021-04-12','2021-04-13','2021-04-14','2021-04-15',
//                 '2021-04-16','2021-04-17','2021-04-18','2021-04-19','2021-04-20',
//                 '2021-04-21','2021-04-22','2021-04-23','2021-04-24','2021-04-25',
//                 '2021-04-26','2021-04-27','2021-04-28','2021-04-29','2021-04-30',
//             ],
//             '64-5' => [
//                 '2021-05-01','2021-05-02','2021-05-03','2021-05-04','2021-05-05',
//                 '2021-05-06','2021-05-07','2021-05-08','2021-05-09','2021-05-10',
//                 '2021-05-11','2021-05-12','2021-05-13','2021-05-14','2021-05-15',
//                 '2021-05-16','2021-05-17','2021-05-18','2021-05-19','2021-05-20',
//                 '2021-05-21','2021-05-22','2021-05-23','2021-05-24','2021-05-25',
//                 '2021-05-26','2021-05-27','2021-05-28','2021-05-29','2021-05-30','2021-05-31'
//             ],



//         ];
//             $arr = collect([]);
// foreach($dates as $key => $date){
//     foreach($date as $d){
//             $paids =  Invoice::with(
//                 [
//                     'user_profile' => function($query){
//                         return $query->select('user_id', 'name', 'address');
//                    },
//                    'usermeterinfos'=> function($query){
//                         return $query->select('user_id', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id');
//                    },
//                    'usermeterinfos.zone' => function($query){
//                         return $query->select('id', 'zone_name');
//                     },
//                     'usermeterinfos.subzone'=> function($query){
//                         return $query->select('id', 'subzone_name');
//                     },
//                     'invoice_period' => function($query){
//                         return $query->select('id', 'inv_period_name');
//                     },
//                     'accounting'=> function($query){
//                         return $query->select('id', 'cashier', 'total', 'updated_at');
//                     },
//                     'accounting.cashier'=> function($query){
//                         return $query->select('user_id', 'name', 'updated_at');
//                     }

//                 ]
//             )
//             ->select(
//                 'user_id', 'currentmeter', 'lastmeter', 'recorder_id', 'updated_at',
//                 DB::raw('(currentmeter - lastmeter) as water_used'), 'status as iv_status',
//                 DB::raw('(currentmeter - lastmeter)*8 as mustpaid'), 'receipt_id', 'inv_period_id'

//             )->where('receipt_id', '<>', 0)
//             ->where('status', '=', 'paid')
//             // ->where(DB::raw("date(updated_at)"), '>=', $fromdate)
//             // ->where(DB::raw("date(updated_at)"), '<=', $todate)
//              ->where('updated_at', '>=', $d.' 00:00:00')
//              ->where('updated_at', '<=', $d.' 23:59:59')

//             ->get();

        //    $filter =  collect($paids)->filter(function($v) use($d){
        //         return $v->accounting->updated_at >= $d." 00:00:00" && $v->accounting->updated_at <= $d." 23:59:59" ;
        //     });

//             $arr->push([
//                 'month' => $key,
//                 'date' => $d,
//                 'paid' => collect($filter)->sum('mustpaid'),
//                 'reserve' => collect($filter)->filter(function($v){
//                     return $v->water_used ==0;
//                 })->count()*10,
//                 'total' =>collect($filter)->sum('mustpaid') +  collect($filter)->filter(function($v){
//                     return $v->water_used ==0;
//                 })->count()*10
//             ]);
//         }
// }
// return collect($arr)->groupBy('month');



// summary function

//  $sum_water_used = DB::table('invoice as iv')
        //  ->join('user_meter_infos as umf', 'umf.user_id', '=', 'iv.user_id')
        // ->select(
        //     // 'iv.id',
        //     'iv.user_id', 'iv.inv_period_id', 'iv.status',
        //     'iv.deleted', 'iv.receipt_id',
        //     'iv.currentmeter', 'iv.lastmeter',
        //     Db::Raw('iv.currentmeter - iv.lastmeter as diff'),
        //     'umf.undertake_subzone_id'
        // )
        // ->whereIn('iv.inv_period_id', [21])
        // ->orderBy('iv.status', 'desc')
        // ->get();

        // return collect($sum_water_used)->sum('diff')*8;
        // return collect($sum_water_used)->sortBy('status');

    //     $sum_water_used_filter = collect($sum_water_used)->filter(function($v){
    //         return $v->diff >= 0;
    //     });

    //    return $grouped_by_subzone = collect($sum_water_used_filter)->groupBy('undertake_subzone_id')->sort();

    //     $water_used_paid_status = collect($sum_water_used_filter)->filter(function($v){
    //         return $v->status == 'paid';
    //     });

    //     $water_used_owe_status = collect($sum_water_used_filter)->filter(function($v){
    //         return $v->status == 'owe' || $v->status == 'invoice';
    //     });

    //     return [
    //         'members' => collect($sum_water_used_filter)->groupBy('user_id')->count(),
    //         'water_used' => collect($sum_water_used_filter)->sum('diff'),
    //         'net' => collect($sum_water_used_filter)->sum('diff') * 8,
    //         'เก็บได้จริง' => collect($water_used_paid_status)->sum('diff') * 8,
    //         'ค้างชำระ' => collect($water_used_owe_status)->sum('diff') * 8,
    //     ];

    ////$res_totals = collect($totals)->sortBy('ivp')->values();
        // return $res_array;
        // $datasSql = DB::table('invoice as iv')
        //     ->join('invoice_period as ivp', 'ivp.id', '=', 'iv.inv_period_id')
        //     ->select(
        //         'iv.updated_at', 'iv.inv_period_id',
        //         'iv.status as iv_status',
        //         DB::raw('(iv.currentmeter - iv.lastmeter)*8 as mustpaid'),
        //         // 'ivp.inv_period_name',
        //     )
        //     ->where('iv.status', '<>', 'deleted')
        //     ->whereIn('iv.inv_period_id', $invPeriodSelectedBuggetYearArray);

        // $invPeriodDatas = $datasSql
        //     ->orderBy('iv.inv_period_id', 'asc')
        //     ->get();

        // $filterByMonth = collect([]);
        // $groupedByMonth = collect([]);
        // $groupByinvoicePeriod = collect([]);
        // //หาผลรวม โดย  group รอบบิล
        // $invPeriodgroupeds = collect($invPeriodDatas)->groupBy('inv_period_id');
        // foreach ($invPeriodgroupeds as $invPeriodgrouped) {
        //     //ทำการ group โดยแบ่งตามสถานะ
        //     $groupedOweAndInvoice = collect($invPeriodgrouped)->filter(function ($item) {
        //         return $item->iv_status == 'owe' || $item->iv_status == 'invoice';
        //     });
        //     $groupedPaid = collect($invPeriodgrouped)->filter(function ($item) {
        //         return $item->iv_status == 'paid';
        //     });
        //     //หาว่า oweและ invoice เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
        //     $sumWaterUsedOweAndInvoice = collect($groupedOweAndInvoice)->sum('mustpaid');
        //     $reserveMeterOweAndInvoiceCount = collect($groupedOweAndInvoice)->pipe(function ($groupedOweAndInvoice) {
        //         return collect($groupedOweAndInvoice)->sum(function ($item) {
        //             return $item->mustpaid == 0;
        //         }) * 10;
        //     });
        //     //หาว่า paid เป็น ค่าใช้น้ำรวมเป็นเท่าไหร่ , reserve meter เป็นจำนวนเท่าไหร่
        //     $sumWaterUsedPaid = collect($groupedPaid)->sum('mustpaid');
        //     $reserveMeterPaidCount = collect($groupedPaid)->pipe(function ($groupedPaid) {
        //         return collect($groupedPaid)->sum(function ($item) {
        //             return $item->mustpaid == 0;
        //         }) * 10;
        //     });

        //     // หาข้อมูลรอบบิล
        //     $invPeriod = InvoicePeriod::where('id', $invPeriodgrouped[0]->inv_period_id)->get(['inv_period_name']);
        //     $monthArr = explode("-", $invPeriod[0]->inv_period_name);
        //     $monthTh = $apiFuncCtrl->fullThaiMonth($monthArr[0]) . " 25" . $monthArr[1];

        //     $groupByinvoicePeriod->push([
        //         'inv_period_name' => $invPeriod[0]->inv_period_name,
        //         'monthName' => $monthTh,
        //         'oweAndInvoice' => [
        //             'total' => $sumWaterUsedOweAndInvoice + $reserveMeterOweAndInvoiceCount,
        //             'water_used_sum' => $sumWaterUsedOweAndInvoice,
        //             'reservemeter_sum' => $reserveMeterOweAndInvoiceCount,
        //         ],
        //         'paid' => [
        //             'total' => $sumWaterUsedPaid + $reserveMeterPaidCount,
        //             'water_used_sum' => $sumWaterUsedPaid,
        //             'reservemeter_sum' => $reserveMeterPaidCount,
        //         ],
        //     ]);

        // }
        // //หาผลรวม โดย group เดือน
        // for ($i = 2; $i <= 12; $i++) {
        //     $a = $i < 10 ? "0" . $i : $i;
        //     //filter แยกเป้นเดือน
        //     $filtered = collect($invPeriodDatas)->filter(function ($item, $key) use ($a) {
        //         $res = strpos($item->updated_at, '-' . $a . '-');
        //         return $res == 4;
        //     });

        //     $paidTotal = 0;
        //     $paid_reserve = 0;
        //     $oweTotal = 0;
        //     $owe_reserve = 0;
        //     $invoiceTotal = 0;
        //     $invoice_reserve = 0;
        //     $paid_paid = 0;
        //     //filter แยกเป็นสถานะ inv owe paid ของแต่ละเดือน
        //     if (collect($filtered)->isNotEmpty()) {
        //         $groupedByStatus = collect($filtered)->flatten()->groupBy('iv_status');

        //         if (isset($groupedByStatus['paid'])) {
        //             $paid_reserve = collect($groupedByStatus['paid'])->filter(function ($item) {
        //                 return $item->mustpaid == 0;
        //             })->count();
        //             $paidTotal = collect($groupedByStatus['paid'])->sum('mustpaid') + $paid_reserve * 10;
        //         }
        //         if (isset($groupedByStatus['owe'])) {
        //             $owe_reserve = collect($groupedByStatus['owe'])->filter(function ($item) {
        //                 return $item->mustpaid == 0;
        //             })->count();
        //             $oweTotal = collect($groupedByStatus['owe'])->sum('mustpaid') + $owe_reserve * 10;

        //         }
        //         if (isset($groupedByStatus['invoice'])) {
        //             $invoice_reserve = collect($groupedByStatus['invoice'])->filter(function ($item) {
        //                 return $item->mustpaid == 0;
        //             })->count();
        //             $invoiceTotal = collect($groupedByStatus['invoice'])->sum('mustpaid') + $invoice_reserve * 10;

        //         }

        //         //หาเดืิิอน
        //         $findMonth = collect($groupedByStatus)->first()->values();
        //         $mothAndTimeExp = explode(" ", $findMonth[0]->updated_at);
        //         $year = explode("-", $mothAndTimeExp[0])[0] + 543;
        //         $month = $apiFuncCtrl->fullThaiMonth($a) . " " . $year;
        //         $_oweTotal = $oweTotal > 0 ? $oweTotal - ($owe_reserve * 10) : 0;
        //         $_invoiceTotal = $invoiceTotal > 0 ? $invoiceTotal - ($invoice_reserve * 10) : 0;

        //         $groupedByMonth->push([
        //             'month' => $month,
        //             'data' => [
        //                 'paid' => [
        //                     'paidTotal' => $paidTotal,
        //                     'paid_reserve' => $paid_reserve * 10 + $invoice_reserve * 10,
        //                     'used_water_paid' => $paidTotal > 0 ? $paidTotal - ($paid_reserve * 10) : 0,
        //                 ],
        //                 'owe' => [
        //                     'oweTotal' => $oweTotal + $invoiceTotal,
        //                     'owe_reserve' => $owe_reserve * 10,
        //                     'used_water_paid' => $_oweTotal + $_invoiceTotal,
        //                 ],
        //             ],
        //         ]);
        //     } // if collect isNotEmpty
        //     // return($groupedByMonth);

        // } //fore
