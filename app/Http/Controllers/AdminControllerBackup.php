<?php

namespace App\Http\Controllers;

use App\BudgetYear;
use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\InvoicePeriodController;
use App\Http\Controllers\Api\ReportsController;
use App\InvoicePeriod;
use App\Settings;
use App\Subzone;
use App\UserMeterInfos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;

class AdminController extends Controller
{
    public function index(REQUEST $request)
    {
        $settings = Settings::where('name', 'organization')->get();
        if (collect($settings)->count() == 0) {
            $currentBudgetYear = [];
            return view('admin.index', compact('settings', 'currentBudgetYear'));
        }
        $tambonInfos = json_decode($settings[0]['values'], true);
        if (isset($tambonInfos['organization_short_name'])) {
            session(['organization_short_name' => $tambonInfos['organization_short_name']]);
        }
        $logo_query = Settings::where('name', 'logo')->get(['values'])->first();
        if (collect($logo_query)->isEmpty()) {
            session(['logo' => 'init_logo.jpg']);
        } else {
            session(['logo' => $logo_query->values]);
        }

        // หาปีงบประมาณปัจจุบัน
        $currentBudgetYear = BudgetYear::with(['invoicePeriod' => function ($query) {
                return $query->where('deleted', 0);
            }])->where('status', 'active')->first();

        //ถ้ายังไม่มีการตั้งค่าปีงบประมาณ
        if (collect($currentBudgetYear)->isEmpty() || collect($settings)->count() == 0) {
            return view('admin.index', [
                'membersTotal' => 0,
                'totalWaterUsedByBudgetYear' => 0,
                'totalWaterUsed' => 0,
                'receiptSum' => 0,
                'owePriceSum' => 0,
                'membersBysubzone' => [],
                'currentBudgetYear' => [],
                'tambonInfos' => $tambonInfos,
                'settings' => $settings,
                'all_total_water_used' => 0,
                'total_paid' => 0,
                'all_reserve_meter_status_paid' => 0,
                'all_reserve_meter_status_owe_and_invoice' => 0,
                'total_owe_and_invoice' => 0,
            ]);
        } else {
            //สร้างปีงบประมาณแล้วแต่ยังไม่สร้างรอบบิล
            if (collect($currentBudgetYear->invoicePeriod)->isEmpty()) {
                return view('admin.index', [
                    'membersTotal' => 0,
                    'totalWaterUsedByBudgetYear' => 0,
                    'totalWaterUsed' => 0,
                    'receiptSum' => 0,
                    'owePriceSum' => 0,
                    'membersBysubzone' => [],
                    'currentBudgetYear' => $currentBudgetYear,
                    'tambonInfos' => $tambonInfos,
                    'settings' => $settings,
                    'all_total_water_used' => 0,
                    'total_paid' => 0,
                    'all_reserve_meter_status_paid' => 0,
                    'total_owe_and_invoice' => 0,
                ]);
            }
        }

        $apiInvoicePeriodCtrl = new InvoicePeriodController();

        $inv_period_lists = json_decode($apiInvoicePeriodCtrl->inv_period_lists($currentBudgetYear->id)->content(), true);
        $startTime = microtime(true);

        $this->test($settings, $tambonInfos, $currentBudgetYear);
        return("Time:  " . number_format((microtime(true) - $startTime), 4) . " Seconds\n");

        $apiReportsCtrl = new ReportsController();
        // return $testIndex()
        return $water_used_history = json_decode($apiReportsCtrl->meter_record_history_count($currentBudgetYear->id, 'all')->content(), true);

        $all_total_water_used = $apiReportsCtrl->water_used_count($water_used_history);
        $all_water_used_status_paid = $apiReportsCtrl->water_used_status($water_used_history, 'paid');
        $all_reserve_meter_status_paid = $apiReportsCtrl->reserve_meter_status($water_used_history, 'paid');
        $all_water_used_status_owe = $apiReportsCtrl->water_used_status($water_used_history, 'owe');
        $all_reserve_meter_status_owe = $apiReportsCtrl->reserve_meter_status($water_used_history, 'owe');
        $all_water_used_status_invoice = $apiReportsCtrl->water_used_status($water_used_history, 'invoice');
        $all_reserve_meter_status_invoice = $apiReportsCtrl->reserve_meter_status($water_used_history, 'invoice');

        // $waterUsedBysubzonePopularity = \Lava::DataTable();
        $lava = new Lavacharts;
        $waterUsedBysubzonePopularity = $lava->DataTable();

        $waterUsedBysubzonePopularity->addStringColumn('เส้นทาง')
            // ->addNumberColumn('ปริมาณการใช้น้ำทั้งหมด')
            ->addNumberColumn('ชำระแล้ว(หน่วย)')
            ->addNumberColumn('ยังไม่ชำระ(หน่วย)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');

        //หาปริมาณการใช้น้ำแยกตาม subzone
        $subzone_grouped = collect($water_used_history)->groupBy('undertake_subzone_id');

        foreach ($subzone_grouped as $key => $item) {
            $water_used_count_by_subzone = $apiReportsCtrl->water_used_count($item);
            $water_used_status_paid = $apiReportsCtrl->water_used_status($item, 'paid');
            $reserve_meter_status_paid = $apiReportsCtrl->reserve_meter_status($item, 'paid');
            $water_used_status_owe = $apiReportsCtrl->water_used_status($item, 'owe');
            $reserve_meter_status_owe = $apiReportsCtrl->reserve_meter_status($item, 'owe');
            $water_used_status_invoice = $apiReportsCtrl->water_used_status($item, 'invoice');
            $reserve_meter_status_invoice = $apiReportsCtrl->reserve_meter_status($item, 'invoice');

            $subzone_water_used_and_paid = $water_used_status_paid;
            $subzone_owe_and_invoice = $water_used_status_owe + $water_used_status_invoice;
            $waterUsedBysubzonePopularity->addRow(
                [
                    $item[0]['subzone']['subzone_name'],
                    $subzone_water_used_and_paid, $subzone_owe_and_invoice,
                    '(' . $water_used_count_by_subzone . ')',
                    'fill-opacity: 0.6',
                ]
            );
        }
        $waterUsedBysubzone = \Lava::ColumnChart('waterUsedBysubzone', $waterUsedBysubzonePopularity, [
            'title' => '',
            'legend' => ['position' => 'top', 'maxLines' => 3],
            'bar' => ['groupWidth' => '75%'],
            'isStacked' => true,
        ]);
        //สร้างกราฟแสดงจำนวนสมาชิกในแต่ละเส้นทาง
        $membersBysubzonePopularity = \Lava::DataTable();
        $membersBysubzonePopularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('จำนวนผู้ใช้น้ำ(คน)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');

        foreach ($subzone_grouped as $subzone_id_key => $item) {
            $membersBysubzonePopularity->addRow([$item[0]['subzone']['subzone_name'], collect($item)->count(), collect($item)->count(), 'fill-opacity: 0.6']);
        }

        $membersBysubzone = \Lava::ColumnChart('membersBysubzone', $membersBysubzonePopularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],
        ]);
        $membersTotal = collect($water_used_history)->count();
        $membersTotal = UserMeterInfos::where('status', 'active')->count();
        $total_paid = ($all_water_used_status_paid * 8) + ($all_reserve_meter_status_paid * 10);
        $total_owe_and_invoice = ($all_water_used_status_owe * 8) + ($all_reserve_meter_status_owe * 10) + ($all_water_used_status_invoice * 8) + ($all_reserve_meter_status_invoice * 10);
        return view('admin.index', compact(
            'all_total_water_used',
            'membersTotal',
            'total_paid',
            'all_reserve_meter_status_paid',
            'total_owe_and_invoice',
            'settings',
            'membersBysubzone',
            'waterUsedBysubzone',
            'currentBudgetYear',
            'tambonInfos'
        ));
    }

    private function test($settings, $tambonInfos, $currentBudgetYear)
    {
        $membersTotal = UserMeterInfos::where('deleted', 0)->count();
        $inv_piriods = InvoicePeriod::where('budgetyear_id', 2)->get(['id']);
        $startTime = microtime(true);
        $apiReportsCtrl = new ReportsController();

        $usersAll = UserMeterInfos::with([
            'invoice' => function ($query) {
                return $query->select(
                    'user_id',
                    'inv_period_id',
                    DB::RAW('(currentmeter - lastmeter)*8 as paid'),
                    'status',
                    DB::RAW('currentmeter - lastmeter as water_used'),
                )
                    ->whereIn('inv_period_id', [14, 15, 16, 17, 18, 19, 20]);
            }
        ])
            ->limit(3) 
            ->where('undertake_subzone_id', 1)
            ->get(['user_id', 'undertake_subzone_id']);

        $status1 = 'paid';
        $users_invoice_not_empty =    collect($usersAll)->filter(function ($v) {
            return collect($v->invoice)->isNotEmpty();
        });
        $sum_water_used = collect($users_invoice_not_empty)->sum(function ($v) {
            return collect($v->invoice)->sum('water_used');
        });
        $aaa = "invoice";
      return  $reserve_meter = collect($users_invoice_not_empty)->sum(function ($v) use ($aaa) {
            $a = collect($v->invoice)->filter(function ($vv) use ($aaa) {
                return $vv->water_used == 0 && $vv->status  == $aaa;
            });
            return collect($a)->count();
            // return collect($v->invoice->water_used == 0;
        });
        return;
        if (1 == 0) {
            // $all_total_water_used = collect($users)->sum(function ($v) {
            //     return collect($v->invoice)->sum('water_used');

            // });
            // $all_water_used_status_paid = collect($users)->sum(function ($vv) {

            //     $filter = collect($vv->invoice)->filter(function ($v) {
            //         return $v->status == 'paid';
            //     });
            //     // dd($filter);
            //     return collect($filter)->sum('water_used');

            // });
            // $all_water_used_status_owe = collect($users)->sum(function ($vv) {

            //     $filter = collect($vv->invoice)->filter(function ($v) {
            //         return $v->status == 'owe';
            //     });
            //     // dd($filter);
            //     return collect($filter)->sum('water_used');

            // });
            // $all_water_used_status_invoice = collect($users)->sum(function ($vv) {

            //     $filter = collect($vv->invoice)->filter(function ($v) {
            //         return $v->status == 'invoice';
            //     });
            //     // dd($filter);
            //     return collect($filter)->sum('water_used');

            // });

            // //reserve
            // $all_reserve_meter_status_paid = collect($users)->sum(function ($vv) {

            //     $filter = collect($vv->invoice)->filter(function ($v) {
            //         return $v->status == 'paid' && $v->water_used == 0;
            //     });
            //     return collect($filter)->count();
            // });
            // $all_reserve_water_used_status_owe = collect($users)->sum(function ($vv) {

            //     $filter = collect($vv->invoice)->filter(function ($v) {
            //         return $v->status == 'owe' && $v->water_used == 0;
            //     });
            //     return collect($filter)->count();

            // });
            // $all_reserve_water_used_status_invoice = collect($users)->sum(function ($vv) {

            //     $filter = collect($vv->invoice)->filter(function ($v) {
            //         return $v->status == 'invoice' && $v->water_used == 0;
            //     });
            //     return collect($filter)->count();
            //     // dd($filter);

            // });
        }
        $arr = collect([]);
        $waterUsedBysubzonePopularity = \Lava::DataTable();
        $waterUsedBysubzonePopularity->addStringColumn('เส้นทาง')
            // ->addNumberColumn('ปริมาณการใช้น้ำทั้งหมด')
            ->addNumberColumn('ชำระแล้ว(หน่วย)')
            ->addNumberColumn('ยังไม่ชำระ(หน่วย)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');

        $subzone_grouped = collect($users)->groupBy('undertake_subzone_id');
        foreach ($subzone_grouped as $key => $item) {
            $user_in_subzone = collect($item)->count();
            $all_total_water_used = collect($item)->sum(function ($v) {
                return collect($v->invoice)->sum('water_used');
            });
            $all_water_used_status_paid = collect($item)->sum(function ($vv) {

                $filter = collect($vv->invoice)->filter(function ($v) {
                    return $v->status == 'paid';
                });
                // dd($filter);
                return collect($filter)->sum('water_used');
            });
            $all_water_used_status_owe = collect($item)->sum(function ($vv) {

                $filter = collect($vv->invoice)->filter(function ($v) {
                    return $v->status == 'owe';
                });
                // dd($filter);
                return collect($filter)->sum('water_used');
            });
            $all_water_used_status_invoice = collect($item)->sum(function ($vv) {

                $filter = collect($vv->invoice)->filter(function ($v) {
                    return $v->status == 'invoice';
                });
                // dd($filter);
                return collect($filter)->sum('water_used');
            });

            //reserve
            $all_reserve_meter_status_paid = collect($item)->sum(function ($vv) {

                $filter = collect($vv->invoice)->filter(function ($v) {
                    return $v->status == 'paid' && $v->water_used == 0;
                });
                return collect($filter)->count();
            });
            $all_reserve_water_used_status_owe = collect($item)->sum(function ($vv) {

                $filter = collect($vv->invoice)->filter(function ($v) {
                    return $v->status == 'owe' && $v->water_used == 0;
                });
                return collect($filter)->count();
            });
            $all_reserve_water_used_status_invoice = collect($item)->sum(function ($vv) {

                $filter = collect($vv->invoice)->filter(function ($v) {
                    return $v->status == 'invoice' && $v->water_used == 0;
                });
                return collect($filter)->count();
                // dd($filter);

            });

            $arr->push([
                'subzone_id' => $key,
                'subzone_name' => $item[0]->subzone->subzone_name,
                'user_in_subzone' => $user_in_subzone,
                'all_water_used_status_paid' => $all_water_used_status_paid,
                'all_water_used_status_invoice' => $all_water_used_status_invoice,
                'all_water_used_status_owe' => $all_water_used_status_owe,
                'all_reserve_meter_status_paid' => $all_reserve_meter_status_paid,
                'all_reserve_water_used_status_invoice' => $all_reserve_water_used_status_invoice,
                'all_reserve_water_used_status_owe' => $all_reserve_water_used_status_owe,

            ]);
            // return ;
            $waterUsedBysubzonePopularity->addRow(
                [
                    $item[0]->subzone->subzone_name,
                    $all_water_used_status_paid, ($all_water_used_status_owe + $all_water_used_status_invoice),
                    '(' . $all_total_water_used . ')',
                    'fill-opacity: 0.6',
                ]
            );
        }

        $water_used_total = collect($arr)->sum(function ($v) {
            return $v['all_water_used_status_paid'] + $v['all_water_used_status_owe'] + $v['all_water_used_status_invoice'];
        });

        $waterUsedBysubzone = \Lava::ColumnChart('waterUsedBysubzone', $waterUsedBysubzonePopularity, [
            'title' => '',
            'legend' => ['position' => 'top', 'maxLines' => 3],
            'bar' => ['groupWidth' => '75%'],
            'isStacked' => true,
        ]);
        //สร้างกราฟแสดงจำนวนสมาชิกในแต่ละเส้นทาง
        $membersBysubzonePopularity = \Lava::DataTable();
        $membersBysubzonePopularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('จำนวนผู้ใช้น้ำ(คน)')
            ->addRoleColumn('string', 'annotation')
            ->addRoleColumn('string', 'style');

        foreach ($arr as $item) {

            $membersBysubzonePopularity->addRow([$item['subzone_name'], $item['user_in_subzone'], $item['user_in_subzone'], 'fill-opacity: 0.6']);
        }

        $membersBysubzone = \Lava::ColumnChart('membersBysubzone', $membersBysubzonePopularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],
        ]);
        $all_total_water_used = $water_used_total;
        $membersTotal = collect($users)->count();
        $total_paid = collect($arr)->sum(function ($v) {
            return ($v['all_water_used_status_paid'] * 8) + ($v['all_reserve_meter_status_paid'] * 10);
        });
        $all_reserve_meter_status_paid = collect($arr)->sum(function ($v) {
            return $v['all_reserve_meter_status_paid'];
        });
        $all_reserve_meter_status_owe_and_invoice = collect($arr)->sum(function ($v) {
            return $v['all_reserve_water_used_status_invoice'] + $v['all_reserve_water_used_status_owe'];
        });
        $total_owe_and_invoice = collect($arr)->sum(function ($v) {
            $iv_and_owe = ($v['all_water_used_status_invoice'] + $v['all_water_used_status_owe']) * 8;
            $reserve_iv_and_owe = ($v['all_reserve_water_used_status_invoice'] + $v['all_reserve_water_used_status_owe']) * 10;
            return $iv_and_owe + $reserve_iv_and_owe;
        });
        return view('admin.index', compact(
            'all_total_water_used',
            'membersTotal',
            'total_paid',
            'all_reserve_meter_status_paid',
            'all_reserve_meter_status_owe_and_invoice',
            'total_owe_and_invoice',
            'settings',
            'membersBysubzone',
            'waterUsedBysubzone',
            'currentBudgetYear',
            'tambonInfos'
        ));

        dd("Time:  " . number_format((microtime(true) - $startTime), 4) . " Seconds\n");
    }
    public function index2(REQUEST $request)
    {

        $settings = Settings::where('name', 'organization')->get();
        if (collect($settings)->count() == 0) {
            $currentBudgetYear = [];
            return view('admin.index', compact('settings', 'currentBudgetYear'));
        }
        $tambonInfos = json_decode($settings[0]['values'], true);
        $logo_query = Settings::where('name', 'logo')->get(['values'])->first();
        $currentBudgetYear = BudgetYear::with(['invoicePeriod' => function ($query) {
                return $query->where('deleted', 0);
            }])
            ->where('status', 'active')->first();
        if (collect($currentBudgetYear)->isEmpty() || collect($settings)->count() == 0) {
            return view('admin.index', [
                'membersTotal' => 0,
                'totalWaterUsedByBudgetYear' => 0,
                'totalWaterUsed' => 0,
                'receiptSum' => 0,
                'owePriceSum' => 0,
                'membersBysubzone' => 0,
                'currentBudgetYear' => $currentBudgetYear,
                'tambonInfos' => $tambonInfos,
                'settings' => $settings,
            ]);
        }
        $members = UserMeterInfos::all(); //2878คน
        $invoicePeriodIdArray = collect([]);

        foreach ($currentBudgetYear->invoicePeriod as $item) {
            $invoicePeriodIdArray->push($item->id);
        }

        return $find_sum_currentmeter_and_sum_lastmeter = DB::table('invoice')
            ->where('deleted', 0)
            ->whereIn('inv_period_id', $invoicePeriodIdArray)
            ->select(DB::raw('SUM(currentmeter) as sum_currentmeter'), DB::raw('SUM(lastmeter) as sum_lastmeter'))
            ->get();

        $invSql = DB::table('invoice')
            ->whereIn('inv_period_id', $invoicePeriodIdArray)
            ->where('deleted', 0)
            ->select(
                'currentmeter',
                'lastmeter',
                'inv_period_id',
                'meter_id',
                'status',
                DB::raw('currentmeter - lastmeter as diff'),
                DB::raw('((currentmeter - lastmeter) * 8) as diffplus8')
            );
        // return collect($invSql)->sum('diff');

        //จำนวนรายการที่ติดหนี้
        $oweStatement = $invSql->where('status', 'owe')->get();

        //จำนวนคนที่ค้างชำระ
        $oweMembers = $oweStatement->groupBy('meter_id')->count();
        //ยอดเงินค้าง
        $owePriceSum = collect($oweStatement)->sum('diffplus8');

        $apiInvCtrl = new InvoiceController();
        $membersTotal = collect($members)->count();
        //หาผลรวมเงินที่เก็บได้ในรอบปีงบประมาณ
        $receiptSum = DB::table('invoice as inv')
            ->join('accounting as acc', 'acc.id', '=', 'inv.receipt_id')
            ->select('acc.total')
            ->whereIn('inv_period_id', $invoicePeriodIdArray)
            ->sum('acc.total');

        // หาผลรวมการใช้น้ำของปีงบประมาณปัจจุบัน
        $waterUsedByInvPeriodCollection = collect([]);
        $waterPriceByInvPeriodCollection = collect([]);
        $currentBudgetYear = DB::table('budget_year')
            ->join('invoice_period', 'invoice_period.budgetyear_id', '=', 'budget_year.id')
            ->select('budget_year.budgetyear', 'invoice_period.id', 'invoice_period.inv_period_name')
            ->where('invoice_period.status', 'active')
            ->where('budget_year.status', 'active')
            ->get();

        $waterUsedCurrentBudgetYearCollection = collect($currentBudgetYear)->reduce(function ($carry, $item) use ($waterUsedByInvPeriodCollection) {
            $sql = DB::table('invoice as iv')
                ->select(
                    'inv_period_id',
                    'lastmeter',
                    'currentmeter',
                    'meter_id',
                    DB::raw('currentmeter - lastmeter as diff')
                )
                ->where('inv_period_id', '=', $item->id);

            $sumWaterUsedByInvPeriod = $sql->get();

            $oweByInvPeriodSql = $sql->where('status', '=', 'owe');

            $oweByInvPeriod = $oweByInvPeriodSql->get();
            $oweByInvPeriodCount = $oweByInvPeriodSql->count();
            $oweByInvPeriodPrice = collect($oweByInvPeriod)->sum('diff');
            $sum = 0;
            if (collect($sumWaterUsedByInvPeriod)->isNotEmpty()) {
                $sum = collect($sumWaterUsedByInvPeriod)->sum('diff');
            }
            //หา diff == 0;
            $diff_0_count = collect($sumWaterUsedByInvPeriod)->filter(function ($item) {
                return $item->diff == 0;
            })->count();

            $waterUsedByInvPeriodCollection->push([
                'inv_peroid_id' => $item->id,
                'inv_peroid_name' => $item->inv_period_name,
                'waterUsed' => $sum,
                'diff_0' => $diff_0_count == null ? 0 : $diff_0_count,
                'total_price' => ($sum * 8) + ($diff_0_count * 10),
                'oweCount' => $oweByInvPeriodCount,
                'owePrice' => $oweByInvPeriodPrice * 8,
                'oweDiff' => $oweByInvPeriodPrice,
                'cutmeter' => 0,
            ]);

            return $carry + $sum;
        }, 0);

        $totalWaterUsedByBudgetYear = collect($waterUsedByInvPeriodCollection)->sum('waterUsed');
        $totalWaterUsed = $apiInvCtrl->totalWaterUsed();
        $owePriceSum = collect($waterUsedByInvPeriodCollection)->sum('owePrice');

        $waterUsedBysubzonePopularity = \Lava::DataTable();
        $waterUsedBysubzonePopularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('ปริมาณการใช้น้ำ');

        foreach ($waterUsedByInvPeriodCollection as $key => $item) {
            $waterUsedBysubzonePopularity->addRow([$item['inv_peroid_name'], $item['waterUsed']]);
        }

        $waterUsedBysubzone = \Lava::ColumnChart('waterUsedBysubzone', $waterUsedBysubzonePopularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],
        ]);
        //สร้างกราฟแสดงจำนวนสมาชิกในแต่ละเส้นทาง
        $membersBysubzoneElq = $this->membersBysubzone($invoicePeriodIdArray);
        $membersBysubzonePopularity = \Lava::DataTable();
        $membersBysubzonePopularity->addStringColumn('เส้นทาง')
            ->addNumberColumn('จำนวนผู้ใช้น้ำ');

        foreach ($membersBysubzoneElq as $subzone_id_key => $item) {
            $membersBysubzonePopularity->addRow([$item['subzone'], $item['members']]);
        }

        $membersBysubzone = \Lava::ColumnChart('membersBysubzone', $membersBysubzonePopularity, [
            'title' => '',
            'legend' => [
                'position' => 'in',
            ],

        ]);

        //หาคนค้างเกิน 5 รอบ
        $cutmeter = UsermeterInfos::where('status', 'cutmeter')->count();
        return view('admin.index', compact(
            'membersTotal',
            'totalWaterUsed',
            'settings',
            'receiptSum',
            'owePriceSum',
            'membersBysubzone',
            'waterUsedBysubzone',
            'currentBudgetYear',
            'find_sum_currentmeter_and_sum_lastmeter',
            'tambonInfos',
            'cutmeter'
        ));
    }

    private function membersBysubzone($invoicePeriodIdArray)
    {
        //จะถูกใข้ถ้าใช้ function index เดิม
        $members = UserMeterInfos::where('status', 'active')->get();
        $membersGroup = collect($members)->groupBy('undertake_subzone_id');
        $dataCollections = collect([]);
        foreach ($membersGroup as $key => $val) {
            $undertake_subzon_name = collect(Subzone::where('id', $val[0]->undertake_subzone_id)->get('subzone_name'));
            $dataCollections->push([
                'subzone' => $undertake_subzon_name[0]->subzone_name,
                'members' => collect($val)->count()
            ]);
        }
        return $dataCollections;
    }

    public function create()
    {
        return view("admin.create");
    }

    public function payment()
    {

        return view('admin.payment');
    }

    public function print_invoice($id)
    {
        # code...
    }

    public function paymenthistory($id)
    {
        return view('admin.paymenthistory');
    }

    public function overdue($id)
    {
        //ค้างชำระ
        return view('admin.overdue');
    }
}
