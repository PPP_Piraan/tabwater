<?php

namespace App\Http\Controllers;

use App\BudgetYear;
use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\UsermeterInfosController;
use App\Invoice;
use App\InvoicePeriod;
use App\UserMeterInfos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvoicePeriodController extends Controller
{
    public function index()
    {
        $funcCtrl = new FunctionsController();

        //1.check ว่ามีปีงบประมาณที่ active ไหม ถ้าไม่มีให้ทำการสร้างปีงบประมาณก่อน
        $budgetyearModel = BudgetYear::where('status', 'active')->get();
        $budgetyearCount = collect($budgetyearModel)->count();

        $budgetyear = $budgetyearCount == 0 ? $budgetyearCount : $budgetyearModel[0]->id;
        $invoice_periods = InvoicePeriod::with('budgetyear')
            ->where('deleted', '<>', 1)->orderBy('startdate', 'desc')
            ->where('budgetyear_id', $budgetyear)
            ->get();

        foreach ($invoice_periods as $invoice_period) {
            $invoice_period->startdate = $funcCtrl->engDateToThaiDateFormat($invoice_period->startdate);
            $invoice_period->enddate = $funcCtrl->engDateToThaiDateFormat($invoice_period->enddate);
        }

        return view('invoice_period.index', compact('invoice_periods', 'budgetyearCount'));
    }

    public function create()
    {
        $budgetyear = BudgetYear::where('status', 'active')->first();
        if (collect($budgetyear)->isEmpty()) {
            //ยังไม่ได้สร้างปีงบประมาณ ให้ redirect ไปสร้าง
            return redirect()->action('BudgetYearController@index');
        }
        $lastInvPeriod = InvoicePeriod::where('deleted', 0)->get()->last();

        $invStatusInit = collect([]);
        if(collect($lastInvPeriod)->isNotEmpty()){
            $invStatusInit = Invoice::where('inv_period_id', $lastInvPeriod->id)
            ->where('status', 'init')->get();
        }
        return view('invoice_period.create', compact('budgetyear', 'invStatusInit'));
    }

    public function create_invoices($id)
    {
        //สร้างใบแจ้งหนี้เริ่มต้นของแต่ละ รอบบิลใหม่
        return $invoice_period = InvoicePeriod::with('budgetyear')->where('id', $id)->get()->first();

        return view('invoice_period.create_invoices', \compact('invoice_period'));
    }

    public function store_invoice_init_status(REQUEST $request){
        $items = collect($request->get('data'))->filter(function($v){
            return $v['currentmeter']  >= 0;
        });

        foreach($items as $key => $item){
            Invoice::where('id', $item['id'])->update([
                'currentmeter' => $item['currentmeter'],
                'status' => $item['status'],
                'deleted' => $item['status'] == 'owe' ? 0 : 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            if($item['status'] == 'owe'){
                $owe_count_plus_query = UserMeterInfos::where('user_id', $item['user_id']);
                    $owe_count_plus = $owe_count_plus_query->get('owe_count');
                    $owe_count_plus_query->update([
                        'owe_count' => $owe_count_plus[0]->owe_count +1
                    ]);
            }
        }
        return redirect('/invoice_period/create')->with(['message' => 'ทำการเพิ่มข้อมูลเรียบร้อยแล้ว']);

    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'startdate' => 'required',
            'enddate' => 'required',
            'inv_period_name' => 'required',
        ]);

        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();
        //check ปีงบประมาณใหม่มีการสร้างรอบบิลหรือยัง
        $lastInvPeriod = InvoicePeriod::where('deleted', 0)->get()->last();
        $lastInvPeriodId = 0;
        if(collect($lastInvPeriod)->isNotEmpty()){
            $lasInvPeriodId = $lastInvPeriod->id;
            if($lastInvPeriod->budgetyear_id == $request->get('budgetyear_id') && $lastInvPeriod->status == 'active'){
                InvoicePeriod::where('id', $lastInvPeriod->id)->update([
                    'status' => 'inactive'
                ]);
            }else{
                //ถ้าเป็นการสร้างรอบบิลครั้งแรกเริ่มต้นระบบ
                $invQuery = Invoice::where('inv_period_id', $lastInvPeriod->id);
                //อัพเดท status  ของ invoice รอบบิลก่อน
                $newInvoice = $invQuery
                    ->get(['inv_period_id', 'lastmeter', 'currentmeter', 'user_id']);
                        $invQuery->where('status', 'invoice')->update([
                            'status' => 'owe'
                        ]);
                $invQuery->where('status', 'init')->update([
                    'status' => 'no record',
                    'deleted' => 1
                ]);
            }
        }//outer if

        //สร้างรอบบิลใหม่
        $newInvPeriod = new InvoicePeriod();
        $newInvPeriod->inv_period_name = $request->get('inv_period_name') . "-" . $request->get('inv_period_name_year');
        $newInvPeriod->budgetyear_id = $request->get('budgetyear_id');
        $newInvPeriod->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('startdate'));
        $newInvPeriod->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('enddate'));
        $newInvPeriod->status = 'active';
        $newInvPeriod->created_at = date('Y-m-d H:i:s');
        $newInvPeriod->updated_at = date('Y-m-d H:i:s');
        $newInvPeriod->save();

        $_lastInvPeriod = $lastInvPeriodId == 0 ? $newInvPeriod->id : 0;
        $users= UserMeterInfos::with([
            'invoice' => function($query)use ($_lastInvPeriod){
                return $query->select('meter_id_fk', 'inv_period_id','currentmeter')->where('inv_period_id', $_lastInvPeriod);
            }
        ])->where('status', 'active')->get('meternumber');

        Invoice::where('status', 'invoice')->where('deleted', 0)->update([
            'status' => 'owe'
        ]);
        Invoice::where('status', 'init')->where('deleted', 0)->update([
            'deleted' => 1,
            'status'   => 'no record',
        ]);
        $inv_array = [];
        foreach($users as $user){
            $inv_array[]  = [
                'meter_id_fk'    => $user->meternumber,
                'inv_period_id'  => $newInvPeriod->id,
                'lastmeter'      => collect($user->invoice)->isNotEmpty() ? $user->invoice[0]['currentmeter'] : 0  ,
                'currentmeter'   => 0,
                'status'         => 'init',
                'recorder_id'    => Auth::id(),
            ];
        }
        Invoice::insert($inv_array);

        $userMeterInfos = new UsermeterInfosController;
        $userMeterInfos->manageOweCount();


        return redirect('/invoice_period')->with(['message' => 'ทำการเพิ่มข้อมูลเรียบร้อยแล้ว']);
    }

    private function testGetLastInv($last_prev_inv_period_id){
        return $userMeterInfos = UserMeterInfos::where('status', 'active')
                 ->with([
                     'invoice' => function ($query) use ($last_prev_inv_period_id) {
                         return $query->select('user_id', 'currentmeter', 'lastmeter', 'status')->where('inv_period_id', $last_prev_inv_period_id);
                     },
                 ])
                 ->where('deleted', 0)
                 ->get(['user_id']);
     }

    public function store2(Request $request)
    {
        $validatedData = $request->validate([
            'startdate' => 'required',
            'enddate' => 'required',
            'inv_period_name' => 'required',
        ]);
        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();
        //check ปีงบประมาณใหม่มีการสร้างรอบบิลหรือยัง
        $currentBudgetyear = BudgetYear::where('id', $request->get('budgetyear_id'))->get();
        if($currentBudgetyear[0]->status == 'active'){
            //get ค่า inv_period ของ current budgetyear ที่ status != deleted
            $lastInvPeriodOfCurrentBudgetYear = InvoicePeriod::where('budgetyear_id', $request->get('budgetyear_id'))
                                                ->where('deleted', 0)
                                                ->get()->last();

             //ทำการเพิ่ม new row inv period
            //บันทึกข้อมูล รอบบิล ใหม่
            $invoice_period = new InvoicePeriod();
            $invoice_period->inv_period_name = $request->get('inv_period_name') . "-" . $request->get('inv_period_name_year');
            $invoice_period->budgetyear_id = $request->get('budgetyear_id');
            $invoice_period->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('startdate'));
            $invoice_period->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('enddate'));
            $invoice_period->status = 'active';
            $invoice_period->created_at = date('Y-m-d H:i:s');
            $invoice_period->updated_at = date('Y-m-d H:i:s');
            $invoice_period->save();

            if(collect($lastInvPeriodOfCurrentBudgetYear)->isNotEmpty()){
                if($lastInvPeriodOfCurrentBudgetYear->status == 'active'){
                    //เป็นการ create Inv_period ในปีงบปัจจุบันอยู่  ให้ inactive  last inv peroid
                    InvoicePeriod::where('id', $lastInvPeriodOfCurrentBudgetYear->id)->update([
                        'status' => 'inactive',
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            }
            // ทำการ  update  invoice table โดย
            if(collect($lastInvPeriodOfCurrentBudgetYear)->isNotEmpty()){
                $find_status_eq_invoice_in_inv_table_sql = Invoice::where('inv_period_id', $lastInvPeriodOfCurrentBudgetYear->id)
                ->where('deleted', 0)->where('status', 'invoice');

            // ให้ status ของ inv_peroid_id ล่าสุดที่เป็น inactive  -> status = 'owe'
            $updatePrevInvPrdInInvStatusToOwe = $find_status_eq_invoice_in_inv_table_sql->update([
                'status' => 'owe',
                'recorder_id' => Auth::id(),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            // ให้ status ของ inv_peroid_id ล่าสุดที่เป็น init  -> status = 'permanent deleted'
            Invoice::where('inv_period_id', $lastInvPeriodOfCurrentBudgetYear->id)
                ->where('deleted', 0)->where('status', 'init')->update([
                'status' => 'permanent deleted',
                'deleted' => 1,
                'recorder_id' => Auth::id(),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);


            //เพิ่ม owe_count ใน user_meter_infos table
            $inv_status_eq_invoice = $find_status_eq_invoice_in_inv_table_sql->get(['id', 'user_id']);
            $last_prev_inv_period = $lastInvPeriodOfCurrentBudgetYear->id;
            $userMeterInfos = UserMeterInfos::where('status', 'active')
                ->with([
                    'invoice' => function ($query) use ($last_prev_inv_period_id) {
                        return $query->select('user_id', 'currentmeter', 'inv_period_id')->where('inv_period_id', $last_prev_inv_period_id);
                    },
                ])
                ->where('deleted', 0)
                ->get(['id', 'user_id', 'owe_count']);
            $not_current_inv_peroid = collect($userMeterInfos)->filter(function ($v) {
                return collect($v->invoice)->isEmpty();
            });

            $newInvoice = [];
            foreach ($userMeterInfos as $active_user) {
                array_push($newInvoice, [
                    'inv_period_id' => isset($invoice_period->id) ? $invoice_period->id : 0,
                    'user_id' => $active_user->user_id,
                    'meter_id' => $active_user->user_id,
                    'lastmeter' => collect($active_user->invoice)->isNotEmpty() ? $active_user->invoice[0]->currentmeter : 0,
                    'currentmeter' => 0,
                    'status' => 'init',
                    'recorder_id' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),

                ]);
            }

            //หาเลขมิเตอร์ล่าสุดกรณีที่ไม่มีการสร้าง invoice รอบบิลปัจจุบัน
            if (collect($not_current_inv_peroid)->isNotEmpty()) {
                foreach ($not_current_inv_peroid as $v) {
                    $res = Invoice::where('user_id', $v->user_id)
                        ->orderByDesc('inv_period_id')->limit(1)
                        ->get(['inv_period_id', 'currentmeter']);
                    if (collect($res)->isNotEmpty()) {

                        $plus1_owe_count = $active_user->owe_count + 1;
                        UserMeterInfos::where('id', $active_user->id)->update([
                            'owe_count' => $plus1_owe_count,
                            'cutmeter' => $plus1_owe_count >= 3 ? 1 : 0,
                            'comment' => $plus1_owe_count >= 3 ? 'ค้างชำระเกิน 3 งวด' : '',
                        ]);
                        array_push($newInvoice, [
                            'inv_period_id' => isset($invoice_period->id) ? $invoice_period->id : 0,
                            'user_id' => $v->user_id,
                            'meter_id' => $v->user_id,
                            'lastmeter' => collect($v->invoice)->isNotEmpty() ? $v->currentmeter : 0,
                            'currentmeter' => 0,
                            'status' => 'init',
                            'recorder_id' => 0,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),

                        ]);
                    }
                }
            }

            // return` $newInvoice;
            Invoice::insert($newInvoice);
        }
        }else{
            //ไม่มี budgetyear ที่ไม่มี current budgetyaer
            return 'no active current budgetyear ';
        }

        return redirect('/invoice_period')->with(['message' => 'ทำการเพิ่มข้อมูลเรียบร้อยแล้ว']);
    }

    private function createInvoiceWhenCreateNewInvPeroid($inv_peroid)
    {
        $userMeterInfos = UserMeterInfos::where('status', 'active')->get();
        $newInvoice = [];
        foreach ($userMeterInfos as $active_user) {
            array_push($newInvoice, [
                'inv_period_id' => $inv_peroid,
                'user_id' => $active_user->user_id,
                'meter_id' => $active_user->user_id,
                'lastmeter' => 0,
                'currentmeter' => 0,
                'status' => 'init',
                'recorder_id' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ]);
        }

        Invoice::insert($newInvoice);

    }

    public function edit($id)
    {
        $funcCtrl = new FunctionsController();

        $invoice_period = InvoicePeriod::with('budgetyear')->find($id);
        $invoice_period->startdate = $funcCtrl->engDateToThaiDateFormat($invoice_period->startdate);
        $invoice_period->enddate = $funcCtrl->engDateToThaiDateFormat($invoice_period->enddate);
        return view('invoice_period.edit', compact('invoice_period'));
    }

    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();
        $invoice_period = InvoicePeriod::find($id);
        $invoice_period->inv_period_name = $request->get('inv_period_name');
        $invoice_period->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('startdate'));
        $invoice_period->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('enddate'));
        $invoice_period->status = $request->get('status');
        $invoice_period->save();

        return redirect('/invoice_period')->with(['message' => 'ทำการอัพเดทข้อมูลเรียบร้อยแล้ว']);
    }

    public function delete($id)
    {
        // ถ้ารอบบิลนี้มีการใช้ในการบันทึก invoice แล้วแจ้งเตือนก่อนว่าจะต้องการลบไหม
        $delete = InvoicePeriod::find($id);
        $delete->update([
            'status' => 'deleted',
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return redirect('/invoice_period')->with(['message' => 'ทำการลบข้อมูลเรียบร้อยแล้ว']);
    }
}
