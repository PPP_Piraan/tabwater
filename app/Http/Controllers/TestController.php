<?php

namespace App\Http\Controllers;

class TestController extends Controller
{
    public function TestApiInvoiceCreate()
    {
        return $response = $this->postJson('/api/invoice/create', ['inv_period_id' => 1,
            'meter_id' => 1,
            'user_id' => 1,
            'lastmeter' => 0,
            'currentmeter' => 10,
            'recorder_id' => 1,

        ]);

        $response
            ->assertStatus(201)
            ->assertJsonPath('team.owner.name', 'Darian');

    }
}