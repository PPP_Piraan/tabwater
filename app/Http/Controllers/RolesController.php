<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;

class RolesController extends Controller
{
    public function index(){
        $roles = Roles::all();
        return view('roles.index', compact('roles'));
    }

    public function create(){
        return view('roles.create');
    }

    public function store(Request $request){
        date_default_timezone_set('Asia/Bangkok');
        $role = new Roles();
        $role->name         = $request->get('role');
        $role->display_name = $request->get('display_name'); 
        $role->description  = $request->get('description');   
        $role->created_at   = date('Y-m-d H:i:s');
        $role->updated_at   = date('Y-m-d H:i:s');
        // $role->save();
        return \redirect('index');
    }
   
}
