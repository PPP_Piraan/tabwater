<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\TabwaterMeterController;
use App\Invoice;
use App\InvoicePeriod;
use App\Province;
use App\Subzone;
use App\TabwaterMeter;
use App\User;
use App\Usercategory;
use App\UserMeterInfos;
use App\UserProfile;
use App\NumberSequence;
use App\Models\InvoiceHistory;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UsersController extends Controller
{
    public function index()
    {
        //checkว่ามีการตั้งค่า ค่า zone, tabwatermeterหรือยัง
        $checkZone = Zone::all();
        $apiTabwatermeterCtrl = new TabwaterMeterController();
        $checkTabwatermeter = $apiTabwatermeterCtrl->checkTabwatermeters();
        if (collect($checkZone)->isEmpty() || $checkTabwatermeter == 0) {
            return view('users.index', compact('checkZone', 'checkTabwatermeter'));
        }
        $activeMeter = UserMeterInfos::where('status', 'active')->get();
        $inactiveMeterQuery = UserMeterInfos::whereIn('status', ['inactive', 'deleted'])
            ->with(['user_profile' => function ($query) {
                $query->select('user_id', 'name', 'id_card', 'phone', 'address', 'zone_id', 'subzone_id', 'status');
            }, 'user_profile.zone'])
            ->get(['user_id_fk', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id', 'status',  'updated_at']);

        $inactiveMeter = collect($inactiveMeterQuery)->filter(function ($item) {
            return collect($item->user_profile)->isNotEmpty();
        });
        $subzones = Subzone::where('deleted', 0)->get();
        return view('users.index', compact('activeMeter', 'inactiveMeter', 'checkZone', 'checkTabwatermeter', 'subzones'));
    }

    public function create($renew = 0, $user_id = 0)
    {
        $user = new User();
        $provinces = Province::all();
        $usercategories = Usercategory::all();
        $last_user_id = User::get()->last(); //UserMeterInfos::get()->last();

        $funcCtrl = new FunctionsController();
        $nextId = collect($last_user_id)->isEmpty() ? 1 : $last_user_id->id + 1;
        $nextMeter = $funcCtrl->createInvoiceNumberString($nextId);
        $zones = Zone::all();

        if ($renew == 1) {
            $user = UserMeterInfos::where('user_id', $user_id)
                ->with('user_profile')
                ->get()->first();
        }
        $tabwatermeters = TabwaterMeter::all(['id', 'typemetername']);
        return view('users.create', compact('user', 'provinces', 'usercategories', 'zones', 'tabwatermeters',
            'nextMeter', 'nextId', 'renew'));
    }

    public function store(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $user_id = "";
        //ผู้ขอใช้น้ำรายใหม่
        // $user_number_next = NumberSequence::get('user_number_next');
        $new_user_number = $request->get('nextId');//$user_number_next[0]->user_number_next;
        $user               = new User();
        $user->username     = 'user' . $new_user_number;
        $user->password     = Hash::make("1234");
        $user->email        = 'user' . $this->generateRandomString(4). $new_user_number . '@gmail.com';
        $user->user_cat_id  = 3;
        $user->created_at   = date('Y-m-d H:i:s');
        $user->updated_at   = date('Y-m-d H:i:s');
        $user->save();

        $user_profile = new UserProfile;
        $user_profile->user_id = $new_user_number;
        $user_profile->name = $request->get('name');
        $user_profile->gender = $request->get('gender');
        $user_profile->id_card = $request->get('id_card');
        $user_profile->phone = $request->get('phone');
        $user_profile->address = $request->get('address');
        $user_profile->province_code = $request->get('province_code');
        $user_profile->district_code = $request->get('district_code');
        $user_profile->tambon_code = $request->get('tambon_code');
        $user_profile->zone_id = $request->get('zone_id');
        $user_profile->subzone_id = $request->get('zone_id');
        $user_profile->created_at = date('Y-m-d H:i:s');
        $user_profile->updated_at = date('Y-m-d H:i:s');
        $user_profile->save();

        $usermeterInfo = new UserMeterInfos;
        $usermeterInfo->user_id = $new_user_number;
        $usermeterInfo->meternumber = $request->get('meternumber');
        $usermeterInfo->metertype = $request->get('metertype');
        $usermeterInfo->counter_unit = $request->get('counter_unit');
        $usermeterInfo->metersize = $request->get('metersize');
        $usermeterInfo->undertake_zone_id = $request->get('undertake_zone_id');
        $usermeterInfo->undertake_subzone_id = $request->get('undertake_subzone_id');

        $date = explode('/', $request->get('acceptance_date'));
        $usermeterInfo->acceptace_date = date('Y') . '-' . $date[1] . '-' . $date[0];
        $usermeterInfo->payment_id = $request->get('payment_id');
        $usermeterInfo->discounttype = $request->get('discounttype');
        $usermeterInfo->recorder_id = Auth::id();
        $usermeterInfo->created_at = date('Y-m-d H:i:s');
        $usermeterInfo->updated_at = date('Y-m-d H:i:s');
        $usermeterInfo->save();
        if($request->get('wanted_rec_curemeter') == 'yes'){
            $current_inv_period = InvoicePeriod::where('status', 'active')->get('id');
            $invoice = new Invoice();
            $invoice->inv_period_id = $current_inv_period[0]->id;
            $invoice->user_id = 3178;//$user->id;
            $invoice->meter_id = 3178;//$user->id;
            $invoice->lastmeter = 0;
            $invoice->currentmeter = 0;
            $invoice->status = 'init';
            $invoice->created_at = date('Y-m-d H:i:s');
            $invoice->updated_at = date('Y-m-d H:i:s');
            $invoice->save();
        }

        //เพิ่ม user_role  = 3
        $user_role = User::find($user->id);
        $user_role->attachRole('user');

        //เพิ่ม user_number_next ใน number sequences table  + 1
        NumberSequence::where('id', 1)->update([
            'user_number_next' => $new_user_number +1
        ]);

        if ($request->get('old_meter_id') > 0) {
            //ทำการ update status ของ old_meter_id ให้เป็นยกเลิกใช้งานถาวร จาก inactive เป็น deleted
            UserMeterInfos::where('user_id', $request->get('old_meter_id'))->update(['status' => 'deleted']);
        }
        return redirect('users')->with(['success' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    private function generateRandomString($length = 4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function test(REQUEST $request)
    {

    }

    public function edit($user_cat_id, $id, $changemeter = 'nochange', $meter_id = 0)
    {
        $funcCtrl = new FunctionsController();
        $user = UserMeterInfos::where('user_id', $id)
            ->with('user_profile')
            ->get()->first();
        $user->acceptace_date = $funcCtrl->engDateToThaiDateFormat($user->acceptace_date);
        $provinces = Province::all();
        $usercategories = Usercategory::all();
        $zones = Zone::all();
        $tabwatermeters = TabwaterMeter::all(['id', 'typemetername']);
        $user_subzone = Subzone::where('zone_id', $user->undertake_zone_id)->get();
        return view('users.edit', compact('user', 'zones', 'provinces', 'usercategories', 'tabwatermeters',
            'user_subzone', 'changemeter'));
    }

    public function update(REQUEST $request, $id)
    {
        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();
        if ($request->get('status') == 'inactive') {
            //ยกเลิกการใช้มิเตอร์  หรือ เปลี่ยนมิเตอร์
            //ให้ทำการยกเลือก UserMeterInfos เดิมก่อน
            $updateInactive = UserMeterInfos::where('user_id', $request->get('user_id'))
                ->update([
                    'status' => $request->get('status'),
                    'deleted' => 1,
                    'comment' => 'ยกเลิกการใช้งาน',
                    'recorder_id' => Auth::id(),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $inactiveUserProfile = UserProfile::where('user_id', $request->get('user_id'))
                ->update([
                    'status' => 0,
                    'deleted' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $inactiveUser = User::where('id', $request->get('user_id'))
                ->update([
                    'status' => 'deleted',
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $inactiveInvoice = DB::table('invoice')
                                ->where('user_id', $id)
                                ->update(['deleted' => 1]);

            $updateInvoiceStatusToOwe = Invoice::where('user_id', $id)
                ->where('deleted', 1)->where('status', 'invoice')
                ->update([
                    'status' => 'owe',
                ]);
            $updateInitStatusToDeleted = Invoice::where('user_id', $id)
                ->where('deleted', 1)->where('status', 'init')
                ->update([
                    'status' => 'deleted',
                ]);

        } else { // แก้ไขข้อมูล UserProfile และ UserMeterInfos (active)
            $user = UserProfile::where('user_id', $request->get('user_id'))->update([
                'name' => $request->get('name'),
                'id_card' => $request->get('id_card'),
                'phone' => $request->get('phone'),
                'gender' => $request->get('gender'),
                'address' => $request->get('address'),
                'zone_id' => $request->get('zone_id'),
                'subzone_id' => $request->get('zone_id'),
                'tambon_code' => $request->get('tambon_code'),
                'district_code' => $request->get('district_code'),
                'province_code' => $request->get('province_code'),
            ]);

            $userMeterInfo = UserMeterInfos::where('user_id', $request->get('user_id'))->update([
                'meternumber' => $request->get('meternumber'),
                'metertype' => $request->get('metertype'),
                'counter_unit' => $request->get('counter_unit'),
                'metersize' => $request->get('metersize'),
                'undertake_zone_id' => $request->get('undertake_zone_id'),
                'undertake_subzone_id' => $request->get('undertake_subzone_id'),
                'status' => $request->get('status'),
                'recorder_id' => Auth::id(),
                'acceptace_date' => $funcCtrl->thaiDateToEngDateFormat($request->get('acceptance_date')),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
// return $request;
        return redirect('users');

    }

    public function update_backup(REQUEST $request, $id)
    {
        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();
        if ($request->get('status') == 'inactive' || $request->get('status') == 'changemeter') {
            //ยกเลิกการใช้มิเตอร์  หรือ เปลี่ยนมิเตอร์
            //ให้ทำการยกเลือก UserMeterInfos เดิมก่อน
            $updateInactive = UserMeterInfos::where('user_id', $id)
                ->update([
                    'status' => $request->get('status') == 'changemeter' ? 'changemeter' : 'inactive',
                    'deleted' => 1,
                    'comment' => $request->get('status') == 'changemeter' ? 'เปลี่ยนมิเตอร์' : 'ยกเลิกการใช้งาน',
                    'recorder_id' => Auth::id(),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $inactiveUserProfile = UserProfile::where('user_id', $request->get('user_id'))
                ->update([
                    'status' => 0,
                    'deleted' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $inactiveUser = User::where('id', $request->get('user_id'))
                ->update([
                    'status' => 'deleted',
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $inactiveInvoice = Invoice::where('user_id', $id)
                ->update([
                    'deleted' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            //  ถ้า $request(status) == 'changemeter' ให้ทำการเพิ่ม invoice รอบบิลปัจจุบัน
            // status = owe //comment = เปลี่ยนมิเตอร์
            if ($request->get('status') == 'changemeter') {
                $current_inv_period = InvoicePeriod::where('status', 'active')->get();
                $last_inv_for_changemeter = new Invoice();

                $last_inv_for_changemeter->inv_period_id = $current_inv_period[0]->id;
                $last_inv_for_changemeter->user_id = $request->get('user_id');
                $last_inv_for_changemeter->meter_id = $request->get('user_id');
                $last_inv_for_changemeter->lastmeter = $request->get('lastmeter_before_changemeter_val');
                $last_inv_for_changemeter->currentmeter = $request->get('currentmeter_before_changemeter_val');
                $last_inv_for_changemeter->status = 'owe';
                $last_inv_for_changemeter->recorder_id = Auth::id();
                $last_inv_for_changemeter->created_at = date('Y-m-d H:i:s');
                $last_inv_for_changemeter->updated_at = date('Y-m-d H:i:s');
                $last_inv_for_changemeter->save();
            }

        } else { // แก้ไขข้อมูล UserProfile และ UserMeterInfos (active)
            $user = UserProfile::where('user_id', $request->get('user_id'))->update([
                'name' => $request->get('name'),
                'id_card' => $request->get('id_card'),
                'phone' => $request->get('phone'),
                'gender' => $request->get('gender'),
                'address' => $request->get('address'),
                'zone_id' => $request->get('zone_id'),
                'subzone_id' => $request->get('zone_id'),
                'tambon_code' => $request->get('tambon_code'),
                'district_code' => $request->get('district_code'),
                'province_code' => $request->get('province_code'),
            ]);

            $userMeterInfo = UserMeterInfos::where('user_id', $request->get('user_id'))->update([
                'meternumber' => $request->get('meternumber'),
                'metertype' => $request->get('metertype'),
                'counter_unit' => $request->get('counter_unit'),
                'metersize' => $request->get('metersize'),
                'undertake_zone_id' => $request->get('undertake_zone_id'),
                'undertake_subzone_id' => $request->get('undertake_subzone_id'),
                'status' => $request->get('status'),
                'recorder_id' => Auth::id(),
                'acceptace_date' => $funcCtrl->thaiDateToEngDateFormat($request->get('acceptance_date')),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        return redirect('users');

    }

    public function show($user_id = 1)
    {
        // phpinfo();
        // $inv = Invoice::where('user_id', $user_id)->get();
        $apiInvoiceCtrl = new InvoiceController();
        $userInvHistory = InvoiceHistory::where('user_id', $user_id)
                    ->with(
                        'invoice_period.budgetyear', function($query){
                        return $query->select('id', 'budgetyear');
                    })
                    ->get();
        $invoice = Invoice::where('user_id', $user_id)
        ->with(
            'invoice_period.budgetyear', function($query){
            return $query->select('id', 'budgetyear');
        })
        ->get();
        $invoice_concatenated = $userInvHistory->concat($invoice);
        $invoice_budgetyear_grouped = collect($invoice_concatenated)->groupBy(function($item){
            return $item->invoice_period->budgetyear->id;
        });
        $user = UserMeterInfos::where('user_id', $user_id)
            ->with([
                'user_profile' => function ($query) {
                    return $query->select('name', 'user_id', 'id_card', 'phone', 'address', 'zone_id');
                },
                'invoice_by_user_id' => function ($query) {
                    return $query->where('status', 'invoice')->orWhere('status', 'paid');
                },
                'invoice.invoice_period',
            ])
            ->get(['id', 'user_id', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id', 'deleted' ]);
        $owes = Invoice::where('user_id', $user_id)
            ->with(
                'invoice_period.budgetyear', function($query){
                return $query->select('id', 'budgetyear');
            })
            ->whereIn('status', ['owe', 'invoice'])
            ->get();
        return view('users.show', compact('user', 'invoice_budgetyear_grouped', 'owes'));
    }

    public function delete($id)
    {
        $userSqlTemp = User::where('id', $id);
        $user = $userSqlTemp->update([
            'status' => 'deleted',
        ]);
        $userPRofileDelete = UserProfile::where('user_id', $id)->update([
            'status' => 0,
            'deleted' => 1,
        ]);
        $userMeterInfosDeleteTemp = UserMeterInfos::where('user_id', $id);

        $meterId = $userMeterInfosDeleteTemp->get();

        $userMeterInfosDeleteTemp->update([
            'status' => 'inactive',
        ]);
        $oweInv = Invoice::where('meter_id', $meterId->id)
            ->where('status', 'invoice')
            ->update([
                'status' => 'owe',
            ]);
        return redirect('users');
    }

    public function userInfos($user_id)
    {
        $sql = DB::table('user_meter_infos as umf')
            ->join('user_profile as uf', 'uf.user_id', '=', 'umf.user_id')
            ->join('zone', 'zone.id', '=', 'umf.undertake_zone_id')
            ->join('subzone', 'subzone.id', '=', 'umf.undertake_subzone_id')
            ->where('umf.status', '=', 'active')
            ->select('umf.meternumber',
                'umf.user_id',
                'uf.name', 'uf.address',
                'zone.zone_name',
                'subzone.subzone_name'
            );
        return $sql;
    }
}
