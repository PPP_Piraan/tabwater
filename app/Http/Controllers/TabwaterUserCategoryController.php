<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TabwaterUserCategory;
class TabwaterUserCategoryController extends Controller
{
    public function index(){
        $tab_user_cate = TabwaterUserCategory::all();
        return view('settings.tabwater_user_category.index', \compact('tab_user_cate'));
    }

    public function create(){
        return view('settings.tabwater_user_category.create');
    }

    public function store(REQUEST $request){
        date_default_timezone_set('Asia/Bangkok');
        foreach($request->get('tabwatermetertype') as $tabwatermetertype){
            $tabCat = new TabwaterUserCategory();
            $tabCat->name = $tabwatermetertype['type'];
            $tabCat->unitname = $tabwatermetertype['unit_name'];
            $tabCat->price_per_unit = $tabwatermetertype['price_per_unit'];
            $tabCat->unit_num = $tabwatermetertype['unit_num'];
            $tabCat->created_at = date('Y-m-d H:i:s');
            $tabCat->updated_at = date('Y-m-d H:i:s');

            $tabCat->save();
        }

        return redirect('tabwater_user_category');
    }

    public function edit($id){
        return view('settings.tabwater_user_category.edit', \compact('id'));
    }
    
}
