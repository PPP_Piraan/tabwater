<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usercategory;

class UserCategoryController extends Controller
{
    public function index(){
        $usercategory = Usercategory::all();
        return view('usercategory.index', compact('usercategory'));
    }

    public function create(){
        $usercategory = new Usercategory;
        return view('usercategory.create', compact('usercategory'));
    }

    public function store(REQUEST $request){
        date_default_timezone_set('Asia/Bangkok');

        $storeUserCategory = new Usercategory;
        $storeUserCategory->user_cate_name = $request->get('usercategory_name');
        $storeUserCategory->created_at = date('Y-m-d H:i:s');
        $storeUserCategory->updated_at = date('Y-m-d H:i:s');
        $storeUserCategory->save();
        
        return redirect('usercategory')->with(['massage' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    public function update($id){
        $usercategory = Usercategory::find($id);
        return view('usercategory.update', compact('usercategory'));
    }

    public function edit(REQUEST $request , $id){
        date_default_timezone_set('Asia/Bangkok');

        $usercategory = Usercategory::find($id);
        $usercategory->user_cate_name = $request->get('usercategory_name');
        $usercategory->updated_at = date('Y-m-d H:i:s');
        $usercategory->save();
        return redirect('usercategory')->with(['massage' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    public function delete($id){
        $usercategory = Usercategory::find($id);
        $usercategory->delete();
        return redirect('usercategory')->with(['massage' => 'ทำการลบข้อมูลเรียบร้อยแล้ว']);

    }
}
