<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadBankSlip extends Model
{
    protected $table = 'upload_bank_slip';

    public function user_profile()
    {
        return $this->belongsTo('App\UserProfile', 'user_id', 'user_id');
    }

}