<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'inv_period_id',
    ];
    protected $table = 'invoice';

    public function invoice_period()
    {
        return $this->belongsTo('App\InvoicePeriod', 'inv_period_id', 'id');
    }
    public function usermeterinfos()
    {
        return $this->belongsTo('App\UserMeterInfos', 'meter_id_fk', 'meternumber');
    }

    public function accounting()
    {
        return $this->belongsTo('App\Accounting', 'receipt_id');
    }

}
