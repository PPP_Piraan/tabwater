<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profile';

    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function province(){
        return $this->hasOne('App\Province', 'province_code', 'province_code');
    }

    public function district(){
        return $this->hasOne('App\District', 'district_code', 'district_code');
    }

    public function tambon(){
        return $this->hasOne('App\Tambon', 'tambon_code', 'tambon_code');
    }

    public function zone(){
        return $this->belongsTo('App\Zone', 'zone_id', 'id');
    }
    public function subzone(){
        return $this->belongsTo('App\Subzone', 'subzone_id', 'id');
    }


    public function userMeterInfos(){
        return $this->hasone('App\UserMeterInfos', 'user_id_fk', 'user_id');
    }

}
