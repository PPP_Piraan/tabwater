<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Accounting extends Model
{
    protected $table = 'accounting';

    public function invoice(){
        return $this->hasMany('App\Invoice','receipt_id', 'id');
    }    
}
