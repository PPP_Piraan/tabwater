<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Accounting extends Model
{
    protected $table = 'accounting';

    public function invoice(){
        return $this->hasMany('App\Invoice','receipt_id', 'id');
    }    
    public function user_profile(){
        return $this->belongsTo('App\UserProfile','cashier', 'user_id');
    }  
    public function cashier_info(){
        return $this->belongsTo('App\UserProfile','cashier', 'user_id');
    }   
}
