<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tambon extends Model
{
    protected $table = 'tambon';

    public function district(){
        return $this->belongsTo('App\District','district_code','district_code');
    }

    public function zone(){
        return $this->hasMany('App\Zone', 'tambon_code', 'tambon_code');
    }
}
