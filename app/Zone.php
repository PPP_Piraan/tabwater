<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'zone';

    public function subzone(){
        return $this->hasMany('App\Subzone', 'zone_id');
    }

    public function tambon(){
        
        return $this->belongsTo('App\Tambon','tambon_code','tambon_code');
    }
}
