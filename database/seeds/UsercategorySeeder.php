<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsercategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usercategory')->insert([
            ['user_cate_name' => 'ผู้ดูแลระบบ'],
            ['user_cate_name' => 'เจ้าหน้าที่การเงิน'],
            ['user_cate_name' => 'สมาชิกผู้ใช้น้ำประปา'],
            ['user_cate_name' => 'เจ้าหน้าที่จดมิเตอร์']
        ]);
    }
}
