<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('user_profile')->insert([
          'user_id' => 1,
          'name' => 'ผู้ดูแลระบบ',
          'id_card' => '0000000000000',
          'phone' => '0000000000',
          'gender' => 'm',
          'address' => '00',
          'zone_id' => 1,
          'subzone_id' => 1,
          'tambon_code' =>350805,
          'district_code' =>3508,
          'province_code' =>35
      ]);

      DB::table('role_user')->insert([
        'role_id' => 1,
        'user_id' => 1,
        'user_type' => 'App\User'
      ]);
        
    }
}
