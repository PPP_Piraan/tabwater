<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(LaratrustSeeder::class);
        $this->call([UsersTableSeeder::class]);
        // $this->call([UserProfileSeeder::class]);
        // $this->call([UsercategorySeeder::class]);
        // $this->call([TambonSeeder::class]);
        // $this->call([ProvinceSeeder::class]);
    }
}
