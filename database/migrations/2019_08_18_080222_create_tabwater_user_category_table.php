<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabwaterUserCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabwater_use_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('unitname');
            $table->float('price_per_unit', 5,2);
            $table->float('unit_num', 5,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabwater_user_category');
    }
}
