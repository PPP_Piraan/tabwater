<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_temp', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('inv_period_id');
                $table->integer('month')->default(0);
                $table->integer('user_id');
                $table->integer('meter_id');
                $table->float('lastmeter');
                $table->float('currentmeter');
                $table->enum('status', ['init','invoice', 'paid', 'owe','deleted']);
                $table->string('receipt_id')->default(0);
                $table->integer('deleted')->default(0);
                $table->integer('printed_time');
                $table->string('comment')->nullable();
                $table->integer('recorder_id');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_temp');
    }
}
