<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('inv_period_id');
            $table->unsignedBigInteger('meter_id_fk');
            $table->float('lastmeter',8,2);
            $table->float('currentmeter',8,2);
            $table->float('water_used',8,2);
            $table->float('paid',8,2);
            $table->float('vat',8,2);
            $table->float('net_paid',8,2)->comment('paid+vat');
            $table->enum('invioce_type', ['use_water', 'reserve'])->comment('reserve = รักษามิเตอร์(water_used เป็น 0)');
            $table->enum('status', ['init','invoice', 'paid', 'owe','deleted', 'no_record']);
            $table->string('receipt_id')->default(0);
            $table->integer('deleted')->default(0);
            $table->integer('printed_time');
            $table->string('comment')->nullable();
            $table->integer('recorder_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
