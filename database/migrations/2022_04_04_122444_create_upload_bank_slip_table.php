<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadBankSlipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_bank_slip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->comment('user_id ผู้ค้าง');
            $table->text('line_id')->comment('line_id ผู้ส่งสลืป');
            $table->text('invoice_id_list')->comment('รายการใบแจ้งหนี้เก็บเป็น json');
            $table->float('mustpaid');
            $table->text('image');
            $table->integer('deleted')->comment('0 ปกติ 1 ลบ');
            $table->enum('status', [0, 1, 2])->comment('0=ยกเลิก, 1=รอตรวจสอบ, 2= ตรวจสอบแล้วถูกต้อง	');
            $table->integer('recorder_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_bank_slip');
    }
}