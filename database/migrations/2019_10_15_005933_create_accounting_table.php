<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('acc_sum_paid',8,2)->comment('ยอดจ่ายจริง');
            $table->float('acc_sum_vat',8,2)->comment('ผลรวมของ vat ในกลุ่ม invoice');
            $table->float('acc_sum_net_paid',8,2)->comment('ผลรวมของ vat + acc_sum_paid ในกลุ่ม invoice');
            $table->integer('cashier');
            $table->text('comment');
            $table->enum('status',['active', 'inactive', 'deleted']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting');
    }
}
