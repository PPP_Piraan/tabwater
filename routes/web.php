<?php

Route::get('/', function () {
    Session::flush();
    Auth::logout();
    return redirect()->route('login');
});
Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});


Route::get('/line_liff/index', 'LineLiffController@index');
Route::get('/line_liff/history', 'LineLiffController@history');
Route::get('/line_liff/upload_slip', 'LineLiffController@upload_slip');
Route::post('/line_liff/save', 'LineLiffController@save');
Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::prefix('invoice')->group(function () {
        Route::get('/index/{budgetyear?}/{inv_period?}', 'InvoiceController@index');
        Route::get('/paid/{id}', 'InvoiceController@paid');
        Route::get('/invoiced_lists/{id}', 'InvoiceController@invoiced_lists');
        Route::get('/print_multi_invoice/{id}', 'InvoiceController@print_multi_invoice');
        Route::get('/create/{invoice_id}', 'InvoiceController@create');
        Route::get('/edit/{invoice_id}', 'InvoiceController@edit');
        Route::put('/update/{invoice_id}', 'InvoiceController@update');
        Route::get('/zone_info/{zone}', 'InvoiceController@zone_info');
        Route::get('/zone_create/{zone}/{new_user?}', 'InvoiceController@zone_create');
        Route::post('/zone_store', 'InvoiceController@zone_store');
        Route::get('/zone_edit/{zone}', 'InvoiceController@zone_edit');
        Route::put('/zone_update/{zone_id}', 'InvoiceController@zone_update');
        Route::post('/print_multi_invoice', 'InvoiceController@print_multi_invoice');
        Route::get('/search_from_meternumber/{meternumber}/{zone_id}', 'InvoiceController@search_from_meternumber');
        Route::post('/store', 'InvoiceController@store');
        Route::get('/show/{id}', 'InvoiceController@show');
        Route::get('/delete/{inv_id}/{comment}', 'InvoiceController@delete');
        Route::get('/invoice_by_user/{type?}/{status}/{user_id}', 'InvoiceController@owe_by_user');
        Route::get('/invoice_edit/{invoice_id}', 'InvoiceController@invoice_edit');
        Route::get('/test/{subzone_id}/{new_userstatus}', 'InvoiceController@test');
        Route::post('/zone_create_for_new_users', 'InvoiceController@zone_create_for_new_users');

    });

    Route::prefix('invoice_period')->group(function () {
        Route::get('/', 'InvoicePeriodController@index');
        Route::get('/create', 'InvoicePeriodController@create');
        Route::post('/store', 'InvoicePeriodController@store');
        Route::post('/store_invoice_init_status', 'InvoicePeriodController@store_invoice_init_status');
        Route::get('/edit/{id}', 'InvoicePeriodController@edit');
        Route::put('/update/{invoice_period_id}', 'InvoicePeriodController@update');
        Route::get('/delete/{id}', 'InvoicePeriodController@delete');
        Route::get('/create_invoices/{id}', 'InvoicePeriodController@create_invoices');
        Route::get('/test', 'InvoicePeriodController@test');
    });

    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::prefix('admin')->group(function () {
        Route::get('/payment/{id}', 'InvoiceController@payment');
        Route::get('/paymenthistory/{id}', 'AdminController@paymenthistory');
        Route::get('/overdue/{id}', 'AdminController@overdue');
        Route::get('/print_invoice/{id}', 'AdminController@print_invoice');
        Route::get('/create', 'AdminController@create');

    });

    Route::prefix('users')->group(function () {
        Route::get('/', 'UsersController@index');
        Route::get('/create/{renew?}/{user_id?}', 'UsersController@create');
        Route::post('/store', 'UsersController@store');
        Route::put('/update/{user_id}', 'UsersController@update');
        Route::get('/edit/{user_cat_id}/{id}/{changemeter?}/{meter_id?}', 'UsersController@edit');
        Route::get('/delete/{id}', 'UsersController@delete');
        Route::get('/show/{id}', 'UsersController@show');
        Route::get('/test', 'UsersController@test');
    });

    Route::prefix('role_and_permission')->group(function () {
        Route::get('/', 'RolesController@index');
        Route::get('/create', 'RolesController@create');
        Route::post('/store', 'RolesController@store');
        Route::get('/edit/{id}', 'RolesController@edit');

    });

    Route::prefix('settings')->group(function () {
        Route::get('/', 'SettingsController@index');
        Route::get('/settings/budgetyear', 'SettingsController@budgetyear');
        Route::post('/create_and_update', 'SettingsController@create_and_update');
        Route::get('/edit', 'SettingsController@edit');
        Route::get('/import_excel/{import_type?}', 'SettingsController@import_excel');
        Route::post('/upload_excel', 'SettingsController@upload_excel');



    });

    Route::prefix('upload_bank_slip')->group(function () {
        Route::get('/index', 'UploadBankSlipController@index');
        Route::get('/print/{id}', 'UploadBankSlipController@print');

    });

    Route::prefix('tabwatermeter')->group(function () {
        Route::get('/', 'TabwaterMeterController@index');
        Route::get('/create', 'TabwaterMeterController@create');
        Route::get('/{id}/edit', 'TabwaterMeterController@edit');
        Route::get('/infos/{id}', 'TabwaterMeterController@infos');
        Route::post('/store', 'TabwaterMeterController@store');
        Route::put('/{id}/update', 'TabwaterMeterController@update');
        Route::get('/{id}/delete', 'TabwaterMeterController@delete');
    });

    //ประเภทผู้ใช้น้ำ
    Route::get('/tabwater_user_category', 'TabwaterUserCategoryController@index');
    Route::get('/tabwater_user_category/create', 'TabwaterUserCategoryController@create');
    Route::post('/tabwater_user_category/store', 'TabwaterUserCategoryController@store');
    Route::get('/tabwater_user_category/{id}/edit', 'TabwaterUserCategoryController@edit');

    Route::prefix('undertaker_subzone')->group(function () {
        Route::get('/', 'UndertakerSubzoneController@index');
        Route::get('/create', 'UndertakerSubzoneController@create');
        Route::post('/store', 'UndertakerSubzoneController@store');
        Route::get('/update/{id}', 'UndertakerSubzoneController@update');
        Route::get('/edit/{id}', 'UndertakerSubzoneController@edit');
        Route::get('/delete/{id}', 'UndertakerSubzoneController@delete');

    });
    //Province
    Route::get('province/getProvince', 'ProvinceController@getProvince');

    //District
    Route::get('district/getDistrict/{province_code}', 'DistrictController@getDistrict');

    //Tambon
    Route::get('tambon/getTambon/{district_code}', 'TambonController@getTambon');

    Route::prefix('zone')->group(function () {
        Route::get('/', 'ZoneController@index');
        Route::get('/create', 'ZoneController@create');
        Route::post('/store', 'ZoneController@store');
        Route::get('/update/{id}', 'ZoneController@update');
        Route::put('/edit/{id}', 'ZoneController@edit');
        Route::get('/delete/{id}', 'ZoneController@delete');
        Route::get('/getZone/{tambon_code}', 'ZoneController@getZone');
    });

    Route::prefix('changemeter')->group(function () {
        Route::get('/', 'ChangemeterController@index');
    });

    Route::prefix('subzone')->group(function () {
        Route::get('/', 'SubzoneController@index');
        Route::get('/create', 'SubzoneController@create');
        Route::post('/store', 'SubzoneController@store');
        Route::get('/edit/{id}', 'SubzoneController@edit');
        Route::put('/update/{id}', 'SubzoneController@update');
        Route::get('/delete/{id}', 'SubzoneController@delete');
        Route::get('/add_staff_in_subzone', 'SubzoneController@add_staff_in_subzone');
        Route::get('/users_in_subzone_create/{subzone_id}', 'SubzoneController@users_in_subzone_create');
        Route::get('/getSubzone/{zone_id}', 'SubzoneController@getSubzone');
    });
    Route::prefix('staff')->group(function () {
        Route::get('/', 'StaffController@index');
        Route::get('/create', 'StaffController@create');
        Route::post('/store', 'StaffController@store');
        Route::put('/update/{user_id}', 'StaffController@update');
        Route::get('/edit/{id}', 'StaffController@edit');
        Route::get('/delete/{id}', 'StaffController@delete');
        Route::get('/show/{id}', 'StaffController@show');
    });

    Route::prefix('usercategory')->group(function () {
        Route::get('/', 'UserCategoryController@index');
        Route::get('/create', 'UserCategoryController@create');
        Route::post('/store', 'UserCategoryController@store');
        Route::get('/update/{id}', 'UserCategoryController@update');
        Route::put('/edit/{id}', 'UserCategoryController@edit');
        Route::get('/delete/{id}', 'UserCategoryController@delete');
    });

    Route::prefix('budgetyear')->group(function () {
        Route::get('/', 'BudgetYearController@index');
        Route::get('/create', 'BudgetYearController@create');
        Route::post('/store', 'BudgetYearController@store');
        Route::put('/update/{id}', 'BudgetYearController@update');
        Route::get('/edit/{id}', 'BudgetYearController@edit');
        Route::get('/delete/{id}', 'BudgetYearController@delete');
    });

    Route::prefix('payment')->group(function () {
        Route::get('/', 'PaymentController@index');
        Route::get('/search', 'PaymentController@search');
        Route::post('/create', 'PaymentController@create');
        Route::post('/store', 'PaymentController@store')->name('payment.store');
        Route::get('/print_payment_history/{receipt_id}', 'PaymentController@print_payment_history');
        Route::get('/remove/{receipt_id}', 'PaymentController@remove');
        Route::get('/paymenthistory/{inv_period?}/{subzone_id?}', 'PaymentController@paymenthistory');
        Route::get('/receipt_print/{receipt_id}', 'PaymentController@receipt_print');
        Route::get('/receipted_list/{user_id}', 'PaymentController@receipted_list');
        Route::get('/test', 'PaymentController@test');
        Route::get('/upload_slip', 'PaymentController@upload_slip');
        Route::get('/autocompletesearch', 'PaymentController@autocompleteSearch');
        Route::post('/index_search_by_suzone','PaymentController@index_search_by_suzone')->name('payment.index_search_by_suzone');

    });

    Route::prefix('reports')->group(function () {
        Route::get('/', 'ReportsController@index');
        Route::get('/users', 'ReportsController@users');
        Route::get('/owe', 'ReportsController@owe');
        Route::post('/owe', 'ReportsController@owe');
        Route::get('/payment', 'ReportsController@payment');
        Route::get('/ledger', 'ReportsController@ledger');
        Route::get('/usedtabwater_infos', 'ReportsController@usedtabwater_infos');
        Route::get('/invoicereport', 'ReportsController@invoicereport');
        Route::get('/search', 'ReportsController@search');
        Route::get('/daily_receipt/{date?}', 'ReportsController@daily_receipt');
        Route::get('/water_used', 'ReportsController@water_used');
        Route::get('/meter_record_history/{budgetyear?}/{zone_id?}', 'ReportsController@meter_record_history');
        Route::get('/summary', 'ReportsController@summary');
        Route::get('/cutmeter', 'ReportsController@cutmeter');
    });

    Route::prefix('cutmeter')->group(function () {
        Route::get('/index/{subzone_id?}', 'CutmeterController@index');
        Route::get('/edit/{user_id}', 'CutmeterController@edit');
        Route::get('/create/{user_id}', 'CutmeterController@create');
        Route::get('/reset_cutmeter_status/{user_id}', 'CutmeterController@reset_cutmeter_status');
        Route::post('/store', 'CutmeterController@store');
        Route::put('/update/{user_id}', 'CutmeterController@update');
    });

    Route::prefix('owepaper')->group(function () {
        Route::get('/index', 'OwepaperController@index');
        Route::post('/print', 'OwepaperController@print');
    });

    Route::prefix('user_meter_infos')->group(function () {
        Route::get('/find_subzone/{zone_id}/{subzone_id?}', 'UserMeterInfosController@find_subzone');

    });

    Route::prefix('test')->group(function () {
        Route::get('/', 'TestController@index');
    });

});

Route::get('storetest', 'PaymentController@storetest');
Route::get('laratrust_user', function () {
    \Laratrust::ability('admin,owner', 'create-post,edit-user');
});
