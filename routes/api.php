<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('users')->group(function () {
    Route::get('/', 'Api\UsersController@index');
    Route::get('/users', 'Api\UsersController@users');
    Route::post('/users_post', 'Api\UsersController@users_post');
    Route::get('/user/{user_id}', 'Api\UsersController@user');
    Route::get('/check_line_id/{id}', 'Api\UsersController@check_line_id');
    Route::get('/update_line_id/{user_id}/{line_id}', 'Api\UsersController@update_line_id');
    Route::get('/search/{val}/{type?}', 'Api\UsersController@search');
    Route::post('/search2', 'Api\UsersController@search2');
    Route::get('/search2_get', 'Api\UsersController@search2_get');

    Route::get('/by_zone/{zone_id}', 'Api\UsersController@by_zone');
    Route::get('/report_by_subzone/{zone_id}', 'Api\UsersController@report_by_subzone');
    Route::get('/findsearchselected/{val}', 'Api\UsersController@findsearchselected');
    Route::get('/{user_cat_id}/{twman_id}', 'Api\UsersController@users_by_subzone');
    Route::get('/usersbycategory/{cate_id}', 'Api\UsersController@usersbycategory');
    Route::post('/store', 'Api\UsersController@store');
    Route::get('/set_session_id/{user_id}/{session_id}', 'Api\UsersController@set_session_id');
    Route::get('/init_settings', 'Api\UsersController@init_settings');

});

Route::prefix('subzone')->group(function () {
    Route::get('/{zone_id}', 'Api\SubzoneController@subzone');
    Route::get('/delete/{id}', 'Api\SubzoneController@delete');
    Route::get('/get_members_subzone_infos/{zone_id}', 'Api\SubzoneController@get_members_subzone_infos');
    Route::get('/get_members_last_inactive_invperiod/{zone_id}', 'Api\SubzoneController@get_members_last_inactive_invperiod');
});

Route::prefix('upload_bank_slip')->group(function () {
    Route::get('count/{status?}', 'Api\UploadBankSlipController@count');
});

Route::prefix('reports')->group(function () {
    Route::get('/invoicereport', 'Api\ReportsController@invoicereport');
    Route::get('/get_used_water', 'Api\ReportsController@get_used_water');
    Route::get('/users/{zone_id?}/{subzone_id?}', 'Api\ReportsController@users');
    Route::get('/owe', 'Api\ReportsController@owe');
    Route::get('/cutmeter', 'Api\ReportsController@cutmeter');
    Route::get('/daily_receipt/{date}/{month}/{year}', 'Api\ReportsController@daily_receipt');
    Route::get('/payment_summary', 'Api\ReportsController@payment_summary');
    Route::get('/meter_record_history/{budgetyear?}/{zone_id?}', 'Api\ReportsController@meter_record_history');
    Route::get('/meter_record_history_count/{budgetyear?}/{zone_id?}', 'Api\ReportsController@meter_record_history_count');
    Route::get('/water_used', 'Api\ReportsController@water_used');

});

Route::prefix('zone')->group(function () {
    Route::get('/', 'Api\ZoneController@index');
    Route::get('/delete/{id}', 'Api\ZoneController@delete');
    Route::get('/getzone_and_subzone', 'Api\ZoneController@getZoneAndSubzone');
    Route::get('/users_by_zone/{zone_id}', 'Api\ZoneController@users_by_zone');
    Route::get('/undertakenZoneAndSubzone/{id}', 'Api\ZoneController@undertakenZoneAndSubzone');
});

Route::prefix('tabwatermeter')->group(function () {
    Route::get('/', 'Api\TabwaterMeterController@index');
    Route::get('/{id}/edit', 'Api\TabwaterMeterController@edit');
    Route::post('/store', 'Api\TabwaterMeterController@store');
    Route::put('/{id}/update', 'Api\TabwaterMeterController@update');
    Route::get('/{id}/delete', 'Api\TabwaterMeterController@delete');
    Route::get('/checkTabwatermeterMatchedUserMeterInfos/{tabwatermeter_id}', 'Api\TabwaterMeterController@checkTabwatermeterMatchedUserMeterInfos');
});

//ประเภทผู้ใช้น้ำ
Route::prefix('tabwater_user_category')->group(function () {
    Route::get('/', 'Api\TabwaterUserCategoryController@index');
    Route::get('/{id}/edit', 'Api\TabwaterUserCategoryController@edit');
    Route::post('/store', 'Api\TabwaterUserCategoryController@store');
    Route::put('/{id}/update', 'Api\TabwaterUserCategoryController@update');
    Route::get('/{id}/delete', 'Api\TabwaterUserCategoryController@delete');
});

Route::prefix('invoice')->group(function () {
    Route::put('/update/{invoice_id}', 'Api\InvoiceController@update');
    Route::post('/update2', 'Api\InvoiceController@update2');
    Route::post('/create', 'Api\InvoiceController@create');
    Route::post('/create_for_mobile_app', 'Api\InvoiceController@create_for_mobile_app');
    // Route::get('/create_for_mobile_app', 'Api\InvoiceController@create_for_mobile_app');

    Route::get('/', 'Api\InvoiceController@index');
    Route::get('/getLastInvoice/{user_id}', 'Api\InvoiceController@getLastInvoice');
    Route::get('/{user_id}', 'Api\InvoiceController@get_user_invoice');
    Route::get('/invoice_history_current_budget_year/{user_id}', 'Api\InvoiceController@invoice_history_current_budget_year');
    Route::get('/paid_invoice/{invoice_id}', 'Api\InvoiceController@paid_invoice');
    Route::get('/print/{id_array}', 'Api\InvoiceController@print');
    Route::get('/totalWaterByInvPeriod/{inv_id}', 'Api\InvoiceController@totalWaterByInvPeriod');
    Route::get('/totalWaterByInvPeriodAndSubzone/{inv_id}/{subzone_id}', 'Api\InvoiceController@totalWaterByInvPeriodAndSubzone');
    Route::get('/get_user_invoice_by_invId_and_mode/{inv_id}/{mode}', 'Api\InvoiceController@get_user_invoice_by_invId_and_mode');
    Route::get('/checkInvoice/{inv_id}', 'Api\InvoiceController@checkInvoice');
    Route::get('/zone_edit/{zone}', 'Api\InvoiceController@zone_edit');
    Route::get('/invoiced_lists/{subzone_id}', 'Api\InvoiceController@invoiced_lists');
});

Route::prefix('payment')->group(function () {
    Route::get('/history/{meter_id}/{from?}', 'Api\PaymentController@history');
    Route::get('/history2/{meter_id}/{from?}', 'Api\PaymentController@history2');
    Route::get('/users', 'Api\PaymentController@users');
    Route::get('/receipted_list/{user_id}', 'Api\PaymentController@receipted_list');

});

Route::prefix('budgetyear')->group(function () {
    Route::post('/store', 'Api\BudgetYearController@store');
    Route::get('/edit', 'Api\BudgetYearController@edit');
    Route::post('/update', 'Api\BudgetYearController@update');
    Route::get('/find_active_status', 'Api\BudgetYearController@find_active_status');
});

Route::prefix('invoice_period')->group(function () {
    Route::get('/prensent_budgetyear', 'Api\InvoicePeriodController@prensent_budgetyear');
    Route::post('/store', 'Api\InvoicePeriodController@store');
    Route::get('/edit/{id}', 'Api\InvoicePeriodController@edit');
    Route::post('/update', 'Api\InvoicePeriodController@update');
    Route::get('/inv_period_lists/{budgetyear_id}', 'Api\InvoicePeriodController@inv_period_lists');
    Route::get('/check_invoice_period_by_budgetyear/{budgetyear_id}', 'Api\InvoicePeriodController@check_invoice_period_by_budgetyear');
});

Route::prefix('owepaper')->group(function () {
    Route::get('/index', 'Api\OwepaperController@index');
    Route::get('/owe', 'Api\OwepaperController@owe');
    Route::get('/get_reciepting', 'Api\OwepaperController@get_reciepting');
    Route::get('/user_owe_infos/{user_id}', 'Api\OwepaperController@user_owe_infos');
    Route::get('/test', 'Api\OwepaperController@test');
    Route::get('/testIndex', 'Api\OwepaperController@testIndex');
    Route::get('/testIndexFilterOweAndInvoice', 'Api\OwepaperController@testIndexFilterOweAndInvoice');
    Route::get('/indexFilterOweAndInvoiceCount', 'Api\OwepaperController@indexFilterOweAndInvoiceCount');
    Route::get('/testOweAndInvoioceDivideBySubzone', 'Api\OwepaperController@testOweAndInvoioceDivideBySubzone');
    Route::get('/OwepaperController/{user_id}', 'Api\OwepaperController@oweAndInvoiceByUserId');

});

Route::prefix('cutmeter')->group(function () {
    Route::get('/index/{zone_id?}/{subzone_id?}', 'Api\CutmeterController@index');
    Route::get('/owe', 'Api\CutmeterController@owe');
    Route::get('/get_reciepting', 'Api\CutmeterController@get_reciepting');
    Route::get('/user_owe_infos/{user_id}', 'Api\CutmeterController@user_owe_infos');
    Route::get('/get_cutmeter_history/{user_id}', 'Api\CutmeterController@get_cutmeter_history');
    Route::get('/get_process_history/{user_id}/{inv_period_id}', 'Api\CutmeterController@get_process_history');
    Route::get('/count', 'Api\CutmeterController@count');
    Route::get('/test', 'Api\CutmeterController@test');
    Route::get('/getOweOver3CountDivideBySubzone', 'Api\CutmeterController@getOweOver3CountDivideBySubzone');

});

Route::prefix('settings')->group(function () {
    Route::get('/get_values/{values_name}', 'Api\SettingsController@get_values');

});

Route::prefix('functions')->group(function () {
    Route::post('/forget_session', 'Api\FunctionsController@forget_session');

});
Route::post('/authen', 'Api\UsersController@authen');
Route::post('/staff_authen', 'Api\UsersController@staff_authen');
Route::post('/trash', 'Api\TrashController@store');
Route::get('/usercategory', 'Api\UserCategoryController@index');

Route::post('/sakon', 'Api\SakonController@index');

Route::get('/test2', 'Api\InvoiceController@test2');
