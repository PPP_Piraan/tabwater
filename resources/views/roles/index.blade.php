@extends('layouts.adminlte')

@section('mainheader')
 กฎและสิทธิ์
@endsection
@section('role_and_permission')
    active
@endsection
@section('nav')
<a href="{{url('/role_and_permission')}}"> กฎและสิทธิ์การเข้าใช้งานระบบ</a>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{ url('role_and_permission/create') }}" class="btn btn-info">เพิ่มกฏ</a> 
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>กฏ</th>
                        <th>ชื่อกฏที่แสดง</th>
                        <th>คำอธิบาย</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $item)
                    <tr>
                        <th>{{ $item->name }}</th>
                        <th>{{ $item->display_name }}</th>
                        <th>{{ $item->description }}</th>
                        <th>
                            <a href="{{ url('role_and_permission/edit/'.$item->id) }}" class="btn btn-warning">แก้ไข</a>
                            <a href="{{ url('role_and_permission/delete/'.$item->id) }}" class="btn btn-danger">ลบ</a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection