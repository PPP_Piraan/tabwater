@extends('layouts.adminlte')

@section('mainheader')
เพิ่มข้อมูลประปา
 {{$member_not_yet_recorded_present_inv_period[0]->zone['zone_name']}}
    @if (!isset($member_not_yet_recorded_present_inv_period[0]->invoice[0]->invoice_period))
    [รอบบิล {{$member_not_yet_recorded_present_inv_period[0]->invoice[0]->inv_period_name}}]
    @else
    [รอบบิล {{$member_not_yet_recorded_present_inv_period[0]->invoice[0]->invoice_period->inv_period_name}}]
    @endif

@endsection
@section('invoice')
    active
@endsection
@section('nav')
<a href="{{url('/invoice/index')}}"> งานประปา</a>
@endsection
@section('style')
    <style>
        .username{
            color: blue;
            cursor: pointer;
            padding-left: 20px
        }
        table {
            table-layout: fixed;
            word-wrap: break-word;
        }
        table th, table td {  min-width: 80px; }
        table td.name{ min-width: 200px;}
        .meter_reserve_price{ min-width: 100px;}

    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-body table-responsive">
        <form action="{{url('invoice/store')}}" method="POST">

            @csrf
            <input type="hidden" name="data_id" value="{{$member_not_yet_recorded_present_inv_period[0]->zone['id']}}">
            <input type="hidden" name="invoice_period_id" value="{{$presentInvoicePeriod->id}}">
            <input type="submit" class="btn btn-primary col-2 mb-3" id="print_multi_inv" value="บันทึก">

            <table class="table  table-striped datatable w-auto" id="DivIdToPrint">
                <thead class="bg-light">
                    <tr>
                        <th></th>
                        <th class="text-center">เลขมิเตอร์</th>
                        <th class="text-center">ชื่อ-สกุล</th>
                        <th class="text-center">บ้านเลขที่</th>
                        <th class="text-center">ยกยอดมา</th>
                        <th class="text-center">มิเตอร์ปัจจุบัน</th>
                        <th class="text-center">จำนวนสุทธิ<div>(หน่วย)</div></th>
                        <th class="text-center meter_reserve_price">ค่ารักษามาตร<div>(บาท)</div></th>
                        <th class="text-center">เป็นเงิน<div>(บาท)</div></th>
                        <th class="text-center">vat {{ $vat->values }}%<div>(บาท)</div></th>
                        <th class="text-center">รวมทั้งสิ้น<div>(บาท)</div></th>
                    </tr>
                </thead>

                <tbody id="app">
                    <?php $i=1;?>
                    @foreach ($member_not_yet_recorded_present_inv_period as $item)
                    <tr data-id="{{$i}}" class="data">
                        <th class="text-center" width="2%">
                            <a href="javascript:void(0)" class="btn btn-outline-warning delbtn2" onclick="del('{{$item->invoice[0]->iv_id}}')">
                                <i class="fas fa-trash"></i>
                            </a>
                        </th>
                        <td class="border-0 text-center">
                            {{$item->meternumberStr}}
                            <input type="hidden" value="{{ $item->invoice[0]->iv_id }}"  name="data[{{$i}}][iv_id]">
                            <input type="hidden" value="{{$item->meternumber}}" name="data[{{$i}}][meternumber]"
                            data-id="{{$i}}" id="meternumber{{$i}}" class="form-control text-right meternumber border-primary text-sm text-bold" readonly>
                        </td>
                        <td class="border-0 text-left name">
                            @if ($item->user_profile != null)
                            <span class="username" placeholder="กดดูประวัติมิเตอร์น้ำ" data-user_id="{{$item->user_profile['user_id']}}">{{$item->user_profile['prefix']."".$item->user_profile['name']}}</span>
                            <input type="hidden" name="data[{{$i}}][user_id]" value="{{$item->user_profile['user_id']}}">
                            @else
                                {{"sdsda<br>".dd($item)}}
                            @endif
                        </td>
                        <td class="text-center">
                            {{$item->user_profile['address']}}
                            <input type="hidden" readonly class="form-control " value="{{$item->user_profile['address']}}" name="data[{{$i}}][address]">
                        </td>
                        @if (collect($item->invoice_last_inctive_inv_period)->isEmpty())
                        {{'sss<br>'.dd($item)}}

                        @endif
                        <td class="border-0">
                            <input type="text" value="{{$item->invoice[0]['lastmeter']}}" name="data[{{$i}}][lastmeter]"
                            data-id="{{$i}}" id="lastmeter{{$i}}" class="form-control text-right lastmeter" >
                        </td>
                        <td class="border-0 text-right">
                            <input type="text" value="" name="data[{{$i}}][currentmeter]" data-id="{{$i}}"
                            id="currentmeter{{$i}}" class="form-control text-right currentmeter border-success">
                        </td>
                        <td class="border-0 text-right">
                            <input type="text" readonly class="form-control text-right water_used_net"  id="water_used_net{{$i}}"
                            name="data[{{$i}}][water_used_net]" value="">
                        </td>
                        <td class="border-0 text-right">
                            <input type="text" readonly class="form-control text-right meter_reserve_price"  id="meter_reserve_price{{$i}}"
                            name="data[{{$i}}][meter_reserve_price]" value="">
                        </td>

                        <td class="border-0 text-right">
                            <input type="text" readonly class="form-control text-right total" id="total{{$i}}"
                            name="data[{{$i}}][total]" value="">
                        </td>
                        <td class="border-0 text-right">
                            <input type="text" readonly class="form-control text-right vat" id="vat{{$i}}"
                            name="data[{{$i}}][vat]" value="">
                        </td>
                        <td class="border-0 text-right">
                            <input type="text" readonly class="form-control text-right net" id="net{{$i}}"
                            name="data[{{$i}}][net]" value="">
                        </td>
                    </tr>

                    <?php $i++;?>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>
@endsection


@section('script')
<script>

    let i = 0;

    function del(id){
        let res =  window.confirm('ต้องการลบข้อมูลใช่หรือไม่ !!!')
        if(res){
            window.location.href = `/invoice/delete/${id}/ลบ`
        }else{
            return false
        }
    }
    //ค้นหาโดยเลขมิเตอร์
    $('#meternumber').keyup(function(){
        $('.init').val('')
        let meternumber = $('#meternumber').val();

        if(meternumber.length > 4){
            let zone_id = "<?php echo $member_not_yet_recorded_present_inv_period[0]->undertake_zone_id; ?>";
            console.log(zone_id)
            $.get(`../../invoice/search_from_meternumber/${meternumber}/${zone_id}`).done(function(data){
                console.log('data',data)
                if(data.usermeterInfos  === null){
                    $('.addBtn').addClass('hidden');
                    $('.empty_user').text('ไม่พบผู้ใช้งานเลขมิเตอร์นี้')
                }else{
                    $('#currentmeter').focus();
                    let address = `${data.usermeterInfos.user.usermeter_info.zone.zone_name}  ${data.usermeterInfos.user.usermeter_info.zone.location}`;
                    $('#feFirstName').val(data.usermeterInfos.user.user_profile.name);
                    $('#feInputAddress').val(address);
                    $('.empty_user').text('')
                    //ถ้า invoice = 0
                    if(data.invoice === null){
                        $('#lastmeter').val(0);
                        $('#last_invoice').val(-1);
                    }else{
                        $('#lastmeter').val(data.invoice.currentmeter);
                        $('#last_invoice').val(data.invoice.id);
                    }

                    $('#user_id').val(data.usermeterInfos.user.id);

                    if($('.addBtn').hasClass('hidden')){
                        $('.addBtn').removeClass('hidden');
                    }
                }
            });
        }
    })

    //คำนวนเงินค่าใช้น้ำ
    $('.currentmeter').keyup(function(){
        let inv_id = $(this).data('id')
        let currentmeter = $(this).val()
        let lastmeter = $(`#lastmeter${inv_id}`).val()
        let net = currentmeter=='' ? 0 : currentmeter - lastmeter;
        let total = (net * 8) + check_meter_reserve_price(inv_id);
        let vat = (`<?php echo $vat->values;?>`*total)/100;
        $('#vat'+inv_id).val(vat);

        $('#water_used_net'+inv_id).val(net)
        $('#total'+inv_id).val(total);
        $('#net'+inv_id).val(total+ vat);
    });
    $('.lastmeter').keyup(function(){
        let inv_id = $(this).data('id')
        let lastmeter = $(this).val()

        let currentmeter = $(`#currentmeter${inv_id}`).val()
        let net = currentmeter - lastmeter
        let total = (net * 8) + check_meter_reserve_price(inv_id);
        let vat = (`<?php echo $vat->values;?>`*total)/100;
        $('#vat'+inv_id).val(vat);

        $('#water_used_net'+inv_id).val(net)
        $('#total'+inv_id).val(total);
        $('#net'+inv_id).val(total+ vat);

    });

    function check_meter_reserve_price(inv_id){
        let lastmeter =$(`#lastmeter${inv_id}`).val()
        let currentmeter = $(`#currentmeter${inv_id}`).val();

        let diff = currentmeter - lastmeter;

        let res =  diff == 0 ? 10 : 0;
        $('#meter_reserve_price'+inv_id).val(res)
        return res;
    }

    var table = $('.datatable').DataTable({
        "pagingType": "listbox",
        "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "ทั้งหมด"]],
        "language": {
            "search": "ค้นหา:",
            "lengthMenu": "แสดง _MENU_ แถว",
            "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
            "paginate": {
                "info": "แสดง _MENU_ แถว",
            },
        },
    })

    $(document).ready(function(){
        $('.paginate_page').text('หน้า')
        let val = $('.paginate_of').text()
        $('.paginate_of').text(val.replace('of', 'จาก'));

        // if(a){
                       $('.datatable thead tr').clone().appendTo('.datatable thead');
                        // a= false
                    // }
                    $('.datatable thead tr:eq(1) th').each( function (index) {
                        var title = $(this).text();
                        $(this).removeClass('sorting')
                        $(this).removeClass('sorting_asc')
                        if(index > 0 && index < 4){
                            $(this).html( `<input type="text" data-id="${index}" class="col-md-12" id="search_col_${index}" placeholder="ค้นหา" />` );
                        }else{
                            $(this).html('')
                        }
                    } );

                $('.dataTables_filter').remove();

                let col_index = -1
                $('.datatable thead input[type="text"]').keyup(function(){
                    let that = $(this)
                    var col = parseInt(that.data('id'))

                    if(col !== col_index && col_index !== -1){
                        $('#search_col_'+col_index).val('')
                        table.column(col_index)
                        .search('')
                        .draw();
                    }
                    setTimeout(function(){

                        let _val = that.val()
                        if(col===3){
                            var val = $.fn.dataTable.util.escapeRegex(
                                _val
                            );
                            table.column(col)
                            .search( val ? '^'+val+'.*$' : '', true, false )
                            .draw();
                        }else{
                            table.column(col)
                            .search( _val )
                            .draw();
                        }
                    }, 300);

                    col_index = col

                })


    })

    //เพิ่มข้อมูลลงตาราง lists

    $(document).on('click','.username',function(){
        let user_id = $(this).data('user_id')

        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            //หาข้อมูลการชำระค่าน้ำประปาของ  user
        $.get(`../../api/users/user/${user_id}`).done(function(data){
            console.log('data',data)
            row.child( format(data)).show();
            tr.addClass('shown');
        });

        }
    })


    function format(d){
        console.log(d)
        let text =  `<table class="table table-striped">
                    <thead>
                        <tr>
                        <th>เลขมิเตอร์</th>
                        <th>วันที่</th>
                        <th>รอบบิล</th>
                        <th>ยอดครั้งก่อน</th>
                        <th>ยอดปัจจุบัน</th>
                        <th>จำนวนที่ใช้</th>
                        <th>คิดเป็นเงิน(บาท)</th>
                        <th>สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>`;
            d[0].invoice.forEach(element => {
                let _status = 'รอบันทึกข้อมูล'
                if(element.status === "invoice"){
                    _status = '<button class="btn btn-sm btn-outline-info">ออกใบแจ้งหนี้แล้ว</button>'
                }else if(element.status === "paid"){
                    _status = '<button class="btn btn-sm btn-outline-success">ชำระเงินแล้ว</button>'
                }else if(element.status === "owe"){
                    _status = '<button class="btn btn-sm btn-outline-warning">ค้างชำระ</button>'
                }
                if(element.status !== 'init'){
                    text += `
                            <tr>
                            <td>${d[0].user_meter_infos.meternumber}</td>
                            <td>${element.updated_at_th}</td>
                            <td>${element.invoice_period.inv_period_name}</td>
                            <td>${element.lastmeter}</td>
                            <td>${element.currentmeter}</td>
                            <td>${element.currentmeter - element.lastmeter }</td>
                            <td>${(element.currentmeter - element.lastmeter)*8 }</td>
                            <td>${_status}</td>
                            </tr>
                    `;
                }//if
            });

            text +=`</tbody>
                </table>`;
        return text;
    }

    function printtag(tagid) {
        var hashid = "#"+ tagid;
            var tagname =  $(hashid).prop("tagName").toLowerCase() ;
            var attributes = "";
            var attrs = document.getElementById(tagid).attributes;
              $.each(attrs,function(i,elem){
                attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
              })
            var divToPrint= $(hashid).html() ;
            var head = "<html><head>"+ $("head").html() + "</head>" ;
            var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
            var newWin=window.open('','Print-Window');
            newWin.document.open();
            newWin.document.write(allcontent);
            newWin.document.close();
           setTimeout(function(){newWin.close();},10);
    }
    $(".exportToExcel").click(function(){
        $("#DivIdToPrint").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: "SomeFile", //do not include extension
            fileext: ".xls" // file extension
        });
    });


</script>
@endsection

