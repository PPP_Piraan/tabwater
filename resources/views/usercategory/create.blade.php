@extends('layouts.argon')

@section('title')
    ตั้งค่า / ประเภทผู้ใช้งาน / เพิ่มข้อมูลประเภทผู้ใช้งาน
@endsection

@section('content')
    <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
            </div>
            <div class="card-body">
                <form action="{{url('usercategory/store')}}" method="post">
                    @csrf
                    @include('usercategory.form', ['formMode' => 'create'])
                </form>
            </div>
          </div>
        </div>
    </div>
    
@endsection