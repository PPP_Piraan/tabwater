<div class="row">
    <div class="col-md-2">ชื่อประเภทผู้ใช้งาน</div>
    <div class="col-md-4">
        <div class="form-group">
            <input type="text" class="form-control" value="{{$usercategory->user_cate_name}}" id="usercategory_name" name="usercategory_name">
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <input type="submit" class="{{$formMode =='create' ? 'btn btn-info' : 'btn btn-warning'}}" value="บันทึก"/>
        </div>
    </div>
</div>
