@extends('layouts.argon')

@section('title')
    ตั้งค่า / ตารางประเภทผู้ใช้งาน
@endsection

@section('content')
<div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">ตารางประเภทผู้ใช้งาน</h3>
              {{-- <div class="text-right" style="float:right"> --}}
                <a href="/usercategory/create" class="btn btn-primary my-4 float-r">เพิ่มประเภทผู้ใช้งาน</a>
              {{-- </div> --}}
            </div>
            @if (count($usercategory) == 0)
            <nav class="navbar navbar-expand-lg navbar-dark bg-warning" style="margin:10px 10px">
                    <div class="container">
                        <a class="navbar-brand" href="#">ไม่พบข้อมูลประเภทผู้ใช้งาน</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                       
                    </div>
                </nav>
            @else
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">ประเภทผู้ใช้งาน</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    @foreach ($usercategory as $item)
                                        <tr>
                                            <th>{{$i++}}</th>
                                            <th scope="row">
                                                {{$item->user_cate_name}}
                                            </th>
            
            
                                            <td class="text-right">
            
                                                <a href="/usercategory/update/{{$item->id}}"  class="btn btn-icon btn-2 btn-primary">
                                                    <span class="btn-inner--icon"><i class="fas fa-edit"></i></span>
            
                                                </a>
                                                <a href="javascript:void(0)" data-id="{{$item->id}}" class="btn btn-icon btn-2 btn-danger" data-toggle="modal" data-target="#exampleModal">
                                                    <span class="btn-inner--icon"><i class="fas fa-trash"></i></span>
            
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                   
                                </tbody>
                            </table>

                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            @endif
            
         
          </div>
        </div>
      </div>

      
    @endsection

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">ยืนยันการลบข้อมูล</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  ต้องการลบข้อมูลหรือไม่ ?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                  <a href="/usercategory/delete/{{$item->id}}" class="btn btn-danger"> ต้องการลบข้อมูล</a>
                </div>
              </div>
            </div>
          </div>