@extends('layouts.argon')

@section('title')
    ตั้งค่า / ประเภทผู้ใช้งาน / แก้ไขข้อมูลประเภทผู้ใช้งาน
@endsection

@section('content')
    <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
            </div>
            <div class="card-body">
                <form action="/usercategory/edit/{{$usercategory->id}}" method="post">
                    @csrf
                    @method('PUT')
                    @include('usercategory.form', ['formMode' => 'update'])
                </form>
            </div>
          </div>
        </div>
    </div>
    
@endsection