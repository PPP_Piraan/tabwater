@extends('layouts.print')



@section('content')
<?php
use App\Http\Controllers\Api\FunctionsController;

?>

<head>

    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
</head>

<?php $index =  0; ?>

    @section('style')
        <style>
            table{
                font-size: 1.08rem;
                width: 100%;
                
            }
            td{
                /* padding: 0.5% */
            }
            th{
                text-align:  center
            }

            td div{
                padding-top: 2px !important;
                padding-bottom: 2px !important;

            }
            .indent{
                text-indent: 5rem;
            }
            .page-break {
                /* page-break-before: always; */
                page-break-after: always;
    
            }


            @media print { 
                @page { margin: 0; }
                * { overflow: hidden!important; } 
            }
        </style>
    @endsection
    @section('content')
               
        {{-- <div class="row"> --}}
            <table class="table">
                <tr>
                    <td style="width:30%">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                @foreach ($bankSlips as $item)
                                <h3 class="profile-username text-center">{{ $item->user_profile->name }}</h3>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>ที่อยู่</b> <a class="float-right">{{ $item->user_profile->address }}
                                            {{ $item->user_profile->zone->zone_name }}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>เส้นทางจดมิเตอร์</b> <a
                                            class="float-right">{{ $item->user_profile->userMeterInfos->subzone->subzone_record_route }}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>เลขที่ผู้ใช้น้ำ</b> <a class="float-right">{{ $item->user_id}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>เลขมิเตอร์</b> <a
                                            class="float-right">{{ $item->user_profile->userMeterInfos->meternumber }}</a>
                                    </li>
                                </ul>
                                @endforeach


                            </div>
                            <!-- /.card-body -->
                        </div>
                    </td>
                    <td style="width:20%">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile text-center">
                                <h3 class="profile-username text-center">สลิป</h3>
                                <img src="{{ asset('slip_images/'. $bankSlips[0]->image) }}" style="width:220px">
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </td>
                    <td style="width:50%">
                        <h3 class="profile-username text-center">รายการค้างชำระ</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>รอบบิล</th>
                                    <th><div>ก่อนจด</div>(หน่วย)</th>
                                    <th><div>หลังจด</div>(หน่วย)</th>
                                    <th><div>จำนวนที่</div>ต้องจ่าย(บาท)</th>
                                    <th>สถานะ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($invoiceArray as $item)
                                <tr>
                                    <th>{{ $item[0]->invoice_period->inv_period_name }}</th>
                                    <th>{{ $item[0]->lastmeter }}</th>
                                    <th>{{ $item[0]->currentmeter }}</th>
                                    <th>{{ ($item[0]->currentmeter - $item[0]->lastmeter) *8 }}</th>
                                    <th> {{ $item[0]->status }}</th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                       
                    </td>
                </tr>
            </table>
        {{-- </div> --}}


        <div class="page-break"></div>
    @endsection

    @section('script')

    <script>
        $(document).ready(function () {
            $('.btnprint').hide();
            var css = '@page {  }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');
            style.type = 'text/css';
            style.media = 'print';
            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }
            head.appendChild(style);

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);
            
             window.print();
            setTimeout(function(){ 
                    window.location.href = '../../upload_bank_slip/index'; 
               
            }, 200);

         
        });
    </script>

    @endsection
