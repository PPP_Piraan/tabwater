<div class="card card-info">
      <div class="card-body">
        <div class="form-group row">
          <label for="organize_address" class="col-sm-2 col-form-label">เลขที่</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_address" 
            value="{{ isset($organizations['organize_address']) ? $organizations['organize_address'] : '99'  }}"
            name="organize_address">
          </div>
        </div>
        <div class="form-group row">
          <label for="organize_zone" class="col-sm-2 col-form-label">หมู่</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_zone" 
            value="{{ isset($organizations['organize_zone']) ? $organizations['organize_zone'] : 'หมู่ 19'  }}"
            name="organize_zone">
          </div>
        </div>
        <div class="form-group row">
          <label for="organize_road" class="col-sm-2 col-form-label">ถนน</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_road" 
            value="{{ isset($organizations['organize_road']) ? $organizations['organize_road'] : 'เลิงนกทา-โพนทอง'  }}"
            name="organize_road">
          </div>
        </div>

        <div class="form-group row">
          <label for="organize_tambon" class="col-sm-2 col-form-label">ตำบล</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_tambon" 
            value="{{ isset($organizations['organize_tambon']) ? $organizations['organize_tambon'] : 'ห้องแซง'  }}"
            name="organize_tambon">
          </div>
        </div>
        <div class="form-group row">
          <label for="organize_district" class="col-sm-2 col-form-label">อำเภอ</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_district" 
            value="{{ isset($organizations['organize_district']) ? $organizations['organize_district'] : 'เลิงนกทา'  }}"
             name="organize_district">
          </div>
        </div> 
        <div class="form-group row">
          <label for="organize_province" class="col-sm-2 col-form-label">จังหวัด</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_province" 
            value="{{ isset($organizations['organize_province']) ? $organizations['organize_province'] : 'ยโสธร'  }}"
             
            name="organize_province">
          </div>
        </div>
        <div class="form-group row">
          <label for="organize_zipcode" class="col-sm-2 col-form-label">รหัสไปรษณีย์</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_zipcode"
            value="{{ isset($organizations['organize_zipcode']) ? $organizations['organize_zipcode'] : '35120'  }}"
            name="organize_zipcode">
          </div>
        </div>

        <div class="form-group row">
          <label for="organize_phone" class="col-sm-2 col-form-label">เบอร์โทร</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_phone" 
            value="{{ isset($organizations['organize_phone']) ? $organizations['organize_phone'] : '0452394234'  }}"
            name="organize_phone">
          </div>
        </div>
        <div class="form-group row">
          <label for="organize_email" class="col-sm-2 col-form-label">อีเมลล์</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="organize_email" 
            value="{{ isset($organizations['organize_email']) ? $organizations['organize_email'] : 'hs_tabwater@gmail.com'  }}"
             name="organize_email">
          </div>
        </div>
       
      </div>
      <!-- /.card-body -->
  </div>
