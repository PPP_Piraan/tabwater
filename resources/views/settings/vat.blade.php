<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title"></h3>
    </div>
    <!-- /.card-header -->
        <div class="card-body row">
            <div class="form-group col-md-3">
                <label for="vat">ภาษีมูลค่าเพิ่ม</label>
                <div class="input-group mb-3 ">
                    <input type="text" class="form-control" name="vat" id="vat"
                    value="{{ isset($vat['values']) ? $vat['values'] : ''  }}">
                    <div class="input-group-append">
                      <span class="input-group-text">%</span>
                    </div>
                  </div>
            </div>
        </div>
</div>
