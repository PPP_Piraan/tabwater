@extends('layouts.adminlte')

@section('mainheader')
import excel
@endsection
@section('nav')
<a href="{{'settings'}}">ตั้งค่า</a>
@endsection
@section('import_excel')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }

</style>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
    </div>
    <div class="card-body">

        <div class="row">
            <div class="col-5 col-sm-2">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                    aria-orientation="vertical">
                    <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home"
                        role="tab" aria-controls="vert-tabs-home" aria-selected="true">เจ้าหน้าที่ประปา</a>
                    <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile"
                        role="tab" aria-controls="vert-tabs-profile" aria-selected="false">ผู้ใช้น้ำประปา</a>
                </div>
            </div>
            <div class="col-7 col-sm-10">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane text-left fade show active" id="vert-tabs-home" role="tabpanel"
                        aria-labelledby="vert-tabs-home-tab">
                        <div class="row p-3">
                            <div class="col-md-12 head"></div>
                            <!-- Excel file upload form -->
                            <div class="col-md-12">
                                <form action="{{ url('settings/upload_excel') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputFile">Import Excel ข้อมูลเจ้าหน้าที่ประปา</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="file" id="fileInput">
                                                <label class="custom-file-label" for="exampleInputFile" id="fileInputLabel"></label>
                                            </div>
                                            <div class="input-group-append">
                                                    <input type="submit" class="btn btn-primary mb-3" name="importSubmit" value="Upload">
                                                    <input type="hidden" name="info_type" value="staff">
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- Data list table -->
                            @isset($staff)
                                <table class="table table-striped table-bordered">
                                    <thead class="table-dark">
                                        <tr>
                                            <th>#</th>
                                            <th>username</th>
                                            <th>ชื่อ-สกุล</th>
                                            <th>เบอร์โทรศัพท์</th>
                                            <th>ประเภทผู้ใช้งาน</th>
                                            <th>สถานะ</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (collect($staffs)->count() > 0)
                                        <?php $i = 1; ?>
                                        @foreach ($staffs as $item)
                                        <tr>
                                            <td>{{ $i++ }} </td>
                                            <td>{{ $item->username }} </td>
                                            <td>{{ $item->name }} </td>
                                            <td>{{ $item->phone }} </td>
                                            <td>{{ $item->status }} </td>
                                            <td>{{ $item->user_cate_id }} </td>
                                            <td>{{ $item->created_at }} </td>
                                        </tr>
                                        @endforeach

                                        @else
                                            <tr>
                                                <td colspan="7" class="text-center">ยังไม่มีข้อมูล</td>
                                            </tr>
                                        @endif


                                    </tbody>
                                </table>
                            @endisset

                        </div>
                    </div>
                    <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel"
                        aria-labelledby="vert-tabs-profile-tab">
                        <div class="row p-3">
                            <div class="col-md-12 head"></div>
                            <!-- Excel file upload form -->
                            <div class="col-md-12">
                                <form action="{{ url('settings/upload_excel') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputFile">Import Excel ข้อมูลผู้ใช้น้ำประปา</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="file" id="fileInput_member">
                                                <label class="custom-file-label" for="exampleInputFile_member" id="fileInputLabel_member"></label>
                                            </div>
                                            <div class="input-group-append">
                                                    <input type="submit" class="btn btn-primary mb-3" name="importSubmit" value="Upload">
                                                    <input type="hidden" name="info_type" value="member">
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- Data list table -->
                            @isset($members)
                            <table class="table table-striped table-bordered">
                                <thead class="table-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>username</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>เบอร์โทรศัพท์</th>
                                        <th>ประเภทผู้ใช้งาน</th>
                                        <th>สถานะ</th>
                                        <th>Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (collect($staffs)->count() > 0)
                                    <?php $i = 1; ?>
                                    @foreach ($staffs as $item)
                                    <tr>
                                        <td>{{ $i++ }} </td>
                                        <td>{{ $item->username }} </td>
                                        <td>{{ $item->name }} </td>
                                        <td>{{ $item->phone }} </td>
                                        <td>{{ $item->status }} </td>
                                        <td>{{ $item->user_cate_id }} </td>
                                        <td>{{ $item->created_at }} </td>
                                    </tr>
                                    @endforeach

                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">ยังไม่มีข้อมูล</td>
                                        </tr>
                                    @endif


                                </tbody>
                            </table>
                            @endisset

                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>
  </div>
@endsection


@section('script')
<script>
 function preview(id) {
            $(`#img_name${id}`).val(1)

            $(`#frame${id}`).attr('src', URL.createObjectURL(event.target.files[0]));
            if(id == 0){
                $('#logo_change_image').val(1)
            }
        }

        let preview_count = 1;
        $(document).on('click','.append_sign_form_btn',()=>{
             preview_count = parseInt($('#preview_count').val())+1;
            let text = `
            <div class="form-group row" id="form${preview_count}">
                        <div class="col-sm-1  trash_div" onclick="del('${preview_count}')">
                            <label for="organize_address" class="col-form-label ">&nbsp;</label>
                            <i class="fas fa-trash-alt text-danger form-control"></i>
                        </div>
                        <div class="col-sm-6 row">
                            <div class="col-md-12">
                                <label for="organize_address" class=" col-form-label">ชื่อ-สกุล</label>
                                    <input type="text" class="form-control" name="sign[${preview_count}][name]" id="sign${preview_count}">
                            </div>
                            <div class="col-md-12">
                                <label for="organize_address" class=" col-form-label">ตำแหน่ง</label>
                                    <input type="text" class="form-control" name="sign[${preview_count}][position]">
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label for="organize_address" class="col-sm-2 col-form-label">&nbsp;</label>
                            <img id="frame${preview_count}" src="" class="mt-4" width="200px" />
                            <input type="text" name="sign[${preview_count}][img_name]" id="img_name${preview_count}" value="0">

                            <input type="file" onchange="preview(${preview_count})" class="form-control filenames" style="position: absolute;
                                bottom: 0px;" name="sign[${preview_count}][filenames]">
                        </div>

                    </div>

            `;
            $('#preview_count').val(preview_count)
            $('#append_sign_form').append(text)
            preview_count = preview_count+1;
        });

        $('#logo_old_image_name').change(()=>{
            alert()
        })


        function del(id){
            $(`#form${id}`).remove()
        }
</script>
<script>


    $('#fileInput').change(function(){
        $('#fileInputLabel').html($("#fileInput")[0].files[0].name);
    })
    $('#fileInput_member').change(function(){
        $('#fileInputLabel_member').html($("#fileInput_member")[0].files[0].name);
    })
    </script>
@endsection
