<div class="card card-primary">
    <div class="card-header ">รูปตราหน่วยงานที่ใช้ปัจจุบัน</div>

    <div class="card-body">
        {{-- <img src="{{url('img/hslogo.jpg')}}" alt=""> --}}
        @if (isset($logo['values']))
            @if($logo['values'] != "")
                <img src="{{asset('logo/'.$logo['values'])}}"  width="200"/>
            @endif
        @else
            <img src="{{asset('logo/init_logo2.png')}}"  width="200"/>

            <div class="text-center h5 text-red mt-3">ยังไม่รูปตราสัญลักษณ์ของหน่วยงาน</div>
        @endif
      
    </div>
</div>

<div class="card  card-secondary">
    <div class="card-header">เลือกภาพที่ต้องการอัพโหลด</div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="logo" id="logo" onchange="preview('0')" class="form-control mt-4">
                <input type="text" name="logo_change_image" id="logo_change_image" value="0">
                <input type="text" name="logo_old_image_name" id="logo_old_image_name" value="{{isset($logo['values']) ? $logo['values'] : 0}}">
            </div>
            <div class="col-md-6 text-center">
                <img id="frame0" src="" width="200px" />

            </div>
        </div>
    </div>
</div>

