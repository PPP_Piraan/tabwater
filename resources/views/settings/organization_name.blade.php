<div class="row">
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-body">
                <div class="form-group">
                    <label for="organization_name">ชื่อองค์กร</label>
                    <input type="text" class="form-control" id="organization_name" name="organization_name" 
                    value="{{ isset($organizations['organization_name']) ? $organizations['organization_name'] : 'เทศบาลตำบลห้องแซง'  }}"
                    placeholder="เทศบาลตำบลห้องแซง">
                </div>
                <div class="form-group">
                    <label for="organization_short_name">ชื่อย่อองค์กร</label>
                    <input type="text" class="form-control" id="organization_short_name"  name="organization_short_name"
                    value="{{ isset($organizations['organization_short_name']) ? $organizations['organization_short_name'] : 'ทต.ห้องแซง'  }}"
                    placeholder="ทต.ห้องแซง">
                </div>
            
                <div class="form-group">
                    <label for="department_name">ชื่อหน่วยงาน</label>
                    <input type="text" class="form-control" id="department_name" name="department_name" 
                    value="{{ isset($organizations['department_name']) ? $organizations['department_name'] : 'งานประปาเทศบาลตำบลห้องแซง'  }}"
                    
                    placeholder="งานประปาเทศบาลตำบลห้องแซง">
                </div>
                <div class="form-group">
                    <label for="department_short_name">ชื่อย่อหน่วยงาน</label>
                    <input type="text" class="form-control" id="department_short_name" name="department_short_name"
                    value="ปป.ห้องแซง" placeholder="ปป.ห้องแซง">
                </div>
                <div class="form-group">
                    <label for="department_phone">เบอโทรร์ติดต่อหน่วยงาน</label>
                    <input type="text" class="form-control" id="department_phone" name="department_phone" value="0984567854">
                </div>
                
            </div>
            <!-- /.card-body -->

        </div>
        <!-- /.card -->

    </div>
</div>
