<?php
use App\Http\Controllers\Api\FunctionsController;
$fnc = new FunctionsController();
?>

<style>
    .textbetweenKrut{
        padding-top:60pt !important
    }
    .tesabanAddr{
        padding-left: 60pt
    }
    .date{
        padding-left: 45pt
    }
</style>
<table class="table" style="width:220mm; margin-left:5rem">
    <tr>
        <td style="padding-left: 4rem;padding-right: 1rem;">
            <div class="row">
                <div class="col-12">
                    <div style="margin-top: 2rem"></div>
                    <div>กิจการประปาเทศบาลตำบลห้องแซง</div>
                    <div>222 หมู่ 17 ต.ห้องแซง</div>
                    <div>อ.เลิงนกทา จ.ยโสธร 35120</div>
                </div>
            </div>
            <div class="row mb-5" style="margin-top:2rem">
                <div class="col-4 text-center"></div>
                <div class="col-6 text-left pt-3 ml-5">
                    <div>เรียน &nbsp; {{$item['res'][0]['user_profile']['name']}}</div>
                    <div class="pl-5">{{$item['res'][0]['user_profile']['address']." ".$item['res'][0]['user_profile']['zone']['zone_name']}} ต.ห้องแซง </div>
                    <div class="pl-5">อ.เลิงนกทา จ.ยโสธร 35120</div>
                </div>
            </div>
            <hr>

            <div style="margin-top: 2rem"></div>

            <div class="row">
                <div class="col-3 textbetweenKrut">ที่ ยส 73602/</div>
                <div class="col-4 text-right">
                    <img src="{{asset('/img/krut.png')}}" style="width: 3cm; height:3cm">
                </div>
                <div class="col-5 textbetweenKrut tesabanAddr">
                    <div>สำนักงานเทศบาลตำบลห้องแซง</div>
                    <div>ถนนเลิงนกทา-หนองพอก 35120</div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-5"></div>
                <div class="col-4 date">
                    <?php
                        $currentYear = date('Y') + 543;
                        $monthThai =  $fnc->fullThaiMonth(date('m'));
                    ?>
                    วันที่ {{date('d')." ".$monthThai." ".$currentYear}}
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">เรื่อง ขอให้ชำระหนี้ค้างค่าน้ำประปา</div>
            </div>
            <div class="row mt-3">
                <div class="col-12">เรียน &nbsp; <b>{{$item['res'][0]['user_profile']['name']}}</b> </div>
            </div>
            <div class="row mt-3">
                <div class="col-12 indent">
                    <div style="letter-spacing: 1.2px;">ตามที่ท่านเป็นผู้ใช้น้ำของ กิจการประปาเทศบาลตำบลห้องแซง
                        เลขที่ผู้ใช้น้ำ
                        <b>{{$item['res'][0]['user_profile']['userMeterInfos']['meternumber']}}</b>
                    </div>ในนามของ
                    <b>{{$item['res'][0]['user_profile']['name']}}</b>
                    ซึ่งมีหนี้ค้างชำระค่าน้ำประปา ดังนี้

                </div>
                <div class="col-12">
                    <table class="table mt-4 text-center table-bordered">
                        <tr>
                            <th class="waterUsedHisHead td_history text-center">ประจำเดือน</th>
                            <?php $count_history = 0; ?>
                            @foreach ($item['res'] as $history)
                                <th class="text-center td_history">
                                    {{ $history['invoice_period']['inv_period_name'] }}
                                </th>
                            @endforeach
                            <th class="text-center td_history">รวม (บาท)</th>
                        </tr>
                        <tr>
                            <th class="waterUsedHisHead td_history text-center">จำนวนเงิน(บาท)</th>
                            <?php $oweSum = 0;  ?>
                            <?php $count_history = 0; ?>
                            @foreach ($item['res'] as $history)
                            
                                <td class="text-center td_history">
                                    <?php 
                                        $history_diff = $history['currentmeter'] - $history['lastmeter'];
                                        $history_diff_plus = $history_diff == 0 ? 10 : $history_diff*8;
                                        $history_status = $history['status'] =='owe' ? '(ค้าง)' : '';
                                        $oweSum += $history_diff * 8;
                                        if($history_diff == 0){
                                            $oweSum += 10;
                                            $history_diff_plus = 10;
                                        }
                                        
                                    ?>

                                    {{ $history_diff_plus }}
                                </td>
                            
                            @endforeach
                            <td class="text-center td_history">
                                <?php 
                                    echo number_format($oweSum,2);
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-12 indent" style="">
                    ค้างชำระ <b>{{collect($item['res'])->count()}}</b> เดือน
                    รวมเป็นเงินที่ต้องชำระทั้งสิ้น <b>{{number_format($oweSum, 2)}} </b>บาท
                </div>
            </div>
            <div class="row">
                <div class="col-12 indent" style="line-height: 1.8rem">
                    <?php
                        $currentMonthExpTemp = date('m');
                        $splitArr = str_split($currentMonthExpTemp);
                        $currentYear = date('Y') + 543;
                        $currentMonthExp = "";
                        if($splitArr[0] == "0"){
                            $plus = $splitArr[1] +1;
                            $currentMonthExp =  $plus < 10 ? "0".$plus : $plus;
                        }else{
                            //check เดือน 12 ?
                            if($currentMonthExpTemp == 12){
                                $currentMonthExp = "01";
                                $currentYear = $currentYear+1;
                            }else{
                                $currentMonthExp = $currentMonthExpTemp + 1 ;
                            }
                        } 
                        $monthThai =  $fnc->fullThaiMonth($currentMonthExp);
                        $paidBeforeDate = 30;
                    ?>
                    <div style="letter-spacing: -0.35px;">
                        ในการนี้ จึงขอให้ท่านโปรดติดต่อชำระค่าน้ำประปา ได้ที่ กองคลังเทศบาลตำบลห้องแซง ให้แล้วเสร็จ
                    </div>
                    ภายในวันที่_________________________
                    {{-- <b>{{ $paidBeforeDate.' '.$monthThai. ' ' .$currentYear}}</b> --}}
                    หากเกินกำหนด กิจการประปามีความจำเป็นต้องระงับการใช้น้ำในวันถัดไป
                    และจะจ่ายน้ำใหม่หลังจากได้รับการชำระหนี้ค้างทั้งหมดพร้อมทั้งค่าธรรมเนียมการใช้น้ำแล้ว
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-12 indent" style=" line-height: 1.8rem">
                    จึงเรียนมาเพื่อโปรดชำระค่าน้ำประปาภายในกำหนด หวังเป็นอย่างยิ่งคงได้รับความร่วมมือด้วยดีเช่นเคย
                    ขอขอบพระคุณมา ณ โอกาสนี้
                    หรือหากท่านชำระค่าน้ำประปาแล้วต้องขออภัยไว้ ณ ที่นี้ด้วย
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-5"></div>
                <div class="col-4 text-center">ขอแสดงความนับถือ </div>
            </div>
            <div class="row mt-5">
                <div class="col-5"></div>
                <div class="col-4 text-center pt-3" style="line-height: 1.8rem">
                    <div>( นางละเอียด ศรีสุข )</div>
                    <div>นายกเทศมนตรีตำบลห้องแซง </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12" style="line-height: 1.8rem; font-size:0.9rem">
                    <div>กิจการประปา เทศบาลตำบลห้องแซง</div>
                    <div>โทร 088-100-5436 </div>
                </div>
            </div>
        </td>
    </tr>
</table>