@extends('layouts.adminlte')

@section('mainheader')
    เพิ่มเส้นทางจดมิเตอร์ พื้นที่ {{$zone[0]->zone_name}}
@endsection
@section('nav')
<a href="{{url('zone')}}"> พื้นที่จดมิเตอร์น้ำประปา</a>
@endsection
@section('zone')
    active
@endsection


@section('content')
{{-- {{dd($zone)}} --}}

<form action="{{url('subzone/update/'.$zone[0]->id)}}" method="POST" onsubmit="return checkZoneNameValues()">
    @csrf
    @method('PUT')
    <div class="row">

        <div class="col-lg-8 mb-4">
            <div class="card card-widget widget-user">                
                <div class="card-footer">
                    <div id="routList">
                        <?php $i = 0; ?>
                        @foreach ($zone[0]->subzone as $item)
                            @if (collect($zone[0]->subzone)->count()== $i)
                                <div class="input-group mb-3 route ">
                                    <input type="text" class="form-control subzone_name" data-id="{{$i}}" name="subzone[{{$i}}][{{$item['id']}}][subzone_name]" id="routeNameTemp" value="{{$item['subzone_name']}}">
                                    <a href="javascript:void(0)" class="btn btn-primary permanentVal addRoute " data-subzone_id="{{$item['id']}}"  data-i="{{$i}}">เพิ่ม</a>
                                </div>
                            @else
                                <div class="input-group mb-3 route last">
                                    <input type="text" class="form-control" name="subzone[{{$i}}][{{$item['id']}}][subzone_name]" id="routeNameTemp{{$item['id']}}" value="{{$item['subzone_name']}}">
                                    <a href="javascript:void(0)" data-subzone_id="{{$item['id']}}" data-i="{{$i}}" id="a_del{{$i}}" class="btn btn-danger permanentVal removeRoute">ลบ</a>
                                </div>
                            @endif
                            <?php $i++; ?>
                        @endforeach
                        <div class="input-group mb-3 route last">
                            <input type="text" class="form-control subzone_name" data-id="{{$i}}" name="subzone[{{$i}}][new][subzone_name]" id="routeNameTemp" value="">
                            <a href="javascript:void(0)" class="btn btn-primary permanentVal addRoute last" data-subzone_id="new" id="a_del{{$i}}"  data-i="{{$i}}">เพิ่ม</a>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success submitBtn">บันทึก</button>
                </div>
              </div>
            
        </div>
    </div>

</form>

@endsection

@section('script')
    <script>

        let i = 1;
        //เพิ่ม list เส้นทาง
        $('body').on('click','.addRoute',function(e){
            if($(this).hasClass('last')){
                $(this).removeClass('last')
                i = $(this).data('i') +1

                console.log('i',i)
            }
            let text = `
                <div class="input-group mb-3 route last">
                    <input type="text" class="form-control subzone_name" data-id="${i}" name="subzone[${i}][new][subzone_name]"  value="">
                    <a href="javascript:void(0)" class="btn btn-primary addRoute last" id="a_del${i}" data-i="${i}">เพิ่ม</a>
                </div>

            `;
            
            $('#routList').append(text);
            i++;

            //เปลี่ยนคุณสมบัติ list  ทุกอัน ยกเว้น list สุดท้าย
            $('.addRoute').each(function(index, element){
                if(!$(this).hasClass('last')){
                    $(this).removeClass('addRoute');
                    $(this).addClass('removeRoute');
                    $(this).html('ลบ')
                    $(this).removeClass('btn-primary');
                    $(this).addClass('btn-danger')
                    $(this).removeClass('last')
                }else{
                    $(this).parent().siblings().val('')
                }
            });

            // $('.permanentVal').each(function(ele){
            //     $(this).addClass('hidden');

            // })

        });

        //ลบ list เส้นทาง
        $("body").on("click",".removeRoute",function(e){
            var i = $(this).data('i')
            console.log('i=',i)
            var res = false
            var subzone_id = $(this).data('subzone_id');
            $.get(`../../user_meter_infos/find_subzone/0/${subzone_id}`, function(data){
                console.log(data)
                if(data == 1){
                    alert(`ไม่สามารถลบข้อมูล ได้เนื่องจากมีข้อมูลสมาชิกในหมู่นี้อยู่`)
                }else{
                    res = true

                }

                if(res === true){
                let res2 = window.confirm('ต้องการลบข้อมูลพื้นที่จัดเก็บใช่หรือไม่ !!!')
             
                    if(res2 === true ){
                        $.get(`../../api/subzone/delete/${subzone_id}`, function(data){
                            if(data == 1){
                                alert('ทำการลบข้อมูลเรียบร้อยแล้ว');
                                $(`#a_del${i}`).parent().remove();

                            }
                        });
                    }

                }else{
                console.log(res)

                return false
                }
            });


        });


        
        $('#zone').change(function(){
            if($('.submitBtn').hasClass('hidden')){
                $('.submitBtn').removeClass('hidden');
                $('.submitBtn').addClass('show');
            }
        });

        function checkZoneNameValues(){
            let res = true;
            let errTxt = '';
            let i = 0;
            let last_i = $('.subzone_name').last().data('id')
            $('.subzone_name').each(function(val){
                
                if($(this).val() === "" && $(this).data('id') !== last_i){
                    errTxt += '- ชื่อเส้นทางจัดเก็บต้องไม่เป็นค่าว่าง\n'
                    res =  false;
                    return false;
                }
            });
           
            if(res === false){
                alert(errTxt);
            }  
            return res;

            
        }

    </script>
@endsection