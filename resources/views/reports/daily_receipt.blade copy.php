@extends('layouts.adminlte')

@section('mainheader')
รายรับประจำวัน
@endsection
@section('nav')
    <a href="{{'reports'}}">รายงาน</a>
@endsection
@section('report-daily_receipt')
    active
@endsection
@section('style')

    <style>
      
        td.detailss-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
        }

        tr.shown td.detailss-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
        .hide{
            display: none
        }
        .show{
            display: block;
        }
        #example th{
            text-align: center;
            background-color:lightblue
        }
    </style>
@endsection

@section('content')

        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>

            <div class="info-box-content">
                <form action="{{url('reports/daily_receipt')}}" method="GET">
                    @csrf
                    <div class="row">
                    
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="search" class="col-sm-3 col-form-label">วันที่:</label>
                                <div class="col-sm-9">
                                    <input class="form-control datepicker" type="text" name="date"  id="date"> 
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <button type="submit" class="form-control btn btn-primary">ค้นหา</button>
                        </div>
                        <div class="col-md-7 text-right">
                            <button class="btn btn-outline-info" id="btnExport">Excel</button>
                        </div>
                    </div>
            </form>
            </div>
            <!-- /.info-box-content -->
            </div>
    

<div class="card">
    <div class="card-header">
        <div class="card-title">รายงานรายรับค่าน้ำประปา ประจำวันที่{{$date}}</div>
        <div class="card-tools">
            <div class="form-group row">
                <label for="search" class="col-sm-3 col-form-label">ค้นหา:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="search" >
                </div>
            </div>
        </div>
    </div>
    
    <div class="card-body table-responsive">
        <table id="aa" class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขใบ<br>เสร็จรับเงิน</th>
                    <th>ชื่อ-สกุล</th>
                    <th>บ้านเลขที่</th>
                    <th>หมู่</th>
                    <th>เส้นทาง</th>
                    <th>เลขใบ<br>แจ้งหนี้</th>
                    <th>รอบบิล</th>
                    <th>ค่าน้ำประปา <br>(บาท)</th>
                    <th>ค่ารักษามิเตอร์ <br>(บาท)</th>
                    <th>รวมทั้งสิน <br>(บาท)</th>
                   
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 0;
                    $reservemeterSum = 0;
                    $totalSum = 0;
                    $diff_meter_priceSum = 0;
                ?>

                @foreach ($receipts  as $item)
                    <?php  $i++;?>
                    @foreach ($item['invoice'] as $key => $invoice)
                        <?php 
                            $diff_meter = $invoice['currentmeter'] - $invoice['lastmeter'];
                            $diff_meter_price = $diff_meter *8;
                            $reservemeter =  $diff_meter_price == 0 ? 10 : 0;
                            $total = $diff_meter_price + $reservemeter;

                            $reservemeterSum += $reservemeter;
                            $totalSum += $total;
                            $diff_meter_priceSum += $diff_meter_price;
                            
                        
                        ?>

                        @if ($key == 0)
                        
                            <tr class="tr{{$i}} a" data-id="{{$i}}">
                                <td class="">{{ $i}}</td>
                                <td class="text-right">{{ $item['id'] }}</td>
                                <td class=""> {{ $invoice['user_profile']['name'] }}</td>
                                <td class="">{{ $invoice['user_profile']['address'] }}</td>
                                <td class="">{{ $invoice['user_profile']['zone']['zone_name'] }}</td>
                                <td class="">{{ $invoice['usermeterinfos']['subzone']['subzone_name'] }}</td>
                                <td class="text-right">{{ $invoice['id'] }}</td>
                                <td class="text-center">{{ $invoice['invoice_period']['inv_period_name'] }}</td>
                                <td class="text-right">{{ number_format($diff_meter_price) }}</td>
                                <td class="text-right reservemeter">{{ number_format($reservemeter) }}</td>
                                <td class="text-right">{{ number_format($item['net']) }}</td>
                                
                            </tr>
                        @else
                        <tr class="tr{{$i}} a" data-id="{{$i}}">
                            <td class="duplicate">{{ $i}}</td>
                            <td class="text-right duplicate">{{ $item['id'] }}</td>
                            <td class="duplicate"> {{ $invoice['user_profile']['name'] }}</td>
                            <td class="duplicate">{{ $invoice['user_profile']['address'] }}</td>
                            <td class="duplicate">{{ $invoice['user_profile']['zone']['zone_name'] }}</td>
                            <td class="duplicate">{{ $invoice['usermeterinfos']['subzone']['subzone_name'] }}</td>
                            <td class="text-right">{{ $invoice['id'] }}</td>
                            <td class="text-center">{{ $invoice['invoice_period']['inv_period_name'] }}</td>
                            <td class="text-right">{{ number_format($diff_meter_price) }}</td>
                            <td class="text-right reservemeter">{{ number_format($reservemeter) }}</td>
                            <td class="text-right duplicate">{{ number_format($item['net']) }}</td>
                            
                        </tr>
                        @endif
                        <?php  //$i++ ;?>
                    @endforeach
                 
                @endforeach
                {{-- <tr>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
                    <td class="bg-info h5 text-right">{{ number_format($diff_meter_priceSum) }}</td>
                    <td class="bg-info h5 text-right">{{ number_format($reservemeterSum) }}</td>
                    <td class="bg-info h5 text-right">{{ number_format($totalSum) }} </td>
                </tr> --}}
            </tbody>
           
        </table>
    </div>
    
</div>
{{-- <div id="test"></div>
<table id="acaa" cellspacing="0" border="0" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>เลขใบ<br>เสร็จรับเงิน</th>
            <th>ชื่อ-สกุล</th>
            <th>บ้านเลขที่</th>
            <th>หมู่</th>
            <th>เส้นทาง</th>
            <th>เลขใบ<br>แจ้งหนี้</th>
            <th>รอบบิล</th>
            <th>ค่าน้ำประปา <br>(บาท)</th>
            <th>ค่ารักษามิเตอร์ <br>(บาท)</th>
            <th>รวมทั้งสิน <br>(บาท)</th>
           
        </tr>
    </thead>
</table> --}}
@endsection


@section('script')
 <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
 <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
 <script src="https://cdn.datatables.net/rowgroup/1.1.2/js/dataTables.rowGroup.min.js"></script>
<script>

    $(document).ready(function(){
        var _data;

        $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true              //Set เป็นปี พ.ศ.
        }).datepicker("setDate", new Date());  //กำหนดเป็นวันปัจุบัน

        $('.duplicate').
        $.get('../api/reports/daily_receipt/20/05/2564').done(function(receipts){
           
            
    //         let txt =`<table id="aaaw" cellspacing="0" border="0" class="table table-striped table-bordered">
    // <thead>
    //     <tr>
    //         <th>#</th>
    //         <th>เลขใบ<br>เสร็จรับเงิน</th>
    //         <th>ชื่อ-สกุล</th>
    //         <th>บ้านเลขที่</th>
    //         <th>หมู่</th>
    //         <th>เส้นทาง</th>
    //         <th>เลขใบ<br>แจ้งหนี้</th>
    //         <th>รอบบิล</th>
    //         <th>ค่าน้ำประปา <br>(บาท)</th>
    //         <th>ค่ารักษามิเตอร์ <br>(บาท)</th>
    //         <th>รวมทั้งสิน <br>(บาท)</th>
           
    //     </tr>
    // </thead>
            
    //         <tbody>`;
                
    //                 let i = 0;
    //                 let reservemeterSum = 0;
    //                 let totalSum = 0;
    //                 let diff_meter_priceSum = 0;
    //                 // console.log('item.invoice', item);
      
    //         receipts.forEach(item => {

    //                 i++;
    //             item.invoice.forEach((invoice, key) => {
    //                 let diff_meter = invoice.currentmeter - invoice.lastmeter;
    //                 let diff_meter_price = diff_meter *8;
    //                 let reservemeter =  diff_meter_price == 0 ? 10 : 0;
    //                 let total = diff_meter_price + reservemeter;

    //                 reservemeterSum += reservemeter;
    //                 totalSum += total;
    //                 diff_meter_priceSum += diff_meter_price;
                     
    //                 if (key === 0){
    //                     let rowspan = Object.keys(item.invoice).length 

    //                         txt +=
    //                             `<tr class="tr${i} a" data-id="${i}">
    //                                 <td class="">${i}</td>
    //                                 <td class="text-right">${item.id}</td>
    //                                 <td class=""> ${invoice.user_profile.name}</td>
    //                                 <td class="">${invoice.user_profile.address}</td>
    //                                 <td class="">${invoice.user_profile.zone.zone_name}</td>
    //                                 <td class="">${invoice.usermeterinfos.subzone.subzone_name}</td>
    //                                 <td class="text-right">${invoice.id}</td>
    //                                 <td class="text-center">${invoice.invoice_period.inv_period_name}</td>
    //                                 <td class="text-right">${diff_meter_price}</td>
    //                                 <td class="text-right reservemeter">${reservemeter}</td>
    //                                 <td class="text-right">${item.net}</td>
                                    
    //                             </tr>`;
    //                 }else{
    //                     txt +=`
    //                         <tr class="tr${i} a aa2" data-id="${i}"> 
    //                             <td></td>
    //                             <td></td>
    //                             <td></td>
    //                             <td></td>
    //                             <td></td>
    //                             <td></td>
    //                             <td class="text-right">${invoice.id}</td>
    //                             <td class="text-center">${invoice.invoice_period.inv_period_name }</td>
    //                             <td class="text-right">${diff_meter_price}</td>
    //                             <td class="text-right">${reservemeter}</td>
    //                             <td></td>
    //                         </tr>`;
    //                 }//else
    //             });//foreach item
                        
                 
    //         });//foreach receipts
    //         txt +=`    
    //             <tr>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="border-right-0 border-left-0 border-bottom-0">&nbsp;</td>
    //                 <td class="bg-info h5 text-right">${diff_meter_priceSum}</td>
    //                 <td class="bg-info h5 text-right">${reservemeterSum}</td>
    //                 <td class="bg-info h5 text-right">${totalSum} </td>
    //             </tr>
    //         </tbody>
    //      </table>  
    //     `;
    //     $('#test').append(txt)

        });//$.get()
        $('#aa').DataTable();

    //     $('#aa').DataTable({
    //     ajax: {
    //     url: '../api/reports/daily_receipt/20/05/2564',
    //     dataSrc: ''
    // },
    //     "columns":[
    //         {"data": "receipt_id"},
    //         {"data": "name"},
    //         {"data": "zone_name"},
    //         {"data": "currentmeter"},
    //     ],
    //     rowGroup: {
    //          dataSrc:  ['name']
    //     }

    // })
        // $(`tr.a`).each(function(){
        //     let id = $(this).data('id');
        //     // console.log(parseInt(id)%2)
        //     if(id%2 === 0){
        //         //even row
        //         console.log(id)
        //         $(this).css({'background-color': 'rgba(0,0,0,.05)', "border": "solid 1px #dee2e6"})
        //     }
        // });
       
    });//$.document ready

   


    let  tr_id  = 0;
    $('#search').keyup(function(){
        let value = $('#search').val();
        
        if(value === ""){
            $(`tr.a`).removeClass('hide')
        }else{
            if($(`tr.a`).hasClass('hide')){
                $(`tr.a`).removeClass('hide')
            }
            tr_id = $(`td:contains("${value}")`).closest('tr').data('id');

            $(`tr.a`).addClass('hide')
            $(`td:contains("${value}")`).each(function(index){
                console.log('index',$(this).closest('tr').data('id'))
                let a = $(this).closest('tr').data('id')
                $(`.tr${a}`).removeClass('hide');
            });
        }
    }); //#searchBtn

    $("#btnExport").click(function (e) {
        // $('tr.aa2').each(function(){
        //     $(this).prepend(`
        //         <td class="add_for_print_excel">&nbsp;</td>
        //         <td class="add_for_print_excel">&nbsp;</td>
        //         <td class="add_for_print_excel">&nbsp;</td>
        //         <td class="add_for_print_excel">&nbsp;</td>
        //         <td class="add_for_print_excel">&nbsp;</td>
        //         <td class="add_for_print_excel">&nbsp;</td>
        //     `);
        // })
        $("#example").table2excel({
            exclude: ".excludeThisClass",
            name: "Worksheet Name",
            filename: "SomeFile.xls", // do include extension
            preserveColors: false // set to true if you want background colors and font colors preserved
        });

        $('td.add_for_print_excel').remove()
    });
            
    </script>
@endsection
