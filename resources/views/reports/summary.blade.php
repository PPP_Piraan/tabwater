@extends('layouts.adminlte')
<?php
     use App\Http\Controllers\Api\FunctionsController; 
     $apiFuncCtrl = new FunctionsController();
?>
@section('mainheader')
รายงานสรุปการชำระค่าน้ำประปา
@endsection
@section('nav')
<a href="{{'reports'}}">รายงาน</a>
@endsection
@section('report-summary')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }
    th, th div{
        text-align: center
    }
    .subzone_name{
        padding-left: 20px;
        padding-right: 20px;
    }
    .water_used{
        background-color: lightblue;
        text-align: right
    }
    .inv_period_header{
        /* background-color: lightblue; */

    }

</style>
@endsection

@section('content')


{{-- ตารางสรุปแยกรอบบิล --}}
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <form action="{{ url('reports/summary') }}" method="GET" class="row">
                ปีงบประมาณ
                <select name="budgetyear" id="budgetyear" class="form-control col-md-4">
                    @foreach ($budgetyears as $budgetyear)
                        <option value="{{$budgetyear->id}}" {{$budgetyear->id == $selectedBudgetYear[0]->id ? 'selected': ''}}> {{ $budgetyear->budgetyear }} </option>
                    @endforeach
                </select>
                <input type="submit" value="ค้นหา" class="btn btn-info col-md-3 ml-1">
            </form>
        </div>
        <div class="card-tools">
            <button class="btn btn-primary" id="printBtn">ปริ้น</button>
            <button class="btn btn-success" id="excelBtn">Excel</button>
        </div>
    </div>
    <div id="DivIdToExport"> 
       
        <div class="card-body">
            <div class="card">
                <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                    <h3 class="card-title">
                        <i class="fas fa-th mr-1"></i>
                        <table><tr><td colspan="17" class="text-center h5">สรุปยอดการจ่ายค่าน้ำประปาแยกตามรอบบิล ปีงบประมาณ {{ $selectedBudgetYear[0]->budgetyear }}</td></tr></table>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered text-nowrap" id="example1">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="subzone_name"> หมู่ที่</th>
                                    @foreach ($res as $items)
                                        @foreach ($items as $item)
                                        <th colspan="2" class="inv_period_header"> รอบบิลที่ {{$item['invoice_period']}}</th>
                
                                        @endforeach
                                        <?php break; ?>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($res as $items)
                                        @foreach ($items as $item)
                                        <th >จำนวน<div>(ราย)</div></th>
                                        <th >จำนวนที่ใช้น้ำ</th>
                                        {{-- <th >เก็บเงินได้</th>
                                        <th >ค่ารักษามิเตอร์</th>
                                        <th >เป็นเงิน</th>
                                        <th class="text-red" >ค้าง</th> --}}
                                        @endforeach
                                        <?php break; ?>
                                    @endforeach
                
                                </tr>
                            </thead>
                            <tbody>
                               
                                @foreach ($res as  $items)
                               
                                <tr>
                                    <td>{{ $items[0]['subzone_name'] }}</td>
                                @foreach ($items as $item)
                                   
                                    <td class="text-right members">{{ $item['members'] }}</td>
                                    <td class="text-right">{{ number_format($item['water_used']) }}</td>
                                    {{-- <td class="text-right">{{ number_format($item['water_used_paid'] ) }}</td>
                                    <td class="text-right">{{ number_format($item['reserve_paid']) }}</td>
                                    <td class="water_used text-right">{{ number_format($item['water_used_paid'] + $item['reserve_paid']) }}</td>
                                    <td class="text-red text-right">{{ number_format($item['water_used_owe'] + $item['reserve_owe']) }}</td> --}}
                                {{-- {{dd($item)}} --}}
                                @endforeach
                            </tr>
                               
                                @endforeach
                
                                <tr>
                                    <td></td>

                                   @foreach ($res_totals as $item)
                                       <td  class="text-right">{{ number_format($item['members_total']) }}</td>
                                       <td  class="text-right">{{ number_format($item['water_used_total']) }}</td>
                                   @endforeach

                                </tr>
                           
                            </tbody>
                        </table>
                    </div>
                    {{-- <table class="table  table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center align-top" rowspan="2">รอบบิล</th>
                                <th class="text-center align-top" rowspan="2">เดือน</th>
                                <th colspan="3" class="text-center bg-success">ชำระเงินแล้ว</th>
                                <th colspan="3" class="text-center bg-warning">ค้างชำระ</th>
                                <th class="text-center align-top bg-info" rowspan="2">รวมทั้งหมด</th>
                            </tr> 
                            <tr>
                            
                                <th class="text-right">ค่าน้ำประปา</th>
                                <th class="text-right">ค่ารักษามิเตอร์</th>
                                <th class="text-right bg-success">รวม</th>
                                <th class="text-right">ค่าน้ำประปา</th>
                                <th class="text-right">ค่ารักษามิเตอร์</th>
                                <th class="text-right bg-warning">รวม</th>
                            </tr>
                        </thead>
                        <tbody>
                            <php
                                $paid_water_used_total      = 0;
                                $paid_reserve_meter_total   = 0;
                                $paid_total_total           = 0;
                                $owe_water_used_total       = 0;
                                $owe_reserve_meter_total    = 0;
                                $owe_total_total            = 0;
                                $total                      = 0;
                            ?>
                            @foreach ($groupByinvoicePeriod as $item)
                            <php
                                $paid_water_used_total += $item['paid']['water_used_sum'];
                                $paid_reserve_meter_total += $item['paid']['reservemeter_sum'];
                                $paid_total_total += $item['paid']['total'];
                                $owe_water_used_total += $item['oweAndInvoice']['water_used_sum'];
                                $owe_reserve_meter_total += $item['oweAndInvoice']['reservemeter_sum'];
                                $owe_total_total += $item['oweAndInvoice']['total'];
                                $total += $item['paid']['total'] + $item['oweAndInvoice']['total'];
                            ?>
                            <tr>
                                <td class="text-right">{{ $item['inv_period_name'] }}</td>
                                <td class="text-right">{{ $item['monthName'] }}</td>
                                <td class="text-right">{{ number_format($item['paid']['water_used_sum']) }}</td>
                                <td class="text-right">{{ number_format($item['paid']['reservemeter_sum']) }}</td>
                                <td class="text-right bg-success">{{ number_format($item['paid']['total']) }}</td>
                                <td class="text-right">{{ number_format($item['oweAndInvoice']['water_used_sum']) }}</td>
                                <td class="text-right">{{ number_format($item['oweAndInvoice']['reservemeter_sum']) }}</td>
                                <td class="text-right bg-warning">{{ number_format($item['oweAndInvoice']['total']) }}</td>
                                <td class="text-right bg-info">{{ number_format($item['paid']['total'] + $item['oweAndInvoice']['total'] ) }}</td>            
                            </tr>
                            @endforeach
                            <tr>
                                <th class="bg-primary text-center" colspan="2"> รวม</th>
                                <th class="bg-primary text-right">{{ number_format($paid_water_used_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($paid_reserve_meter_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($paid_total_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($owe_water_used_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($owe_reserve_meter_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($owe_total_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($total) }}</th>
                            </tr>
                        </tbody>
                    </table> --}}
                </div>
            </div>

            {{-- <div class="card">
                <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                    <h3 class="card-title">
                        <i class="fas fa-th mr-1"></i>
                        <table><tr><td>สรุปยอดการจ่ายค่าน้ำประปาแยกตามเดือน ปีงบประมาณ {{ $selectedBudgetYear[0]->budgetyear }}</td></tr></table>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
            
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center align-top" rowspan="2">เดือน</th>
                                <th colspan="3" class="text-center bg-success">ชำระเงินแล้ว</th>
                                <th colspan="3" class="text-center bg-warning">ค้างชำระ</th>
                                <th class="text-center align-top bg-info" rowspan="2">รวมทั้งหมด</th>
                            </tr> 
                            <tr>
                                <th class="text-center">ค่าน้ำประปา</th>
                                <th class="text-center">ค่ารักษามิเตอร์</th>
                                <th class="text-center bg-success">รวม</th>
                                <th class="text-center">ค่าน้ำประปา</th>
                                <th class="text-center">ค่ารักษามิเตอร์</th>
                                <th class="text-center bg-warning">รวม</th>
                            </tr>
                        </thead>
                        <?php
                            $paid_water_used_total2      = 0;
                            $paid_reserve_meter_total2   = 0;
                            $paid_total_total2           = 0;
                            $owe_water_used_total2       = 0;
                            $owe_reserve_meter_total2    = 0;
                            $owe_total_total2            = 0;
                            $total2                      = 0;
                        ?>
                    @foreach ($groupedByMonth as $item)
                        <?php
                            $paid_water_used_total2     += $item['data']['paid']['used_water_paid'];
                            $paid_reserve_meter_total2  += $item['data']['paid']['paid_reserve'];
                            $paid_total_total2          += $item['data']['paid']['paidTotal'];
                            $owe_water_used_total2      += $item['data']['owe']['used_water_paid'];
                            $owe_reserve_meter_total2   += $item['data']['owe']['owe_reserve'];
                            $owe_total_total2           += $item['data']['owe']['oweTotal'];
                            $total2                     += $item['data']['paid']['paidTotal']  + $item['data']['owe']['oweTotal'];
                        ?>
                        <tr>
                            <td class="text-right">{{$item['month']}}</td>
                            <td class="text-right">{{number_format($item['data']['paid']['used_water_paid'])}}</td>
                            <td class="text-right">{{number_format($item['data']['paid']['paid_reserve'])}}</td>
                            <td class="text-right bg-success">{{ number_format( $item['data']['paid']['paidTotal'] ) }}</td>
                            <td class="text-right">{{number_format($item['data']['owe']['used_water_paid'])}}</td>
                            <td class="text-right">{{number_format($item['data']['owe']['owe_reserve'])}}</td>
                            <td class="text-right bg-warning">{{ number_format( $item['data']['owe']['oweTotal'] ) }}</td>
                            <td class="text-right bg-info">{{ number_format( $item['data']['paid']['paidTotal']  + $item['data']['owe']['oweTotal'] ) }}</td>
                        </tr>
                    @endforeach
                        <tr>
                            <th class="bg-primary text-center"> รวม</th>
                            <th class="bg-primary text-right">{{ number_format($paid_water_used_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($paid_reserve_meter_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($paid_total_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($owe_water_used_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($owe_reserve_meter_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($owe_total_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($total2) }}</th>
                        </tr>

                    </table>
                </div>
            </div> --}}
        </div>
    </div><!--<div id="DivIdToExport">-->
</div>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <form action="{{ url('reports/summary') }}" method="GET" class="row">
                ปีงบประมาณ
                <select name="budgetyear" id="budgetyear" class="form-control col-md-4">
                    @foreach ($budgetyears as $budgetyear)
                        <option value="{{$budgetyear->id}}" {{$budgetyear->id == $selectedBudgetYear[0]->id ? 'selected': ''}}> {{ $budgetyear->budgetyear }} </option>
                    @endforeach
                </select>
                <input type="submit" value="ค้นหา" class="btn btn-info col-md-3 ml-1">
            </form>
        </div>
        <div class="card-tools">
            <button class="btn btn-primary" id="printBtn2">ปริ้น</button>
            <button class="btn btn-success" id="excelBtn2">Excel</button>
        </div>
    </div>
    <div id="DivIdToExport2"> 
       
        <div class="card-body">
            <div class="card">
                <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                    <h3 class="card-title">
                        <i class="fas fa-th mr-1"></i>
                        <table><tr><td colspan="17" class="text-center h5">สรุปยอดการออกใบเสร็จรับเงินกับจำนวนหน่วยการใช้น้ำทั้งหมดในแต่ละเดือน ปีงบประมาณ {{ $selectedBudgetYear[0]->budgetyear }}</td></tr></table>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered text-nowrap" id="example1">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="subzone_name"> หมู่ที่</th>
                                    @foreach ($res as $items)
                                        @foreach ($items as $item)
                                        <th colspan="6" class="inv_period_header"> รอบบิลที่ {{$item['invoice_period']}}</th>
                
                                        @endforeach
                                        <?php break; ?>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($res as $items)
                                        @foreach ($items as $item)
                                        <th >จำนวน<div>(ราย)</div></th>
                                        <th >จำนวนที่ใช้น้ำ</th>
                                        <th >อัตตราค่าจัด<div>เก็บต่อหน่วย</div></th>
                                        <th >จำนวนเงิน</th>
                                        <th >ค่ารักษามิเตอร์</th>

                                        <th >รวมจำนวนเงิน</th>
                                        @endforeach
                                        <?php break; ?>
                                    @endforeach
                
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0;?>

                                @foreach ($res as  $items)
                                <tr>
                                    <td>{{ $items[0]['subzone_name'] }}</td>
                                @foreach ($items as $item)
                                   
                                    <td class="text-right members{{$i}}">{{ $item['members'] }}</td>
                                    <td class="text-right">{{ number_format($item['water_used']) }}</td>
                                    <td class="text-center">8</td>
                                    <td class="text-right">{{ number_format($item['water_used']*8 ) }}</td>
                                    <td class="text-right">{{ number_format($item['reserve_paid']) }}</td>
                                    <td class=" text-right">{{ number_format(($item['water_used']*8) + $item['reserve_paid']) }}</td>
                                {{-- {{dd($item)}} --}}
                                @endforeach
                                <?php $i++;?>

                            </tr>
                               
                                @endforeach
                            <tfoot>
                                <tr>
                                    <td class="water_used"></td>

                                   @foreach ($res_totals as $item)
                                       <td  class="text-right water_used">{{ number_format($item['members_total']) }}</td>
                                       <td  class="text-right water_used">{{ number_format($item['water_used_total']) }}</td>
                                       <td class="water_used">&nbsp;</td><!-- 8 บาท -->
                                       <td  class="text-right water_used">{{ number_format($item['water_used_total_x_8']) }}</td>
                                       <td  class="text-right water_used">{{ number_format($item['reserve_paid_total']) }}</td>
                                       <td  class="text-right water_used">{{ number_format($item['total']) }}</td>

                                   @endforeach

                                </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                    
        </div>
    </div><!--<div id="DivIdToExport">-->
</div>
@endsection


@section('script')

<script
    src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.12.1/api/sum().js"></script>
</script>

<script src="{{asset('/js/my_script.js')}}"></script>
<script>
    $(document).ready(()=>{
        $('m1').each(()=>{
            console.log($(this).text())
        })
    })
</script>
<script>

        var table = $('#example').DataTable({
                    "pagingType": "listbox",
                    "searching": false,
                    "lengthMenu": [
                        [ -1],
                        [ "ทั้งหมด"]
                    ],
                    "language": {
                        "search": "ค้นหา:",
                        "lengthMenu": "แสดง _MENU_ แถว",
                        "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                        "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                        "paginate": {
                            "info": "แสดง _MENU_ แถว",
                        },
                    
                    },
                    drawCallback: function () {
                        var api = this.api();
                        $( api.table().footer() ).html(
                            api.column( 1, {page:'current'} ).data().sum()
                            
                        );
                       
                    }
                                
                });
    $(document).ready(function(){
        $('.members').each(function() {
            console.log($(this).text())
        });
    })

    $('#printBtn').click(function () {
        var tagid = 'DivIdToExport'
        var hashid = "#" + tagid;
        var tagname = $(hashid).prop("tagName").toLowerCase();
        var attributes = "";
        var attrs = document.getElementById(tagid).attributes;
        $.each(attrs, function (i, elem) {
            attributes += " " + elem.name + " ='" + elem.value + "' ";
        })
        var divToPrint = $(hashid).html();
        var head = "<html><head>" + $("head").html() + "</head>";
        var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
            divToPrint + "</" + tagname + ">" + "</body></html>";
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write(allcontent);
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    })
    $('#printBtn2').click(function () {
        var tagid = 'DivIdToExport2'
        var hashid = "#" + tagid;
        var tagname = $(hashid).prop("tagName").toLowerCase();
        var attributes = "";
        var attrs = document.getElementById(tagid).attributes;
        $.each(attrs, function (i, elem) {
            attributes += " " + elem.name + " ='" + elem.value + "' ";
        })
        var divToPrint = $(hashid).html();
        var head = "<html><head>" + $("head").html() + "</head>";
        var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
            divToPrint + "</" + tagname + ">" + "</body></html>";
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write(allcontent);
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    })

    $('#excelBtn').click(function () {
        $("#DivIdToExport").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    });
    $('#excelBtn2').click(function () {
        $("#DivIdToExport2").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    });

</script>
@endsection

                         
