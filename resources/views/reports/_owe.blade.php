@extends('layouts.adminlte')

@section('mainheader')
ผู้ค้างชำระค่าน้ำประปา
@endsection
@section('nav')
    <a href="{{'reports'}}">รายงาน</a>
@endsection
@section('report-owe')
    active
@endsection
@section('style')
    <style>
        .hidden{display: none}
    </style>
@endsection

@section('content')
<?php 
    $owe_total_count        = 0;
    $reserve_total_sum      = 0;
    $owe_total_sum          = 0;
    $meter_unit_used_sum    = 0;
    $all_total_sum          = 0;
?>

{{-- <div class="row">
    <div class="col-7">
        <div class="card">
            <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
            <h3 class="card-title">
                <i class="fas fa-th mr-1"></i>
                สรุปยอดการจ่ายค่าน้ำประปาแยกตามรอบบิล
            </h3>

            <div class="card-tools">
                <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
                </button>
            </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <tr>
                        <th>รอบบิล</th>
                        <th class="text-center">ประจำเดือน</th>
                        <th class="text-right">ยอดชำระค่าน้ำประปา (บาท)</th>
                    </tr>
                    <php 
                        $paidSummaryByInvPeroid = [
                            [
                                'inv_period_name'=> 'sss',
                                'inv_period_name_th' => 'sss',
                                'sum' => 200
                                        ],
                                        [
                                'inv_period_name'=> 'sss',
                                'inv_period_name_th' => 'sss',
                                'sum' => 200
                                        ],
                                    [
                                'inv_period_name'=> 'sss',
                                'inv_period_name_th' => 'sss',
                                'sum' => 200
                                        ],

                        ]

                    ?>
                    @foreach ($paidSummaryByInvPeroid as $item)
                    <tr>
                        <td>{{ $item['inv_period_name'] }}</td>
                        <td class="text-right"> {{ $item['inv_period_name_th'] }}</td>
                        <td class="text-right">{{ number_format($item['sum']) }}</td>
                    </tr>
                    @endforeach

                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <div class="col-5"></div>
</div> --}}
<div class="card">
    <div class="overlay dark"><i class="fas fa-4x fa-sync-alt fa-spin"></i></div>

    <div class="card-body" id="aa">  
        <form action="{{url('reports/owe')}}" method="get">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>
            
                        <div class="info-box-content">
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="control-label">หมู่ที่</label>
                                    <select class="form-control" name="zone_id" id="zone_id">
                                        <option value="all" selected>ทั้งหมด</option>
                                        @foreach ($zones as $zone)
                                            <option value="{{$zone->id}}">{{$zone->zone_name}}</option>   
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">เส้นทาง</label>
                                    <select class="form-control" name="subzone_id" id="subzone_id">
                                        <option value="all" selected>ทั้งหมด</option>

                                    </select>
                                </div>
                            
                                <?php
                                    $year = date('Y')+543; 
                                    $now = date('d/m/'.$year);
                                ?>
                                <div class="col-md-2">
                                    <label class="control-label">วันที่</label>
                                    <input class="form-control datepicker" type="text" name="invperiodstart"  id="invperiodstart">
                                </div>
                                <div class="col-md-1 text-center">
                                    <label class="control-label">&nbsp;</label>
                                    <input class="form-control text-center border-0 font-weight-bold" type="text" value="ถึง">

                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">วันที่</label>
                                    <input class="form-control datepicker" type="text" name="invperiodend"  id="invperiodend">
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">&nbsp;</label>
                                    <button type="submit" class="form-control btn btn-primary">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                </div>
            </div>             
        </form>
        @if (collect($oweInfos)->isEmpty())
            @if ($start != 'all')
                <div class="card">
                    <div class="card-body"><h4 class="text-center">ไม่พบข้อมูล</h4></div>
                </div>
                
            @endif
        @else

            <div class="row">

                <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                    <h3>{{number_format(collect($oweInfos)->count())}}</h3>

                    <p>จำนวนคนค้าง(คน)</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-bag"></i>
                    </div>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                    <h3 id="meter_unit_used_sum"></h3>

                    <p>จำนวนหน่วย</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                    <h3 id="owe_total_sum"></h3>

                    <p>ค่าน้ำประปา (บาท)</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-person-add"></i>
                    </div>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                    <h3 id="reserve_total_sum"></h3>

                    <p>ค่ารักษามิเตอร์ (บาท)</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                    </div>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner text-right">
                        <h3 id="all_total_sum"></h3>
        
                        <p class="text-right">รวมทั้งสิ้น (บาท)</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                        </div>
                    </div>
                    </div>
                    <!-- ./col -->
            </div>

            
            <div class="card">
                <div class="card-header">
                    <div class="card-title"></div>
                    <div class="card-tools">
                        <button class="btn btn-primary" id="printBtn">ปริ้น</button>
                        <button class="btn btn-success" id="excelBtn">Excel</button>
                    </div>
                </div>

                <div class="card-body table-responsive">
                    <div id="DivIdToExport">
                        <table id="oweTable" class="table text-nowrap" width="100%">
                            <thead>
                                <tr>
                                    <td colspan="14" class="text-center h4">รายงานค้างชำระค่าน้ำประปา</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="text-center">{{ $start == 'all' ? 'หมู่ที่ 1-19' : 'หมู่ที่ '.$oweInfos[0][0]['zone_name'].' เส้นทาง '.$oweInfos[0][0]['subzone_name']}}</td>
                                    <td colspan="8" class="text-center">
                                        @if ($start != 'all')
                                            {{'วันที่ '.$start.' - '.$end }}
                                        @else 
                                            <div id="initdate"></div>
                                        @endif 
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <th>รหัส</th>
                                    <th>มิเตอร์</th>
                                    <th >ชื่อ-สกุล</th>
                                    <th>บ้านเลขที่</th>
                                    <th>หมู่ที่</th>
                                    <th>เส้นทาง</th>
                                    <th>รอบบิลที่</th>
                                    <th>ยกยอดมา</th>
                                    <th>ปัจจุบัน</th>
                                    <th>จำนวนมาตร</th>
                                    <th>ค่าน้ำประปา</th>
                                    <th>ค่ารักษามิเตอร์</th>
                                    <th>เป็นเงิน</th>
                                </tr>
                            </thead> 
                            <tbody>
                                @foreach ($oweInfos as $key => $infos)
                                    <?php $i =1; ?>
                                    @foreach ($infos as $owe)
                                    <?php
                                        $diff = $owe['currentmeter'] - $owe['lastmeter'];
                                        $meter_reserve_price = $diff == 0 ? 10 : 0;
                                        $_total = $diff * 8;
                                        $total = $_total + $meter_reserve_price;

                                        $meter_unit_used_sum += $diff;
                                        $reserve_total_sum   += $meter_reserve_price;
                                        $owe_total_sum       += $_total;
                                        $all_total_sum       += $total;
                                    ?>
                                    <tr>
                                    
                                        @if ($i++ ==1)
                                            <?php 
                                                //นับจำนวนคนค้าง
                                                ++$owe_total_count ;
                                            ?>
                                            
                                            <td class="text-right">{{$infos[0]['user_id_str']}}</td>
                                            <td class="text-right">{{$infos[0]['meternumber']}}</td>
                                            <td>{{$infos[0]['name']}}</td>
                                            <td class="text-right">{{$infos[0]['address']}}</td>
                                            <td class="text-right">{{$infos[0]['zone_name']}}</td>
                                            <td class="text-right">{{$infos[0]['subzone_name']}}</td>
                                        @else
                                            <td style="border-top: none"></td>
                                            <td style="border-top: none"></td>
                                            <td style="border-top: none"></td>
                                            <td style="border-top: none"></td>
                                            <td style="border-top: none"></td>
                                            <td style="border-top: none"></td>
                                        @endif
                                        
                                            <td class="text-right">{{$owe['inv_period_name']}}</td>
                                            <td class="text-right">{{ number_format($owe['lastmeter'])}}</td>
                                            <td class="text-right">{{ number_format($owe['currentmeter'])}}</td>
                                            <td class="text-right unit_used_sum">{{ number_format($diff) }}</td>
                                            <td class="text-right water_price_sum">{{ number_format($_total) }}</td>
                                            <td class="text-right meter_reserve_price_sum">{{ number_format($meter_reserve_price) }}</td>
                                            <td class="text-right">{{ number_format($total)}}</td>
                                    </tr>           
                                    
                                    @endforeach
                                @endforeach
                                @if (collect($oweInfos)->count() == $owe_total_count)
                                    <tr>
                                        <th class="h4">สรุป</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-right h5">{{number_format($meter_unit_used_sum)}}</th>
                                        <th class="text-right h5 ">{{number_format($owe_total_sum)}}</th>
                                        <th class="text-right h5">{{number_format($reserve_total_sum)}}</th>
                                        <th class="text-right h5">{{number_format($all_total_sum)}}</th>
                                    </tr>

                                @endif
                            </tbody>
                        </table>
                        <input type="hidden" value="{{number_format($meter_unit_used_sum)}}" class="meter_unit_used_sum">
                        <input type="hidden" value="{{number_format($owe_total_sum)}}" class="owe_total_sum">
                        <input type="hidden" value="{{number_format($reserve_total_sum)}}" class="reserve_total_sum">
                        <input type="hidden" value="{{number_format($all_total_sum)}}" class="all_total_sum">
                    </div>
                </div><!--card-body-->
            </div>
             
        @endif
    </div>

</div>
 @endsection


@section('script')

<script src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js"></script>

<script src="{{asset('/js/my_script.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true,
        }).datepicker("setDate", new Date());;  //กำหนดเป็นวันปัจุบัน


        //แสดงinitdate
        let invperiodstart = $('#invperiodstart').val();
        let invperiodend = $('#invperiodend').val();
        $('#initdate').text(`วันที่ ${invperiodstart} - ${invperiodend}`);
    })
    $('#oweTable').DataTable({
        responsive: true,
        order: false,
        "pagingType": "listbox",
        "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "ทั้งหมด"]],
        "language": {
            "search": "ค้นหา:",
            "lengthMenu": "แสดง _MENU_ แถว", 
            "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
            "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
            "paginate": {
                "info": "แสดง _MENU_ แถว",
            },
        }
    })

    $(document).ready(function(){
        $('.paginate_page').text('หน้า')
        let val = $('.paginate_of').text()
        $('.paginate_of').text(val.replace('of', 'จาก')); 

        //เอาค่าผลรวมไปแสดงตารางบนสุด
        $('#meter_unit_used_sum').html($('.meter_unit_used_sum').val())                     
        $('#owe_total_sum').html($('.owe_total_sum').val())                      
        $('#reserve_total_sum').html($('.reserve_total_sum').val())                       
        $('#all_total_sum').html($('.all_total_sum').val())
        $('.overlay').remove()
    })//document

    $('#zone_id').change(function(){
        //get ค่าsubzone 
        $.get(`../api/subzone/${$(this).val()}`)
            .done(function(data){
                let text= '<option>เลือก</option>';
                data.forEach(element => {
                    text +=`<option value="${element.id}">${element.subzone_name}</option>`
                });
                $('#subzone_id').html(text)
            });
    });  
        
           
    $('#printBtn').click(function(){
        var tagid = 'oweTable'
        var hashid = "#"+ tagid;
        var tagname =  $(hashid).prop("tagName").toLowerCase() ;
        var attributes = ""; 
        var attrs = document.getElementById(tagid).attributes;
            $.each(attrs,function(i,elem){
            attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
            })
        var divToPrint= $(hashid).html() ;
        var head = "<html><head>"+ $("head").html() + "</head>" ;
        var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write(allcontent);
        newWin.document.close();
        setTimeout(function(){newWin.close();},10);
        })
    
        $('#excelBtn').click(function(){
            $("#DivIdToExport").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    })
    
</script>
@endsection
