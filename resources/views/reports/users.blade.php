@extends('layouts.adminlte')

@section('mainheader')
    ข้อมูลผู้ใช้น้ำประปา
@endsection
@section('nav')
    <a href="{{ url('/reports') }}"> รายงาน</a>
@endsection
@section('report-users')
    active
@endsection
@section('style')
    <style href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"></style>
    <style>
        th,
        td {
            white-space: nowrap;
        }

        .hide {
            display: none
        }

        .username {
            color: blue;
            cursor: pointer;
            /* padding-left: 20px */
        }

    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-2">
            <label class="control-label">หมู่ที่</label>
            <select class="form-control" name="zone_id" id="zone_id">
                <option value="all" selected>ทั้งหมด</option>
                @foreach ($zones as $zone)
                    <option value="{{ $zone->id }}">{{ $zone->zone_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <label class="control-label">เส้นทาง</label>
            <select class="form-control" name="subzone_id" id="subzone_id"></select>
        </div>

        <div class="col-md-2">
            <label class="control-label">&nbsp;</label>
            <button type="button" class="form-control btn btn-primary searchBtn">ค้นหา</button>
        </div>
    </div>
    <div class="card mt-2">
        <div class="card-header">
            <div class="card-title"></div>
            <div class="card-tools">
                <button class="btn btn-primary" id="printBtn">ปริ้น</button>
                <button class="btn btn-success" id="excelbtn2">Excel</button>
            </div>
        </div>
        <div class="card-body">
            <div class="p-0  pb-3">
                <table id="example" class="display " width="100%"></table>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <script
        src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
    </script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    {{-- <script src="https://cdn.datatables.net/buttons/2.0.0/js/buttons.print.min.js"></script> --}}
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script type="text/javascript" language="javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="{{ asset('/js/my_script.js') }}"></script>

    <script>
        let table;
        let a = true
        let col_index = -1
        let selected_rows = []

        $(document).ready(function() {
            getUsers('all');
        })

        $('#zone_id').change(function() {
            let text = '';
            let zone_id = $(this).val();
            if (zone_id == 'all') {
                text += `<option value="all" selected>ทั้งหมด</option>`;
                $('#subzone_id').html(text)
            } else {
                $.get(`../../api/subzone/${zone_id}`).done(function(data) {
                    if (data.length > 1) {
                        text += `<option value="all" selected>ทั้งหมด</option>`;
                    }
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.subzone_name}</option>`;
                    });
                    $('#subzone_id').html(text)
                });
            }
        });

        $('.searchBtn').click(function() {
            let subzone = $('#subzone_id').val();
            console.log(subzone)
            getUsers(subzone);
        })

        function getUsers(subzone) {
            $.get(`../../api/users/report_by_subzone/${subzone}`).done(function(data) {
                // console.log('data',data)
                $('#example').html(`
                <tr class="overlay_tr">
                    <td>
                        <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div></div>
                    </td>
                </tr>
            `);

            table = $('#example').DataTable({
                destroy: true,
                "pagingType": "listbox",
                "lengthMenu": [
                    [10, 25, 50, 150, -1],
                    [10, 25, 50, 150, "ทั้งหมด"]
                ],
                "language": {
                    "search": "ค้นหา:",
                    "lengthMenu": "แสดง _MENU_ แถว",
                    "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                    "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                    "paginate": {
                        "info": "แสดง _MENU_ แถว",
                    },
                },
                select: {
                    style: 'multi'
                },
                data: data,
                columns: [
                    {
                                'title': '',
                                data: 'user_id',
                                orderable: false,
                                render: function (data) {
                                    return `
                                    <input type="checkbox" class="invoice_id" style="opacity:0" 
                                            name="user_id[${data}]">
                                    <i class="fa fa-plus-circle text-success findInfo" 
                                            data-user_id="${data}"></i>`
                                }
                            },
                    {
                        'title': 'เลขผู้ใช้งาน',
                        data: 'user_id_str'
                    },
                    {
                        'title': 'ชื่อ-สกุล',
                        data: 'name'
                    },
                    {
                        'title': 'เลขมิเตอร์',
                        data: 'meternumber'
                    },
                    {
                        'title': 'บ้านเลขที่',
                        data: 'address'
                    },
                    {
                        'title': 'หมู่ที่',
                        data: 'zone_name'
                    },
                    {
                        'title': 'เส้นทาง',
                        data: 'subzone_name'
                    },


                ],
                dom: 'lBfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    'text': 'Excel',
                    customize: function(xlsx) {

                        // Get number of columns to remove last hidden index column.
                        var numColumns = table.columns().header().count();

                        // Get sheet.
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];

                        var col = $('col', sheet);
                        // Set the column width.
                        $(col[1]).attr('width', 20);

                        // Get a clone of the sheet data.        
                        var sheetData = $('sheetData', sheet).clone();

                        // Clear the current sheet data for appending rows.
                        $('sheetData', sheet).empty();

                        // Row count in Excel sheet.
                        var rowCount = 1;
                        var selected_rows = [];
                        $('.selected').each((index) => {
                            selected_row.push(index)
                        })
                        console.log('selected_rows', selected_rows)
                        // Itereate each row in the sheet data.
                        $(sheetData).children().each(function(index) {

                            // Used for DT row() API to get child data.
                            // if(row.indexOf(index)){
                            var rowIndex = index - 1;

                            // Don't process row if its the header row.
                            if (index > 0) {

                                // Get row
                                var row = $(this.outerHTML);

                                // Set the Excel row attr to the current Excel row count.
                                row.attr('r', rowCount);

                                var colCount = 1;

                                // Iterate each cell in the row to change the rwo number.
                                row.children().each(function(index) {
                                    var cell = $(this);

                                    // Set each cell's row value.
                                    var rc = cell.attr('r');
                                    rc = rc.replace(/\d+$/, "") + rowCount;
                                    cell.attr('r', rc);

                                    if (colCount === numColumns) {
                                        cell.html('');
                                    }

                                    colCount++;
                                });

                                // Get the row HTML and append to sheetData.
                                row = row[0].outerHTML;
                                $('sheetData', sheet).append(row);
                                rowCount++;

                                // Get the child data - could be any data attached to the row.
                                var childData = table.row(':eq(' + rowIndex + ')')
                                    .data().results;
                                if (childData.length > 0) {
                                    // Prepare Excel formated row
                                    headerRow = `
                                <row r="${rowCount}" s="2">
                                    <c t="inlineStr" r="A${rowCount} "><is><t></t></is></c>
                                    <c t="inlineStr" r="B${rowCount} "><is><t></t></is></c>
                                    <c t="inlineStr" r="C${rowCount} " s="2"><is><t>เลขใบแจ้งหนี้</t></is></c>
                                    <c t="inlineStr" r="D${rowCount} " s="2"><is><t>รอบบิล</t></is></c>
                                    <c t="inlineStr" r="E${rowCount} " s="2"><is><t>ยอดปัจจุบัน</t></is></c>
                                    <c t="inlineStr" r="F${rowCount} " s="2"><is><t>ยอดครั้งก่อน</t></is></c>
                                    <c t="inlineStr" r="G${rowCount} " s="2"><is><t>จำนวนที่ใช้</t></is></c>
                                    <c t="inlineStr" r="H${rowCount} " s="2"><is><t>ค่าใช้น้ำ(บาท)</t></is></c>
                                    <c t="inlineStr" r="I${rowCount} " s="2"><is><t>ค่ารักษามิเตอร์(บาท)</t></is></c>
                                    <c t="inlineStr" r="J${rowCount} " s="2"><is><t>เป็นเงิน(บาท)</t></is></c>
                                </row>`;
                                    // Append header row to sheetData.
                                    if ($('body input[type="checkbox"]').is(
                                            ':checked')) {
                                        $('sheetData', sheet).append(headerRow);
                                        rowCount++; // Inc excelt row counter.

                                    }

                                }
                                // The child data is an array of rows
                                for (c = 0; c < childData.length; c++) {
                                    // Get row data.
                                    child = childData[c];
                                    // Prepare Excel formated row
                                    childRow = `
                                    <row r="${rowCount}" s="2">
                                        <c t="inlineStr" r="A${rowCount}"><is><t></t></is></c>
                                        <c t="inlineStr" r="B${rowCount}"><is><t></t></is></c>
                                        <c t="inlineStr" r="C${rowCount}"><is><t> ${child.invoice_id}</t></is></c>
                                        <c t="inlineStr" r="D${rowCount}"><is><t> ${child.inv_period_name}</t></is></c>
                                        <c t="inlineStr" r="E${rowCount}"><is><t> ${child.currentmeter}</t></is></c>
                                        <c t="inlineStr" r="F${rowCount}"><is><t> ${child.lastmeter}</t></is></c>
                                        <c t="inlineStr" r="G${rowCount}"><is><t> ${child.water_used}</t></is></c>
                                        <c t="inlineStr" r="H${rowCount}"><is><t> ${child.paid}</t></is></c>
                                        <c t="inlineStr" r="I${rowCount}"><is><t> ${child.reserve_paid}</t></is></c>
                                        <c t="inlineStr" r="J${rowCount}"><is><t> ${child.paid_net}</t></is></c>
                                    </row>`;

                                    // Append row to sheetData.
                                    if ($('body input[type="checkbox"]').is(
                                            ':checked')) {
                                        $('sheetData', sheet).append(childRow);
                                        rowCount++; // Inc excelt row counter.

                                    }

                                }
                                // Just append the header row and increment the excel row counter.
                            } else {
                                var row = $(this.outerHTML);

                                var colCount = 1;

                                // Remove the last header cell.
                                row.children().each(function(index) {
                                    var cell = $(this);

                                    if (colCount === numColumns) {
                                        cell.html('');
                                    }

                                    colCount++;
                                });
                                row = row[0].outerHTML;

                                $('sheetData', sheet).append(row);

                                rowCount++;
                            }
                            //}//indexof
                        });
                    },
                }],

            });

            $('#example').find('.overlay_tr').remove()
            $('#example thead').find('#title').remove()
            let title = 'ตารางข้อมูลผู้ใช้น้ำประปาบ้านห้องแซง';
            title += $('#zone_id').val() === 'all' ? 'หมู่ที่ 1 - 19' : data[0].zone_name;

            $('#example thead').prepend(
                `<tr id="title"><td colspan="7" class="text-center h4">${title}</td></tr>`)

            $('.paginate_page').text('หน้า')
            let val = $('.paginate_of').text()
            $('.paginate_of').text(val.replace('of', 'จาก'));

            if (a) {
                $('#example thead tr:eq(1)').clone().appendTo('#example thead');
                a = false
            }
            $('#example thead tr:eq(2) th').each(function(index) {
                var title = $(this).text();
                $(this).removeClass('sorting')
                $(this).removeClass('sorting_asc')
                if (index > 0 && index <= 4) {
                    $(this).html(
                        `<input type="text" data-id="${index}" class="form-control" id="search_col_${index}" placeholder="ค้นหา" />`
                        );
                } else {
                    $(this).html('')
                }
                $('input[type="text"]').keyup(function() {
                    let that = $(this)
                    var col = parseInt(that.data('id'))
                    var _val = that.val()
                    if (col === 1 || col === 4) {
                        var val = $.fn.dataTable.util.escapeRegex(
                            _val
                        );
                        table.column(col)
                            .search(val ? '^' + val + '.*$' : '', true, false)
                            .draw();
                    } else {
                        table.column(col)
                            .search(_val)
                            .draw();
                    }
                    col_index = col
                })

                $('input[type="text"]').focus(function() {
                    var col = parseInt($(this).data('id'))

                    if (col !== col_index && col_index >= 0) {
                        $('input[type="text"]').val('')

                    }
                    col_index = col
                })



            }); //$('#example thead tr:eq(2) th')
            $('.overlay').remove()
            $('#example_filter').remove()
            $('.dt-buttons').prepend(`
                <button class="dt-button buttons-excel buttons-html5 show_all_btn all" >เลือกทั้งหมด</button>
                <input type="checkbox" class="dt-button buttons-excel buttons-html5 show_detail mr-1 mt-1" >แสดงข้อมูลค้างชำระ </input> 
            `);
            $('.dt-buttons').append(
                `<button class="dt-button buttons-excel buttons-html5" data-printtype="" id="printBtn">ปริ้น</button> `
            );

            }); //table
        } //fn get

        $('body').on('click', '.findInfo', function () {
            let user_id = $(this).data('user_id')

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            console.log('row', row)
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                //หาข้อมูลการชำระค่าน้ำประปาของ  user
                $.get(`../../api/users/user/${user_id}`).done(function (data) {
                    console.log('user_id', data)
                    row.child(format(data)).show();
                    tr.prop('shown');
                });

            }
            if ($(this).hasClass('fa-plus-circle')) {
                $(this).removeClass('fa-plus-circle')
                $(this).removeClass('text-success')
                $(this).addClass('fa-minus-circle')
                $(this).addClass('text-info')

                // aa(user_id, tr)

            } else {
                $(this).addClass('fa-plus-circle')
                $(this).addClass('text-success')
                $(this).removeClass('fa-minus-circle')
                $(this).removeClass('text-info');
            }

        });
        function aa(user_id, tr) {
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                //หาข้อมูลการชำระค่าน้ำประปาของ  user
                $.get(`../../api/users/user/${user_id}`).done(function(data) {
                    // console.log('xx',format(data))
                    row.child(format(data)).show();
                    tr.addClass('shown');
                });

            }

        }

        $('#printBtn').click(function() {
            var tagid = 'example'
            var hashid = "#" + tagid;
            var tagname = $(hashid).prop("tagName").toLowerCase();
            var attributes = "";
            var attrs = document.getElementById(tagid).attributes;
            $.each(attrs, function(i, elem) {
                attributes += " " + elem.name + " ='" + elem.value + "' ";
            })
            var divToPrint = $(hashid).html();
            var head = "<html><head>" + $("head").html() + "</head>";
            var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
                divToPrint + "</" + tagname + ">" + "</body></html>";
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write(allcontent);
            newWin.document.close();
            setTimeout(function() {
                newWin.close();
            }, 10);
        })
        $('#excelbtn2').click(function() {
            $("#example").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                name: "Worksheet Name",
                filename: 'aa', //do not include extension
                fileext: ".xls" // file extension
            })
        })


        $('body').on('click', '.show_all_btn', function() {
            let _val = $(this).hasClass('all') ? 'all' : '';
            openAllChildTable(_val)
        })

        

        $('body').on('click', '#printBtn', function() {
            let texttitle = `<tr id="texttitle"><th colspan="9">รายงานผู้ค้างชำระค่าน้ำประปา</th></tr>`
            $('#example thead tr:eq(1)').addClass('hidden')

            $('#example thead').prepend(texttitle)
            var tagid = 'example'
            var hashid = "#" + tagid;
            var tagname = $(hashid).prop("tagName").toLowerCase();
            var attributes = "";
            var attrs = document.getElementById(tagid).attributes;
            $.each(attrs, function(i, elem) {
                attributes += " " + elem.name + " ='" + elem.value + "' ";
            })
            var divToPrint = $(hashid).html();
            var head = '<html><head>' +
                $("head").html() +
                ' <style>body{background-color:white !important;}@page { size: landscape; }</style></head>';
            var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
                divToPrint + "</" + tagname + ">" + "</body></html>";
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write(allcontent);
            newWin.document.close();
            setTimeout(function() {
                newWin.close();
                $('#texttitle').remove()

                $('#example thead tr:eq(1)').removeClass('hidden')

            }, 220);

        })

        function openAllChildTable(_val) {
            console.log('openAllChildTable', _val)
            selected_rows.length = 0;

            if (_val === 'all') {
                $("table > tbody > tr[role='row']").each(function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    if ($('.show_detail').is(':checked')) {
                        row.child(format(row.data())).show();

                    }
                    tr.addClass('shown');
                    tr.addClass('selected')

                    $('.show_all_btn').removeClass('all')
                    $('.show_all_btn').text('ยกเลิกเลือกทั้งหมด')
                    selected_rows.push(row.data().user_id)
                })

            } else {
                $("table > tbody > tr[role='row']").each(function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    row.child.hide();
                    tr.removeClass('shown');
                    setTimeout(() => {
                        tr.removeClass('selected');
                    }, 50)
                    $('.show_all_btn').addClass('all')
                    $('.show_all_btn').text('เลือกทั้งหมด')
                    $('.show_detail').prop('checked', false);

                })

            }
            console.log('selected_rows_all', selected_rows)
        }

        //click เลือก ที่ละ row
        $('#example').on('click', 'tr[role="row"]', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (tr.hasClass('selected')) {
                //เอา selected, shown  ออก เอา row.child ออก
                row.child.hide();
                tr.removeClass('shown');
                setTimeout(() => {
                    tr.removeClass('selected')
                }, 100)

                selected_rows = selected_rows.filter(function(item) {
                    // console.log('item', item)
                    return item.user_id !== row.data().user_id
                })
            } else {
                var data = row.data();
                selected_rows.push(data)

                // if ($('.show_detail').is(':checked')) {
            
                    showPaidInvoiceInfo(data)
               // }
                tr.addClass('shown');
                setTimeout(() => {
                    tr.addClass('selected')
                }, 50)
            }
            // console.log('selected_rows', selected_rows)
        });

        $('#zone_id').change(function() {
            //get ค่าsubzone 
            let zone_id = $(this).val()

            $.get(`../api/subzone/${zone_id}`)
                .done(function(data) {
                    let text = zone_id !== 'all' ? '<option value="">เลือก</option>' :
                        '<option value="all">ทั้งหมด</option>';
                    if (data.length > 1) {
                        text += `<option value="all">ทั้งหมด</option>`;
                    }
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.subzone_name}</option>`
                    });
                    $('#subzone_id').html(text)
                });
        });

        $('.searchBtn').click(function() {
            let zone_id = $('#zone_id').val()
            let subzone_id = $('#subzone_id').val()
            if (zone_id !== 'all' && subzone_id === '') {
                alert('ยังไม่ได้เลือกเส้นทาง')
                $('#subzone_id').addClass('border border-danger rounded')
                return false
            }
            $('#example').DataTable().destroy();

            getOweInfos(zone_id, subzone_id)
        });

        function showPaidInvoiceInfo(data) {

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            if (row.child.isShown()) {
                console.log('rows', data)

                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {

                // Open this row
                //หาข้อมูลการชำระค่าน้ำประปาของ  user
                $.get(`../../api/users/user/${data.user_id}`).done(function(data) {
                    setTimeout(()=>{

                        row.child(format(data)).show();
                        tr.addClass('shown');

                    },100)
                    
                });

            }
            if ($(this).hasClass('fa-plus-circle')) {
                $(this).removeClass('fa-plus-circle')
                $(this).removeClass('text-success')
                $(this).addClass('fa-minus-circle')
                $(this).addClass('text-danger')

                // aa(user_id, tr)

            } else {
                $(this).addClass('fa-plus-circle')
                $(this).addClass('text-success')
                $(this).removeClass('fa-minus-circle')
                $(this).removeClass('text-danger');
            }

        }

        function format(d) {
            var html = `
                        <table class="border border-info rounded-pill" cellpadding="5" cellspacing="0" border="0" style="margin-left:50px;font-size:14.5px !important">
                            <thead>
                                <tr>
                                    <th class=text-center> </th>
                                    <th class=text-center>ใบแจ้งหนี้ </th>
                                    <th class=text-center>รอบบิล </th>
                                    <th class=text-center>ยอดปัจจุบัน </th>
                                    <th class=text-center>ยอดครั้งก่อน </th>
                                    <th class=text-center>จำนวนที่ใช้ </th>
                                    <th class=text-center>ค่าใช้น้ำ(บาท) </th>
                                    <th class=text-center>ค่ารักษามิเตอร์(บาท) </th>
                                    <th class=text-center>เป็นเงิน(บาท) </th>
                                </tr>
                            </thead>
                            <tbody>`;
            for (i = 0; i < d[0].invoice.length; i++) {
                result = d[0].invoice[i];
                let diff = result['currentmeter'] -result['lastmeter'];
                let paid = diff === 0 ? 0 : diff * 8;
                let reserve_paid = diff === 0 ? 10 : 0;
                html += `<tr>
                            <td></td>
                            <td class="text-right">${result['id']}</td>
                            <td class="text-center"> ${result.invoice_period.inv_period_name} </td>
                            <td class="text-right"> ${result['currentmeter']} </td>
                            <td class="text-right"> ${result['lastmeter']} </td>
                            <td class="text-right"> ${diff} </td>
                            <td class="text-right"> ${paid} </td>
                            <td class="text-right"> ${reserve_paid} </td>
                            <td class="text-right"> ${paid + reserve_paid} </td>
                        </tr>`;

            }
            html += `</tbody></table>`;



            return html;
        }
    </script>
@endsection
