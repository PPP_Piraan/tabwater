@extends('layouts.adminlte')

@section('mainheader')
รายงานการชำระค่าน้ำประปาประจำวัน
@endsection
@section('nav')
<a href="{{'reports'}}">รายงาน</a>
@endsection
@section('report-payment')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }
    table thead th{
        text-align: center
    }

</style>
@endsection

@section('content')

<form action="{{url('reports/payment')}}" method="get" onsubmit="return checkValues();">
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="info-box">
                <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>

                <div class="info-box-content">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="search" class="col-sm-4 col-form-label">หมู่ที่:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="zone_id" id="zone_id">
                                        <option value="all" {{$zone_id == "all" ? 'selected' : ''}}>ทั้งหมด</option>
                                        @foreach ($zones as $zone)
                                        <option value="{{$zone->id}}" {{$zone_id == $zone->id ? 'selected' : ''}}>
                                            {{$zone->zone_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="search" class="col-sm-4 col-form-label">เส้นทาง:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="subzone_id" id="subzone_id">
                                        @if ($subzones == 'all' || $subzones == '')
                                            <option value="all" selected>ทั้งหมด</option>
                                        @else
                                        @foreach ($subzones as $subzone)
                                            <option value="{{ $subzone->id }}"
                                                {{ $subzone_id == $subzone->id ? 'selected' : '' }}>
                                                {{ $subzone->subzone_name }}
                                            </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- {{dd($budgetyear_list)}} --}}

                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="search" class="col-sm-6 col-form-label">ปีงบประมาณ:</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="budgetyear_id" id="budgetyear_id">
                                            <option value="all">ทั้งหมด</option>
                                        @foreach ($budgetyear_list as $list)
                                            <option value="{{ $list->id }}"
                                                {{ $list->id == $budgetyear_selected[0]->id ? 'selected' : '' }}>
                                                {{ $list->budgetyear }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
{{-- {{ dd($budgetyear_selected[0]->invoicePeriod) }} --}}
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="search" class="col-sm-5 col-form-label">รอบบิลที่:</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="inv_period_id" id="inv_period_id">
                                        <option value="all">ทั้งหมด</option>

                                    @if (collect($budgetyear_selected)->isNotEmpty())
                                        @foreach ($budgetyear_selected[0]->invoicePeriod as $item)
                                            <option value="{{ $item->id }}">{{ $item->inv_period_name }}</option>
                                        @endforeach
                                    @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div><!--row-->

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="search" class="col-sm-4 col-form-label">เริ่มวันที่:</label>
                                <div class="col-sm-8">
                                    <input class="form-control datepicker" type="text" name="fromdate" id="fromdate">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="search" class="col-sm-4 col-form-label">ถึงวันที่:</label>
                                <div class="col-sm-8">
                                    <input class="form-control datepicker" type="text" name="todate" id="todate">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label for="search" class="col-sm-4 col-form-label">ผู้รับเงิน:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="cashier_id" id="cashier_id">
                                        <option value="all" selected>ทั้งหมด</option>
                                        @foreach ($receiptions as $receiption)
                                        <option value="{{$receiption->user_id}}">{{$receiption->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            {{-- <label class="control-label">&nbsp;</label> --}}
                            <button type="submit" id="searchBtn" class="form-control btn btn-primary">ค้นหา</button>
                        </div>
                    </div><!--row-->
                </div><!-- /.info-box-content -->
            </div><!--info-box-->
        </div><!--card-body-->
    </div><!--card-->
</form>
@if (collect($paidInfos)->isEmpty())
<div class="card">
    <div class="card-body">
        <h4 class="text-center">ไม่พบข้อมูล</h4>
    </div>
</div>

@else
<div class="card">
    <div class="card-header">
        <div class="card-title"></div>
        <div class="card-tools">
            <button class="btn btn-primary" id="printBtn">ปริ้น</button>
            <button class="btn btn-success" id="excelBtn">Excel</button>
        </div>
    </div>
    <div class="card-body table-responsive">
        <h4>เล็ดเยอร์รายตัวลูกหนี้ (ประเภทใช้มาตรวัดน้ำ)</h4>

        <div id="DivIdToExport">
            <table id="oweTable" class="table text-nowrap" width="100%">
                <thead>
                    <tr>
                        <td colspan="16" class="h4">เล็ดเยอร์รายตัวลูกหนี้ (ประเภทใช้มาตรวัดน้ำ)</td>
                        {{-- <td colspan="16" class="h4">รายงานสรุปการชำระค่าน้ำประปา</td> --}}
                    </tr>
                    <tr>
                        <td colspan="16">
                            <table width="60%">
                                <tr>
                                    <td colspan="4">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

                                            <div class="info-box-content">
                                                <span class="info-box-text">จำนวนหน่วยที่ใช้</span>
                                                <span class="info-box-number diff"></span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                    </td>
                                    <td colspan="4">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

                                            <div class="info-box-content">
                                                <span class="info-box-text">ค่าน้ำประปา</span>
                                                <span class="info-box-number _total"></span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                    </td>
                                    <td colspan="4">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

                                            <div class="info-box-content">
                                                <span class="info-box-text">ค่ารักษามิเตอร์</span>
                                                <span class="info-box-number meter_reserve_price"></span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                    </td>
                                    <td colspan="4">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

                                            <div class="info-box-content">
                                                <span class="info-box-text">รวมเป็นเงิน</span>
                                                <span class="info-box-number total"></span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center">วันที่: {{ $fromdateTh }} - {{ $todateTh }}</td>
                        <td colspan="3" class="text-center">ผู้รับเงิน: {{ $cashier_name }}</td>
                        <td colspan="3" class="text-center">
                            {{ $subzone_id == 'all' ? 'หมู่ที่ 1-19' : $paidInfos[0][0]['zone_name'].' เส้นทาง '.$paidInfos[0][0]['subzone_name']}}
                        </td>
                        <td colspan="3" class="text-center">
                            {{-- รอบบิลที่
                            {{ $currentInvPeriodName['inv_period_name'] ." ( ".$currentInvPeriodNameThai." )"}} --}}
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <th>ผู้ใช้น้ำประปาเลขที่</th>
                        <th>นาม</th>
                        <th>บิลที่</th>
                        <th>มิเตอร์</th>
                        <th>บ้านเลขที่</th>
                        <th>หมู่ที่</th>
                        <th>เส้นทาง</th>
                        <th>รอบบิลที่</th>
                        <th>ยกยอดมา</th>
                        <th>ปัจจุบัน</th>
                        <th>จำนวนหน่วยที่ใช้</th>
                        <th>ค่าน้ำประปา</th>
                        <th>ค่ารักษามิเตอร์</th>
                        <th>เป็นเงิน</th>
                        <th>ผู้รับเงิน</th>
                        <th>วันที่รับเงิน</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sum_diff = 0;
                        $sum_meter_reserve_price = 0;
                        $sum__total = 0;
                        $sum_total  = 0;
                    ?>

                    @foreach ($paidInfos as $key => $infos)

                    <?php $i =1;  ?>
                    @foreach ($infos as $owe)
                    <?php
                    // dd($infos[0]);
                            // $diff = $owe->currentmeter - $owe->lastmeter;//$owe->mustpaid;
                            $meter_reserve_price = $owe['water_used'] == 0 ? 10 : 0;
                            $_total = $owe['water_used'] * 8;
                            $total = $_total + $meter_reserve_price;

                            $sum_diff += $owe['water_used'];
                            $sum_meter_reserve_price += $meter_reserve_price;
                            $sum__total += $_total;
                            $sum_total  += $total;
                        ?>
                    <tr>
                        @if ($i++ ==1)
                        <td class="text-right">{{$infos[0]['user_id']}}</td>
                        <td>{{$infos[0]['user_profile']['name']}}</td>
                        <td class="text-right">{{$infos[0]['accounting']['id']}}</td>
                        <td class="text-right">{{$infos[0]['usermeterinfos']['meternumber']}}</td>
                        <td class="text-right">{{$infos[0]['user_profile']['address']}}</td>
                        <td class="text-right">{{$infos[0]['usermeterinfos']['zone']['zone_name']}}</td>
                        <td class="text-right">{{$infos[0]['usermeterinfos']['subzone']['subzone_name']}}</td>
                        @else
                        <td class="text-right" style="opacity: 0.2">{{$infos[0]['accounting']['id']}}</td>
                        <td class="text-right" style="opacity: 0.2">{{$infos[0]['user_id']}}</td>
                        <td class="text-right" style="opacity: 0.2">{{$infos[0]['usermeterinfos']['meternumber']}}</td>
                        <td style="opacity: 0.2">{{$infos[0]['user_profile']['name']}}</td>
                        <td class="text-right" style="opacity: 0.2">{{$infos[0]['user_profile']['address']}}</td>
                        <td class="text-right" style="opacity: 0.2">{{$infos[0]['usermeterinfos']['zone']['zone_name']}}</td>
                        <td class="text-right" style="opacity: 0.2">{{$infos[0]['usermeterinfos']['subzone']['subzone_name']}}</td>
                        @endif

                        <td class="text-right">{{$owe['invoice_period']['inv_period_name']}}</td>
                        <td class="text-right">{{ number_format($owe['lastmeter'])}}</td>
                        <td class="text-right">{{ number_format($owe['currentmeter'])}}</td>
                        <td class="text-right">{{ number_format($owe['water_used'])}}</td>
                        <td class="text-right">{{ number_format($_total)}}</td>
                        <td class="text-right">{{ number_format($meter_reserve_price)}}</td>
                        <td class="text-right">{{ number_format($total)}}</td>
                        <td class="text-right">{{ $infos[0]['accounting']['cashier_info']['name'] }}</td>
                        <td class="text-right">{{ $infos[0]['accounting']['updated_at'] }}</td>

                    </tr>

                    @endforeach
                    @endforeach
                    <input type="text" style="opacity: 0" value="{{ number_format($sum_diff) }}" id="diff">
                    <input type="text" style="opacity: 0" value="{{ number_format($sum_meter_reserve_price) }}"
                        id="meter_reserve_price">
                    <input type="text" style="opacity: 0" value="{{ number_format($sum__total) }}" id="_total">
                    <input type="text" style="opacity: 0" value="{{ number_format($sum_total) }}" id="total">
                </tbody>
            </table>
        </div>
    </div>
    <!--card-body-->
</div>
@endif
</div>




@endsection


@section('script')

<script
    src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
</script>

<script src="{{asset('js/my_script.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true,
        }).datepicker("setDate", new Date());; //กำหนดเป็นวันปัจุบัน


        $('.diff').html($('#diff').val())
        $('.meter_reserve_price').html($('#meter_reserve_price').val())
        $('._total').html($('#_total').val())
        $('.total').html($('#total').val())

    })
    $('#example').DataTable();
    $('#oweTable').DataTable({
        responsive: true,
        // order: false,
        "pagingType": "listbox",
        "lengthMenu": [
            [10, 25, 50, 150, -1],
            [10, 25, 50, 150, "ทั้งหมด"]
        ],
        "language": {
            "search": "ค้นหา:",
            "lengthMenu": "แสดง _MENU_ แถว",
            "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
            "paginate": {
                "info": "แสดง _MENU_ แถว",
            },
        }
    })
    $(document).ready(function () {
        $('.paginate_page').text('หน้า')
        let val = $('.paginate_of').text()
        $('.paginate_of').text(val.replace('of', 'จาก'));

    })

    $('#zone_id').change(function () {
        //get ค่าsubzone
        $.get(`../api/subzone/${$(this).val()}`)
            .done(function (data) {
                let text = '<option value="all" selected>ทั้งหมด</option>';
                data.forEach(element => {
                    text += `<option value="${element.id}">${element.subzone_name}</option>`
                });
                $('#subzone_id').html(text)
            });
    });

    $('#printBtn').click(function () {
        var tagid = 'oweTable'
        var hashid = "#" + tagid;
        var tagname = $(hashid).prop("tagName").toLowerCase();
        var attributes = "";
        var attrs = document.getElementById(tagid).attributes;
        $.each(attrs, function (i, elem) {
            attributes += " " + elem.name + " ='" + elem.value + "' ";
        })
        var divToPrint = $(hashid).html();
        var head = "<html><head>" + $("head").html() + "</head>";
        var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
            divToPrint + "</" + tagname + ">" + "</body></html>";
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write(allcontent);
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    })

    $('#excelBtn').click(function () {
        $("#DivIdToExport").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    });

    $('#budgetyear_id').change(()=>{
        let budgetyear_id = $('#budgetyear_id').val()
        $.get('../api/invoice_period/inv_period_lists/'+budgetyear_id).done(function(data){
            let text ='<option>เลือก...</option>';
            data.forEach(element => {
                text+= `<option value=${element.id}>${element.inv_period_name}</option>`
            });

            $('#inv_period_id').html(text)
        })
    })

    function checkValues(){

    }

</script>
@endsection

