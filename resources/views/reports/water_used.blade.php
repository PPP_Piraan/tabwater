@extends('layouts.adminlte')

@section('mainheader')
ข้อมูลปริมาณการใช้น้ำประปา
@endsection
@section('nav')
<a href="{{url('/reports')}}"> รายงาน</a>
@endsection
@section('report-water_used')
active
@endsection
@section('style')
    <style>
        #stocks-div{
      height: 100% !important;
      width: 100% !important;
}

        </style>
@endsection
@section('content')

<div class="card">
    <div class="card-body">
        {{-- ค้นหา --}}
        <form action="{{url('/reports/water_used')}}" method="GET">
            @csrf
            <div class="info-box">
                <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>
                <div class="info-box-content">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label">ปีงบประมาณ</label>

                            <select class="form-control" name="budgetyear_id" id="budgetyear_id">
                                <option value="all" selected>ทั้งหมด</option>
                                @foreach ($budgetyears as $budgetyear)
                                <option value="{{$budgetyear->id}}" {{ $budgetyear->id == $selected_budgetYear->id ? 'selected' : '' }}>{{$budgetyear->budgetyear}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">หมู่ที่</label>
                            <select class="form-control" name="zone_id" id="zone_id">
                                <option value="all" selected>ทั้งหมด</option>
                                @foreach ($zones as $zone)
                                <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">เส้นทาง</label>
                            <select class="form-control" name="subzone_id" id="subzone_id">
                                <option value="all" selected>ทั้งหมด</option>

                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">&nbsp;</label>
                            <button type="submit" class="form-control btn btn-primary searchBtn">ค้นหา</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

{{-- ส่วนแสดง graph และตาราง --}}
@if (collect($datas)->count() == 0)
     {{-- ถ้าข้อมูลการใช้น้ำยังไม่มี --}}
     <div class="card">
         <div class="card-body text-center"><h3>ยังไม่ข้อมูลการใช้น้ำ</h3></div>
     </div>
@else
     {{-- ถ้ามีข้อมูลการใช้น้ำ --}}
    <div class="card">
        <div class="card-header">
            <div class="card-title"></div>
            <div class="card-tools">
                <button class="btn btn-primary" id="printBtn">ปริ้น</button>
                <button class="btn btn-success" id="excelBtn">Excel</button>
            </div>
        </div>
        <div class="card-body" id="DivIdToExport">
            <?php 
                $sum_total = 0 ;
                $colspan = collect($datas[0]['values'])->count();
                $last_colspan = $colspan + 2;
                $head_info_colspan = $colspan + 3;
            ?>
            <div class="card card-primary">
                <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body ">
                    <div id="stocks-div"></div>
                </div>
            </div>

            
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">
                    ตารางสรุปปริมาณการใช้น้ำ {{ $zone_and_subzone_selected_text }} ปีงบประมาณ {{ $selected_budgetYear->budgetyear }}
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-striped text-nowrap">
                        <thead>
                            <tr>
                                <td colspan="{{ $head_info_colspan }}" style="opacity: 0">
                                    ตารางสรุปปริมาณการใช้น้ำ {{ $zone_and_subzone_selected_text }} ปีงบประมาณ {{ $selected_budgetYear->budgetyear }}
                                </td>
                            </tr>
                            <tr class="bg-primary">
                                <th class="text-center bg-dark">หมู่ที่</th>
                                <th class="text-center bg-dark">สมาชิก(ราย)</th>
                                <th class="text-center bg-info ">เส้นทาง</th>
                                @foreach ($datas[0]['values'] as $item)
                                    <th class="text-center">{{ $item['inv_period_name'] }}</th>
                                @endforeach
                                <th class="text-center bg-success">รวม(หน่วย)</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data)
                                <?php $index = 0; ?>
                                <?php $sum_total += $data['total'];  ?>
                                <tr>
                                    <td class="bg-dark">{{ $data['zone_id'] }}</td>
                                <th class="text-center bg-dark">{{ $data['members'] }}</th>
                                    
                                    <td class="bg-info">{{ $data['subzone_name'] }}</td>
                                    @foreach ($data['values'] as $item)
                                        <td class="text-right  ivpsum{{$index++}}">{{ number_format($item['water_used_sum']) }}</td>
                                    @endforeach
                                    <td class="text-right bg-success">{{ number_format($data['total']) }}</td>
                                </tr>
                            @endforeach
                        
                            {{-- <tr class="bg-warning xx">
                                <th class="text-center" colspan="2">รวม</th>
                            </tr> --}}

                        
                        </tbody>
                    </table>
                </div>
            </div>

        </div><!--DivIdToExport-->
    </div>
    @columnchart('Stocks', 'stocks-div')

@endif


@endsection


@section('script')
<script src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js"></script>

<script>
    $(document).ready(()=>{
        let sum = 0;
        let total = 0;
        for(let i=0; i < 12; i++){
            sum = 0;
            $(`.ivpsum${i}`).each((index, v)=>{
                sum+=  parseInt($(v).text().split(',').join(''))
            });
            // if(sum >0){
                $('.xx').append(`<th class="text-right">
                ${new Intl.NumberFormat('en-IN').format(sum)}
                </th>`)
                total += sum;
            // }

        }
        $('.xx').append(`<th class="bg-info text-right">${new Intl.NumberFormat('th-TH').format(total)}</th>`)
    });
       
       
  $('#zone_id').change(function () {
      //get ค่าsubzone 
      $.get(`../api/subzone/${$(this).val()}`)
          .done(function (data) {
              let text = '<option value="all" selected>ทั้งหมด</option>';
              data.forEach(element => {
                  text += `<option value="${element.id}">${element.subzone_name}</option>`
              });
              $('#subzone_id').html(text)
          });
  });//$#zone_id

  $('#printBtn').click(function () {
        var tagid = 'DivIdToExport'
        var hashid = "#" + tagid;
        var tagname = $(hashid).prop("tagName").toLowerCase();
        var attributes = "";
        var attrs = document.getElementById(tagid).attributes;
        $.each(attrs, function (i, elem) {
            attributes += " " + elem.name + " ='" + elem.value + "' ";
        })
        var divToPrint = $(hashid).html();
        var head = "<html><head>" + $("head").html() + "</head>";
        var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
            divToPrint + "</" + tagname + ">" + "</body></html>";
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write(allcontent);
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    })

    $('#excelBtn').click(function () {
        $("#DivIdToExport").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    });


     // fetch(`{{ url('../api/reports/water_used') }}`)
        //     .then(function (response) {
        //         return response.json() // แปลงข้อมูลที่ได้เป็น json
        //     })
        //     .then(function (data) {
        //         console.log(data); // แสดงข้อมูล JSON จาก then ข้างบน
        //         table =   $('#example').DataTable( {
        //         destroy: true,
        //         "pagingType": "listbox",
        //         "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "ทั้งหมด"]],
        //         "language": {
        //             "search": "ค้นหา:",
        //             "lengthMenu": "แสดง _MENU_ แถว", 
        //             "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
        //             "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
        //             "paginate": {
        //                 "info": "แสดง _MENU_ แถว",
        //             },
        //         },
        //             data: data,
        //             columns: [
        //                 {
        //                     'title': '',
        //                     data: 'user_id',
        //                     render: function(data){ 
        //                         return `<i class="fa fa-plus-circle text-success findInfo" 
        //                                     data-user_id="${data}"></i>`
        //                         }
        //                 },
        //                 {'title': 'เลขผู้ใช้งาน', data: 'user_id_str'},
        //                 {'title': 'ชื่อ-สกุล', data: 'name'},
        //                 {'title': 'เลขมิเตอร์', data: 'meternumber'},
        //                 {'title': 'บ้านเลขที่', data: 'address'},
        //                 {'title': 'หมู่ที่', data: 'zone_name'},
        //                 {'title': 'เส้นทาง', data: 'subzone_name'},
                        

        //         ]
        //         });
        //         // $('.overlay').css('display', 'none');
        //         $('#example').find('.overlay_tr').remove()
        //         $('#example thead').find('#title').remove()
        //         let title = 'ตารางข้อมูลผู้ใช้น้ำประปาบ้านห้องแซง';
        //         title += $('#zone_id').val() === 'all' ? 'หมู่ที่ 1 - 19' :data[0].zone_name;
        //         // $('#example thead').first().remove()
        //         $('#example thead').prepend(`<tr id="title"><td colspan="7" class="text-center h4">${title}</td></tr>`)

        //         $('.paginate_page').text('หน้า')
        //         let val = $('.paginate_of').text()
        //         $('.paginate_of').text(val.replace('of', 'จาก')); 
        //     });
        //     })
         

        // $.get('../api/reports/water_used')
        // .done(function(data){
        //     console.log(data)
        // });
    // });s

</script>  
   

@endsection


