@extends('layouts.admin')

@section('mainheader')
รายงาน
@endsection
@section('presentheader')
รายงานการใช้น้ำประปา
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header border-bottom">ค้นหาการใช้น้ำประปา</div>
                <div class="card card-body">
                    <div class="row">
                        <div class="col-md-3">
                                <label class="control-label">ประเภท</label>
                                <div class="radio">
                                    <label><input type="radio" name="usedTwaterType" checked> โซน</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="usedTwaterType"> บุคคล</label>
                                </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">โซน</label>
                            <select class="form-control" name="zone" id="zone">
                                
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">รอบบิล</label>
                            <input class="form-control datepicker" type="text" name="invperiodstart" id="invperiodstart">
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="control-label">&nbsp;</label>
                            <input class="form-control text-center border-0 font-weight-bold" type="text" value="ถึง">

                        </div>
                        <div class="col-md-2">
                            <label class="control-label">รอบบิล</label>
                            <input class="form-control datepicker" type="text" name="invperiodend" id="invperiodend">
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">&nbsp;</label>
                            <button class="form-control btn btn-primary" onclick="search2()">ค้นหา</button>
                        </div>
                    </div>
                    <div class="p-0 mt-5 pb-3 text-center">
                        <div class="alert alert-success search_res_div hidden ">
                                <strong>ผลการค้นหา </strong> <span class="search_res"></span> 
                        </div>
                        <table class="table mb-0 datatable">
                            <thead class="bg-light">
                                <tr>
                                        <th scope="col-md-1" class="border-0">#</th>
                                        <th scope="col-md-2" class="border-0">ชื่อผู้ประปา</th>
                                        <th scope="col-md-2" class="border-0">รอบบิลที่</th>
                                        <th scope="col-md-2" class="border-0">สถานะ</th>
                                </tr>
                            </thead>
                            <tbody id="info">
                                
                            </tbody>
                            
                        </table>
                    </div>
                           
                </div>
            </div>
        </div>
    </div>

    {{-- content --}}


</div>
<script src="{{asset('/js/app.js')}}"></script>

{{-- <script src="{{asset('/js/app.js')}}"></script> --}}
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        
        <script src="{{asset('/js/pdfmake.min.js')}}"></script>
        <script src="{{asset('/js/vfs_fonts.js')}}"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
        <script>
            pdfMake.fonts = {
                Roboto:{
                    normal:"THSarabun.ttf",
                    bold:"THSarabun.ttf",
                    italics:"THSarabun.ttf",
                    bolditalics:"THSarabun.ttf"  //ใช้ THSarabun.tff
                }
            };
            $(document).ready(() => {
                $('.datatable').DataTable({
                    "searching": false,
                    "paging": false,
                    "bInfo" : false,
                    dom: 'Bfrtip',
                    buttons: [
                        // {extend: 'copy',text: 'Copy'},
                        // {extend: 'csv',text: 'Export CSV'},
                        {extend: 'excel',text: 'Export Excel'},
                        {
                            extend: 'pdf',
                            text: 'Export PDF',
                        },        
                        {extend: 'print',text: 'Print'}
                    ],
                    "language": {
                        "emptyTable": "ไม่พบข้อมูล"
                    }
                });
                $('.dt-buttons').addClass('m-2')
                $('.dt-button').prop('style', 'border-radius: 20px 20px !important')
            });
            
    </script>
<script>
    $('#usedTwaterType').change(function(){
        console.log($(this).val())
    })

    function search2(){
        var invoicetype = $('#invoicetype').val();
        console.log(invoicetype)
        if(invoicetype === ""){
            return false
        }
        
        var invperiodstart = $('#invperiodstart').val();
        var invperiodend = $('#invperiodend').val();
        console.log(invperiodstart)
        $.get( "/api/reports/invoicereport", {
            "invoicetype": invoicetype,
            "invperiodstart": invperiodstart,
            "invperiodend": invperiodend,
        })
        .done(function( data ) {
            console.log(  data );
            $('.search_res_div').addClass('hidden');
            var text ="";
            if(Object.keys(data).length > 0){            
                $('.search_res_div').removeClass('hidden');
                $('.search_res').text('พบข้อมูลดังแสดงในตาราง')                
                table = $('.datatable').DataTable();  
                var i = 1;  
                data.forEach(element => {
                    table.row.add([ 
                        i++,
                        element.users.user_profile.name, 
                        element.invoice_period.inv_period_name, 
                        element.status 
                    ]);
                });              
                table.draw();

            }else{
                $('.search_res_div').removeClass('hidden');
                $('.search_res').text = 'ไม่พบข้อมูล'
            }//if
            setTimeout(() => {
                $('.search_res_div').addClass('hidden');
            }, 5000);
        });
        
    }
</script>
@endsection


