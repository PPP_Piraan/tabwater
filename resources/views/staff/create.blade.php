@extends('layouts.adminlte')

@section('mainheader')
เพิ่มข้อมูลเจ้าหน้าที่งานประปา
@endsection
@section('nav')
<a href="{{url('staff')}}"> ข้อมูลเจ้าหน้าที่งานประปา</a>
@endsection
@section('staff')
active
@endsection
@section('content')
<form action="{{url('/staff/store')}}" method="post" id="form" onsubmit="return check();">
    @csrf
    <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                            src="{{asset('/img/icons-user.png')}}" alt="User profile picture">
                    </div>
                    <p class="text-muted text-center">เจ้าหน้าที่งานประปา</p>
                    <a href="#" class="btn btn-primary btn-block"><b></b></a>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="activity">
                            <div class="card card-small mb-4">
                                <div class="text-center alerttext h5 mt-2 text-red"></div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-3">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="feFirstName">ชื่อ - สกุล</label>
                                                        <input type="text" class="form-control" id="name" name="name"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->name : ''}}">
                                                    </div>

                                                    <div class="form-group col-md-">
                                                        <label for="feGender">เพศ</label>
                                                        <select name="gender" id="gender" class="form-control">
                                                            <option>เลือก...</option>
                                                            @if (collect($user)->isNotEmpty())
                                                            <option value="m"
                                                                {{ $user->user_profile->gender == 'm' ? 'selected' : ''}}>
                                                                ชาย</option>
                                                            <option value="w"
                                                                {{ $user->user_profile->gender == 'w' ? 'selected' : ''}}>
                                                                หญิง</option>
                                                            @else
                                                            <option value="m">ชาย</option>
                                                            <option value="w">หญิง</option>
                                                            @endif

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label for="feID_card">ตำแหน่ง</label>
                                                        <select name="usercategory_id" id="usercategory_id"
                                                            class="form-control">
                                                            <option>เลือก...</option>
                                                            @foreach ($usercategories as $item)
                                                            <option value="{{$item->id}}">{{$item->user_cate_name}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-5">
                                                        <label for="feID_card">เลขบัตรประจำตัวประชาชน</label>
                                                        <input type="text" class="form-control" id="id_card"  name="id_card"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->id_card : ''}}">
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <label for="fePhone">เบอร์โทรศัพท์</label>
                                                        <input type="text" class="form-control" id="phone" name="phone"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->phone : ''}}">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-3">
                                                        <label for="feInputAddress">ที่อยู่</label>
                                                        <input type="text" class="form-control" id="address" name="address"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->address : ''}}">
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <label>หมู่ที่</label>
                                                        <select class="form-control" name="zone_id" id="zone_id">
                                                            <option>เลือก...</option>
                                                            @if (collect($user)->isNotEmpty())
                                                            @foreach ($zones as $zone)
                                                            <option value="{{$zone->id}}"
                                                                {{ $user->user_profile->zone_id == $zone->id ? 'selected' : ''}}>
                                                                {{$zone->zone_name}}</option>
                                                            @endforeach
                                                            @else
                                                            @foreach ($zones as $zone)
                                                            <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                                                            @endforeach
                                                            @endif

                                                        </select>
                                                    </div>


                                                    <div class="form-group col-3">
                                                        <label>จังหวัด</label>
                                                        <select class="form-control" name="province_code"
                                                            id="province_code" onchange="getDistrict()">
                                                            <option value="35">ยโสธร</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <label>อำเภอ</label>
                                                        <select class="form-control" name="district_code"
                                                            id="district_code" onchange="getTambon()">
                                                            <option value="3508">เลิงนกทา</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <label>ตำบล</label>
                                                        <select class="form-control" name="tambon_code" id="tambon_code"
                                                            onchange="getZone()">
                                                            <option value="350805">ห้องแซง</option>

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item p-3">
                                        <div class="row">
                                            <div class="form-group col-4">
                                                <label for="feInputAddress">Username</label>
                                                <input type="text" class="form-control" id="username" name="username">
                                            </div>
                                            <div class="form-group col-4">
                                                <label for="feInputAddress">Password</label>
                                                <input type="text" class="form-control" id="password"
                                                    name="password">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <button type="submit" class="btn btn-success col-6">บันทึก</button>

                        </div>

                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

</form>


@endsection


@section('script')
<script>
    $(document).ready(function () {
    
        // autoTest()  ทดสอบใส่ data
         $('#name').val('officer1 officer11')
         $('#gender').val('m')
         $('#usercategory_id').val(1)
         $('#id_card').val('3350800554321')
         $('#phone').val('0984434434')
         $('#address').val('20')
         $('#zone_id').val('1')
         $('#username').val('officer1')
         $('#password').val('1234')
    });


    function getDistrict() {
        var id = $("#province_code").val();
        $.get("/district/getDistrict/" + id).done(function (data) {
            var text = "<option>--Select--</option>";
            data.forEach(function (val) {
                text += "<option value='" + val.district_code + "'>" + val.district_name + "</option>";
            });
            $("#district_code").html(text);
        });
    }

    function getTambon() {
        var id = $("#district_code").val();
        $.get("/tambon/getTambon/" + id).done(function (data) {
            console.log(data)
            var text = "<option>--Select--</option>";
            data.forEach(function (val) {
                text += "<option value='" + val.tambon_code + "'>" + val.tambon_name + "</option>";
            });
            $("#tambon_code").html(text);
        });
    }

    function getZone() {
        var id = $("#tambon_code").val();
        $.get("../../../zone/getZone/" + id).done(function (data) {
            // $.get("/zone/getZone/" + id).done(function (data) {
            var text = "<option>--Select--</option>";
            data.forEach(function (val) {
                text += "<option value='" + val.id + "'>" + val.zone_name + "</option>";
            });
            $("#zone_id").html(text);
        });
    }

    function getSubzone() {
        var id = $("#undertake_zone_id").val();
        $.get("../../../subzone/getSubzone/" + id).done(function (data) {
            // $.get("/subzone/getSubzone/" + id).done(function (data) {

            var text = "<option>เลือก...</option>";
            console.log('data', data)
            if (data.length == 0) {
                text += "<option value='0'>-</option>";
            } else {
                data.forEach(function (val) {
                    text += "<option value='" + val.id + "'>" + val.subzone_name + "</option>";
                });
            }
            $("#undertake_subzone_id").html(text);
        });
    }

    $('select').change(()=>{
        check()
    });

    $('input').keyup(()=>{
        check()
    });

    function check(){

       let name = $('#name').val()  === "" ? $('#name').addClass('border-danger rounded') : true;
       let gender = $('#gender').val() === "เลือก..." ? $('#gender').addClass('border-danger rounded') : true;
       let usercategory_id = $('#usercategory_id').val() === "เลือก..." ? $('#usercategory_id').addClass('border-danger rounded') : true;
       let id_card = $('#id_card').val() === "" ? $('#id_card').addClass('border-danger rounded') : true;
       let phone = $('#phone').val() === "" ? $('#phone').addClass('border-danger rounded') : true;
       let address = $('#address').val() === "" ? $('#address').addClass('border-danger rounded') : true;
       let zone = $('#zone_id').val() === "เลือก..." ? $('#zone_id').addClass('border-danger rounded') : true;
       let username = $('#username').val() === "" ? $('#username').addClass('border-danger rounded') : true;
       let password = $('#password').val() === "" ? $('#password').addClass('border-danger rounded') : true;

        name === true ? $('#name').removeClass('border-danger rounded') : true
        gender === true ? $('#gender').removeClass('border-danger rounded') : true
        usercategory_id === true ? $('#usercategory_id').removeClass('border-danger rounded') : true
        id_card === true ? $('#id_card').removeClass('border-danger rounded') : true
        phone === true ? $('#phone').removeClass('border-danger rounded') : true
        address === true ? $('#address').removeClass('border-danger rounded') : true
        zone === true ? $('#zone').removeClass('border-danger rounded') : true
        username === true ? $('#username').removeClass('border-danger rounded') : true
        password === true ? $('#password').removeClass('border-danger rounded') : true
       
        $('.alerttext').html('')
       if(name === true && gender === true && usercategory_id === true && id_card === true 
            && phone === true && address === true && zone === true && username === true && password === true){
            return true
       }else{
           console.log('ss')
           $('.alerttext').html('กรุณาใส่ข้อมูลให้ถูกต้องและครบถ้วน')
           return false
       }
       
       
    }

</script>
@endsection
