@extends('layouts.adminlte')
<?php 
use App\Http\Controllers\Api\FunctionsController;
$apiFunc = new FunctionsController;
?>
@section('mainheader')
 ข้อมูลเจ้าหน้าที่งานประปา
@endsection
@section('staff')
    active
@endsection
@section('nav')
<a href="{{url('/staff')}}"> ข้อมูลเจ้าหน้าที่งานประปา</a>
@endsection

@section('content')

@if ($message = Session::get('massage'))
<div class="alert alert-{{Session::get('color')}} alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif
  <div class="card">
    <div class="card-body">
  <div class="row">

    <div class="col-12 col-sm-12">
    <a href="{{url('staff/create')}}" class="btn btn-info pull-right mb-2">สร้างข้อมูลเจ้าหน้าที่งานประปา</a>
            {{-- เลิกใช้งาน --}}
            @if (collect($staffs)->isEmpty())
                <div class="card">
                  <div class="card-body text-center"><h3>ไม่มีข้อมูล</h3></div>
                </div>
            @else
              <table class="table" id="example">
                <thead>
                  <tr>
                    <th>รหัส</th>
                    <th>ชื่อ-สกุล</th>
                    <th>ตำแหน่ง</th>
                    <th>ที่อยู่</th>
                    <th>หมู่ที่</th>
                    <th>เบอร์โทรศัพท์</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($staffs as $staff)
                  <tr>
                    <td>  {{$apiFunc->createInvoiceNumberString($staff->id)}}</td>

                    <td>  {{$staff->user_profile->name}}</td>
                    <td>  {{$staff->position}}</td>
                    <td>  {{$staff->user_profile->address}}</td>
                    <td>  {{$staff->user_profile->zone->zone_name}}</td>
                    <td>  {{$staff->user_profile->phone}}</td>
                    <td>
                      <a href="{{url('/staff/edit/'.$staff->id)}}" class="btn btn-warning">แก้ไข</a>
                      <a href="{{url('/staff/delete/'.$staff->id)}}" class="btn btn-danger delBtn"> ลบ</a>
                    </td>
                  </tr>

                @endforeach
                </tbody>
              </table>
            @endif                   
      </div>
      <!-- /.card -->
    </div>
    </div><!--card-body-->
  </div><!--card-->
@endsection


@section('script')
    <script>    
      // $('.dataTable').dataTable();
      $('#example').DataTable( {
            "pagingType": "listbox",
            "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "All"]],
            "language": {
                "search": "ค้นหา:",
                "lengthMenu": "แสดง _MENU_ แถว", 
                "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
                "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
                "paginate": {
                    "info": "แสดง _MENU_ แถว",
                },
            },

            
          } );
      $(document).ready(function(){
     
          $('.paginate_page').text('หน้า')
          let val = $('.paginate_of').text()
          $('.paginate_of').text(val.replace('of', 'จาก')); 
        
      })


      $('.delBtn').click(function(){
        let res = window.confirm('ต้องการลบข้อมูลเจ้าหน้าที่งานประปาใช่หรือไม่ !!!')
          return  res === true ? true : false;
      });

      setTimeout(()=>{
        $('.alert').toggle('slow')
      },2000)
      
    </script>
@endsection