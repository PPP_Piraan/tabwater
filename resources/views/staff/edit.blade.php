@extends('layouts.adminlte')

@section('mainheader')
    แก้ไขข้อมูลเจ้าหน้าที่งานประปา
@endsection
@section('nav')
<a href="{{url('staff')}}"> ข้อมูลเจ้าหน้าที่งานประปา</a>
@endsection
@section('staff')
    active
@endsection
@section('content')
    
        @include('staff.form', ['mode'=> 'edit'])
        
    </form>
@endsection