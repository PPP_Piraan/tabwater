@extends('layouts.adminlte')
ี
@section('mainheader')
  ประวัติ
@endsection
@section('invoice')
    active
@endsection
@section('nav')
<a href="{{url('/invoice')}}"> งานประปา</a>
@endsection


@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile ">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset('public/adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                                <h3 class="profile-username text-center" id="feFirstName">
                                  {{$user[0]->name}}
                                </h3>
                            </div>
                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>ที่อยู่</b> 
                                    <a class="float-right" id="feInputAddress">
                                      <?php
                                        echo  "บ้านเลขที่ ".$user[0]->address." หมู่ ".$user[0]->zone_id
                                      ?>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>เบอร์โทรศัพท์</b> <a class="float-right" id="phone">
                                      {{$user[0]->phone}}
                                    </a>
                                </li>

                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-9">
                  <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>เลขมิเตอร์</th>
                        <th>วันที่</th>
                        <th>รอบบิล</th>
                        <th>ยอดครั้งก่อน</th>
                        <th>ยอดปัจจุบัน</th>
                        <th>จำนวนที่ใช้</th>
                        <th>คิดเป็นเงิน(บาท)</th>
                        <th>สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($user[0]->invoice as $item)
                        <tr>
                          <td>{{$item->usermeterinfos->meternumber}}</td>
                          <td>{{$item->updated_at_th}}</td>
                          <td>{{$item->invoice_period->inv_period_name}}</td>
                          <td>{{$item->lastmeter}}</td>
                          <td>{{$item->currentmeter}}</td>
                          <td>{{$item->currentmeter - $item->lastmeter }}</td>
                          <td>{{($item->currentmeter - $item->lastmeter)*8 }}</td>
                          <td>{{$item->status}}</td>
                        </tr>
                      @endforeach      
                  </tbody>
                </table>
                </div>
            </div>

        </div><!-- /.card-body -->
    </div>



    {{-- <table class="table">
      <thead>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {{$user[0]->name}}
        @foreach ($user[0]->invoice as $item)
        <tr>
          <td></td>
      
            <td>{{$item->inv_period_id}}</td>
          </tr>
      @endforeach
      </tbody>
    </table> --}}
      


@endsection

