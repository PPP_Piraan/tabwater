@extends('layouts.master')

@section('header')
น้ำประปาเดือนพฤษภาคม
@endsection
@section('content')



<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th></th>
            </tr>
        </thead>
        <tbody>
                @foreach ($members as $member)

            <tr>
                <td>
                    <div class="col-xl-3 col-md-6 mb-2">
                        <div class="card border-left-success shadow h-100 py-2">
                          <div class="card-body">
                            <div class="row no-gutters align-items-center">
                              <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">{{$member['id']}}</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$member['name']}}</div>
                              </div>
                              <div class="col-auto">
                                <i class="fas fa-dollar-sign fa-2x text-gray-1000"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                   
                </td>
            
                
            </tr>
            @endforeach
  
        </tfoot>
    </table>

@endsection
