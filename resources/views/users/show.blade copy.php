@extends('layouts.adminlte')
ี<?php
$func = new App\Http\Controllers\Api\FunctionsController();

?>
@section('mainheader')
ประวัติ
@endsection
@section('users')
active
@endsection
@section('nav')
<a href="{{ url('/users') }}"> ข้อมูลผู้ใช้น้ำ</a>
@endsection

@section('style')
<style>
    th {
        text-align: center;
    }

    .card-danger {
        border: 2px solid red !important;
        border-radius: 5px 5px;
    }

</style>
@endsection
@section('content')
<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            Vertical Tabs Examples
        </h3>
    </div>
    <div class="card-body">

        <div class="row">
            <div class="col-5 col-sm-3">

                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                    aria-orientation="vertical">

                    <div class="card card-info card-outline">
                        <div class="card-body box-profile ">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{ asset('img/icons-user.png') }}"
                                    alt="User profile picture">
                                <h3 class="profile-username text-center" id="feFirstName">
                                    {{ $user[0]->user_profile->name }}
                                </h3>
                            </div>
                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>ที่อยู่</b>
                                    <a class="float-right" id="feInputAddress">
                                        <?php
                                            echo 'บ้านเลขที่ ' . $user[0]->user_profile->address . ' หมู่ ' . $user[0]->user_profile->zone_id;
                                            ?>
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <b>เบอร์โทรศัพท์</b> <a class="float-right" id="phone">
                                        {{ $user[0]->user_profile->phone }}
                                    </a>
                                </li>

                                <li class="list-group-item text-center">
                                    <b>สถานะมิเตอร์</b> <br><a
                                        class=" h4 {{ $user[0]->deleted == 0 ? 'text-success' : 'text-danger' }}"
                                        id="phone">
                                        {{ $user[0]->deleted == 0 ? ' เปิดใช้งาน' : 'ยกเลิกการใช้งาน' }}
                                    </a>
                                </li>


                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>

                    <?php
                        $i = 0;
                        ?>
                    @foreach($userInvHistory as $item)
                        {{-- {{ dd($item->invoice_period->budgetyear->budgetyear) }} --}}
                        @if($i == 0)
                            <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill"
                                href="#tab{{ $i++ }}" role="tab" aria-controls="vert-tabs-home"
                                aria-selected="true">{{ $item->invoice_period->budgetyear->budgetyear }}</a>
                        @else
                            <a class="nav-link" id="vert-tabs-home-tab" data-toggle="pill" href="#tab{{ $i++ }}"
                                role="tab" aria-controls="vert-tabs-home"
                                aria-selected="false">{{ $item->invoice_period->budgetyear->budgetyear }}</a>
                        @endif
                    @endforeach
                    <a class="nav-link" id="vert-tabs-home-tab" data-toggle="pill" href="#tab-not-paid" role="tab"
                        aria-controls="vert-tabs-home" aria-selected="false">ยังไม่ชำระ</a>


                </div>

            </div>
            <div class="col-7 col-sm-9">
                <!-- <div class="tab-content" id="vert-tabs-tabContent">
                        <?php
                        $i = 0;
                        ?>
@foreach($userInvHistory as $item)
@if($i == 0)
                                <div class="tab-pane text-left fade show active" id="tab{{ $i++ }}"
                                    role="tabpanel" aria-labelledby="vert-tabs-home-tab">
@else
                                    <div class="tab-pane text-left fade" id="tab{{ $i++ }}" role="tabpanel"
                                        aria-labelledby="vert-tabs-home-tab">

                                        {{-- </div> --}}
@endif

                            <div class=" table-responsive">
                                <table class="table table-striped text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>เลขใบแจ้งหนี้</th>
                                            <th>รอบบิล</th>

                                            <th>วันที่จดมิเตอร์</th>
                                            <th>ยอดครั้งก่อน</th>
                                            <th>ยอดปัจจุบัน</th>
                                            <th>จำนวนที่ใช้</th>
                                            <th>คิดเป็นเงิน(บาท)</th>
                                            <th>สถานะ</th>
                                            <th>หมายเหตุ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @foreach (json_decode($item->json) as $_item)
                                            <?php
                                            $bg_status = '';
                                            if ($_item->status == 'owe') {
                                                $bg_status = 'bg-warning';
                                            } elseif ($_item->status == 'invoice') {
                                                $bg_status = 'bg-info';
                                            }
                                            ?>
                                            <tr class="{{ $bg_status }}">
                                                <td class="text-right">HS{{ $func->createInvoiceNumberString($_item->id) }}
                                                </td>
                                                <td>{{ $_item->inv_period_id }}</td>

                                                <td>
                                                    <?php
                                                    $exp = explode('T', $_item->updated_at);
                                                    $date = $func->engDateToThaiDateFormat($exp[0]);
                                                    $time = explode(':', $exp[1]);
                                                    echo $date . '  ' . $time[0] . ':' . $time[1] . 'น.';

                                                    ?>
                                                </td>
                                                <td class="text-right">{{ $_item->lastmeter }}</td>
                                                <td class="text-right">{{ $_item->currentmeter }}</td>
                                                <td class="text-right">{{ $_item->currentmeter - $_item->lastmeter }}</td>
                                                <td class="text-right">{{ ($_item->currentmeter - $_item->lastmeter) * 8 }}
                                                </td>
                                                <td class="text-center">{{ $func->statusThai($_item->status) }}</td>
                                                <td>{{ $_item->comment }}</td>
                                            </tr>
@endforeach--}}
                                    </tbody>
                                </table>
                            </div>
                    </div> <!-- tab-pane-->
                @endforeach
                <div class="tab-pane text-left fade" id="tab-not-paid" role="tabpanel"
                    aria-labelledby="vert-tabs-home-tab">

                    <div class="table-responsive">
                        <table class="table table-striped text-nowrap">
                            <thead>
                                <tr>
                                    <th>เลขใบแจ้งหนี้</th>
                                    <th>รหัสผู้ใช้งาน</th>
                                    <th>เลขมิเตอร์</th>
                                    <th>รอบบิล</th>
                                    <th>วันที่จดมิเตอร์</th>
                                    <th>ยอดครั้งก่อน</th>
                                    <th>ยอดปัจจุบัน</th>
                                    <th>จำนวนที่ใช้</th>
                                    <th>คิดเป็นเงิน(บาท)</th>
                                    <th>สถานะ</th>
                                    <th>หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user[0]->invoice as $item)
                                    <?php
                                        $bg_status = '';
                                        if ($item->status == 'owe') {
                                            $bg_status = 'bg-warning';
                                        } elseif ($item->status == 'invoice') {
                                            $bg_status = 'bg-info';
                                        }
                                        ?>
                                    <tr class="{{ $bg_status }}">
                                        <td class="text-right">
                                            IVHS{{ $func->createInvoiceNumberString($item->id) }}
                                        </td>
                                        <td>TWU{{ $func->createInvoiceNumberString($item->usermeterinfos->user_id) }}
                                        </td>
                                        <td>{{ $item->usermeterinfos->meternumber }}</td>
                                        <td>{{ $item->invoice_period->inv_period_name }}</td>
                                        <td>
                                            <?php
                                                $exp = explode(' ', $item->updated_at);
                                                $date = $func->engDateToThaiDateFormat($exp[0]);
                                                $time = explode(':', $exp[1]);
                                                ?>
                                            {{ $date . '  ' . $time[0] . ':' . $time[1] . 'น.' }}
                                        </td>
                                        <td class="text-right">{{ $item->lastmeter }}</td>
                                        <td class="text-right">{{ $item->currentmeter }}</td>
                                        <td class="text-right">{{ $item->currentmeter - $item->lastmeter }}
                                        </td>
                                        <td class="text-right">
                                            {{ ($item->currentmeter - $item->lastmeter) * 8 }}</td>
                                        <td class="text-center">{{ $func->statusThai($item->status) }}</td>
                                        <td>{{ $item->comment }}</td>
                                        {{-- <td><a href="{{url('invoice/invoice_edit/'.$item->id) }}"
                                        class="btn btn-warning">แก้ไข</a></td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> -->
            </div>
        </div>
    </div>


</div>

</div>

@endsection
