<link href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
<link href="https://https://cdn.datatables.net/buttons/2.4.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
<table id="myTable" class="table table-bordered display nowrap">
    <thead>
        <tr>
            <th>id</th>
            <th>user_id</th>
            <th>username</th>
            <th>name</th>
            <th>ประเภทผู้ใช้งาน</th>
            <th>ที่อยู่</th>
            <th>หมู่</th>
            <th>เลขผู้ใช้น้ำ</th>
            <th>สังกัดพื้นที่<div>จัดเก็บ(หมู่)</div></th>
            <th>สังกัดพื้นที่<div>จัดเก็บ(เส้น)</div></th>
            <th>สถานะใช้งาน</th>
            <th>รอบบิลล่าสุด</th>
            <th>สถานะรอบบิล</th>
            <th>ก่อนจด</th>
            <th>หลังจด</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)

        <tr>
            <td>{{ isset($user->users->id) ? $user->users->id : '-' }}</td>
            <td>{{ isset($user->user_id) ? $user->user_id : '-' }}</td>
            <td>{{ isset($user->users->username) ? $user->users->username : '-' }}</td>
            <td>{{ isset($user->name) ? $user->name : '-' }}</td>
            <td>{{ isset($user->users->user_cat_name) ? $user->users->user_cat_name : '-' }}</td>

            <td>{{ isset($user->address) ? $user->address : '-' }}</td>

            <td>{{ isset($user->zone->zone_name) ? $user->zone->zone_name : '-' }}</td>

            <td>{{ isset($user->userMeterInfos->meternumber) ? $user->userMeterInfos->meternumber : '-' }}</td>
            <td>{{ isset($user->userMeterInfos->undertake_subzone) ? $user->userMeterInfos->undertake_subzone : '-' }}</td>
            <td>{{ isset($user->userMeterInfos->undertake_subzone) ? $user->userMeterInfos->undertake_subzone : '-' }}</td>
            <td>{{ isset($user->userMeterInfos->umf_status) ? $user->userMeterInfos->umf_status : 'inactive' }}</td>
            <td>{{ isset($user->invlast->invoice_period->inv_period_name) ? $user->invlast->invoice_period->inv_period_name : '-' }}</td>
            <td>{{ isset($user->invlast->inv_status) ? $user->invlast->inv_status : '-' }}</td>
            <td>{{ isset($user->invlast->lastmeter) ? $user->invlast->lastmeter : '-' }}</td>
            <td>{{ isset($user->invlast->currentmeter) ? $user->invlast->currentmeter : '-' }}</td>

        </tr>
        @endforeach
    </tbody>
</table>
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>

<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.print.min.js"></script>

<script>
    $(document).ready(function() {
    $('#myTable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
