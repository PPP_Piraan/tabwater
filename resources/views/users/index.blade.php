@extends('layouts.adminlte')

@section('mainheader')
    ข้อมูลผู้ใช้น้ำประปา
@endsection
@section('users')
    active
@endsection
@section('nav')
    <a href="{{ url('/users') }}"> ข้อมูลผู้ใช้น้ำประปา</a>
@endsection

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row" id="data">
        @if (collect($checkZone)->isEmpty() || $checkTabwatermeter == 0)
            @if (collect($checkZone)->isEmpty())
                <div class="col-md-5 col-sm-6 col-12">
                    <div class="info-box bg-warning">
                        <span class="info-box-icon"><i class="fas fa-comments"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number text-center font-weight-bold">ยังไม่ได้สร้างพื้นที่จดมิเตอร์</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description text-center">
                                <a href="{{ url('/zone') }}" class="btn btn-info btn-sm">สร้างพื้นที่จดมิเตอร์</a>
                            </span>
                        </div>
                    </div>
                </div>
            @endif
            @if ($checkTabwatermeter == 0)
                <div class="col-md-5 col-sm-6 col-12">
                    <div class="info-box bg-warning">
                        <span class="info-box-icon"><i class="fas fa-comments"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number text-center font-weight-bold">ยังไม่ได้ตั้งค่าขนาดมิเตอร์</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description text-center">
                                <a href="{{ url('/tabwatermeter') }}" class="btn btn-info btn-sm">ตั้งค่าขนาดมิเตอร์</a>
                            </span>
                        </div>
                    </div>
                </div>
            @endif
        @else
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 h5">ค้นหาตามเส้นทางจัดเก็บค่าน้ำประปา</div>
                            @foreach (collect($subzones)->sortBy('zone_id') as $subzone)
                                <div class="col-12 col-lg-2 col-md-2 col-sm-2">
                                    <div class="form-check">
                                        <input class="form-check-input zone" type="checkbox" value="{{ $subzone->id }}"
                                            name="zone[]">
                                        <label class="control-label" for="customCheck1">
                                            <span class="text-sm text-secondary"> เส้น ::</span>
                                            <span class="text-md ">{{ $subzone->subzone_name }}</span>
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <button type="button" class="btn btn-info btn-sm text-end find_by_zone_btn">ค้นหา</button>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ url('users/create') }}" class="btn btn-info pull-right mb-2">เพิ่มผู้ใช้น้ำประปา</a>
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill"
                                            href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home"
                                            aria-selected="false"> ใช้งานอยู่</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill"
                                            href="#custom-tabs-one-profile" role="tab"
                                            aria-controls="custom-tabs-one-profile"
                                            aria-selected="false">ยกเลิกการใช้งาน</a>
                                    </li>
                                    <li class="nav-item">
                                        {{-- <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="true">เปลี่ยนมิเตอร์</a> --}}
                                    </li>

                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                    <div class="tab-pane fade active show table-responsive" id="custom-tabs-one-home"
                                        role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                        <button class="btn btn-info mb-3" id="xx">เลือกทั้งหมด</button>
                                        <div class="table-responsive">
                                            <table id="example" class="table">
                                                <thead>
                                                    <tr>
                                                        <th width="2%">#</th>
                                                        <th width="8%">เลขมิเตอร์</th>
                                                        <th width="20%">ชื่อ-สกุล</th>
                                                        <th width="6%">อาชีพ</th>
                                                        <th width="12%">เริ่มใช้งาน</th>
                                                        <th width="8%">ที่อยู่</th>
                                                        <th width="8%">หมู่</th>
                                                        <th width="8%">โซน</th>
                                                        <th width="10%">หมายเหตุ</th>
                                                        <th width="10%"></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-profile-tab">
                                        {{-- เลิกใช้งาน --}}
                                        @if (collect($inactiveMeter)->isEmpty())
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <h3>ไม่มีข้อมูล</h3>
                                                </div>
                                            </div>
                                        @else
                                            <div id="cancelMeter" class="table-responsive">
                                                <table class="table text-nowrap" id="example2">
                                                    <thead>
                                                        <tr>
                                                            {{-- <th>รหัสผู้ใช้น้ำ</th> --}}
                                                            <th>เลขมิเตอร์</th>
                                                            <th>ชื่อ-สกุล</th>
                                                            <th>อาชีพ</th>
                                                            <th>ที่อยู่</th>
                                                            <th>หมู่ที่</th>
                                                            <th>วันที่ยกเลิกใช้งาน</th>
                                                            {{-- <th>หมายเหตุ</th> --}}
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($inactiveMeter as $inactive)
                                                            {{-- {{dd($inactive)}} --}}
                                                            @if (collect($inactive->user_profile)->isEmpty())
                                                                {{ dd($inactive) }}
                                                            @endif
                                                            <tr>
                                                                {{-- <td>  {{$inactive->user_profile->user_id}}</td> --}}
                                                                <td> {{ $inactive->meternumber }}</td>
                                                                <td> {{ $inactive->user_profile->name }}</td>
                                                                <td> เกษตรกร</td>
                                                                <td> {{ $inactive->user_profile->address }}</td>
                                                                <td> {{ $inactive->user_profile->zone == null ? '' : $inactive->user_profile->zone->zone_name }}
                                                                </td>
                                                                <td> {{ $inactive->updated_at }}</td>
                                                                {{-- <td>  {{$inactive->comment}}</td> --}}
                                                                <td>
                                                                    <a href="{{ url('/users/show/' . $inactive->user_profile->user_id) }}"
                                                                        class="btn btn-info">ประวัติการใช้น้ำ</a>
                                                                    <a href="{{ url('/users/create/1/' . $inactive->user_profile->user_id) }}"
                                                                        class="btn btn-success">ขอกลับมาใช้น้ำใหม่</a>

                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif

                                    </div>
                                    <div class="tab-pane fade " id="custom-tabs-one-messages" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-messages-tab">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


@section('script')
    <script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.7.0/js/dataTables.select.min.js"></script>
    <script>
        let table
        // $('.dataTable').dataTable();
        $(document).ready(function() {
            $.get('../../api/users').done(function(data) {
                console.log('data',Object.keys(data).length)
                if(Object.keys(data).lenght === 0){
                    $('#data').remove()
                }else{
                    table = $('#example').DataTable({
                        "pagingType": "listbox",
                        dom: 'lBfrtip',
                        buttons: [{
                            extend: 'excelHtml5',
                            'text': 'Excel',
                            exportOptions: {
                                rows: ['.selected']
                            }
                        }],
                        "lengthMenu": [
                            [10, 25, 50, 150, -1],
                            [10, 25, 50, 150, "All"]
                        ],
                        "language": {
                            "search": "ค้นหา:",
                            "lengthMenu": "แสดง _MENU_ แถว",
                            "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                            "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                            "paginate": {
                                "info": "แสดง _MENU_ แถว",
                            },
                        },

                        data: data,

                    });


                }//else
            }); //document.ready



            $('#example2').DataTable({
                "pagingType": "listbox",
                "lengthMenu": [
                    [10, 25, 50, 150, -1],
                    [10, 25, 50, 150, "All"]
                ],
                "language": {
                    "search": "ค้นหา:",
                    "lengthMenu": "แสดง _MENU_ แถว",
                    "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                    "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                    "paginate": {
                        "info": "แสดง _MENU_ แถว",
                    },
                },

            });
            $('.paginate_page').text('หน้า')
            let val = $('.paginate_of').text()
            $('.paginate_of').text(val.replace('of', 'จาก'));


        });

        let init_select = 1
        $('.find_by_zone_btn').click(function() {
            let arr = []
            $('.zone:checked').each(function(index, val) {
                arr.push($(this).val())
            })
            $.post("../../api/users/users_post", {
                'choices[]': arr
            }).done(function(data) {
                $('#example').DataTable().destroy();
                table = $('#example').DataTable({
                    "pagingType": "listbox",
                    dom: 'lBfrtip',
                    buttons: [{
                        extend: 'excelHtml5',
                        'text': 'Excel',
                        exportOptions: {
                            rows: ['.selected']
                        }
                    }],
                    "lengthMenu": [
                        [10, 25, 50, 150, -1],
                        [10, 25, 50, 150, "All"]
                    ],
                    "language": {
                        "search": "ค้นหา:",
                        "lengthMenu": "แสดง _MENU_ แถว",
                        "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                        "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                        "paginate": {
                            "info": "แสดง _MENU_ แถว",
                        },
                    },

                    data: data,
                    columns: [
                        //   {title: 'เลขผู้ใช้น้ำ', data: 'user_id_str', 'width':'15%'},
                        {
                            title: "เลขผู้ใช้น้ำ",
                            data: 'meternumber',
                            'width': '10%'
                        },
                        {
                            title: "ชื่อ-สกุล",
                            data: 'name',
                            'width': '27%'
                        },
                        {
                            title: "อาชีพ",
                            data: 'job',
                            'width': '10%'
                        },
                        {
                            title: "วันที่เริ่มใช้",
                            data: 'start_date',
                            'width': '27%'
                        },
                        {
                            title: "ที่อยู่",
                            data: 'address',
                            'width': '13%'
                        },
                        {
                            title: "หมู่",
                            data: 'zone_name',
                            'width': '10%'
                        },
                        {
                            title: "เส้นทาง",
                            data: 'subzone_name',
                            'width': '10%'
                        },
                        {
                            title: "หมายเหตุ",
                            data: 'comment',
                            'width': '10%'
                        },
                        //   { title: "" , data: 'ledgerLink', 'width':'4%'},
                        {
                            title: "",
                            data: 'showLink',
                            'width': '2%'
                        },
                        {
                            title: "",
                            data: 'editLink',
                            'width': '2%'
                        },
                        // { title: "" , data: 'deleteLink', 'width':'3%'},

                    ]
                });

                if (init_select === 1) {
                    $('#example tbody tr').addClass('selected bg-light')
                    init_select = 0
                }
                $('.paginate_page').text('หน้า')
                let val = $('.paginate_of').text()
                $('.paginate_of').text(val.replace('of', 'จาก'));

                setTimeout(() => {
                    $('.alert').toggle('slow')
                }, 1500)
            });

        });

        $('select[name="example_length"]').on('change', function(e) {
            setTimeout(() => {
                $('#example tbody tr.selected').each(function(index) {
                    $(this).removeClass('selected bg-light')
                })
            }, 50);

            setTimeout(() => {
                $('#example tbody tr').each(function(index) {
                    $(this).addClass('selected bg-light')
                })
            }, 100);
        });

        $('#xx').click(() => {
            setTimeout(() => {
                $('#example tbody tr.selected').each(function(index) {
                    $(this).removeClass('selected bg-light')
                })
            }, 50);

            setTimeout(() => {
                $('#example tbody tr').each(function(index) {
                    $(this).addClass('selected bg-light')
                })
            }, 100);
        })
    </script>
@endsection
