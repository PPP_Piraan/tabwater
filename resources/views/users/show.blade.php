@extends('layouts.adminlte')
ี<?php
$func = new App\Http\Controllers\Api\FunctionsController();

?>
@section('mainheader')
    ประวัติ
@endsection
@section('users')
    active
@endsection
@section('nav')
    <a href="{{ url('/users') }}"> ข้อมูลผู้ใช้น้ำ</a>
@endsection

@section('style')
    <style>
        th {
            text-align: center;
        }

        .card-danger {
            border: 2px solid red !important;
            border-radius: 5px 5px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-sm-8">
            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                <div class="card card-info card-outline">
                    <div class="card-body box-profile row">
                        <div class="text-center col-12 col-sm-6">
                            <img class="profile-user-img img-fluid img-circle" src="{{ asset('img/icons-user.png') }}"
                                alt="User profile picture">
                            <h3 class="profile-username text-center" id="feFirstName">
                                {{ $user[0]->user_profile->name }}
                            </h3>
                        </div>
                        <div class="col-12 col-sm-6">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        ที่อยู่
                                        <span class="float-right badge bg-primary">
                                            {{ 'บ้านเลขที่ ' . $user[0]->user_profile->address . ' หมู่ ' . $user[0]->user_profile->zone_id }}

                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        เบอร์โทรศัพท์ <span
                                            class="float-right badge bg-info">{{ $user[0]->user_profile->phone }}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        สถานะมิเตอร์ <span class="float-right badge bg-success">

                                            {{ $user[0]->deleted == 0 ? ' เปิดใช้งาน' : 'ยกเลิกการใช้งาน' }}
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>

                <?php
                $i = 0;
                ?>


            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-primary card-outline card-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    @php
                        $i = 0;
                    @endphp
                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                        @foreach ($invoice_budgetyear_grouped as $key => $item)
                            <li class="nav-item">
                                <a class="nav-link {{ $i == 0 ? 'active show' : '' }}" id="custom-tabs-three-home-tab"
                                    data-toggle="pill" href="#tab{{ $key }}" role="tab"
                                    aria-controls="custom-tabs-three-home" aria-selected="false">ปีงบประมาณ
                                    {{ $item[0]->invoice_period->budgetyear->budgetyear }} <span
                                        class="badge bg-info">{{ collect($item)->count() }}</a>
                            </li>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                        <li class="nav-item">
                            <a class="nav-link text-danger" id="vert-tabs-home-tab" data-toggle="pill" href="#tab-not-paid"
                                role="tab" aria-controls="vert-tabs-home" aria-selected="false">ยังไม่ชำระ <span
                                    class="badge bg-warning">{{ collect($owes)->count() }}</span></a>

                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-three-tabContent">
                        @php
                            $first_tab = 1;
                        @endphp
                        @foreach ($invoice_budgetyear_grouped as $key => $budgetyear)
                            <div class="tab-pane fade {{ $first_tab++ == 1 ? 'active show' : '' }}"
                                id="tab{{ $key }}" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                <div class=" table-responsive">
                                    <table class="table table-striped text-nowrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>เลขใบแจ้งหนี้</th>
                                                <th>รอบบิล</th>
                                                <th>วันที่จดมิเตอร์</th>
                                                <th>ยอดครั้งก่อน</th>
                                                <th>ยอดปัจจุบัน</th>
                                                <th>จำนวนที่ใช้</th>
                                                <th>คิดเป็นเงิน(บาท)</th>
                                                <th>สถานะ</th>
                                                <th>หมายเหตุ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($budgetyear as $invoice_period)
                                                <?php
                                                $bg_status  = '';
                                                $_paid   = ($invoice_period->currentmeter - $invoice_period->lastmeter) * 8;
                                                   $paid    = $_paid == 0 ? 10 : $_paid;
                                                if ($invoice_period->status == 'owe') {
                                                    $bg_status = 'bg-danger disabled';
                                                }elseif ($invoice_period->status == 'invoice') {
                                                    $bg_status = 'bg-warning disabled';
                                                }
                                                ?>
                                                <tr class="{{ $bg_status }}">
                                                    <td>{{ $i++ }}</td>
                                                    <td class="text-right">
                                                        Inv-{{ $func->createInvoiceNumberString($invoice_period->id) }}
                                                    </td>
                                                    <td>{{ $invoice_period->invoice_period->inv_period_name }}</td>

                                                    <td>
                                                        <?php
                                                        $exp = explode(' ', $invoice_period->updated_at);
                                                        $date = $func->engDateToThaiDateFormat($exp[0]);
                                                        $time = explode(':', $exp[1]);
                                                        echo $date . '  ' . $time[0] . ':' . $time[1] . 'น.';

                                                        ?>
                                                    </td>
                                                    <td class="text-right">{{ $invoice_period->lastmeter }}</td>
                                                    <td class="text-right">{{ $invoice_period->currentmeter }}</td>
                                                    <td class="text-right">
                                                        {{ $invoice_period->currentmeter - $invoice_period->lastmeter }}
                                                    </td>
                                                    <td class="text-right">
                                                        {{ $paid }}
                                                    </td>
                                                    <td class="text-center">
                                                        {{ $func->statusThai($invoice_period->status) }}</td>
                                                    <td>{{ $invoice_period->comment }}</td>
                                                </tr>
                                            @endforeach

                                        </tbody>

                                    </table>
                                </div><!-- table-responsive-->
                            </div>
                        @endforeach

                        <div class="tab-pane fade" id="tab-not-paid" role="tabpanel"
                            aria-labelledby="custom-tabs-three-profile-tab">

                            <div class=" table-responsive">
                                <table class="table table-striped text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>เลขใบแจ้งหนี้</th>
                                            <th>รอบบิล</th>

                                            <th>วันที่จดมิเตอร์</th>
                                            <th>ยอดครั้งก่อน</th>
                                            <th>ยอดปัจจุบัน</th>
                                            <th>จำนวนที่ใช้</th>
                                            <th>คิดเป็นเงิน(บาท)</th>
                                            <th>สถานะ</th>
                                            <th>หมายเหตุ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($owes as $invoice_period)
                                            <?php
                                            $bg_status = '';
                                            if ($invoice_period->status == 'owe') {
                                                $bg_status = 'bg-danger disabled';
                                            } elseif ($invoice_period->status == 'invoice') {
                                                $bg_status = 'bg-warning disabled';
                                            }
                                            ?>
                                            <tr class="{{ $bg_status }}">
                                                <td>{{ $i++ }}</td>
                                                <td class="text-right">
                                                    Inv-{{ $func->createInvoiceNumberString($invoice_period->id) }}
                                                </td>
                                                <td>{{ $invoice_period->invoice_period->inv_period_name }}</td>

                                                <td>
                                                    <?php
                                                    $exp = explode(' ', $invoice_period->updated_at);
                                                    $date = $func->engDateToThaiDateFormat($exp[0]);
                                                    $time = explode(':', $exp[1]);
                                                    echo $date . '  ' . $time[0] . ':' . $time[1] . 'น.';

                                                    ?>
                                                </td>
                                                <td class="text-right">{{ $invoice_period->lastmeter }}</td>
                                                <td class="text-right">{{ $invoice_period->currentmeter }}</td>
                                                <td class="text-right">
                                                    {{ $invoice_period->currentmeter - $invoice_period->lastmeter }}
                                                </td>
                                                <td class="text-right">
                                                    {{ ($invoice_period->currentmeter - $invoice_period->lastmeter) * 8 }}
                                                </td>
                                                <td class="text-center">
                                                    {{ $func->statusThai($invoice_period->status) }}</td>
                                                <td>{{ $invoice_period->comment }}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>

                                </table>
                            </div><!-- table-responsive-->

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
