@extends('layouts.adminlte')

@section('mainheader')
    แก้ไขข้อมูลผู้ใช้น้ำ
@endsection
@section('nav')
<a href="{{url('users')}}"> ข้อมูลผู้ใช้น้ำ</a>
@endsection
@section('users')
    active
@endsection
@section('content')
    
        @include('users.form', ['mode'=> 'edit'])
        
    </form>
@endsection