<?php
use App\Http\Controllers\Api\FunctionsController;
$fnc = new FunctionsController();
?>

<style>
    .t2{
        margin-left:9px !important;
        /* border:  1px solid red */
        }
    </style>
<table border="0" width="99%" style="margin-top:18px !important;" class="t2" >
    <tr>   
        <td colspan="5" class="text-left head pt-2 pb-2">&nbsp;</td>
        <td colspan="2" class="text-center head2 pt-4 pb-2" ><div >วันที่</div> 
            <div> {{$invoicesPaidForPrint['reciepts'][0][0]->receipt_th_date}}</div>
        </td>
    </tr>

    <tr>
        <td colspan="5" class="text-left text-primary address">
            &nbsp;
            <div class="address">&nbsp;</div>
        </td>
        <td colspan="2" class="text-center">
            @if ($invoicesPaidForPrint['reciepts'][0][0]->receipt_th_date != $fnc->engDateToThaiDateFormat(date('Y-m-d')))
            
                <div style="font-size: 0.9rem;"> ( วันที่ปริ้น: {{$fnc->engDateToThaiDateFormat(date('Y-m-d'))}} ) </div>
        @endif
        </td>
    </tr>
    <tr>                
        <td class="waterUsedHisHead" style="width:80px;">&nbsp;</td>
        <td colspan="4"> 
            {{$invoicesPaidForPrint['user']->name}}
        </td>

        <td colspan="2" rowspan="2" class="text-center border-right-none border-bottom-none">
            &nbsp;{{-- <img src="{{asset('/img/hslogo.jpg')}}" width="70"> --}}
        </td>
    </tr>
    <tr >
        <td class="waterUsedHisHead">&nbsp;</td>
        <td colspan="4" class="address" style="padding-top: 10px !important">  
            {{$invoicesPaidForPrint['user']->address}} 
            {{$invoicesPaidForPrint['user']->zone_name}} ต.ห้องแซง  
            อ.เลิงนกทา จ.ยโสธร 35120
        </td>
    </tr>
</table>
<table border="0"  width="99%" class="t2" style="margin-top:1.8rem !important">
    <tr>

        <td width="20%" class="waterUsedHisHead"> &nbsp;</td>
        <td width="30%" class="text-center pl-1"> {{$invoicesPaidForPrint['user']->user_id}}</td>
        <td width="20%" class="waterUsedHisHead"> &nbsp;</td>
        <td width="30%" class="text-center"> {{$invoicesPaidForPrint['user']->meternumber}}</td>
    </tr>
</table>
<table border="0" width="99%"   class="t2" style="margin-top:1.9% !important">

    <tr>
        <td class="waterUsedHisHead text-center" width="12%">&nbsp;<br>&nbsp;</td>
        <td class="waterUsedHisHead text-center" width="20%">&nbsp;<br>&nbsp;</td>
        <td class="waterUsedHisHead text-center" width="10%">&nbsp;<br>&nbsp;</td>
        <td class="waterUsedHisHead text-center" width="10%">&nbsp;<br>&nbsp;</td>
        <td class="waterUsedHisHead text-center" width="11%">&nbsp;<br>&nbsp;</td>
        <td class="waterUsedHisHead text-center" width="14%">&nbsp;<br>&nbsp;</td>
        <td class="waterUsedHisHead text-center" width="10%">&nbsp;<br>&nbsp;</td>
        <td class="waterUsedHisHead text-center" width="13%">&nbsp;<br>&nbsp;</td>
    </tr>
    <?php $total = 0; $reserveMeter = 0; ?>
    @foreach ($invoicesPaidForPrint['reciepts'][0] as $key => $item)
    <tr>
        <td class="text-center">{{ $item->inv_period_name }}</td>
        <td class="text-center">
            <?php
                $date = Str::substr($item->record_meternumber_date, 0, 10);
            ?>
            {{ $fnc->engDateToThaiDateFormat($date) }}
        </td>
        <td class="text-right">{{number_format($item->currentmeter)}}</td>
        <td class="text-right">{{number_format($item->lastmeter)}}</td>
        <td class="text-right number">
            <?php 
                $waterUsedNet   = $item->currentmeter - $item->lastmeter;
                $reserveMeter   = $waterUsedNet == 0 ? 10 : 0;
                $used_price     = ($waterUsedNet * 8);
                $paid           = $used_price + $reserveMeter;
                $total         += $paid;
            ?>
            <span id="unit_used">{{ number_format($waterUsedNet) }}</span> 
        </td>
        <td class="text-right number">{{ $used_price }}</td>
        <td class="text-right number">{{ $reserveMeter }}</td>
        <td class="text-right number t2-pr-3">{{ number_format($paid) }}</td>
    </tr>
    @endforeach
  
    @if (collect($invoicesPaidForPrint['reciepts'][0])->count() == 1)
        <tr><td colspan="7">&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
    @elseif(collect($invoicesPaidForPrint['reciepts'][0])->count() == 2)
        <tr><td colspan="7">&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
    @elseif(collect($invoicesPaidForPrint['reciepts'][0])->count() == 3)
        <tr><td colspan="7">&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
    @elseif(collect($invoicesPaidForPrint['reciepts'][0])->count() == 4)
        <tr><td colspan="7">&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td colspan="7" class="border-top-none">&nbsp;</td><td class="border-top-none">&nbsp;</td></tr>
    @elseif(collect($invoicesPaidForPrint['reciepts'][0])->count() == 5)
        <tr><td colspan="7">&nbsp;</td><td>&nbsp;</td></tr>
    @endif
    <tr>
        <td colspan="4" rowspan="3" class="border-bottom-none border-left-none text-center">
            {{ QrCode::size(70)->generate($newId) }}
            <div class='mt-0'>เลขใบเสร็จรับเงิน:  {{ $newId }} </div>
        </td>
        <td class="" colspan="3">&nbsp;</td>
        <td class="text-right t2-pr-3 number">
            {{ number_format($total) }}
        </td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
        <td class="text-right t2-pr-3 number">
            <?php 
                $vat = 0;
                $total = $total + $vat;
            ?>
            {{ number_format($vat) }}
        </td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
        <td class="text-right t2-pr-3 number head2">
            {{ number_format($total) }}
        </td>
    </tr>
 

</table>
<table border="0" width="99%" class="mt-2 t">
    <tr>
        <td colspan="7" class="text-center border-left-none border-right-none pt-3">
            {{$receipter[0]->name}}
           <br>&nbsp;							             
       </td>
    </tr>
</table>