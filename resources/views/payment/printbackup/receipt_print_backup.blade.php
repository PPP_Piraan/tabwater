@extends('layouts.print')



@section('content')
<?php
use App\Http\Controllers\Api\FunctionsController;

?>

<head>

    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
</head>

<?php $index =  0; ?>
<?php 
    $year = date('Y')+543;
    $year2 = date('y')+43;
    $a =1;
    $c2 = 1;
    $invoiceNumber = FunctionsController::invoice_last_record()->id + 1;
?>
    @section('style')
        <style>
            *{
                font: 11pt "Sarabun";
                font-weight: bold
            }
            .address{
                font-size: 11pt; 
            }
            td.waterUsedHisHead{
                background-color: #63dbeb!important;
                color: black;
                font-size: 10pt;
                padding: 2pt
            }

            .head{
                background-color: #63dbeb!important;
                color: black;
                font-size: 16pt;
                font-weight: bold
            }
            .head2{
                background-color: #63dbeb!important;
                color: black;
                font-size: 14pt;
            }
            body {
                -webkit-print-color-adjust: exact !important;
            }
            td{
                padding: 4.7pt;
                /* padding-left: 2pt; */
            }
            .head{
                height: 3pc;
            }
            .m_detail{
                font-size: 0.9rem
            }
            .m_detail2{
                padding-top:1px !important;
                padding-bottom: 1px !important;
            }
           
            .noneborder_top_left{
                border-top: 1px solid white;
                border-right:1px solid white; 
            }
            .barcode{
                height: 50pt; width:160pt
            }
            .border-bottom-none{
                border-bottom: 1px solid white;
            }
            .border-right-none{
                border-right: 1px solid white;
            }
            .border-top-none{
                border-top: 1px solid white;
                padding-left: 10px
            }
            .border-left-none{
                border-left: 1px solid white;
            }
            .border-bottom-fill{
                border-bottom: 1px solid black;
            }
            .border-right-fill{
                border-right: 1px solid black;
            }
            .border-top-fill{
                border-top: 1px solid black;
            }
            .border-left-fill{
                border-left: 1px solid black;
            }
            .unit_usedtext{
                font-size: 8pt;
            }
            @media print { 
                *{
                    font-size: 10.8pt;
                }
                .address{
                    font-size: 9pt; 
                }
                td.waterUsedHisHead{
                    background-color: #63dbeb!important;
                    color: black;
                    font-size: 9pt;
                    padding: 1pt
                }

                .head{
                    background-color: #63dbeb!important;
                    color: black;
                    font-size: 16pt;
                    font-weight: bold
                }
                .head2{
                    background-color: #63dbeb!important;
                    color: black;
                    font-size: 14pt;
                }
            
            }



        </style>
    @endsection
    @section('content')
        <input type="hidden" id="type" value="{{$type}}">
        <div class="row m-3">
            <div class="col-6">
            @include('payment._rc_left_form')
            </div>
            <div class="col-6">
                @include('payment._rc_right_form')
            </div>
        </div>

        <div style="clear: both;page-break-after: always;"></div>
      
    @endsection

    @section('script')

    <script>
        $(document).ready(function () {
            // $('.btnprint').click(function(){
            $('.btnprint').hide();
            var css = '@page {  }',
                head = document.head || document.getElementsByTagName('head')[0],
                style = document.createElement('style');
                style.type = 'text/css';
                style.media = 'print';
                if (style.styleSheet) {
                    style.styleSheet.cssText = css;
                }else{
                    style.appendChild(document.createTextNode(css));
                }
                head.appendChild(style);

                style.type = 'text/css';
                style.media = 'print';

                if (style.styleSheet) {
                    style.styleSheet.cssText = css;
                } else {
                    style.appendChild(document.createTextNode(css));
                }

                head.appendChild(style);

                window.print();
                // if($('#type').val() == 'paid_receipt'){
                //     setTimeout(function(){ window.location.href = '../payment'; }, 300);
                // }else{
                //     //type == history_recipt
                //     setTimeout(function(){ window.location.href = '../search'; }, 300);

                // }
        });
    </script>

    @endsection
