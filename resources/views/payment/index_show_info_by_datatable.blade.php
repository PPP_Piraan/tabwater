@extends('layouts.adminlte')

@section('mainheader')
รับชำระค่าน้ำประปา
@endsection
@section('nav')
<a href="{{'payment/index'}}">รับชำระค่าน้ำประปา</a>
@endsection
@section('payment')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }
    .paidform{
        font-size: 2.28rem; width:8rem; height:5rem
    }
    /* #modal-success{
        top:0
    } */
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')

<table id="example2" class="display" style="width:100%">

</table>
@if (collect($invoice_period)->count() == 0 && $oweInvCountGroupByUserId == 0)

<div class="col-lg-6 col-6">
    <div class="small-box bg-warning">
        <div class="inner">
            <h3>ยังไม่ได้สร้างรอบบิลปัจจุบัน</h3>
            <p>&nbsp;</p>
        </div>
        <div class="icon">
            <i class="fas fa-exclamation-circle"></i>
        </div>
        <a href="{{url('invoice_period')}}" class="small-box-footer">สร้างรอบบิลปัจจุบัน
            <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
@else
<!-- ค้นหา --->
<div id="menuseach">
    {{-- <span style="font-size:20px;cursor:pointer" class="openbtn" onclick="openNav()">&#9776; เปิดหน้าค้นหา</span>
    <a href="javascript:void(0)" style="font-size:20px;cursor:pointer" class="closebtn hidden"
        onclick="closeNav()">&times; ปิดหน้าค้นหา</a> --}}
        
    <div id="mySidenav" class="sidenav" style="width: 400px">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">ค้นหา : ชื่อ,ที่อยู่ ,เลขมิเตอร์</h3>
            </div>
            <div class="card-body">
                <div class="input-group">
                    <input type="text" class="form-control " id="tags">
                </div>
            </div>
        </div>
        {{-- <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">รายการค้างชำระแยก <br>แยกตามเส้นทางจดมิเตอร์</h3>

            </div><!-- /.card-header -->
            <div class="card-body test1" style="display: block;height:370px; overflow-y: scroll">
                กำลังโหลดข้อมูล ...
            </div><!-- /.card-body -->
        </div> --}}
    </div>
</div>
<div class="row" id="main">

    <div class="col-md-12 col-xxl-5 header hidden">
        <div class="card res">
            <div class="card-header ">
                <div class="card-title">

                    <div class="row">

                        <div class="info-box col-12">
                            <span class="info-box-icon bg-warning"><i class="fas fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text" id="subzone_span"> </span>
                                <span class="info-box-number">ค้าง <span class="oweCount h5 text-warning"></span>
                                    คน</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body table-responsive">
                <div id="DivIdToExport">
                    <table id="oweTable" class="table text-nowrap" width="100%">ปป</table>
                </div>
            </div>
            <!--card-body-->
            <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>

        </div>
    </div>

</div>

@endif
<div class="modal fade" id="modal-success">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
            </div>
            <form action="{{url('payment/store')}}" method="post" onsubmit="return check()">
                @csrf
                <div class="modal-body">

                    <div id="activity">
                        <div class="row">
                            <div class="col-md-3">
                                {{-- ข้อมููลผู้ใช้งานที่ถูกเลือก --}}
                                <div class="card card-primary card-outline">
                                    <div class="card-body box-profile">
                                        <div class="text-center">
                                            <img class="profile-user-img img-fluid img-circle"
                                                src="{{asset('/img/icons-user.png')}}">
                                        </div>

                                        <h3 class="profile-username text-center" id="feFirstName"></h3>

                                        <p class="text-muted text-center" id="meternumber2"></p>

                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>ที่อยู่</b> <a class="float-right" id="feInputAddress"></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เบอร์โทรศัพท์</b> <a class="float-right" id="phone"></a>
                                            </li>
                                        </ul>

                                    </div>

                                    <!-- /.card-body -->
                                </div>

                            </div>
                            <!--col-md-3-->
                            <div class="col-md-9">
                                {{-- ข้อมูลใบแจ้งหนี้และการชำระ --}}
                                <input type="hidden" name="mode" id="mode" value="payment">
                                <input type="hidden" name="user_id" id="user_id" value="">

                                <div id="payment_res"> </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-check text-center">
                                            <label class="form-label" for="exampleCheck2">ชำระผ่าน<div>ธนาคาร</div>
                                                </label>
                                            <input type="checkbox" class="form-control" id="paid_with_bank_slip"
                                                name="paid_with_bank_slip">

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">ยอดที่ต้องชำระ</label>
                                        <input type="text" class="form-control text-bold text-center mustpaid paidform"
                                            readonly name="mustpaid">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">รับเงินมา</label>
                                        <input type="text"
                                            class="form-control text-bold text-center cash_from_user paidform"
                                            name="cash_from_user">
                                    </div>
                                    <div class="col-md-1 text-bold pt-2 display-3 bottom">=</div>
                                    <div class="col-md-3">
                                        <label class="control-label">เงินทอน</label>
                                        <input type="text"
                                            class="form-control text-bold text-center border-success cashback  paidform"
                                            readonly value="" name="cashback">
                                    </div>
                                    <button type="submit"
                                        class="col-md-2 btn btn-success mt-4  submitbtn hidden">ชำระเงิน</button>

                                    {{-- <button type="button" class="col-md-2 btn btn-success mt-4  hidden submitbtn"  onclick="check()" >ชำระเงิน</button> --}}
                                </div>
                            </div>
                            <!--class="col-md-9"-->
                        </div>
                        <!--row-->
                    </div>
                    <!--activity-->
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection


    @section('script')
    <script
        src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
    </script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="{{asset('/js/my_script.js')}}"></script>
    <script src="{{asset('/js/1.12.1/jquery-ui.js')}}"></script>
    
   
    <script>
      let name_init =  '';
      $("#tags").autocomplete({
                                source: `../../api/users/search/${name_init}/address`,
                            
                            });
        $('#tags').keyup(function(){

            let name =  $(this).val();
            console.log('nam',name)

            // if(name.match('/^[0-9]/')){
            if(  !isNaN(parseInt(name)) ){
                $.post(`../../api/users/search2`, {name:name, type: 'address'}).done(function(data){
                    console.log('nam',data)

                            $("#tags").autocomplete({
                                source: data,
                                select: function( event, ui ) { 
                                    var meternumber = ui.item.value.split(" - ")
                                    findReciept(meternumber[2])
                                },
                                
                            });
                            // console.log('address',data)

                    }) 
            }else{
                if(name.length >= 2){

$.post(`../../api/users/search2`, {name:name, type: 'name'}).done(function(data){
    console.log('name data',data)

        $("#tags").autocomplete({
            source: data,
            select: function( event, ui ) { 
                console.log('ssss',ui.item)
                var meternumber = ui.item.value.split(" - ")
                findReciept(meternumber[2])
            }
        });

}) 
}//if
            }

        })

    </script>
    <script>
        let a = true
        let table;
        //getข้อมูลจาก api มาแสดงใน datatable
        
        $(document).ready(function () {
            // getOweInfos('<?php echo $zone_id_selected; ?>', '<?php echo $subzone_id_selected; ?>')
            let text = ``;
            // setTimeout(()=>{
                $.get('../../api/owepaper/testOweAndInvoioceDivideBySubzone').done(function(data){

                    data.forEach(element => {
                        text += `
                        <div class="col-md-12 col-sm-6 col-12">
                            <div class="info-box subzone_click" id="btnzone_id${element.zone_id}" data-zone_id="${ element['zone_id'] }" data-subzone_id="${ element['subzone_id'] }"
                            data-zone_name="${ element['zoneName'][0].zone_name }" data-subzone_name="${ element['subzoneName'] }">
                            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">เส้นทาง ${element['subzoneName']}</span>
                                <span class="info-box-number">${element['oweCount']}  คน</span> 
                            </div>
                            <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        `
                      
                    });
            text += ` </div><!--row-->
              </div>
              <!-- /.card-body -->
            </div>`
            $('.test1').html(text)

                })
           
        // },100)
        
        })
        $(document).ready(function () {
            $('.paginate_page').html('หน้า') 
            let val = $('.paginate_of').text()
            $('.paginate_of').text(val.replace('of', 'จาก'));

            //เอาค่าผลรวมไปแสดงตารางบนสุด
            $('#meter_unit_used_sum').html($('.meter_unit_used_sum').val())
            $('#owe_total_sum').html($('.owe_total_sum').val())
            $('#reserve_total_sum').html($('.reserve_total_sum').val())
            $('#all_total_sum').html($('.all_total_sum').val())
        }) //document

        $('body').on('keyup','input[type="search"]',function(){
            setTimeout(()=>{
                let name =  $(this).val();
                console.log('name', name)
                findReciept(name)
            },200)
        })   

        var dataTableDestroy = 0
        $('body').on('click', '.subzone_click', function(){
            var zone_id = $(this).data('zone_id')
            var subzone_id = $(this).data('subzone_id')
            if(dataTableDestroy > 0){
                $('#oweTable').DataTable().destroy();
            }
            
            dataTableDestroy = 1
            getOweInfos(zone_id, subzone_id)
        })

        function findReciept(_name) {
            $('.overlay').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            let name = _name 
            let txt ='';
            let total = 0;
            let i =0;
            $('.cash_from_user').val(0)
            $('.cashback').val(0)
            $('.mustpaid').val(0)

            let user_id_string  = name.toLowerCase().split('hs')[1];
            let user_id =  parseInt(user_id_string)
            $('#user_id').val(user_id)
            console.log(user_id)
            $.get(`../api/invoice/${user_id}`).done(function(invoices){
                console.log('in',invoices)
                $('#invAndOweLists').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>')
                if(Object.keys(invoices).length > 0){
                    txt += `<div class="card card-success border border-success rounded">
                                <div class="card-header">
                                    <h3 class="card-title">รายการค้างชำระ</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i> </button>
                                    </div>
                                </div>
                                <div class="card-body p-0 " style="display: block;height:250px; overflow-y: scroll; font-size:14px !important">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th style="width: 10px">
                                                <input type="checkbox" id="checkAll" checked>
                                                </th>
                                            <th class="text-center">เลขใบ<br>แจ้งหนี้</th>
                                            <th class="text-center">เลขมิเตอร์</th>
                                            <th class="text-center">รอบบิล</th>
                                            <th class="text-center">ยอดครั้งก่อน</th>
                                            <th class="text-center">ยอดปัจจุบัน</th>
                                            <th class="text-center">จำนวน<br>ที่ใช้</th>
                                            <th class="text-center">ค่าใช้น้ำ<br>(บาท)</th>
                                            <th class="text-center">ค่ารักษามิเตอร์</th>
                                            <th class="text-center">เป็นเงิน<br>(บาท)</th>
                                            <th class="text-center">สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>`;
                                        invoices.forEach(element => {
                                            let status = element.status == 'owe' ? 'ค้างชำระ' : 'ออกใบแจ้งหนี้';
                                            let diff = element.currentmeter - element.lastmeter;
                                            let _total = diff*8;
                                            let reserve = _total  == 0 ? 10 : 0;
                                            total += _total + reserve; //   

                                            txt +=` <tr>
                                                        <td><input type="checkbox"  checked  class="control-input invoice_id checkbox" data-inv_id="${element.id}" name="payments[${i}][on]">
                                                        </td>
                                                        <td>${element.id}</td>
                                                        <td>${element.usermeterinfos.meternumber}</td>
                                                        <td>${element.invoice_period.inv_period_name}</td>
                                                        <td>${element.lastmeter}</td>
                                                        <td>${element.currentmeter}</td>
                                                        <td>${diff}</td>
                                                        <td>${ _total }
                                                            <input type="hidden" name="payments[${i}][total]" value="${ _total }">
                                                            <input type="hidden" name="payments[${i}][iv_id]" value="${ element.id }">
                                                            <input type="hidden" name="payments[${i}][status]" value="${ element.status }">
                                                        </td>
                                                        <td>${reserve}</td>
                                                        <td class="total">${_total+ reserve}</td>
                                                        <td class="total">${status}</td>
                                                        
                                                    </tr>
                                            `;  
                                            i++;
                                        }) //foreach
                                    
                    txt +=`             </tbody>
                                    </table>
                                </div>
                            </div>`; 


                    let address = `${invoices[0].usermeterinfos.user_profile.address} 
                                 ${invoices[0].usermeterinfos.user_profile.zone.user_zone_name} \n
                                อำเภอ ${invoices[0].usermeterinfos.user_profile.district.district_name}\n
                                จังหวัด ${invoices[0].usermeterinfos.user_profile.province.province_name}`;
                    
                    $('#feFirstName').html(invoices[0].usermeterinfos.user_profile.name);
                    $('#meternumber2').html(invoices[0].usermeterinfos.meternumber);
                    $('#feInputAddress').html(address);
                    $('#phone').html(invoices[0].usermeterinfos.user_profile.phone);  
                                        
                    $('#payment_res').html(txt);
                    $('.modal').addClass('show')
                    $('.overlay').html('') 
                }else{
                    $('#oweTable').html('ไม่พบรายการค้างชำระ')
                }

                $('.mustpaid').val(total)

            });
        }//text

        $('body').on('click', '#oweTable tbody tr', function () {

            let meternumber = $(this).find('td.meternumber').text()
            findReciept(meternumber)
        });      

        $('.close').click(()=>{
            $('.modal').removeClass('show')
        })
        
        
        $('.searchBtn').click(function () {
            let zone_id = $('#zone_id').val()
            let subzone_id = $('#subzone_id').val()
            if (zone_id !== 'all' && subzone_id === '') {
                alert('ยังไม่ได้เลือกเส้นทาง')
                $('#subzone_id').addClass('border border-danger rounded')
                return false
            }
            $('#oweTable').DataTable().destroy();
            $('#subzone_id').removeClass('border border-danger rounded')

            getOweInfos(zone_id, subzone_id)
        });

        let init = 1;
        function getOweInfos(zone_id = '<?php echo $zone_id_selected; ?>', subzone_id = '<?php echo $subzone_id_selected; ?>') {    
           
           
            let params = {
                zone_id: zone_id,
                subzone_id: subzone_id
            }
            console.log('zzz',zone_id)
            // $('#zone_id').change(zone_id)
            $.get(`../api/owepaper/testIndexFilterOweAndInvoice`, params).done(function (data) {
        
                var zone_name = zone_id === 'all' ? 'ทั้งหมด' : data[0].zone.zone_name
                var subzone_name = subzone_id === 'all' ? 'ทั้งหมด' : data[0].subzone.subzone_name

                $('#subzone_span').html(`<b>${zone_name}  -  ${subzone_name}</b>`)
      
                if (data.length === 0) {
                    $('.res').html('<div class="card-body h3 text-center">ไม่พบข้อมูล</div>')
                } else {
                    $('.oweCount').html(data.length)
                    $('.header').removeClass('hidden')
                  
                    table = $('#oweTable').DataTable({
                        responsive: true,
                        // order: false,
                        // searching:false,
                        "pagingType": "listbox",
                        "lengthMenu": [
                            [10, 25, 50, 150, -1],
                            [10, 25, 50, 150, "ทั้งหมด"]
                        ],
                        "language": {
                            "search": "ค้นหา:",
                            "lengthMenu": "แสดง _MENU_ แถว",
                            "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                            "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                            "paginate": {
                                "info": "แสดง _MENU_ แถว",
                            },

                        },
                        data: data,
                        select:true,
                        columns: [
                            {
                                'title': '&nbsp;&nbsp;&nbsp;&nbsp;เลขที่&nbsp;&nbsp;&nbsp;',
                                data: 'user_id',
                                'className': 'text-center'
                            },
                            {
                                'title': 'ชื่อ-สกุล',
                                data: 'user_profile.name'
                            },
                            {
                                'title': 'เลขมิเตอร์',
                                data: 'meternumber',
                                'className': 'meternumber text-center',
                            },
                            {
                                'title': 'บ้านเลขที่',
                                data: 'user_profile.address',
                                'className': 'text-center'
                            },
                            {
                                'title': 'หมู่',
                                data: 'zone.zone_name',
                                'className': 'text-center'
                            },

                            {
                                'title': 'เส้นทางจดมิเตอร์',
                                data: 'subzone.subzone_name',
                                'className': 'text-center'
                            },
                            {
                                'title': 'ค้าง(เดือน)',
                                data: 'owe_count',
                                'className': 'text-center'
                            },
                            {
                                'title': 'หมายเหตุ',
                                data: 'comment',
                                'className': 'text-center'
                            }



                        ],
                        
                    }) //table
                    if(a){
                       $('#oweTable thead tr').clone().appendTo('#oweTable thead');
                        a= false
                    }
                    $('#oweTable thead tr:eq(1) th').each( function (index) {
                        var title = $(this).text();
                        $(this).removeClass('sorting')
                        $(this).removeClass('sorting_asc')
                        if(index < 4){
                            $(this).html( `<input type="text" data-id="${index}" class="col-md-12" id="search_col_${index}" placeholder="ค้นหา" />` );
                        }else{
                            $(this).html('')
                        }
                    } );
                } //else
                $('.overlay').remove()
                $('#oweTable_filter').remove()

                //custom การค้นหา
                let col_index = -1
                $('#oweTable thead input[type="text"]').keyup(function(){
                    let that = $(this)
                    var col = parseInt(that.data('id'))

                    if(col !== col_index && col_index !== -1){
                        $('#search_col_'+col_index).val('') 
                        table.column(col_index)
                        .search('')
                        .draw();
                    }
                    setTimeout(function(){ 
                        
                        let _val = that.val()
                        if(col === 0 || col===3){
                            var val = $.fn.dataTable.util.escapeRegex(
                                _val
                            );
                            table.column(col)
                            .search( val ? '^'+val+'.*$' : '', true, false )
                            .draw();
                        }else{
                            table.column(col)
                            .search( _val )
                            .draw();
                        }
                    }, 300);
        
                    col_index = col

                })


            }) //.get

        }

        $('#zone_id').change(function () {
            //get ค่าsubzone 
            $.get(`../api/subzone/${$(this).val()}`)
                .done(function (data) {
                    let text = '<option value="">เลือก</option>';
                    if (data.length > 1) {
                        text += `<option value="all">ทั้งหมด</option>`;
                    }
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.subzone_name}</option>`
                    });
                    $('#subzone_id').html(text)
                });
        });

        $(document).on('click','#checkAll', function(){
            let res = $(this).prop('checked')
            $('.checkbox').prop('checked', res)
            checkboxclicked()
        })

        function  showEmptyDataBox(val){
            $('#activity').removeClass('hidden');
            $('.overlay').html('')
            txt = `<div class="text-center h3 mt-2"><b>${val}</b></div>
                    <div class="text-center text-success h4 mb-2">ไม่มีรายการค้างชำระ</div>`;
            
            $('#activity').html(txt)
        }//showEmptyDataBox

        $('.cash_from_user').keyup(function(){
            let mustpaid = $('.mustpaid').val()
            let cash_from_user = $(this).val()
            let cashback = cash_from_user - mustpaid 
          
            $('.cashback').val(cashback)
            if(cash_from_user === ""){
                $('.cashback').val("")
            }
            if(cashback >= 0){
                mustpaid == 0 ? $('.submitbtn').attr('style', 'display:none') : $('.submitbtn').attr('style', 'display:block')
            }else{
                $('.submitbtn').attr('style', 'display:none')
            }
          
        }); //$('.cash_from_user')

        $(document).on('click','.checkbox', function(){
            checkboxclicked()
        });//$(document).on('click','.checkbox',

        function checkboxclicked(){
            let _total = 0;
           // $('#checkAll').prop('checked', true)

            $('.checkbox').each(function(index, element){
                if ($(this).is(":checked")) {
                    let sum =  $(this).parent().siblings('.total').text()
                    _total += parseInt(sum)     
                }else{
                    $('#checkAll').prop('checked', false)

                }
            });
            if( $('.cash_from_user').val() > 0){
                let remain = $('.cash_from_user').val() - _total
                $('.cashback').val(remain)
            }
            
            if(_total == 0){
                $('.cash_form_user').attr('readonly')
            }else if(_total > 0){
                $('.cash_form_user').removeAttr('readonly')
            }

            $('.mustpaid').val(_total)
        }

        $(document).ready(function(){
            let _total = 0;
            $('.checkbox').each(function(index, element){
                if ($(this).is(":checked")) {
                    let sum =  $(this).parent().siblings('.total').text()
                    _total += parseInt(sum)    
                }
            });
            $('.mustpaid').val(_total)
            $('.cash_from_user').val(0)
            $('.cashback').val(0)
        });//$(document).ready(function()

        function check(){
            $('.submitbtn').prop('disabled', true);
            let checkboxChecked = false;
            let cashbackRes = false;
            let errText = '';
            $('.checkbox:checked').each(function(index, element){
                checkboxChecked = true;
            });
            if(checkboxChecked == false){
                errText += '- ยังไม่ได้เลือกรายการค้างชำระ\n';
            }
            let mustpaid = $('.mustpaid').val()
            let cash_from_user = $('.cash_from_user').val()
            let cashback = cash_from_user - mustpaid 
            if(!Number.isNaN(cashback) && cashback >= 0){
                cashbackRes = true;
            }else{
                errText += '- ใส่จำนวนเงินไม่ถูกต้อง'
            }
            if(checkboxChecked == false || cashbackRes == false){
                $('.submitbtn').prop('disabled', false);

                alert(errText)
                return  false;
            }else{
                return true;
                
            }
            
            
        }

    </script>
    @endsection

    <!--ค้นหา -->
    {{-- <div class="row">
        <div class="col-md-8  col-xxl-7" id="aa">
            <div class="info-box">
                <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>
                <div class="info-box-content">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">หมู่ที่</label>
                            <select class="form-control" name="zone_id" id="zone_id">
                                <option value="all">ทั้งหมด</option>
                                @foreach ($zones as $zone)
                                <option value="{{$zone->id}}" {{$zone->id == $zone_id_selected ? 'selected' : ''}}>{{$zone->zone_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">เส้นทาง</label>
                            <select class="form-control" name="subzone_id" id="subzone_id">
                                <option value="all" selected>ทั้งหมด</option>
                             
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">&nbsp;</label>
                            <button type="button" class="form-control btn btn-primary searchBtn">ค้นหา</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}