@extends('layouts.adminlte')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
<style>@page { size: A5 }</style>
@endsection
@section('mainheader')
ค้นหาใบเสร็จรับเงิน
@endsection
@section('payment-search')
    active
@endsection
@section('nav')
<a href="{{url('/payment/search')}}"> ค้นหาใบเสร็จรับเงิน</a>
@endsection

 @section('style')
     <link  rel="stylesheet" href="{{asset('/adminlte/plugins/select2/css/select2.min.css')}}">
     <link rel="stylesheet" href="{{asset('/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    
 @endsection

@section('content')
<div class="row">
    <div class="card card-primary col-md-4">
        <div class="card-header">
            <h3 class="card-title">ค้นหา : ชื่อ,ที่อยู่ ,เลขมิเตอร์</h3>
        </div>
        <div class="card-body">
            <div class="input-group">
                <input type="text" class="form-control" id="tags">
            </div>
        </div>
    </div>
   
</div>
    <div class="row" id="aa" >
        <div class="user_info col-12" style="display: none">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile row">
                        <div class="col-md-2">
                            <img class="profile-user-img img-fluid img-circle"
                                src="{{asset('/adminlte/dist/img/user4-128x128.jpg')}}"
                                alt="User profile picture">
                        </div>
                        <div class="col-md-2 col-sm-3 col-lg-3">
                                <h3 class="profile-username text-center" id="feFirstName"></h3>
                                <h4 class="profile-username text-center" id="meternumber2"></h4>
                        </div>
                        <div class="col-md-7 col-sm-7 col-lg-6">
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>ที่อยู่</b> <a class="float-right" id="feInputAddress"></a>
                                </li>
                                {{-- <li class="list-group-item">
                                    <b>เบอร์โทรศัพท์</b> <a class="float-right" id="phone"></a>
                                </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
        <!-- /.col -->
    </div>

    <div class="row" >
        {{-- <div class="col-4"></div> --}}
        <div class="col-12">
            <div id="emptyDataTxt"></div>
        </div>
    </div>
    <div class="row" >
        {{-- <div class="col-4"></div> --}}
        <div class="col-12 res hidden">
            <div id="receiptInfos"></div>
        </div>
    </div>
       
@endsection

@section('script')
  <script src="{{asset('/js/1.12.1/jquery-ui.js')}}"></script>
    <script>
        let i = 1;
        let getDatares = 0;
        $('#tags').keyup(function(){

            $('.user_info').attr('style', 'display:none')
            $('.result').attr('style', 'display:none')
            $('#activity').removeClass('active');
            $('.res').removeClass('show');
            $('.res').addClass('hidden');

            let name = $(this).val();
            if(name.match(/^[0-9//-]/)){
                $.get(`../api/users/search/${name}/search_history`).done(function(data){
                    $("#tags").autocomplete({
                        source: data,
                    });
                })
            }else{
                if(name.length >= 2){
                    if(name.toLowerCase().indexOf('hs')>= 0 ){
                              let x  = name.replace("-", "");
                                name = x;
                                $('#tags').val(x)
                            }
                    $.get(`../api/users/search/${name}/search_history`).done(function(data){
                        $("#tags").autocomplete({
                            source: data,
                        });
                    }) 
                }//if
            }
            
        })

        $( "#tags" ).autocomplete({
            select: function( event, ui ) { 
                getReceiptInfos(ui.item.value)
            }
        });


        let oldname = 'x';
        $('#tags').change(function(){
            let name =  $(this).val();
            console.log('namechange', name)
            getReceiptInfos(name)
            $(this).val('')
        })
      
      

       function getReceiptInfos(_name) {
            $('#emptyDataTxt').html('')

            $('.overlay').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            let name = _name;
            let txt ='';
            let total = 0;
            let i =0;

            let meter_id  = name.toLowerCase().split('hs')[1];
            if(typeof meter_id === 'undefined' ){
                alert('ไม่พบข้อมูล')
                return;
            }
            if(meter_id.charAt(0) === '0'){
                meter_id = meter_id.substring(1)
            }
            console.log(meter_id)
            $('#user_id').val(meter_id)
            $.get(`../api/payment/history/${meter_id}/receipt_history`).done(function(invoices){
                $('#receiptInfos').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>')

                $('.res').each(function($index){
                    $(this).removeClass('hidden');
                    $(this).addClass('show');
                })  
                if(Object.keys(invoices['reciepts']).length > 0){
                    
                    invoices['reciepts'].forEach((elements, key) => {
                        console.log('invoices[elements]',elements);
                     
                        //รอบบิลที่
                        txt += `
                            <div class="card card-success card-outline">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-4 h5 text-bold">
                                            ใบเสร็จรับเงินเลขที่  ${elements[0].receipt_id}
                                        </div>
                                        <div class=" col-md-4 text-center"> วันที่ ${elements[0].receipt_th_date}</div>
                                        <div class="col-md-4 text-right">
                                            <a href="../payment/print_payment_history/${elements[0].receipt_id}" 
                                                                            class="btn btn-primary">ปริ้นใบเสร็จ</a>
                                            <a href="../payment/remove/${elements[0].receipt_id}" class="btn btn-warning">ยกเลิกใบเสร็จรับเงิน</a>
                                        </div>
                                    </div><!--row-->                                   
                                </div>
                                <div class="card-body p-0 table-responsive">
                                    <table class="table table-striped table-bordered text-nowrap">
                                        <thead>
                                            <tr>
                                            <th class="text-center">เลขใบแจ้งหนี้</th>
                                            <th class="text-center">เลขมิเตอร์</th>
                                            <th class="text-center">รอบบิล</th>
                                            <th class="text-center">ค่าน้ำประปา(บาท)</th>
                                            <th class="text-center">ค่ารักษามิเตอร์(บาท)</th>
                                            <th class="text-center">คิดเป็นเงิน(บาท)</th>
                                            </tr>
                                        </thead> 
                                        <tbody>
                        `;
                        let total = 0; 

                        elements.forEach((element, index) => {
                            //รายละเอียดแต่ละรอบบิล
                            let diff = element.currentmeter - element.lastmeter;
                            let _total =  diff*8;
                            reserve = diff == 0 ? 10 : 0;
                            total += _total + reserve; 

                            txt +=`<tr>
                                        <td class="text-center">${element.iv_id}</td>
                                        <td class="text-center">${element.meternumber}</td>
                                        <td class="text-center">${element.inv_period_name}</td>
                                        <td class="text-right">${_total}</td>
                                        <td class="text-right">${reserve}</td>
                                        <td class="text-right">${_total + reserve}</td>                            
                                    </tr>`;    

                            i++;
                         if(elements.length === i){
                             txt += `<tr>
                                         <td colspan="5" class="text-success h5">รวมเป็นเงิน </td>
                                         <td class="text-right text-success h5">${total}</td>
                                     </tr>` 
                            i = 0;   
                         }
                     

                        });//element  
                        txt += `
                                        </tbody>
                                    </table>
                                </div><!-- card-body -->
                             </div><!--card-->
                        `;  
                                             
                    }); //elements
                  
                }else{//if
                    showEmptyDataBox(name)
                }//else
                   
                    
                $('.card-success').prop('style', 'display:block');

                $('#receiptInfos').html(txt)
                

                $('.overlay').html('')
      
            
                if(Object.keys(invoices['user']).length > 0){
                    let address = `${invoices['user'].address} ${invoices['user'].zone_name} \n
                                    ตำบล ${invoices['user'].tambon_name}\n
                                    อำเภอ ${invoices['user'].district_name}\n
                                    จังหวัด ${invoices['user'].province_name}`;
                    $('#feFirstName').html(invoices['user'].name);
                    $('#meternumber2').html(invoices['user'].meternumber);
                    $('#feInputAddress').html(address);
                    $('.user_info').attr('style', '')
                    $('.result').attr('style', '')

                    $('.res_user').removeClass('hidden')
                    $('.res_user').addClass('show')
                    $('#receiptInfos').html(txt);

                }//if

            });//$.get
        }//btn

        function  showEmptyDataBox(val){
            $('.overlay').html('')
            txt = `
                    <div class="card card-warning card-outline">
                        <div class="card-body text-center">
                        <div class="text-center h3"><b>${val}</b></div>
                        <div class="text-center text-success h4">ไม่มีรายการใบเสร็จรับเงิน</div>
                        </div>
                    </div>`;
                
            $('#emptyDataTxt').html(txt)
        }//showEmptyDataBox
    </script>


@endsection

