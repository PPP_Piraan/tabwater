@extends('layouts.adminlte')

@section('mainheader')
ค้นหาใบเสร็จรับเงิน
@endsection
@section('nav')
<a href="{{'payment/index'}}">ค้นหาใบเสร็จรับเงิน</a>
@endsection
@section('payment-search')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }

</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">

@endsection

@section('content')

@if (collect($invoice_period)->count() == 0 && $oweInvCountGroupByUserId == 0)
<div class="row">
    <div class="col-lg-6 col-6">
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>ยังไม่ได้สร้างรอบบิลปัจจุบัน</h3>
                <p>&nbsp;</p>
            </div>
            <div class="icon">
                <i class="fas fa-exclamation-circle"></i>
            </div>
            <a href="{{url('invoice_period')}}" class="small-box-footer">สร้างรอบบิลปัจจุบัน
                <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

</div>
@else
<div class="row">
    <div class="card card-primary col-md-6">
        <div class="card-header">
            <h3 class="card-title">ค้นหา : ชื่อ,ที่อยู่ ,เลขมิเตอร์</h3>
        </div>
        <div class="card-body">
            <div class="input-group">
                {{-- <input type="text" class="form-control " id="tags"> --}}
                <input type="text" id="tags" name="search" placeholder="Search" class="form-control" />

            </div>
        </div>
    </div>
    <div class="col-md-6" id="user_info">

    </div>
</div>
<div id="res"></div>

<div id="menuseach" class="hidden" >
    <span style="font-size:20px;cursor:pointer" class="openbtn" onclick="openNav()">&#9776; เปิดหน้าค้นหา</span>
    <a href="javascript:void(0)" style="font-size:20px;cursor:pointer" class="closebtn hidden"
        onclick="closeNav()">&times; ปิดหน้าค้นหา</a>
    <div id="mySidenav" class="sidenav ">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">รายการค้างชำระ <br>แยกตามเส้นทางจดมิเตอร์</h3>

            </div><!-- /.card-header -->
            <div class="card-body test1" style="display: block;height:600px; overflow-y: scroll">
                กำลังโหลดข้อมูล ...
            </div><!-- /.card-body -->
        </div>
    </div>
</div>
<div class="row hidden" id="main2">
    <div class="col-md-12 col-xxl-5">
            <div class="card res">
                <div class="card-header header hidden">
                    <div class="card-title">

                        <div class="row">

                        <div class="info-box col-12">
                            <span class="info-box-icon bg-warning"><i class="fas fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text" id="subzone_span"> </span>
                                <span class="info-box-number">ค้าง <span class="oweCount h5 text-warning"></span> คน</span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                        </div>
                    </div>
                    <div class="card-tools">
                        {{-- <input type="submit" class="btn btn-primary mb-3 float-right" id="print_multi_inv"
                        value="ปริ้นใบแจ้งเตือนชำระหนี้ที่เลือก"> --}}
                    </div>
                </div>

                <div class="card-body table-responsive">
                    <div id="DivIdToExport">
                        <table id="oweTable" class="table text-nowrap" width="100%"></table>
                    </div>
                </div>
                <!--card-body-->
                <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>

            </div>
    </div>
</div>
@endif

@endsection


    @section('script')
    <script
        src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
    </script>
    <script src="{{asset('/js/my_script.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js">
    </script>
    <script type="text/javascript">
        var route = "{{ url('payment/autocompletesearch') }}";
        $('#search').typeahead({
            source: function (query, process) {
                return $.get(route, {
                    query:query.toUpperCase().replace('HS-', 'HS')

                }, function (data) {
                    // console.log('dd', data)
                    return process(data);
                });
            },
            updater:function(e){
                let arr =  e.split(" - ")
                aa(arr[2])

            }

        })

    </script>

    <script>
        let name_init = '';
        let a = true
        let table;
        let datt = []
        let cancel_receipt_paper = false
        $.post(`../../api/users/search2`, {
                name: 0,
                type: 'address',
                name_length: 0
            }).done(function (data) {
                datt = data
            })
            $("#tags").autocomplete({
                source: datt,

            });

        $('#tags').keyup(function () {
            let name = $(this).val();
            $('#user_info').html('')
            $('#res').html('')
            if (!isNaN(parseInt(name))) {
                $.post(`../../api/users/search2`, {
                    name: name,
                    type: 'address',
                    name_length: name.length

                }).done(function (data) {

                    $("#tags").autocomplete({
                        source: data,
                        delay:400,

                        select: function (event, ui) {
                            var meternumber = ui.item.value.split(" - ")
                            aa(meternumber[2])
                        },

                    });
                    // console.log('address',data)

                })
            } else {
                if (name.length >1) {
                    let nameUppercase = $(this).val().toUpperCase();
                    name = nameUppercase.replace('HS-', 'HS')
                    // console.log('ss',name)
                    // $(this).val(name);
                    $.post(`../../api/users/search2`, {
                        name: name,
                        type: 'name',
                        name_length: name.length
                    }).done(function (data) {
                         console.log('name data=>', data)

                        $("#tags").autocomplete({
                            source: data,
                            select: function (event, ui) {
                                var meternumber = ui.item.value.split(" - ");

                                var meternumber2 = meternumber[2].split(" ")
                                if(meternumber2[1] === "ยกเลิกการใช้งาน"){
                                    cancel_receipt_paper = true;
                                }
                                aa(meternumber2[0])
                            }
                        });

                    })
                } //if
            }

        }) //tags

        function aa(meternumber) {
            var text = ''
            var user_id = parseInt(meternumber.split('HS')[1]);
            $.get(`../../api/payment/history2/${user_id}/receipt_history`).done(function (data) {
                console.log(data)
                var user_info = `
                    <div class="row" id="aa" >
                        <divs class="user_info col-12">
                            <div class="card card-primary card-outline card_user_info">
                                <div class="card-body box-profile row">

                                <div class="col-md-7 col-sm-7 col-lg-7">
                                    <h3 class="profile-username text-center" id="feFirstName"> ${data.user.name} </h3>
                                    <h4 class="profile-username text-center" id="meternumber2"> ${data.user.meternumber}</h4>
                                    <h4 class="profile-username text-center text-danger"> <span  id="cancel_receipt_text"></span> </h4>
                            </div>
                            <div class="col-md-5 col-sm-5 col-lg-5">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <a class="float-right" id="feInputAddress">
                                                <b>ที่อยู่: </b>
                                                ${data.user.address}

                                                   ${data.user.zone_name}<br>
                                                ตำบล ${data.user.tambon_name}<br>
                                                อำเภอ ${data.user.district_name}<br>
                                                จังหวัด ${data.user.province_name}
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                `;
                $('#user_info').html(user_info)
                if (Object.keys(data.reciepts).length > 0) {

                    data.reciepts.forEach(elements => {
                        var total = 0;
                        var i = 0;
                        text += `
                        <div class="card card-success card-outline ">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-4 h5 text-bold">
                                        ใบเสร็จรับเงินเลขที่   ${elements[0].receipt_id }
                                    </div>
                                    <div class=" col-md-4 text-center"> วันที่  ${elements[0].receipt_th_date }
                                        <div>${elements[0].cashiername}( ผู้รับเงิน )</div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <a href="{{ url('payment/print_payment_history/${elements[0].receipt_id}') }}" class="btn btn-primary">ปริ้นใบเสร็จ</a>`;
                                        if(elements[0].role === 1){
                                            text += `  <a href="{{ url('payment/remove/${elements[0].receipt_id}') }}" class="btn btn-warning cancel_receipt_paper">ยกเลิกใบเสร็จรับเงิน</a>`;
                                        }
                        text +=`
                                    </div>
                                </div><!--row-->
                            </div>
                            <div class="card-body p-0 table-responsive ">
                                <table class="table table-striped table-bordered text-nowrap">
                                <thead>
                                    <tr>
                                    <th class="text-center">เลขใบแจ้งหนี้</th>
                                    <th class="text-center">เลขมิเตอร์</th>
                                    <th class="text-center">รอบบิล</th>
                                    <th class="text-center">ก่อนจดมิเตอร์</th>
                                    <th class="text-center">หลังจดมิเตอร์</th>
                                    <th class="text-center">ค่าน้ำประปา(บาท)</th>
                                    <th class="text-center">ค่ารักษามิเตอร์(บาท)</th>
                                    <th class="text-center">คิดเป็นเงิน(บาท)</th>
                                    </tr>
                                </thead>
                                <tbody>
                            `;
                        elements.forEach(element => {

                            //รายละเอียดแต่ละรอบบิล
                            var diff = element.currentmeter - element.lastmeter;
                            var _total = diff * 8;
                            var reserve = diff == 0 ? 10 : 0;
                            total += _total + reserve;
                            text += `

                                        <tr>
                                            <td class="text-center">${element.iv_id }</td>
                                            <td class="text-center">${element.meternumber }</td>
                                            <td class="text-center">${element.inv_period_name }</td>
                                            <td class="text-center">${element.currentmeter }</td>
                                            <td class="text-center">${element.lastmeter }</td>
                                            <td class="text-right"> ${_total }</td>
                                            <td class="text-right"> ${reserve }</td>
                                            <td class="text-right"> ${_total + reserve }</td>
                                        </tr>
                                        `

                            i++;
                            if (Object.keys(elements).length === i) {
                                text += `
                                            <tr>
                                                <td colspan="7" class="text-success h5">รวมเป็นเงิน </td>
                                                <td class="text-right text-success h5">${total}</td>
                                            </tr>
                                           `;
                                i = 0
                            }
                        });
                        text += `
                            </tbody>
                            </table>
                            </div>
                        </div>
                            `

                    }); //reciepts foreach

                } else {

                } //else


                $("#res").html(text)
                if(data.user.status === 'deleted'){
                    $('.cancel_receipt_paper').addClass('hidden')
                    $('#cancel_receipt_text').html('( ยกเลิกการใช้งาน )')
                    $('.card_user_info').removeClass('card-primary')
                    $('.card_user_info').addClass('card-danger')
                }
            }) //$.get
        } //function


        //getข้อมูลจาก api มาแสดงใน datatable
        $(document).ready(function () {
            getOweInfos()
            $('.paginate_page').html('หน้า') 
            let val = $('.paginate_of').text()
            $('.paginate_of').text(val.replace('of', 'จาก'));

            //เอาค่าผลรวมไปแสดงตารางบนสุด
            $('#meter_unit_used_sum').html($('.meter_unit_used_sum').val())
            $('#owe_total_sum').html($('.owe_total_sum').val())
            $('#reserve_total_sum').html($('.reserve_total_sum').val())
            $('#all_total_sum').html($('.all_total_sum').val())
        }) //document



        $('body').on('click', '#oweTable tbody tr', function () {
            let user_id = $(this).find('td.user_id').text()
            // findReciept(meternumber)

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            // console.log('row', row)
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {

                let data = 'ss';
                row.child(format2(1)).show();

                tr.addClass('shown');
            }
        });


        function format2(user_id) {

            let txt = '';
            let i = 0
            $.get(`../api/payment/history/${user_id}/receipt_history`).done(function (invoices) {
                $('#receiptInfos').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>')

                if (Object.keys(invoices['reciepts']).length > 0) {

                    invoices['reciepts'].forEach((elements, key) => {
                        txt += `
                            <div class="card card-success card-outline">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-4">sfsdf</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    dfsdf
                                </div>

                            </div>
                    `


                    }); //elements

                }
                return txt;
                $('.overlay').html('')

            }); //$.get
        }

        $('.close').click(() => {
            $('.modal').removeClass('show')
        })


        $('.searchBtn').click(function () {
            let zone_id = $('#zone_id').val()
            let subzone_id = $('#subzone_id').val()
            if (zone_id !== 'all' && subzone_id === '') {
                alert('ยังไม่ได้เลือกเส้นทาง')
                $('#subzone_id').addClass('border border-danger rounded')
                return false
            }
            $('#oweTable').DataTable().destroy();
            $('#subzone_id').removeClass('border border-danger rounded')

            getOweInfos(zone_id, subzone_id)
        });

        function getOweInfos(zone_id = 'all', subzone_id = 'all') {
            let params = {
                zone_id: zone_id,
                subzone_id: subzone_id
            }
            $.get(`../api/payment/users`, params).done(function (data) {
                // console.log(data)
                if (data.length === 0) {
                    $('.res').html('<div class="card-body h3 text-center">ไม่พบข้อมูล</div>')
                } else {
                    $('.oweCount').html(data.length)
                    $('.header').removeClass('hidden')

                    table = $('#oweTable').DataTable({
                        responsive: true,
                        // order: false,
                        // searching:false,
                        "pagingType": "listbox",
                        "lengthMenu": [
                            [10, 25, 50, 150, -1],
                            [10, 25, 50, 150, "ทั้งหมด"]
                        ],
                        "language": {
                            "search": "ค้นหา:",
                            "lengthMenu": "แสดง _MENU_ แถว",
                            "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                            "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                            "paginate": {
                                "info": "แสดง _MENU_ แถว",
                            },

                        },
                        data: data,
                        select: true,
                        columns: [{
                                'title': 'เลขที่',
                                data: 'user_id',
                                'className': 'text-center user_id'
                            },
                            {
                                'title': 'ชื่อ-สกุล',
                                data: 'name'
                            },
                            {
                                'title': 'เลขมิเตอร์',
                                data: 'meternumber',
                                'className': 'text-center',
                            },
                            {
                                'title': 'บ้านเลขที่',
                                data: 'address',
                                'className': 'text-right'
                            },
                            {
                                'title': 'หมู่',
                                data: 'zone_name',
                                'className': 'text-center'
                            },

                            {
                                'title': 'เส้นทางจดมิเตอร์',
                                data: 'subzone_name',
                                'className': 'text-center'
                            },
                            {
                                'title': '',
                                data: 'user_id',
                                'className': 'text-center',
                                'render': function (data) {
                                    return `<a href="../payment/receipted_list/${data}" class="btn btn-info">ดูข้อมูล</a>`
                                }
                            }
                        ],

                    }) //table
                    if (a) {
                        $('#oweTable thead tr').clone().appendTo('#oweTable thead');
                        a = false
                    }
                    $('#oweTable thead tr:eq(1) th').each(function (index) {
                        var title = $(this).text();
                        $(this).removeClass('sorting')
                        $(this).removeClass('sorting_asc')
                        if (index < 4) {
                            $(this).html(`<input type="text" data-id="${index}" class="col-md-12" id="search_col_${index}" placeholder="ค้นหา" />`);
                        } else {
                            $(this).html('')
                        }
                    });
                } //else
                $('.overlay').remove()
                $('#oweTable_filter').remove()

                //custom การค้นหา
                let col_index = -1
                $('#oweTable thead input[type="text"]').keyup(function () {
                    let that = $(this)
                    var col = parseInt(that.data('id'))

                    if (col !== col_index && col_index !== -1) {
                        $('#search_col_' + col_index).val('')
                        table.column(col_index)
                            .search('')
                            .draw();
                    }
                    setTimeout(function () {

                        let _val = that.val()
                        if (col === 0 || col === 3) {
                            var val = $.fn.dataTable.util.escapeRegex(
                                _val
                            );
                            table.column(col)
                                .search(val ? '^' + val + '.*$' : '', true, false)
                                .draw();
                        } else {
                            table.column(col)
                                .search(_val)
                                .draw();
                        }
                    }, 300);

                    col_index = col

                })


            }) //.get

        }

        $('#zone_id').change(function () {
            //get ค่าsubzone
            $.get(`../api/subzone/${$(this).val()}`)
                .done(function (data) {
                    let text = '<option value="">เลือก</option>';
                    if (data.length > 1) {
                        text += `<option value="all">ทั้งหมด</option>`;
                    }
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.subzone_name}</option>`
                    });
                    $('#subzone_id').html(text)
                });
        });

        $(document).on('click', '#checkAll', function () {
            let res = $(this).prop('checked')
            $('.checkbox').prop('checked', res)
            checkboxclicked()
        })

        function showEmptyDataBox(val) {
            $('#activity').removeClass('hidden');
            $('.overlay').html('')
            txt = `<div class="text-center h3 mt-2"><b>${val}</b></div>
                    <div class="text-center text-success h4 mb-2">ไม่มีรายการค้างชำระ</div>`;

            $('#activity').html(txt)
        } //showEmptyDataBox

        $('.cash_from_user').keyup(function () {
            let mustpaid = $('.mustpaid').val()
            let cash_from_user = $(this).val()
            let cashback = cash_from_user - mustpaid

            $('.cashback').val(cashback)
            if (cash_from_user === "") {
                $('.cashback').val("")
            }
            if (cashback >= 0) {
                mustpaid == 0 ? $('.submitbtn').attr('style', 'display:none') : $('.submitbtn').attr('style', 'display:block')
            } else {
                $('.submitbtn').attr('style', 'display:none')
            }

        }); //$('.cash_from_user')

        $(document).on('click', '.checkbox', function () {
            checkboxclicked()
        }); //$(document).on('click','.checkbox',

        function checkboxclicked() {
            let _total = 0;
            // $('#checkAll').prop('checked', true)

            $('.checkbox').each(function (index, element) {
                if ($(this).is(":checked")) {
                    let sum = $(this).parent().siblings('.total').text()
                    _total += parseInt(sum)
                } else {
                    $('#checkAll').prop('checked', false)

                }
            });
            if ($('.cash_from_user').val() > 0) {
                let remain = $('.cash_from_user').val() - _total
                $('.cashback').val(remain)
            }

            if (_total == 0) {
                $('.cash_form_user').attr('readonly')
            } else if (_total > 0) {
                $('.cash_form_user').removeAttr('readonly')
            }

            $('.mustpaid').val(_total)
        }

        $(document).ready(function () {
            // let _total = 0;
            // $('.checkbox').each(function(index, element){
            //     if ($(this).is(":checked")) {
            //         let sum =  $(this).parent().siblings('.total').text()
            //         _total += parseInt(sum)
            //     }
            // });
            // $('.mustpaid').val(_total)
            // $('.cash_from_user').val(0)
            // $('.cashback').val(0)
        }); //$(document).ready(function()

        function check() {
            let checkboxChecked = false;
            let cashbackRes = false;
            let errText = '';
            $('.checkbox:checked').each(function (index, element) {
                checkboxChecked = true;
            });
            if (checkboxChecked == false) {
                errText += '- ยังไม่ได้เลือกรายการค้างชำระ\n';
            }
            let mustpaid = $('.mustpaid').val()
            let cash_from_user = $('.cash_from_user').val()
            let cashback = cash_from_user - mustpaid
            if (!Number.isNaN(cashback) && cashback >= 0) {
                cashbackRes = true;
            } else {
                errText += '- ใส่จำนวนเงินไม่ถูกต้อง'
            }
            if (checkboxChecked == false || cashbackRes == false) {
                alert(errText)
                return false;
            } else {
                return true;
            }


        }

    </script>
    @endsection
