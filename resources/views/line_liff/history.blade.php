<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://static.line-scdn.net/liff/edge/2.1/liff.js"></script>
    <title>U-Tabwater</title>
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">

    <style>
        .hidden{
            display: none;
        }
    </style>
</head>
<body>
    <div class="card card-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-info">
        <h3 class="widget-user-username" id="displayName"></h3>
        <h5 class="widget-user-desc">ผู้ใช้น้ำ</h5>
        </div>
        <div class="widget-user-image">
        <img class="img-circle elevation-2" id="pictureUrl" >
        </div>
        <div class="card-footer hidden">
        <div class="row">
            <div class="col-4 border-right">
            <div class="description-block">
                <h5 class="description-header" id="meternumber"></h5>
                <span class="description-text">เลขที่ผู้ใช้</span>
            </div>
            <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-4 border-right">
            <div class="description-block">
                <h5 class="description-header" id="address"></h5>
                <span class="description-text">บ้านเลขที่</span>
            </div>
            <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-4">
            <div class="description-block">
                <h5 class="description-header" id="zone"></h5>
                <span class="description-text">หมู่ที่</span>
            </div>
            <!-- /.description-block -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
    </div>

    <br><br>
    <section id="regis-form" class="hidden">    
        <h5 class="mt-4 mb-2">ใส่หมายเลขผู้ใช้น้ำ <code>เพื่อทำการลงทะเบียน</code></h5>
        <div class="row ">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box bg-gradient-danger">
                <span class="info-box-icon"><i class="fas fa-user-plus"></i></span>
    
                <div class="info-box-content">
                    {{-- <span class="info-box-text"></span> --}}
                    <span class="info-box-number">
                        <input type="text" class="form-control col-8" id="user_id">
                        <input type="hidden" id="line_id">
                    </span>
    
                    <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                        <button type="button" class="btn btn-warning btn-register  float-right">ลงทะเบียน</button>
                    </span>
                </div>
                <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>
    </section>  
    <div id="res" class="p-2"></div>
          
</body>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<script>
   
    function runApp() {

        liff.getProfile().then(profile => {
            check_user(profile);
   
        }).catch(err => console.error(err));
    }

    function check_user(profile){
        //chek ว่า user มี การบันทึก line_id ในระบบหรือยัง
        $.get(`../api/users/check_line_id/${profile.userId}`).done(function(data){
                if(data === "0"){
                    //ถ้ายังไม่ได้ทำการบันทึก
                    document.getElementById('pictureUrl').src = profile.pictureUrl;
                    document.getElementById('displayName').innerHTML = profile.displayName;
                    $('#line_id').val(profile.userId);
                    setTimeout(() => {
                        $('#regis-form').removeClass('hidden')
                    }, 500);
                }else{
                    //ให้แสดงชื่อจริง จาก server
                    document.getElementById('pictureUrl').src = profile.pictureUrl;
                    $('#displayName').html(data[0].name) 
                    invoice_history_current_budget_year(data[0].user_id)
                }
            }).catch(err => console.error(err));
  
    }

    $('.btn-register').click(function(){
        let user_id = $('#user_id').val()
        let line_id = $('#line_id').val()
        //ทำการลงทะเบียน line_id ของ user ใน database
        $.get(`../api/users/update_line_id/${user_id}/${line_id}`).done(function(data){
            $('#displayName').html(data[0].name) 
            invoice_history_current_budget_year(data[0].user_id)
        });// outer $,get
    })//.btn-register

    function invoice_history_current_budget_year(user_id){
            let text = ""
                $.get(`../api/invoice/invoice_history_current_budget_year/${user_id}`).done((res)=>{

                    if (Object.keys(res).length > 0){
                        $('.card-footer').removeClass('hidden')
                        $('#regis-form').addClass('hidden')        

                        text = '<h4>ประวัติการใช้น้ำ</h4>'
                         text += `<table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>เลขใบแจ้งหนี้</th>
                                    <th>รอบบิล</th>
                                    <th>วันที่จดมิเตอร์</th>
                                    <th>ยอดครั้งก่อน</th>
                                    <th>ยอดปัจจุบัน</th>
                                    <th>จำนวนที่ใช้</th>
                                    <th>คิดเป็นเงิน(บาท)</th>
                                    <th>สถานะ</th>
                                </tr>
                            </thead>
                            <tbody>`;
                                Object.values(res[0]['invoice_by_user_id_curr_bugget_year']).forEach(element => {
                                    let diff = element.currentmeter -  element.lastmeter ;
                                    let diffx8 = diff *8;
                                    text +=
                                    `<tr>
                                        <th>${element.id }</th>
                                        <th>${element.inv_period_id}</th>
                                        <th>${element.created_at.split("T")[0] }</th>
                                        <th>${element.lastmeter }</th>
                                        <th>${element.currentmeter }</th>
                                        <th>${diff}</th>
                                        <th>${diffx8 }</th>
                                        <th>${element.status }</th>
                                    </tr>`
                                });
                            text +=`</tbody>
                        </table>`
                    }else{
                       text += `<div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text"></span>
                                <span class="info-box-number">ไม่พบประวัติการใช้น้ำประปา</span>
                            </div>
                            </div>
                        </div>`;
                    }
                    $('#meternumber').html(res[0].meternumber)
                    $('#address').html(res[0].user_profile.address)
                    $('#zone').html(res[0].user_profile.zone_id)
                    $('#res').html(text)
                });
    }

    $('.btn-save').click(function(){
        let user_id = $('#user_id').val()
        let line_id = $('#line_id').val()
      
        $.get(`../api/users/update_line_id/${user_id}/${line_id}`).done(function(data){
            $('#displayName').text(data.name)   
        });
    })

    liff.init({ liffId: '1656872156-Eg6zzo94' }, () => {
        if(liff.isLoggedIn()){
            runApp();
        }else{
            liff.login(); 
        }
    }, err => {
        //liff.closeWindow();
        console.error(err.code, error.message);
    });
    

</script>
</html>