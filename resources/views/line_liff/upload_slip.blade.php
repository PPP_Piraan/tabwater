@extends('layouts.line')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
<script src="https://static.line-scdn.net/liff/edge/2.1/liff.js"></script>

<style>@page { size: A5 }</style>
@endsection
@section('mainheader')
ส่งสลิปจ่ายค่าน้ำประปา
@endsection


@section('content')
<div class="row p-1" id="aa" >
    
    <div class="info-box">
        <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">หมายเลขบัญชีงานประปา ทต.ห้องแซง</span>
          <span class="info-box-number">ธนาคารกรุงไทย</span>
          <span class="info-box-number">133-648-1-1-11</span>
        </div>
        <!-- /.info-box-content -->
      </div>
    <div class="card card-primary col-md-12">
        <div class="card-header">
            <h3 class="card-title">ค้นหา : ชื่อ,ที่อยู่ ,เลขมิเตอร์ ผู้ใช้น้ำ</h3>
        </div>
        <div class="card-body">
            <div class="input-group">
                <input type="text" class="form-control text-s" id="tags">
            </div>
        </div>
    </div>
  
    <!-- /.col -->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card result" style="display:none">
        
            <div class="card-body">
                <div class="tab-content">
                    <div class="row p-1">
                        <div class="user_info" style="display: block">
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile row">
                                    <div class="col-md-12 text-center">
                                        <img class="profile-user-img img-fluid img-circle"  id="pictureUrl"
                                            src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}"
                                            alt="User profile picture">
                                    </div>
                                    <div class="col-md-12">
                                            <h3 class="profile-username text-center" id="feFirstName"></h3>
                                            <h4 class="profile-username text-center" id="meternumber2"></h4>
                                    </div>
                                    <div class="col-md-12">
                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>ที่อยู่</b> <a class="float-right" id="feInputAddress"></a>
                                            </li>
                                           
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                    <div class="tab-pane active" id="activity">
                            <form action="{{url('line_liff/save')}}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <input type="hidden" name="mode" id="mode" value="payment">
                                <input type="hidden" name="user_id" id="user_id" value="">
                                <input type="hidden" name="line_id" id="line_id" value="">
                                 
                                <div id="invAndOweLists"> </div>
                                <div class="card card-success ">
                                    <div class="card-body">
                                        <div class="row">
                                            {{-- <div class="col-md-2"></div> --}}
                                            <div class="col-md-12">
                                                <label class="control-label">ยอดที่ต้องชำระ</label>
                                                <input type="text" class="form-control text-bold text-center mustpaid" readonly 
                                                    style="font-size: 2rem; width:16rem; height:3rem" name="mustpaid"> 
                                            </div>
                                            <img id="previewImg" src="" style="width: 280px" class="mt-2">

                                        </div>
                                    </div>
                                </div>
                                <input type="file" name="image" onchange="previewFile(this);" required>
                                <button type="submit" class="btn btn-success mt-2 btn-block"  >ส่งข้อมูล</button>
                            </form>
                        </div>
                    </div>
                </div><!-- /.card-body -->
            </div>
        </div>
    </div>
</div>

@endsection



@section('script')
  <script src="{{asset('/js/1.12.1/jquery-ui.js')}}"></script>

<script>
  

    function runApp() {

        liff.getProfile().then(profile => {
            check_user(profile);
   
        }).catch(err => console.log(err));
    }

    liff.init({ liffId: '1656872156-Z99WW76m' }, () => {
        if(liff.isLoggedIn()){
            runApp();
        }else{
            liff.login(); 
        }
    }, err => {
        //liff.closeWindow();
        console.log(err.code, error.message);
    });

    function check_user(profile){
        //chek ว่า user มี การบันทึก line_id ในระบบหรือยัง
        $.get(`../api/users/check_line_id/${profile.userId}`).done(function(data){
            console.log('data',data)
                if(data === "0"){
                    //ถ้ายังไม่ได้ทำการบันทึก
                    document.getElementById('pictureUrl').src = profile.pictureUrl;
                    document.getElementById('displayName').innerHTML = profile.displayName;
                    $('#line_id').val(profile.userId);
                    setTimeout(() => {
                        $('#regis-form').removeClass('hidden')
                    }, 500);
                }else{
                    //ให้แสดงชื่อจริง จาก server
                    console.log('profile', profile)
                    document.getElementById('pictureUrl').src = profile.pictureUrl;
                    $('#displayName').html(data[0].name) 
                    // showInvoiceAndOwe(data[0].user_id)
                }
            }).catch(err => console.log(err));
  
    }

</script>


  
  <script>
    function previewFile(input){
        var file = $("input[type=file]").get(0).files[0];
 
        if(file){
            var reader = new FileReader();
 
            reader.onload = function(){
                $("#previewImg").attr("src", reader.result);
            }
 
            reader.readAsDataURL(file);
        }
    }
</script>
    <script>

        
        let i = 1;

        $( "#tags" ).autocomplete({
            select: function( event, ui ) { 
                test(ui.item.value)
            }
        });

        $('#tags').keyup(function(){
            $('.user_info').attr('style', 'display:none')
            $('.result').attr('style', 'display:none')
            $('#activity').removeClass('active');

            let name =  $(this).val();
            if(name.match(/^[0-9]/)){
                $.get(`../../api/users/search/${name}`).done(function(data){
                        $("#tags").autocomplete({
                            source: data,
                        });
                    
                })
            }else{
                if(name.length >= 2){
                    console.log('in', name)
                    $.get(`../../api/users/search/${name}`).done(function(data){
                            $("#tags").autocomplete({
                                source: data,
                            });
                    
                    }) 
                }//if
            }
            
        })


        function test(_name) {
            $('.overlay').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            let name = _name //$('#tags').val();
            let txt ='';
            let total = 0;
            let i =0;

            let user_id  = name.split('- HS')[1];
            $('#user_id').val(user_id)
            $.get(`../../api/invoice/${user_id}`).done(function(invoices){
                $('#invAndOweLists').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>')
                $('#activity').addClass('active');
                if(invoices.length > 0){
                    txt += `<div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">รายการค้างชำระ</h3>
                                </div>
                                <div class="card-body p-0 table-responsive">
                                    <table class="table table-striped text-nowrap">
                                        <thead>
                                            <tr>
                                            <th style="width: 10px">
                                                <input type="checkbox" id="checkAll" checked>
                                                </th>
                                            <th>เลขใบแจ้งหนี้</th>
                                            <th>เลขมิเตอร์</th>
                                            <th>รอบบิล</th>
                                            <th>ยอดครั้งก่อน</th>
                                            <th>ยอดปัจจุบัน</th>
                                            <th>จำนวนที่ใช้</th>
                                            <th>ค่าใช้น้ำ(บาท)</th>
                                            <th>ค่ารักษามิเตอร์</th>
                                            <th>คิดเป็นเงิน(บาท)</th>
                                            </tr>
                                        </thead>
                                        <tbody>`;
                                        invoices.forEach(element => {
                                            let status = element.status == 'owe' ? 'ค้างชำระ' : 'invoice';
                                            let diff = element.currentmeter - element.lastmeter;
                                            let _total = diff*8;
                                            let reserve = _total  == 0 ? 10 : 0;
                                            total += _total + reserve; //   

                                            txt +=` <tr>
                                                        <td><input type="checkbox"  checked  class="control-input invoice_id checkbox" name="payments[${i}][on]">
                                                        </td>
                                                        <td>${element.id}</td>
                                                        <td>${element.usermeterinfos.meternumber}</td>
                                                        <td>${element.invoice_period.inv_period_name}</td>
                                                        <td>${element.lastmeter}</td>
                                                        <td>${element.currentmeter}</td>
                                                        <td>${diff}</td>
                                                        <td>${ _total }
                                                            <input type="hidden" name="payments[${i}][total]" value="${ _total }">
                                                            <input type="hidden" name="payments[${i}][iv_id]" value="${ element.id }">
                                                        </td>
                                                        <td>${reserve}</td>
                                                        <td class="total">${_total+ reserve}</td>

                                                    </tr>
                                            `;  
                                            i++;
                                        }) //foreach
                                    
                    txt +=`             </tbody>
                                    </table>
                                </div>
                            </div>`;    
                    $('.card-success').prop('style', 'display:block');

                }else{
                    showEmptyDataBox();
                }

                if(invoices.length > 0){
                    let address = `${invoices[0].usermeterinfos.user_profile.address} 
                                 ${invoices[0].usermeterinfos.user_profile.zone.zone_name} \n
                                อำเภอ ${invoices[0].usermeterinfos.user_profile.district.district_name}\n
                                จังหวัด ${invoices[0].usermeterinfos.user_profile.province.province_name}`;
                    $('#feFirstName').html(invoices[0].usermeterinfos.user_profile.name);
                    $('#meternumber2').html(invoices[0].usermeterinfos.meternumber);
                    $('#feInputAddress').html(address);
                    $('#phone').html(invoices[0].usermeterinfos.user_profile.phone);
                    $('.user_info').attr('style', '')
                    $('.result').attr('style', '')
                    $('#invAndOweLists').html(txt);

                    $('.overlay').html('')
                    
                    $('.mustpaid').val(0)
                }  
            $('.mustpaid').val(total)

            });
        }//text
  

        function  showEmptyDataBox(){
            $('#activity').addClass('active');
            $('.result').attr('style', '')
            $('.overlay').html('')
            txt = `<div class="text-center h3"><b>${$("#tags").val()}</b></div>
                    <div class="text-center text-success h4">ไม่มีรายการค้างชำระ</div>`;
            
            $('#invAndOweLists').html(txt)
            $('.card-success').prop('style', 'display:none')
        }//showEmptyDataBox

       
        $(document).on('click','.checkbox', function(){
            checkboxclicked()
        });//$(document).on('click','.checkbox',

        function checkboxclicked(){
            let _total = 0;
            $('#checkAll').prop('checked', true)

            $('.checkbox').each(function(index, element){
                if ($(this).is(":checked")) {
                    let sum =  $(this).parent().siblings('.total').text()
                    _total += parseInt(sum)     
                }else{
                    $('#checkAll').prop('checked', false)

                }
            });
            if( $('.cash_from_user').val() > 0){
                let remain = $('.cash_from_user').val() - _total
                $('.cashback').val(remain)
            }
            
            if(_total == 0){
                $('.cash_form_user').attr('readonly')
            }else if(_total > 0){
                $('.cash_form_user').removeAttr('readonly')
            }

            $('.mustpaid').val(_total)
        }

        $(document).ready(function(){
            let _total = 0;
            $('.checkbox').each(function(index, element){
                console.log(index)
                if ($(this).is(":checked")) {
                    let sum =  $(this).parent().siblings('.total').text()
                    _total += parseInt(sum)    
                    console.log('_total',_total) 
                }
            });
            $('.mustpaid').val(_total)
            $('.cash_from_user').val(0)
            $('.cashback').val(0)
        });//$(document).ready(function()

      
    </script>


@endsection

