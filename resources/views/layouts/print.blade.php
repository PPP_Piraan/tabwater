<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ประปา</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css2?family=Sarabun:wght@100;300&display=swap" rel="stylesheet"> 
   <link rel="stylesheet" href="{{asset('/css/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css')}}"  />    {{-- <link href="{{asset('/css/mycss.css')}}"> --}}
  <link rel="stylesheet" href="{{asset('/css/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css')}}"  />    {{-- <link href="{{asset('/css/mycss.css')}}"> --}}
  <link rel="stylesheet" href="{{asset('/datatables.1.10.20/dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('/datatables.1.10.20/buttons.dataTables.1.6.0.min.css')}}">
  
  @yield('style')
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
 
  <div class="content-wrappe">
    

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('adminlte/dist/js/demo.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}"></script>
@yield('script')
</body>
</html>
