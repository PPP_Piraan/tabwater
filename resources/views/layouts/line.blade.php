<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
    <title>ประปา</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
    {{-- <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon-96x96.png')}}"> --}}

    <!-- overlayScrollbars -->
    {{-- <link href="{{asset('/css/mycss.css')}}"> --}}

    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    {{-- <link rel="preconnect" href="https://fonts.gstatic.com"> --}}
    <link href="{{ asset('css/sarabun-webfont-master/fonts/thsarabunnew.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/datatables.1.10.20/dataTables.min.css')}}">
    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css"> --}}
    {{-- <link  rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"> --}}
    <link rel="stylesheet" type="text/css"   href="{{ asset('/datatables.1.10.20/buttons.dataTables.1.6.0.min.css') }} ">
    <style>
        *{
            font-family: 'Sarabun', sans-serif, 'Font Awesome 5 Free' !important;
        }

        .hidden {
            display: none
        }

        .show {
            display: block
        }

        .nav-link-header {
            background-color: #343a40 !important;
            border: 1px solid #007bff !important;
            color: #007bff !important
        }

        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 170px;
            left: 260px;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 10px;
        }

        
    </style>
    
    {{-- <script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script> --}}
    <script src="{{ asset('/js/chartjs/dist/Chart.min.js') }}"></script>
    <script src=" {{ asset('/js/chartjs/dist/chartisan_chartjs.umd.js') }}"></script>
    <!-- Chartisan -->
    {{-- <script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script> --}}
    @yield('style')
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>@yield('mainheader')</h1>
                        </div>
                        <div class="col-sm-6">
                           
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    
    <script src="{{ asset('/js/jquery-1.11.3.min.js') }}"></script>

    <!-- Bootstrap 4 -->
    <script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}">
    </script>
    <script src="{{asset('datatables.1.10.20/dataTables.min.js')}}"></script>
    <script src="{{ asset('/datatables.1.10.20/pagination/select.js') }}"></script>

    <script>
        $(document).ready(()=>{
            $('ul.nav-treeview').find('a.active').parent().parent().css( "display", "block" );
            $('ul.nav-treeview').find('a.active').parent().parent().parent().addClass( "open-menu menu-open" );
        })
        
    </script>
    <!-- Charting library -->
    @yield('script')

  <script type="text/javascript">
    $(document).ready(()=>{
        $.get(`../../api/cutmeter/count`).done(function (data) {
            $('#owe_over3_period').html(parseInt(data))
            $('.oweTotal').html(parseInt(data))
        
      });

   
    })
  </script>
  <script>
    function openNav() {
      document.getElementById("mySidenav").style.width = "250px";
      document.getElementById("main").style.marginLeft = "250px";
      $('.openbtn').addClass('hidden')
      $('.closebtn').removeClass('hidden')
    }
    
    function closeNav() {
      document.getElementById("mySidenav").style.width = "0px";
      document.getElementById("main").style.marginLeft= "0px";
      $('.closebtn').addClass('hidden')
      $('.openbtn').removeClass('hidden')
    }
</script>
</body>

</html>
