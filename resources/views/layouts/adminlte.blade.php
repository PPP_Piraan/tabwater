<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ประปา</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">

    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="{{ asset('css/sarabun-webfont-master/fonts/thsarabunnew.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/datatables.1.10.20/dataTables.min.css')}}">
   <link rel="stylesheet" type="text/css"   href="{{ asset('/datatables.1.10.20/buttons.dataTables.1.6.0.min.css') }} ">
    <style>
        *{
            font-family: 'Sarabun', sans-serif, 'Font Awesome 5 Free' !important;
        }

        .hidden {
            display: none
        }

        .show {
            display: block
        }

        .main-sidebar{
            font-size: 0.85rem;
        }

        .nav-link-header {
            background-color: #343a40 !important;
            border: 1px solid #007bff !important;
            color: #007bff !important
        }

        .sidenav {
            height: 100%;
            width: 0;
            position: absolute;
            z-index: 1;
            top: 150px;
            left: 260px;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 10px;
        }

        .sidenav a {
            padding: 8px 8px 8px 32px;
            text-decoration: none;
            font-size: 25px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }

        .sidenav a:hover {
            color: #f1f1f1;
        }

        .sidenav .closebtn {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }

        #main {
            transition: margin-left .5s;
        }

        @media screen and (max-height: 450px) {
            .sidenav {
                padding-top: 15px;
            }

            .sidenav a {
                font-size: 18px;
            }
        }

    </style>

    <script src="{{ asset('/js/chartjs/dist/Chart.min.js') }}"></script>
    <script src=" {{ asset('/js/chartjs/dist/chartisan_chartjs.umd.js') }}"></script>
    @yield('style')
</head>

<body class="hold-transition sidebar-mini">

    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                </li>
            </ul>


            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link text-primary" data-toggle="dropdown" href="#" aria-expanded="true">
                        {{-- <i class="far fa-user-circle"></i>  --}}
                        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="User Avatar"
                            class="img-size-32 mr-3 img-circle">
                        {{Auth::user()->user_profile->name}}
                        @role('twman')
                        (เจ้าหน้าที่จดมิเตอร์)
                        @endrole

                        @role('superadministrator')
                        (ผู้ดูแลระบบ)
                        @endrole

                        @role('cashier')
                        (การเงิน)
                        @endrole


                    </a>

                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
                        <a href="{{ url('staff/edit/'.Auth::id()) }}" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="User Avatar"
                                    class="img-size-50 mr-3 img-circle ">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        ข้อมูลส่วนตัว
                                    </h3>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>

                        <div class="dropdown-divider"></div>
                        <form action="{{route('logout')}}" method="POST">
                            @csrf
                            <button type="submit" class="dropdown-item dropdown-footer text-danger"><i
                                    class="far fa-sign-out" aria-hidden="true"></i>ออกจากระบบ</button>
                        </form>
                    </div>
                </li>

            </ul>

        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="javascript:void(0)" class="brand-link">
                {{-- <img src="{{asset('logo/'.session('logo'))}}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3 organization_logo" style="opacity: .8"> --}}
                    <img src="{{asset('logo/logo.png')}}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3 organization_logo" style="opacity: .8">
                <span
                    class="brand-text font-weight-light">{{ session('organization_short_name') != "" ? session('organization_short_name') : 'ทด. ห้องแซง'}}</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{url('/admin')}}" class="nav-link active  bg-warning @yield('dashboard')">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    หน้าหลัก
                                </p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview "><!-- menu-open-->
                            <a href="#" class="nav-link active nav-link-header">
                                <i class="nav-icon fas fa-edit"></i>
                                <p>
                                    จัดการใบแจ้งหนี้
                                    <i class="right fas fa-angle-left ml-2"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('invoice/index')}}" class="nav-link @yield('invoice')">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>ออกใบแจ้งหนี้</p>
                                    </a>
                                </li>
                                @role([ 'superadministrator'])

                                <li class="nav-item">
                                    <a href="{{url('owepaper/index')}}" class="nav-link @yield('owepaper')">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>ออกใบแจ้งเตือนชำระหนี้</p>
                                    </a>
                                </li>
                                @endrole
                                <li class="nav-item">
                                    <a href="{{url('cutmeter/index')}}" class="nav-link @yield('cutmeter')">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>ตัดหม้อ <span id="owe_over3_period"
                                                class="badge badge-danger right mr-4">{{ App\Http\Controllers\CutmeterController::cutmeterUserCount() }}</span></p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        @role(['superadministrator', 'administrator', 'cashier'])
                        <li class="nav-item has-treeview  "><!-- menu-open-->
                            <a href="#" class="nav-link active nav-link-header">
                                <i class="nav-icon fas fa-hand-holding-usd"></i>
                                <p>
                                    จัดการใบเสร็จรับเงิน
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('payment')}}" class="nav-link  @yield('payment')">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>รับชำระค่าน้ำประปา</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('payment/search')}}" class="nav-link  @yield('payment-search')">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>ค้นหาใบเสร็จรับเงิน</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('upload_bank_slip/index')}}"
                                        class="nav-link @yield('upload_bank_slip')">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>ตรวจสอบสลิปธนาคาร <span id="slip_count" class="badge badge-danger"></span>
                                        </p>
                                    </a>
                                </li>

                                {{-- <li class="nav-item">
                                <a href="{{url('changemeter')}}" class="nav-link @yield('changemeter')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>เปลี่ยนมิเตอร์</p>
                                </a>
                        </li> --}}

                    </ul>
                    </li>
                    @endrole
                    <li class="nav-item has-treeview "><!-- menu-open-->
                        <a href="#" class="nav-link active nav-link-header">
                            <i class="nav-icon fas fa-file-excel"></i>
                            <p>
                                รายงาน
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            {{-- <li class="nav-item">
                                <a href="{{url('reports/users')}}" class="nav-link @yield('report-users')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ประวัติการใช้น้ำของผู้ใช้น้ำ</p>
                                </a>
                            </li> --}}
                            @role(['superadministrator', 'administrator', 'cashier'])
                            <li class="nav-item">
                                <a href="{{url('reports/owe',[])}}" class="nav-link @yield('report-owe')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ผู้ค้างชำระค่าน้ำประปา</p>
                                </a>
                            </li>
                            {{-- <li class="nav-item">
                                <a href="{{url('reports/cutmeter')}}" class="nav-link @yield('report-cutmeter')">
                            <i class="far fa-circle nav-icon"></i>
                            <p>ผู้ค้างชำระเกิน 3 เดือน</p>
                            </a>
                    </li> --}}
                    <li class="nav-item">
                        <a href="{{url('reports/summary')}}" class="nav-link @yield('report-summary')">
                            <i class="far fa-circle nav-icon"></i>
                            <p>สรุปค่าน้ำประปารายเดือน</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('reports/ledger')}}" class="nav-link @yield('report-ledger')">
                            <i class="far fa-circle nav-icon"></i>
                            <p>เล็ดเยอร์รายตัวลูกหนี้(ป.17)</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('reports/payment')}}" class="nav-link @yield('report-payment')">
                            <i class="far fa-circle nav-icon"></i>
                            <p>การชำระค่าน้ำประจำวัน</p>
                        </a>
                    </li>
                    @endrole
                    <li class="nav-item">
                        <a href="{{url('reports/meter_record_history')}}"
                            class="nav-link @yield('report-meter_record_history')">
                            <i class="far fa-circle nav-icon"></i>
                            <p>สมุดจดเลขอ่านมาตรวัดน้ำ(ป.31)</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('reports/water_used')}}" class="nav-link @yield('report-water_used')">
                            <i class="far fa-circle nav-icon"></i>
                            <p>ปริมาณการใช้น้ำประปา</p>
                        </a>
                    </li>
                    </ul>
                    </li>
                    @role(['superadministrator'])
                    <li class="nav-item has-treeview menu-open">
                        <a href="#" class="nav-link active nav-link-header">
                            <i class="nav-icon fas fa-cogs"></i>
                            <p>
                                ตั้งค่า
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/users')}}" class="nav-link @yield('users')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ทะเบียนผู้เช่ามาตรวัดน้ำ(ป.25)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/staff')}}" class="nav-link @yield('staff')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>เจ้าหน้าที่ประปา</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('zone')" href="{{url('zone')}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>พื้นที่จัดเก็บค่าน้ำประปา</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('undertaker-subzone')" href="{{url('undertaker_subzone')}}">
                                    <i class="far fa-circle nav-icon" style="vertical-align: top"></i>
                                    <p>กำหนดผู้รับผิดชอบพื้นที่
                                        <br class="ml-3">จดมิเตอร์</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{('/tabwatermeter')}}" class="nav-link @yield('tabwatermeter')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ขนาดมิเตอร์</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/budgetyear')}}" class="nav-link @yield('budgetyear')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>สร้างปีงบประมาณ</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/invoice_period')}}" class="nav-link @yield('invoice_period')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>สร้างรอบบิล</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/settings')}}" class="nav-link @yield('settings')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ทั่วไป</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/settings/import_excel')}}" class="nav-link @yield('import_excel')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>import excel</p>
                                </a>
                            </li>
                            @if (Auth::user()->hasRole('superadministrator'))
                            <li class="nav-item">
                                <a href="{{url('/laratrust')}}" class="nav-link @yield('laratrust')">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Set Roles</p>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endrole
                    </ul>


                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>@yield('mainheader')</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">หน้าหลัก</a></li>
                                <li class="breadcrumb-item">@yield('nav')</li>

                                <li class="breadcrumb-item">@yield('mainheader')</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
    </div>
    <!-- /.content-wrapper -->




    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->

    <script src="{{ asset('/js/jquery-1.11.3.min.js') }}"></script>

    <!-- Bootstrap 4 -->
    <script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}">
    </script>
    <script src="{{asset('datatables.1.10.20/dataTables.min.js')}}"></script>
    <script src="{{ asset('/datatables.1.10.20/pagination/select.js') }}"></script>

    <script>
        $(document).ready(()=>{
            $('ul.nav-treeview').find('a.active').parent().parent().css( "display", "block" );
            $('ul.nav-treeview').find('a.active').parent().parent().parent().addClass( "open-menu menu-open" );
        })

    </script>
    <!-- Charting library -->
    @yield('script')

  <script type="text/javascript">


  </script>
  <script>
    // function openNav() {
    //   document.getElementById("mySidenav").style.width = "250px";
    //   document.getElementById("main").style.marginLeft = "250px";
    //   $('.openbtn').addClass('hidden')
    //   $('.closebtn').removeClass('hidden')
    // }

    // function closeNav() {
    //   document.getElementById("mySidenav").style.width = "0px";
    //   document.getElementById("main").style.marginLeft= "0px";
    //   $('.closebtn').addClass('hidden')
    //   $('.openbtn').removeClass('hidden')
    // }

    let res =0;
 $('.nav-link').click(()=>{
        if(res === 0){
                $("#mySidenav").css({"left": "80px"});
                res =1
        }else{
            $("#mySidenav").css({"left": "260px"});
            res =0
        }
    })
</script>
</body>

</html>
