<!-- Ionicons -->
<!-- fullCalendar -->
<link rel="stylesheet" href="{{asset('adminlte/plugins/fullcalendar/main.min.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/plugins/fullcalendar-bootstrap/main.min.css')}}">
<!-- Theme style -->
<!-- Google Font: Source Sans Pro -->


<!-- Content Wrapper. Contains page content -->
 
  <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-body p-0">
              <!-- THE CALENDAR -->
              <div id="calendar"></div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="modal fade" id="modal-sm" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">ปฎิทินกิจกรรม</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-group">
                        <label>หัวข้อ</label>
                        <input type="text" class="form-control" placeholder="Enter ...">
                      </div>
                    <!-- Date range -->
                    <div class="form-group">
                      <label>วันที่</label>
    
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                        <input type="text" class="form-control float-right" id="reservation">
                      </div>
                      <!-- /.input group -->
                    </div>
                    
                            <div class="form-group">
                                <label>เริ่มเวลา:</label>
            
                                <div class="input-group date" id="timepicker" data-target-input="nearest">
                                  <input type="text" class="form-control datetimepicker-input" data-target="#timepicker">
                                  <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                      <div class="input-group-text"><i class="far fa-clock"></i></div>
                                  </div>
                                  </div>
                                <!-- /.input group -->
                              </div>
                       
                            <div class="form-group">
                                <label>สิ้นสุดเวลา:</label>
            
                                <div class="input-group date" id="timepicker" data-target-input="nearest">
                                  <input type="text" class="form-control datetimepicker-input" data-target="#timepicker">
                                  <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                      <div class="input-group-text"><i class="far fa-clock"></i></div>
                                  </div>
                                  </div>
                                <!-- /.input group -->
                              </div>
                      
                    <!-- /.form group -->
                    <div class="form-group">
                        <label>รายละเอียด</label>
                        <textarea  class="form-control" rows="3"></textarea>
                      </div>
                  </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script src="{{asset('adminlte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/fullcalendar/locales/th.js')}}"></script>

<!-- Page specific script -->
<script>
$(function () {

  /* initialize the external events
   -----------------------------------------------------------------*/
  function ini_events(ele) {
    ele.each(function () {

      // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
      // it doesn't need to have a start or end
      var eventObject = {
        title: $.trim($(this).text()) // use the element's text as the event title
      }

      // store the Event Object in the DOM element so we can get to it later
      $(this).data('eventObject', eventObject)

    })
  }

  ini_events($('#external-events div.external-event'))

  /* initialize the calendar
   -----------------------------------------------------------------*/
  //Date for the calendar events (dummy data)
  var date = new Date()
  var d    = date.getDate(),
      m    = date.getMonth(),
      y    = date.getFullYear()

  var Calendar = FullCalendar.Calendar;
  var Draggable = FullCalendarInteraction.Draggable;

  var containerEl = document.getElementById('external-events');
  var checkbox = document.getElementById('drop-remove');
  var calendarEl = document.getElementById('calendar');

  // initialize the external events
  // -----------------------------------------------------------------



  var calendar = new Calendar(calendarEl, {
    locale: 'th',
    plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
    header    : {
      left  : 'prev,next today',
      center: 'title',
      right : ''
    },
    contentHeight: 400,
    'themeSystem': 'bootstrap',
    //Random default events
    events    : [
     
    ],
    selectable: true,
    editable  : true,   
//     dateClick: function(info) {
//     alert('Date: ' + info.dateStr);
//     alert('Resource ID: ' + info.resource.id);
//   },
  select: function(info) {
    $('#modal-sm').modal()
        // alert('selected ' + info.startStr + ' to ' + info.endStr);
      }  
  });

  calendar.render();
  // $('#calendar').fullCalendar()

  /* ADDING EVENTS */
  var currColor = '#3c8dbc' //Red by default
  //Color chooser button
  var colorChooser = $('#color-chooser-btn')
  $('#color-chooser > li > a').click(function (e) {
    e.preventDefault()
    //Save color
    currColor = $(this).css('color')
    //Add color effect to button
    $('#add-new-event').css({
      'background-color': currColor,
      'border-color'    : currColor
    })
  })
  $('#add-new-event').click(function (e) {
    e.preventDefault()
    //Get value and make sure it is not null
    var val = $('#new-event').val()
    if (val.length == 0) {
      return
    }

    //Create events
    var event = $('<div />')
    event.css({
      'background-color': currColor,
      'border-color'    : currColor,
      'color'           : '#fff'
    }).addClass('external-event')
    event.html(val)
    $('#external-events').prepend(event)

    //Add draggable funtionality
    ini_events(event)

    //Remove event from text input
    $('#new-event').val('')
  })
})
</script>