@extends('layouts.admin')

@section('title')
ออกบิล/จ่ายค่าน้ำประปา
@endsection
@section('header')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
        <div class="form-group ml-6">
          <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" id="memberSearch" placeholder="Search" type="text">
          </div>
        </div>
      </form>
@endsection

@section('content')

      <div id="app">
          <invoicelists></invoicelists>
      </div>
<!---------------------->

<div id="printThis">
    <div id="MyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">

        <div class="modal-dialog modal-lg">

            <!-- Modal Content: begins -->
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body" id="modal-body">
                    <div class="body-message">
                        <h4>Any Heading</h4>
                        <p>And a paragraph with a full sentence or something else...</p>
                    </div>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button id="btnPrint" type="button" class="btn btn-default">Print</button>
                </div>

            </div>
            <!-- Modal Content: ends -->

        </div>
    </div>
</div>
      
@endsection