@extends('layouts.adminlte')


@section('nav')
    <a href="{{ url('/dashboard/index') }}"> หน้าหลัก</a>
@endsection
@section('dashboard')
    active
@endsection

@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/3.0.1/css/buttons.dataTables.css">
{{-- user ไม่มีข้อมูลการจดมาตรและสถานะยกเลิกใช้งาน
    <table class="table">

                <tr>
                    <td>user_id</td>
                    <td>name</td>
                    <td></td>
                    <td></td>
                </tr>
                @foreach ($userStatusDeleted1InvHistoryNull as $item)

                <tr>
                    <td>{{ $item->user_id }}</td>
                    <td>{{ $item->user_profile->name }}</td>
                    <td></td>
                    <td></td>
                </tr>
                @endforeach

    </table> --}}

    user มีข้อมูลการจดมาตรและสถานะยกเลิกใช้งาน
    <?php $i = 1; ?>
    <table class="table table-bordered" class="display" id="example">
        <thead>
                <tr>
                    <th>#</th>
                    <th>user_id</th>
                    <th>meternumber</th>
                    <th>name</th>
                    <th>owe_invoice</th>
                    <th>Owe_invoice_history</th>
                </tr>
            </thead>
              <tbody>
                @foreach ($userStatusDeleted1InvHistoryNotNull as $item)

                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $item->user_id }}</td>
                    <td>{{ $item->meternumber }}</td>
                    <td>{{ $item->user_profile->name }}</td>
                    <td>
                        @foreach ($item->invoice as $inv)
                            {{ $inv->nv_period_id.":".$inv->status }}-
                        @endforeach

                    </td>
                    <td>
                        @foreach ($item->invoice_history as $inv_his)
                            {{ $inv_his->nv_period_id.":".$inv_his->status }}-
                        @endforeach

                    </td>
                </tr>
                @endforeach
            </tbody>
    </table>

@endsection

<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
<script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js "></script>
<script src="https://cdn.datatables.net/buttons/3.0.1/js/dataTables.buttons.js"></script>


<script src="https://cdn.datatables.net/buttons/3.0.1/js/buttons.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/3.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js"></script>



@section('script')
    <script>
        $(document).ready(function(){
            $('#example').dataTable( {
                layout: {
        topStart: {
            buttons: [
                {

                    buttons: ['copy', 'excel', 'csv', 'pdf', 'print']
                }
            ]
        }
    },
                lengthMenu: [10, 25, 50,  -1 ]
                } );
        })
    </script>
@endsection
