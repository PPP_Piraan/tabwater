@extends('layouts.argon')

@section('title')
     เพิ่มข้อมูลมิเตอร์น้ำประปา
@endsection

@section('content')
  <div class="container-fluid mt--7">
     @include('/admin/form', ['formMode' => 'create']);
  </div>
@endsection